package com.ruoyi.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;

/**
 * 线程池
 * @author luoyonghui
 * @date 2024/3/22
 * @description:
 */
@EnableAsync
@Configuration
public class TaskPoolConfig {
    @Bean("myTaskExecutor")
    public Executor taskExecutor() {
        int processorCount = Runtime.getRuntime().availableProcessors();
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(processorCount*4);
        executor.setQueueCapacity(processorCount*4*10);
        executor.setKeepAliveSeconds(360);
        executor.setThreadNamePrefix("taskExecutor -");
        executor.setAwaitTerminationSeconds(60);
        executor.setWaitForTasksToCompleteOnShutdown(true);
        executor.setMaxPoolSize(800);
        return executor;
    }
}