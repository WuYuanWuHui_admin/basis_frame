package com.ruoyi.test;

import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping ("/websocket/test")
public class WebsocketTestController {



    @ApiOperation("找零工平台AA测试广播")
    @GetMapping ("/send/{userId}")
    public void testWsSend (@PathVariable (value = "userId") String userId) throws IOException {
        Map <String, Object> map = new HashMap <>();
        map.put("id", "1");
        map.put("platformName", "找零工平台AA");
        map.put("userName", "00000000");

    }

}
