package com.ruoyi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * 启动程序
 *
 * @author ruoyi
 */
@EnableAsync    //开启异步注解功能
@SpringBootApplication (exclude = {DataSourceAutoConfiguration.class})
@EnableConfigurationProperties
@EnableScheduling
public class RuoYiApplication {

    public static void main (String[] args) {
        System.setProperty("spring.devtools.restart.enabled", "false");
        ConfigurableApplicationContext content = SpringApplication.run(RuoYiApplication.class, args);
        //        WebSocketServer.setApplicationContext(content);
        System.out.println("(♥◠‿◠)ﾉﾞ  启动成功   ლ(´ڡ`ლ)ﾞ  \n");
    }
}