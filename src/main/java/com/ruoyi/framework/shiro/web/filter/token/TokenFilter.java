package com.ruoyi.framework.shiro.web.filter.token;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.constant.SystemContstant;
import com.ruoyi.common.utils.StringUtils;

import com.ruoyi.project.utils.RedisUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.web.filter.AccessControlFilter;
import org.apache.shiro.web.servlet.ShiroHttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

@Slf4j
public class TokenFilter extends AccessControlFilter {

    @Autowired
    private RedisUtils redisUtils;
    /**
     * 表示是否允许访问；mappedValue就是[urls]配置中拦截器参数部分，如果允许访问返回true，否则false；
     */
    @Override
    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) throws Exception {
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        log.info("请求地址为：{},请求IP为:{}",httpRequest.getRequestURL(),httpRequest.getRemoteAddr());
        if (httpRequest.getMethod().equals(RequestMethod.OPTIONS.name())) {
            setHeader(httpRequest, httpResponse);
            return true;
        }


        String token = ((ShiroHttpServletRequest) request).getHeader("token");
        if (StringUtils.isEmpty(token)) {
            return false;
        }
        //String afterToken= token.substring(4);

       /* if (!token.contains(SystemContstant.TOKEN)){
            return false;
        }*/

        String userId = redisUtils.get(SystemContstant.TOKEN+token);
        if (StringUtils.isEmpty(userId)) {
            return false;
        }
       // User user = userService.selectUserById(Long.valueOf(userId));
        String ip = httpRequest.getRemoteAddr();
        redisUtils.set("ip_"+userId,ip);
        return true;
    }


    /**
     * 表示当访问拒绝时是否已经处理了；如果返回true表示需要继续处理；如果返回false表示该拦截器实例已经处理了，将直接返回即可。
     */
    @Override
    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {
        String token = ((ShiroHttpServletRequest) request).getHeader("token");

        HttpServletResponse res = (HttpServletResponse) response;
        res.setHeader("Access-Control-Allow-Origin", "*");
        res.setStatus(HttpServletResponse.SC_OK);
        res.setCharacterEncoding("UTF-8");
        PrintWriter writer = res.getWriter();
        Map<String, Object> map = new HashMap<>();
        map.put("code", 401);
        if (StringUtils.isNotBlank(token)){
            map.put("msg", "服务器异常,请稍后再试");
        }else{
            map.put("msg", "未登录");
        }
        writer.write(JSON.toJSONString(map));
        writer.close();
        return false;
    }

    /**
     * 为response设置header，实现跨域
     */
    private void setHeader(HttpServletRequest request, HttpServletResponse response) {
        //跨域的header设置
        response.setHeader("Access-control-Allow-Origin", request.getHeader("Origin"));
        response.setHeader("Access-Control-Allow-Methods", request.getHeader("Access-Control-Request-Method"));
        response.setHeader("Access-Control-Allow-Credentials", "true");
        response.setHeader("Access-Control-Allow-Headers", request.getHeader("Access-Control-Request-Headers"));
        //防止乱码，适用于传输JSON数据
        response.setHeader("Content-Type", "application/json;charset=UTF-8");
        response.setStatus(HttpStatus.OK.value());
    }
}
