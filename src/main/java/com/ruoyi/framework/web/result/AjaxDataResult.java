package com.ruoyi.framework.web.result;

import lombok.Data;

import java.io.Serializable;

/**
 * 操作消息提醒
 *
 * @author xsh
 */
@Data
public class AjaxDataResult implements Serializable {

    //返回code码
    private Integer code;

    //返回msg信息
    private String msg;

    //返回数据
    private Object data;


    /**
     * 状态类型
     */
    public enum Type {
        /**
         * 成功
         */
        SUCCESS(0),
        /**
         * 警告
         */
        WARN(301),
        /**
         * 错误
         */
        ERROR(500),
        /**
         * 错误
         */
        USER_ERROR(531);

        private final int value;

        Type (int value) {
            this.value = value;
        }

        public int value () {
            return this.value;
        }
    }

    /**
     * 初始化一个新创建的 AjaxResult 对象，使其表示一个空消息。
     */
    public AjaxDataResult () {
    }

    /**
     * 初始化一个新创建的 AjaxResult 对象
     *
     * @param type
     *         状态类型
     * @param msg
     *         返回内容
     */
    public AjaxDataResult (Type type, String msg) {
        this.code = type.value;
        this.msg  = msg;
    }

    /**
     * 初始化一个新创建的 AjaxResult 对象
     *
     * @param code
     *         状态类型
     * @param msg
     *         返回内容
     * @param data
     *         数据对象
     */
    public AjaxDataResult (Integer code, String msg, Object data) {
        this.code = code;
        this.msg  = msg;
        this.data = data;

    }

    /**
     * 返回成功消息
     *
     * @return 成功消息
     */
    public static AjaxDataResult success () {
        return AjaxDataResult.success("操作成功");
    }

    /**
     * 返回成功数据
     *
     * @return 成功消息
     */
    public static AjaxDataResult success (Object data) {
        return AjaxDataResult.success("操作成功", data);
    }

    /**
     * 返回成功消息
     *
     * @param msg
     *         返回内容
     *
     * @return 成功消息
     */
    public static AjaxDataResult success (String msg) {
        return AjaxDataResult.success(msg, null);
    }

    /**
     * 返回成功消息
     *
     * @param msg
     *         返回内容
     * @param data
     *         数据对象
     *
     * @return 成功消息
     */
    public static AjaxDataResult success (String msg, Object data) {
        return new AjaxDataResult(Type.SUCCESS.value, msg, data);
    }

    /**
     * 返回警告消息
     *
     * @param msg
     *         返回内容
     *
     * @return 警告消息
     */
    public static AjaxDataResult warn (String msg) {
        return AjaxDataResult.warn(msg, null);
    }

    /**
     * 返回警告消息
     *
     * @param msg
     *         返回内容
     * @param data
     *         数据对象
     *
     * @return 警告消息
     */
    public static AjaxDataResult warn (String msg, Object data) {
        return new AjaxDataResult(Type.WARN.value, msg, data);
    }

    /**
     * 返回错误消息
     *
     * @return
     */
    public static AjaxDataResult error () {
        return AjaxDataResult.error("操作失败");
    }

    /**
     * 返回错误消息
     *
     * @param msg
     *         返回内容
     *
     * @return 警告消息
     */
    public static AjaxDataResult error (String msg) {
        return AjaxDataResult.error(msg, null);
    }

    /**
     * 返回错误消息
     *
     * @param msg
     *         返回内容
     * @param data
     *         数据对象
     *
     * @return 警告消息
     */
    public static AjaxDataResult error (String msg, Object data) {
        return new AjaxDataResult(Type.ERROR.value, msg, data);
    }

    /**
     * 返回错误消息
     *
     * @param msg
     *         返回内容
     * @param data
     *         数据对象
     *
     * @return 警告消息
     */
    public static AjaxDataResult error (Integer code, String msg, Object data) {
        return new AjaxDataResult(code, msg, data);
    }

    /**
     * 返回错误消息
     *
     * @param msg
     *         返回内容
     * @param data
     *         数据对象
     *
     * @return 警告消息
     */
    public static AjaxDataResult error (Type type, String msg, Object data) {
        return new AjaxDataResult(type.value, msg, data);
    }
}
