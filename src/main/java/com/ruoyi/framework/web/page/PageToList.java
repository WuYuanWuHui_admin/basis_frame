/*
 * ...
 */

package com.ruoyi.framework.web.page;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 用于List分页
 *
 * @author 挺好的 2023年02月27日 下午 12:50
 */

@NoArgsConstructor
@Data
public class PageToList {

    public PageToList (int pageSize, long total) {
        this.pageSize = pageSize;
        this.total    = total;
    }

    /**
     * 总数
     */
    private long total;

    /**
     * 每页处理多少条数据
     */
    private int pageSize;

    /**
     * 当前页码
     */
    private int pageNumber;

    public long getStartPosition (int pageNumber) {
        return (pageNumber - 1) * this.pageSize;
    }

    /**
     * 获取全部页数
     *
     * @return
     */
    public long getTotalPages () {
        return this.total % this.pageSize == 0 ? this.total / this.pageSize : this.total / this.pageSize + 1;
    }

}
