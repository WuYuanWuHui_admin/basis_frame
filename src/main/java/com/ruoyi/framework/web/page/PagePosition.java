/*
 * ...
 */

package com.ruoyi.framework.web.page;

import com.ruoyi.project.utils.NumberUtils;

/**
 * @author 挺好的 2023年02月18日 上午 9:06
 */
public class PagePosition {

    /**
     * 默认页数
     */
    private static final int DEFAULT_PAGE_SIZE = 10;

    /**
     * 最大显示数量
     */
    private static final int MAX_PAGE_SIZE = 50;

    /**
     * 第一页
     */
    private static final int FIRST_PAGE_NO = 1;

    /**
     * 当前记录起始索引
     */
    private Integer pageNum;

    /**
     * 每页显示记录数
     */
    private Integer pageSize;

    public PagePosition (Integer pageNumber, Integer pageSize) {
        this.pageNum  = NumberUtils.toInteger(pageNumber, 1);
        this.pageSize = pageSize <= 0 ? DEFAULT_PAGE_SIZE : pageSize;
    }

    public Integer getPageNum () {
        return this.pageNum;
    }

    public void setPageNum (Integer pageNum) {
        this.pageNum = NumberUtils.toInteger(pageNum, 1);
    }

    public Integer getPageSize () {
        return this.pageSize;
    }

    public void setPageSize (Integer pageSize) {
        pageSize = NumberUtils.toInteger(pageSize, DEFAULT_PAGE_SIZE);

        if (pageSize > MAX_PAGE_SIZE) {
            pageSize = MAX_PAGE_SIZE;
        }

        this.pageSize = pageSize;
    }

    /**
     * 获取开始位置
     *
     * @return
     */
    public int getStartPosition () {
        return (this.pageNum - FIRST_PAGE_NO) * this.pageSize;
    }
}
