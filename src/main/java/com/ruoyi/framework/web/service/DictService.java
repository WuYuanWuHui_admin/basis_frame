package com.ruoyi.framework.web.service;

import java.util.*;

import com.ruoyi.project.content.*;
import com.ruoyi.project.system.dict.mapper.DictTypeMapper;
import com.ruoyi.project.system.user.service.IUserService;
import com.ruoyi.project.utils.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.system.dict.domain.DictData;
import com.ruoyi.project.system.dict.service.IDictDataService;

import javax.annotation.Resource;

/**
 * RuoYi首创 html调用 thymeleaf 实现字典读取
 *
 * @author ruoyi
 */
@Service("dict")
public class DictService
{
    @Autowired
    private IDictDataService dictDataService;
    @Autowired
    private RedisUtils redisUtils;

    @Autowired
    private IUserService userService;
    @Autowired
    private DictTypeMapper dictTypeMapper;

    /**
     * 根据字典类型查询字典数据信息
     *
     * @param dictType 字典类型
     * @return 参数键值
     */
    public List<DictData> getType(String dictType)
    {
        return dictDataService.selectDictDataByType(dictType);
    }

    /**
     * 根据字典类型和字典键值查询字典数据信息
     *
     * @param dictType 字典类型
     * @param dictValue 字典键值
     * @return 字典标签
     */
    public String getLabel(String dictType, String dictValue)
    {
        return dictDataService.selectDictLabel(dictType, dictValue);
    }

    /**
     * 获取配置类型
     *
     * @return
     */
    public List<Map> getPropertyType() {
        return PropertyTypeEnum.getAllEnum();
    }

    public List<Map> getTaskStatus() {
        return TaskStatusEnum.getAllEnum();
    }

    /**
     * 根据配置类型获取数据列表
     *
     * @param type
     * @return
     */
    public List<Map> getPropertyByType(Integer type) {
        List<Map> result = new LinkedList<>();
        Map<Object, Object> hmget = redisUtils.hmget(PropertyTypeEnum.findByValue(type).getText());
        if (hmget != null && hmget.size() > 0) {
            for (Object key : hmget.keySet()) {
                Map<String, Object> map = new HashMap<>();
                map.put("dictValue", String.valueOf(key));
                map.put("dictLabel", String.valueOf(hmget.get(key)));
                result.add(map);
            }
        }
        return result;
    }

    /**
     * 订单状态
     * @return
     */
    public List<Map> getOrderStatus(){
        return OrderStatusEnum.getAllEnum();
    }


    /**
     * 流水
     * @return
     */
    public List<Map> getFlowType() {
        return FlowTypeEnum.getAllEnum();
    }
    public List<Map> getFlowTypezj() {
        return FlowTypeEnum.getAllEnumzj();
    }

    /**
     * 提现类型
     * @return
     */
    public List<Map> getWithdrawalType() {
        return WithdrawalTypeEnum.getAllEnum();
    }



    public List<Map> getCommonUser(){
        return userService.selectCommonUser();
    }



    //查询等级
    public List<Map> getGrade(){
        List<Map> maps = new ArrayList<>();
//        HashMap map = new HashMap();
//        map.put("dictValue","0");
//        map.put("dictLabel","全部");

        HashMap lowMap = new HashMap();
        lowMap.put("dictValue","1");
        lowMap.put("dictLabel","低");

        HashMap centreMap = new HashMap();
        centreMap.put("dictValue","2");
        centreMap.put("dictLabel","中");

        HashMap highMap = new HashMap();
        highMap.put("dictValue","3");
        highMap.put("dictLabel","高");

//        maps.add(map);
        maps.add(lowMap);
        maps.add(centreMap);
        maps.add(highMap);
        return maps;
    }

    //查询性别
    public List<Map> getSex(){
        List<Map> maps = new ArrayList<>();
        HashMap map = new HashMap();
        map.put("dictValue","0");
        map.put("dictLabel","不限");

        HashMap manMap = new HashMap();
        manMap.put("dictValue","1");
        manMap.put("dictLabel","男");

        HashMap womanMap = new HashMap();
        womanMap.put("dictValue","2");
        womanMap.put("dictLabel","女");

        maps.add(map);
        maps.add(manMap);
        maps.add(womanMap);
        return maps;
    }


    //查询埋点类型
    public List<Map> getBurialPointType(){
        List<Map> maps = new ArrayList<>();
        HashMap map1 = new HashMap();
        map1.put("dictValue","1");
        map1.put("dictLabel","发布任务使用输入框");

        HashMap map2 = new HashMap();
        map2.put("dictValue","2");
        map2.put("dictLabel","发布任务使用滑动条");

        maps.add(map1);
        maps.add(map2);
        return maps;
    }


    //查询投诉状态
    public List<Map> getComplaintStatus(){
        List<Map> maps = new ArrayList<>();
        HashMap map1 = new HashMap();
        map1.put("dictValue","1");
        map1.put("dictLabel","未申诉");

        HashMap map2 = new HashMap();
        map2.put("dictValue","2");
        map2.put("dictLabel","申诉中");

        HashMap map3 = new HashMap();
        map3.put("dictValue","3");
        map3.put("dictLabel","已结案");

        maps.add(map1);
        maps.add(map2);
        maps.add(map3);
        return maps;
    }

    //查询投诉结果
    public List<Map> getComplaintResults(){
        List<Map> maps = new ArrayList<>();
        HashMap map1 = new HashMap();
        map1.put("dictValue","1");
        map1.put("dictLabel","申诉成功");

        HashMap map2 = new HashMap();
        map2.put("dictValue","2");
        map2.put("dictLabel","申诉失败");

        maps.add(map1);
        maps.add(map2);
        return maps;
    }

    //查询投诉板块
    public List<Map> getComplaintPlate(){
        List<Map> complaintPlate = dictTypeMapper.getComplaintPlate();
        return complaintPlate;
    }

    //查询投诉类型
    public List<Map> getComplaintType(){
        List<Map> complaintPlate = dictTypeMapper.getComplaintType();
        return complaintPlate;
    }


    //查询申诉状态
    public List<Map> getAppealStatus(){
        List<Map> maps = new ArrayList<>();
        HashMap map1 = new HashMap();
        map1.put("dictValue","1");
        map1.put("dictLabel","未申诉");

        HashMap map2 = new HashMap();
        map2.put("dictValue","2");
        map2.put("dictLabel","申诉中");

        HashMap map3 = new HashMap();
        map3.put("dictValue","3");
        map3.put("dictLabel","已结案");

        maps.add(map1);
        maps.add(map2);
        maps.add(map3);
        return maps;
    }

    //查询申诉结果
    public List<Map> getAppealResults(){
        List<Map> maps = new ArrayList<>();
        HashMap map1 = new HashMap();
        map1.put("dictValue","1");
        map1.put("dictLabel","申诉成功");

        HashMap map2 = new HashMap();
        map2.put("dictValue","2");
        map2.put("dictLabel","申诉失败");

        maps.add(map1);
        maps.add(map2);
        return maps;
    }

    //查询扣分类型
    public List<Map> getDeductionType(){
        List<Map> maps = new ArrayList<>();

        String string = "无工装打卡-迟到-辞工-强制收工-差评-投诉";
        String[] split = string.split("-");

        for (String s:split) {
            HashMap map = new HashMap();
            map.put("dictValue",s);
            map.put("dictLabel",s);
            maps.add(map);
        }
        return maps;
    }

}
