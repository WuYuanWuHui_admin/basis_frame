package com.ruoyi.framework.web.controller;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ruoyi.common.constant.SystemContstant;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.security.ShiroUtils;
import com.ruoyi.common.utils.sql.SqlUtil;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.domain.AjaxResult.Type;
import com.ruoyi.framework.web.page.PageDomain;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.framework.web.page.TableSupport;
import com.ruoyi.project.system.user.domain.User;
import com.ruoyi.project.utils.IdempotentUtils;
import com.ruoyi.project.utils.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;

import java.beans.PropertyEditorSupport;
import java.util.Date;
import java.util.List;

/**
 * web层通用数据处理
 *
 * @author ruoyi
 */
public class BaseController {

    @Autowired
    private RedisUtils redisUtils;

    /**
     * 将前台传递过来的日期格式的字符串，自动转化为Date类型
     */
    @InitBinder
    public void initBinder (WebDataBinder binder) {
        // Date 类型转换
        binder.registerCustomEditor(Date.class, new PropertyEditorSupport() {
            @Override
            public void setAsText (String text) {
                this.setValue(DateUtils.parseDate(text));
            }
        });
    }

    /**
     * 设置请求分页数据
     */
    protected void startPage () {
        PageDomain pageDomain = TableSupport.buildPageRequest();
        Integer pageNum = pageDomain.getPageNum();
        Integer pageSize = pageDomain.getPageSize();

        if (StringUtils.isNotNull(pageNum) && StringUtils.isNotNull(pageSize)) {
            String orderBy = SqlUtil.escapeOrderBySql(pageDomain.getOrderBy());
            PageHelper.startPage(pageNum, pageSize, orderBy);
        }
    }

    /**
     * 设置请求分页数据
     */
    protected void startPage (Integer pageNum,Integer pageSize) {
        String orderBy = SqlUtil.escapeOrderBySql(null);
        PageHelper.startPage(pageNum, pageSize, orderBy);
    }

    protected void startPageOffCount () {
        PageDomain pageDomain = TableSupport.buildPageRequest();
        Integer pageNum = pageDomain.getPageNum();
        Integer pageSize = pageDomain.getPageSize();

        if (StringUtils.isNotNull(pageNum) && StringUtils.isNotNull(pageSize)) {
            //String orderBy = SqlUtil.escapeOrderBySql(pageDomain.getOrderBy());
            Page page = PageHelper.startPage(pageNum, pageSize, false);
            //page.setOrderBy(orderBy);
        }

    }

    /**
     * 响应请求分页数据
     */
    @SuppressWarnings ({"rawtypes", "unchecked"})
    protected TableDataInfo getDataTable (List <?> list) {
        TableDataInfo rspData = new TableDataInfo();
        rspData.setCode(0);
        rspData.setRows(list);
        rspData.setTotal(new PageInfo(list).getTotal());
        return rspData;
    }

    /**
     * 响应返回结果
     *
     * @param rows
     *         影响行数
     *
     * @return 操作结果
     */
    protected AjaxResult toAjax (int rows) {
        return rows > 0 ? this.success() : this.error();
    }

    /**
     * 响应返回结果
     *
     * @param result
     *         结果
     *
     * @return 操作结果
     */
    protected AjaxResult toAjax (boolean result) {
        return result ? this.success() : this.error();
    }

    /**
     * 返回成功
     */
    public AjaxResult success () {
        return AjaxResult.success();
    }

    /**
     * 返回失败消息
     */
    public AjaxResult error () {
        return AjaxResult.error();
    }

    /**
     * 返回成功消息
     */
    public AjaxResult success (String message) {
        return AjaxResult.success(message);
    }

    /**
     * 返回失败消息
     */
    public AjaxResult error (String message) {
        return AjaxResult.error(message);
    }

    /**
     * 返回错误码消息
     */
    public AjaxResult error (Type type, String message) {
        return new AjaxResult(type, message);
    }

    /**
     * 页面跳转
     */
    public String redirect (String url) {
        return StringUtils.format("redirect:{}", url);
    }

    public User getSysUser () {
        return ShiroUtils.getSysUser();
    }

    public void setSysUser (User user) {
        ShiroUtils.setSysUser(user);
    }

    public Long getUserId () {
        return this.getSysUser().getUserId();
    }

    public Long getAppUid () throws Exception {
        String token = ServletUtils.getRequest().getHeader("token");
        String uid = this.redisUtils.get(SystemContstant.TOKEN + token);
        if (StringUtils.isBlank(uid)) {
            throw new RuntimeException("用户未登录, 请登录后重试");
        }
        return new Long(uid);
    }

    public Long getAppUserId ()  {
        String token = ServletUtils.getRequest().getHeader("token");
        String uid = this.redisUtils.get(SystemContstant.TOKEN + token);
        if (StringUtils.isBlank(uid)) {
            return 0L;
        }
        return new Long(uid);
    }

    /**
     * 获取接口接入渠道：
     * 0：APP
     * 1：小程序
     * @return
     * @throws Exception
     */
    public Long getAppProjectType () throws Exception {
        String platType = ServletUtils.getRequest().getHeader("platType");

        return new Long(platType);
    }

    public String getRegion () throws Exception {
        String region = ServletUtils.getRequest().getHeader("region");
        if (StringUtils.isBlank(region)) {
            return null;
        }
        return region.substring(0,6);
    }

    public String getLoginName () {
        return this.getSysUser().getLoginName();
    }

    /**
     * 为空时报错
     */
    public void isNotBlank (String str, String remark) throws Exception {
        if (StringUtils.isBlank(str)) {
            throw new RuntimeException(remark);
        }
    }

    public void isNotBlankToRun (String str, String remark) throws Exception {
        if (StringUtils.isBlank(str)) {
            throw new RuntimeException(remark);
        }
    }

    /**
     * 为空时报错
     *
     * @param obj
     * @param remark
     *
     * @throws Exception
     */
    public void isNotNull (Object obj, String remark) throws Exception {
        if (obj == null) {
            throw new RuntimeException(remark);
        }
    }

    public void isNotNull (Object obj, String remark, String id) throws Exception {
        if (obj == null) {
            try {
                IdempotentUtils.remobc(id, this.getClass());
            } catch (Exception e) {
                // TODO: handle exception
            }

            throw new RuntimeException(remark);
        }
    }

    /**
     * 非正确值时报错
     *
     * @param flag
     * @param remark
     *
     * @throws Exception
     */
    public void isTrue (boolean flag, String remark) throws Exception {
        if (!flag) {

            throw new RuntimeException(remark);
        }
    }

    public void isTrue (boolean flag, String remark, String id) throws Exception {
        if (!flag) {
            try {
                IdempotentUtils.remobc(id, this.getClass());
            } catch (Exception e) {
                // TODO: handle exception
            }

            throw new RuntimeException(remark);
        }
    }


    public static void checkUni (RedisUtils redisUtils, String token, String url, String remark) throws Exception {
        String s = redisUtils.get(SystemContstant.REQUEST_TOKEN + token + "_" + url);
        if (StringUtils.isNotBlank(s) && (DateUtils.getNowDate().getTime() - Long.valueOf(s) <= 10000)) {
            throw new RuntimeException(remark);
        }
        redisUtils.set(SystemContstant.REQUEST_TOKEN + token + "_" + url, DateUtils.getNowDate().getTime(), 20);
    }

    /**
     * xss攻击校验
     *
     * @param obj
     * @param remark
     *
     * @throws Exception
     */
    public void xssCheck (String obj, String remark) throws Exception {
        if (!(StringUtils.isNotBlank(obj) && obj.indexOf("<") == -1 && obj.indexOf(">") == -1)) {
            throw new RuntimeException(remark);
        }
    }

    public void xssCheck (String obj, String remark, String id) throws Exception {
        if (!(StringUtils.isNotBlank(obj) && obj.indexOf("<") == -1 && obj.indexOf(">") == -1)) {
            try {
                IdempotentUtils.remobc(id, this.getClass());
            } catch (RuntimeException e) {
                // TODO: handle exception
            }


            throw new RuntimeException(remark);
        }
    }

}
