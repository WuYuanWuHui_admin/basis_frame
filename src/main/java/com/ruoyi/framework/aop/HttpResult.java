package com.ruoyi.framework.aop;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author 杨航
 * @Version: V1.0
 * @since 2020/12/8 13:32
 */
@Data
@ApiModel(value = "响应参数实体", description = "响应参数实体")
public class HttpResult implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 返回结果报文 状态码 : 返回标记：成功标记=0，失败标记非0
     */
    @ApiModelProperty(value = "返回标记：成功标记=0，失败标记非0", name = "code", notes = "返回标记：成功标记=0，失败标记非0", example = "0", required = true, hidden = false)
    private Integer code;

    /**
     * 返回结果信息 : code为0的时候为: 请求成功、code为非0的时候为: 请求失败
     */
    @ApiModelProperty(value = "code为0的时候请求成功、code为非0的时候请求失败", name = "msg", notes = "code为0的时候请求成功、code为非0的时候请求失败", example = "请求成功", required = true, hidden = false)
    private String msg;

    /**
     * 返回结果数据
     */
    @ApiModelProperty(value = "返回结果数据", name = "data", notes = "返回结果数据", example = "true", required = true, hidden = false)
    private Object data;

}
