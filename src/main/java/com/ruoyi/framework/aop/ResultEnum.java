package com.ruoyi.framework.aop;

import lombok.Getter;


/**
 * <p>
 * 返回结果枚举工具类
 * </p>
 *
 * @author YangHang
 * @Version: V1.0
 * @since 2019-03-05 10:31
 */
@Getter
public enum ResultEnum {
   /*
   * 请求相关
   */
    SUCCESS_CODE(0, "请求成功"),
    ERROR_CODE(1, "请求失败"),
    REQUEST_BODY_EMPTY(2, "请求json格式不正确"),
    REQUEST_LIMIT(3, "你的操作过于频繁,请休息一下吧!"),
    REPECT_SUBMIT(4, "系统正在处理、请勿重复提交!"),

    /*
    * 文件相关
    */
    UPLOAD_SUCCESS_CODE(5, "上传成功"),
    UPLOAD_ERROR_CODE(6, "上传失败"),
    IMPORT_SUCCESS_CODE(7, "导入成功"),
    IMPORT_ERROR_CODE(8, "导入失败"),
    EXPORT_SUCCESS_CODE(7, "导出成功"),
    EXPORT_ERROR_CODE(8, "导出失败"),

    /*
    *其他
    */
    PLATE_NUMBER_EXIST_CODE(9, "车牌号已经存在"),
    TOKEN_LOSE_EFFICACY(10, "当前会话失效、请退出重登录再试"),
    EXCEL_UPLOAD_LIMIT(11, "当前系统正在处理刚刚上传Excel批量文件、请稍后再上传"),
    REDIS_ERROR(12, "操作Redis异常"),
    REQUEST_METHOD_ERROR(13, "请求方式不正确，请核对post、get、put、delete其中一种请求方式"),
    MYSQL_ERROR(14, "数据库操作出错"),

    /**
     *用户相关
     */
    USER_NOT_EXIST(16, "该用户不存在，请注册"),
    USER_LOGGED_OUT(17, "该用户已注销"),
    USER_ALREADY_FROZEN(18, "该用户已冻结"),

    REQUEST_PARAM_NOT_EMPTY(100, "请求参数不能为空"),
    BAD_REQUEST(400, "请求异常！"),
    QUERY_DATA_NOT_EXIST(404, "查询数据不存在"),
    ACCESS_DENIED_ERROR(403, "令牌错误或权限不足、请联系管理员!"),

    TOMANY_DATA(411, "查询数据过多，建议减小时间粒度分批导出"),

    INTER_ERROR(500, "系统报错! 请联系管理员处理!"),

    NETWORK_ERROR(600, "网络错误，请重试"),

    PARAM_IS_ERROR(700, "输入的参数不正确"),

    USERNAME_PASSWORD_ERROR(5001, "用户名或密码错误！"),

    TIMESTAMP_NOT_EMPTY(7003, "timestamp不能为空"),

    TIMESTAMP_TIME_OUT(7004, "timestamp 超时"),

    SIGN_ERROR(7005, "签名错误"),

    PARAM_SIGN_CHECK_ERROR(7006, "参数与签名核验错误"),

    DATA_ENCRYPT_ERROR(7007, "数据加密错误"),

    SIGN_NOT_EMPTY(7008, "签名不能为空"),

    REDIS_LOCK_FAIL(7009, "请求超时 请稍后再试!"),

    /**
     * 购物车
     */
    CART_FAIL(220001,"添加购物车失败"),


    /**
     *验证码
     */
    CODE_TO_ERROR(240001,"接收号码不合规"),
    CODE_LIMITED(240002,"验证码发送过快"),
    CODE_ERROR(240003,"验证码错误"),
    CODE_CAPTCHA_ERROR(240101,"图形验证码错误"),
    SMS_CAPTCHA_ERROR(240101,"短信验证码错误"),

    /**
     * 账号
     */
    ACCOUNT_REPEAT(250001,"账号已经存在"),
    ACCOUNT_UNREGISTER(250002,"账号不存在"),
    ACCOUNT_PWD_ERROR(250003,"账号或者密码错误"),
    ACCOUNT_UNLOGIN(250004,"账号未登录"),

    /**
     * 优惠券
     */
    COUPON_CONDITION_ERROR(270001,"优惠券条件错误"),
    COUPON_UNAVAILABLE(270002,"没有可用的优惠券"),
    COUPON_NO_EXITS(270003,"优惠券不存在"),
    COUPON_NO_STOCK(270005,"优惠券库存不足"),
    COUPON_OUT_OF_LIMIT(270006,"优惠券领取超过限制次数"),
    COUPON_OUT_OF_TIME(270407,"优惠券不在领取时间范围"),
    COUPON_GET_FAIL(270407,"优惠券领取失败"),
    COUPON_RECORD_LOCK_FAIL(270409,"优惠券锁定失败"),


    /**
     * 订单
     */
    ORDER_CONFIRM_COUPON_FAIL(280001,"创建订单-优惠券使用失败,不满足价格条件"),
    ORDER_CONFIRM_PRICE_FAIL(280002,"创建订单-验价失败"),
    ORDER_CONFIRM_LOCK_PRODUCT_FAIL(280003,"创建订单-商品库存不足锁定失败"),
    ORDER_CONFIRM_ADD_STOCK_TASK_FAIL(280004,"创建订单-新增商品库存锁定任务"),
    ORDER_CONFIRM_TOKEN_NOT_EXIST(280008,"订单令牌缺少"),
    ORDER_CONFIRM_TOKEN_EQUAL_FAIL(280009,"订单令牌不正确"),
    ORDER_CONFIRM_NOT_EXIST(280010,"订单不存在"),
    ORDER_CONFIRM_CART_ITEM_NOT_EXIST(280011,"购物车商品项不存在"),

    /**
     * 收货地址
     */
    ADDRESS_ADD_FAIL(290001,"新增收货地址失败"),
    ADDRESS_DEL_FAIL(290002,"删除收货地址失败"),
    ADDRESS_NO_EXITS(290003,"地址不存在"),

    /**
     * 支付
     */
    PAY_ORDER_FAIL(300001,"创建支付订单失败"),
    PAY_ORDER_CALLBACK_SIGN_FAIL(300002,"支付订单回调验证签失败"),
    PAY_ORDER_CALLBACK_NOT_SUCCESS(300003,"支付宝回调更新订单失败"),
    PAY_ORDER_NOT_EXIST(300005,"订单不存在"),
    PAY_ORDER_STATE_ERROR(300006,"订单状态不正常"),
    PAY_ORDER_PAY_TIMEOUT(300007,"订单支付超时"),


    /**
     * 流控操作
     */

    CONTROL_FLOW(500101,"限流控制"),
    CONTROL_DEGRADE(500201,"降级控制"),
    CONTROL_AUTH(500301,"认证控制"),


    /**
     * 文件相关
     */
    FILE_UPLOAD_USER_IMG_FAIL(600101,"用户头像文件上传失败");

    private Integer code;

    private String message;

    ResultEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

}
