package com.ruoyi.framework.aop;

import cn.hutool.core.util.ObjectUtil;
import com.ruoyi.project.utils.RedisUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.transaction.TransactionException;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@Aspect
@Component
/**
 * @功能描述 aop解析注解
 * @author www.gaozz.club
 * @date 2018-11-02
 */
@Slf4j
public class NoRepeatTwoSubmitAop {
    
    @Resource
    private RedisUtils redisUtils;
    
    @Before("@annotation(com.ruoyi.framework.aop.NoRepeatTwoSubmit)")
    public void before(JoinPoint pjp) throws TransactionException {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        String sessionId = RequestContextHolder.getRequestAttributes().getSessionId();
        HttpServletRequest request = attributes.getRequest();
        String key = sessionId + "-" + request.getServletPath();
        String keyCount = ObjectUtil.isEmpty(redisUtils.get(key)) ? "0" : redisUtils.get(key);
        Long keyLong =Long.parseLong(keyCount);
        if (keyLong < 5) {// 如果缓存中有这个url视为重复提交
            redisUtils.setPivotal(key, keyLong+1, 1L);
        } else {
            log.error("重复提交");
            throw new RuntimeException("亲！您慢一点~");
        }
    }

}
