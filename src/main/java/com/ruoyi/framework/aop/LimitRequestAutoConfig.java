package com.ruoyi.framework.aop;

import lombok.AllArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import java.io.Serializable;


/**
 * <p>
 * 限流注解Bean自动注入到spring容器中。其他模块使用
 * </p>
 *
 * @author 杨朝勇
 * @Version: V1.0
 * @since 2019-03-06 12:59
 */
@Configuration
@AllArgsConstructor
@ConditionalOnWebApplication
public class LimitRequestAutoConfig {

	@Bean
	public LimitRequestInterceptor limitInterceptor() {
		return new LimitRequestInterceptor();
	}

	@Bean
	public RedisTemplate<String, Serializable> limitRedisTemplate(LettuceConnectionFactory redisConnectionFactory) {
		RedisTemplate<String, Serializable> template = new RedisTemplate<>();
		template.setKeySerializer(new StringRedisSerializer());
		template.setValueSerializer(new GenericJackson2JsonRedisSerializer());
		template.setConnectionFactory(redisConnectionFactory);
		return template;
	}

}
