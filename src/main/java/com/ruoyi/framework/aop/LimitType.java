package com.ruoyi.framework.aop;


/**
 * <p>
 * 限流注解枚举类型
 * </p>
 *
 * @author 杨航
 * @Version: V1.0
 * @since 2019-04-13 17:46
 */
public enum LimitType {

	/**
	 * 自定义key
	 */
	CUSTOMER,

	/**
	 * 根据请求IP
	 */
	IP,
}
