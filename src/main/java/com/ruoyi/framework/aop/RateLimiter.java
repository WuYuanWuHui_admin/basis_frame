package com.ruoyi.framework.aop;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author 挺好的 2023年06月28日 下午 16:23
 */
@Target (ElementType.METHOD)
@Retention (RetentionPolicy.RUNTIME)
@Documented
public @interface RateLimiter {

    /**
     * 限流key
     */
    String key () default "RATE_LIMIT_KEY";

    /**
     * 限流时间,单位秒
     */
    int time () default 60;

    /**
     * 限流次数
     */
    int count () default 100;

    /**
     * 限流类型
     */
    LimitType limitType () default LimitType.IP;

    /**
     * 限流后返回的文字
     */
    String limitMsg () default "访问过于频繁，请稍候再试";
}
