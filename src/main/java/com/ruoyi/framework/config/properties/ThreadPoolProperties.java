/*
 * Copyright 2008-2021 tianzhu.cloud. All rights reserved.
 * Support: http://www.tianzhu.cloud
 * License: http://www.tianzhu.cloud/license
 */

package com.ruoyi.framework.config.properties;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 线程池配置
 *
 * @author 挺好的 2023年02月27日 9:42
 */
@ConfigurationProperties ("thread-pool")
@Component ("threadPoolProperties")
@Data
@EqualsAndHashCode (callSuper = false)
public class ThreadPoolProperties {

    private int coreSize;

    private int maxSize;

    private int queueCapacity;

    private String threadPoolNamePrefix;

    private int keepAliveSeconds;


}
