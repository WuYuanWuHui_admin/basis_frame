package com.ruoyi.mongodb.bajiaostar.user.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.Date;

/**
 * 用户信息对象 t_user
 * 
 * @author 谢少辉
 * @date 2024-03-31
 */
@Document(collection = "userAppEntity")
public class UserAppEntity
{
    /** 用户id */
    @Id
    @Field("_id")
    private String id;

    /** 用户名称 */
    private String username;

    /** 手机号码 */
    private String moble;

    /** 上级用户集合 */
    private String pushuserids;
    //身份证
    private String sfz;

    /** 推荐人id */
    private String pushuserid;


    /** 推广码 */
    private String pushcode;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMoble() {
        return moble;
    }

    public void setMoble(String moble) {
        this.moble = moble;
    }

    public String getPushuserids() {
        return pushuserids;
    }

    public void setPushuserids(String pushuserids) {
        this.pushuserids = pushuserids;
    }

    public String getSfz() {
        return sfz;
    }

    public void setSfz(String sfz) {
        this.sfz = sfz;
    }

    public String getPushuserid() {
        return pushuserid;
    }

    public void setPushuserid(String pushuserid) {
        this.pushuserid = pushuserid;
    }

    public String getPushcode() {
        return pushcode;
    }

    public void setPushcode(String pushcode) {
        this.pushcode = pushcode;
    }
}
