package com.ruoyi.mongodb.bajiaostar.user.service;

import com.ruoyi.mongodb.bajiaostar.user.entity.UserAppEntity;
import com.ruoyi.mongodb.bajiaostar.user.repository.UserAppEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.List;

@Service
public class UserAppEntityService {
    @Autowired
    UserAppEntityRepository userAppEntityRepository;
    @Autowired
    private MongoTemplate mongoTemplate;

    /**
     * 根据用户id获取数据
     * @param id
     * @return
     */
    public UserAppEntity findById(String id) {
        Query query1 = new Query(Criteria
                .where("id").is(id));
        //List<UserAppEntity> userList = mongoTemplate.find(query1, UserAppEntity.class);

        Query query2 = new Query(Criteria
                .where("_id").is(id));
        //List<UserAppEntity> userList1 = mongoTemplate.find(query2, UserAppEntity.class);

        UserAppEntity entity = userAppEntityRepository.findById(id).orElse(null);
        return entity;
    }

    /**
     * 保存信息
     * @param id
     * @return
     */
    public UserAppEntity add(UserAppEntity id) {
        UserAppEntity save = userAppEntityRepository.save(id);
        return save;
    }

    /**
     * 根据用户id查询用户
     * @param userId
     * @return
     */
    public UserAppEntity findUsersOne(Long userId){
        Query query1 = new Query(Criteria
                .where("_id").is(userId));
        //UserAppEntity user = mongoTemplate.findById(query1, UserAppEntity.class);
        UserAppEntity user = userAppEntityRepository.findById(userId.toString()).orElse(null);
        return user;

    }

    /**
     *
     * @param userName
     * @param flag
     * @return
     */
    public List<UserAppEntity> findUserLevelDirectPush(String userName,boolean flag) {

        Query query0 = new Query();
        query0.addCriteria(Criteria.where("pushuserids").regex( ","+userName+","));
        if(flag){
            query0.addCriteria(Criteria.where("sfz").ne(null));
        }
        List<UserAppEntity> entityList = mongoTemplate.find(query0, UserAppEntity.class);
        return entityList;
    }

    /**
     *
     * @param userName
     * @param flag
     * @return
     */
    public Integer findUserLevelDirectPushCount(String userName,boolean flag) {

        Query query0 = new Query();
        query0.addCriteria(Criteria.where("pushuserids").regex( ","+userName+","));
        if(flag){
            query0.addCriteria(Criteria.where("sfz").ne(null));
        }
        List<UserAppEntity> entityList = mongoTemplate.find(query0, UserAppEntity.class);
        return ObjectUtils.isEmpty(entityList)?0:entityList.size();
    }
}
