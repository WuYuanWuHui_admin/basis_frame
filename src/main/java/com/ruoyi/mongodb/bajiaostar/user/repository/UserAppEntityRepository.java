package com.ruoyi.mongodb.bajiaostar.user.repository;

import com.ruoyi.mongodb.bajiaostar.user.entity.UserAppEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

@Component
public interface UserAppEntityRepository extends MongoRepository<UserAppEntity, String> {

}
