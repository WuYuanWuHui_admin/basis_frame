package com.ruoyi.project.push;

import cn.hutool.core.date.DateUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.ruoyi.framework.web.domain.AjaxResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;


@Component
public class XcxMsgPush {

    private static final Logger log = LoggerFactory.getLogger(XcxMsgPush.class);
    @Value ("${wx.xcx.appid}")
    private String xcxAppid;

    @Value ("${wx.xcx.secretid}")
    private String xcxSecret;

    public String getAccessToken() {
        /*String appId = "wx7dedaad636ca78fe";
        String appSecret = "0798c8c83853a2fa1948f2862a749a01";*/
        String result = cn.hutool.http.HttpUtil.get("https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" + xcxAppid + "&secret=" + xcxSecret);
        JSONObject jsonObject = JSONUtil.parseObj(result);
        return jsonObject.getStr("access_token");
    }

    /**
     * 发送消息
     * @param openId  用户openId
     * @param templateId  模板id
     * @param taskNmae 任务名称
     * @param remarks  备注
     * @return
     */
    public AjaxResult send(String openId,String templateId,String taskNmae,String remarks){
        JSONObject body=new JSONObject();
        body.set("touser",openId);
        body.set("template_id",templateId);
        JSONObject json=new JSONObject();
        json.set("thing6",new JSONObject().set("value","20200820757539"));
        json.set("date4",new JSONObject().set("value", LocalDateTime.now()));
        json.set("amount3",new JSONObject().set("value",taskNmae));
        json.set("character_string2",new JSONObject().set("value", DateUtil.now()));
        json.set("thing5",new JSONObject().set("value",remarks));
        body.set("data",json);
        //发送
        String accessToken= getAccessToken();
        String post =  cn.hutool.http.HttpUtil.post("https://api.weixin.qq.com/cgi-bin/message/subscribe/send?access_token=" + accessToken, body.toString());
        System.out.println("消息订阅结果："+post);
        return AjaxResult.success("成功",post);
    }

    public static void main (String[] args) {
         String appId = "wx7dedaad636ca78fe";
        String appSecret = "0798c8c83853a2fa1948f2862a749a01";
        String result = cn.hutool.http.HttpUtil.get("https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" + appId + "&secret=" + appSecret);
        JSONObject jsonObject = JSONUtil.parseObj(result);
        String accessToken=  jsonObject.getStr("access_token");

        String openId = "oucdf5Ca06bZ49FxWhmEHVdHv_cU";
        String templateId="RUR6tVbBCfrN7AVuAvJlpJIhmQ-Gd3uWh79dnnf_98o";
        String taskNmae="找零工测试消息提醒";
        String remarks="您的任务已收工，请尽快支付工资";
        JSONObject body=new JSONObject();
        body.set("touser",openId);
        body.set("template_id",templateId);
        JSONObject json=new JSONObject();
        json.set("thing6",new JSONObject().set("value","20200820757539"));
        json.set("date4",new JSONObject().set("value", LocalDateTime.now()));
        json.set("amount3",new JSONObject().set("value",taskNmae));
        json.set("character_string2",new JSONObject().set("value", DateUtil.now()));
        json.set("thing5",new JSONObject().set("value",remarks));
        body.set("data",json);
        //发送
        String post =  cn.hutool.http.HttpUtil.post("https://api.weixin.qq.com/cgi-bin/message/subscribe/send?access_token=" + accessToken, body.toString());
        System.out.println("消息订阅结果："+post);

    }

}
