package com.ruoyi.project.payUtils.leshua.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LsNotifyParam {
    private String mch_id;
    private String openid;
    private String transaction_id;
    private String out_trade_no;
    private int total_fee;
    private String sign;
}
