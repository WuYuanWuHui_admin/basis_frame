/*
 * ...
 */

package com.ruoyi.project.payUtils.leshua.model;

import lombok.Data;

import java.io.Serializable;

/**
 * @author 挺好的 2023年11月23日 下午 15:05
 */
@Data
public class LsSelectOrderVo implements Serializable {

    /**
     * 商户号
     */
    private String mch_id;

    /**
     * 订单号
     */
    private String  out_trade_no;

    /**
     * 支付订单号
     */
    private String  transaction_id;

    /**
     * 金额
     */
    private String  total_fee;

    /**
     * 支付类型
     */
    private String 	pay_type;

    /**
     * 内容
     */
    private String  body;

    /**
     * 创建时间
     */
    private String  times;

    /**
     * 支付时间
     */
    private String  pay_times;

    /**
     * 状态  0:未支付
     * 1:已支付
     * 2:已退款
     * -1:异常
     */
    private String  state;

}
