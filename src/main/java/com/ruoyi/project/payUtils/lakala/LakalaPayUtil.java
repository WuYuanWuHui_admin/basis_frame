package com.ruoyi.project.payUtils.lakala;


import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.XmlUtil;
import cn.hutool.crypto.digest.DigestUtil;
import cn.hutool.http.HttpRequest;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lkl.laop.sdk.LKLSDK;
import com.lkl.laop.sdk.request.V3CcssCounterOrderCreateRequest;
import com.lkl.laop.sdk.request.V3CcssCounterOrderQueryRequest;
import com.lkl.laop.sdk.request.model.V3CcssOrderGoodsFieldInfo;
import com.lkl.laop.sdk.request.model.V3CcssOrderOutSplitInfo;
import com.lkl.laop.sdk.request.model.V3CcssOrderSceneFieldInfo;
import com.lkl.laop.sdk.utils.JsonUtils;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.project.payUtils.lakala.config.LakalaConfig;
import com.ruoyi.project.payUtils.lakala.entity.req.V3CcssCounterOrderCreateReq;
import com.ruoyi.project.payUtils.lakala.entity.req.V3CcssCounterOrderQueryReq;
import com.ruoyi.project.payUtils.leshua.model.LsSelectOrderVo;
import com.ruoyi.project.payUtils.wx.sdk.WxConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;


@Component
public class LakalaPayUtil {

    private static final Logger log = LoggerFactory.getLogger(LakalaPayUtil.class);
    /**
     * 拉卡拉收银台订单创建回调地址
     */
    @Value("${pay.lakala.lklNotifyUrl}")
    private   String lklNotifyUrl ;

    @Autowired
    private LakalaConfig lakalaConfig;

    /**
     * 收银台订单创建
     * @return
     * @throws Exception
     */
    public String v3CcssCounterOrderCreateRequest (V3CcssCounterOrderCreateReq resq) throws Exception {

        V3CcssCounterOrderCreateRequest req = new V3CcssCounterOrderCreateRequest();
        req.setOutOrderNo(resq.getOutOrderNo());//商户订单号
        req.setMerchantNo(resq.getMerchantNo());//银联商户号
        req.setTotalAmount(resq.getTotalAmount());//订单金额，单位：分
        req.setOrderEfficientTime(DateUtils.parseDateToStr(DateUtils.YYYYMMDDHHMMSS,DateUtils.addMinuteTime(new Date(),30)));
        req.setNotifyUrl(this.lklNotifyUrl);
        req.setOrderInfo("收银台");
        /*req.setSupportRefund(1);
        req.setSupportRepeatPay(1);

        req.setCounterParam("{\"pay_mode\":\""+resq.getCounterParam()+"\"}");
       // req.setGoodsMark("1");

        V3CcssOrderGoodsFieldInfo v3CcssOrderGoodsFieldInfo = new V3CcssOrderGoodsFieldInfo();
        v3CcssOrderGoodsFieldInfo.setGoodsAmt(1L);
        v3CcssOrderGoodsFieldInfo.setGoodsNum(1);
        v3CcssOrderGoodsFieldInfo.setGoodsPricingUnit("4");
        v3CcssOrderGoodsFieldInfo.setGoodsName("外网交易充值");
        v3CcssOrderGoodsFieldInfo.setTePlatformType("1");
        v3CcssOrderGoodsFieldInfo.setTePlatformName("E-BAY");
        v3CcssOrderGoodsFieldInfo.setGoodsType("4");
        req.setGoodsField(JsonUtils.toJSONString(v3CcssOrderGoodsFieldInfo));
        req.setSupportCancel(0);
        //req.setBusiTypeParam("[{\"busi_type\":\"UPCARD\",\"params\":{\"crd_flg\":\"CRDFLG_D|CRDFLG_C|CRDFLG_OTH\"}},{\"busi_type\":\"SCPAY\",\"params\":{\"pay_mode\":\"WECHAT\",\"crd_flg\":\"CRDFLG_D\"}}]");

        //req.setSplitMark("1");
        req.setTermNo("T12331234");

        List<String> sgnInfos = new ArrayList<>();
        sgnInfos.add("1");
        req.setSgnInfo(sgnInfos);
        V3CcssOrderSceneFieldInfo info = new V3CcssOrderSceneFieldInfo();
        V3CcssOrderSceneFieldInfo.HbFqSceneInfo hbFqSceneInfo = new V3CcssOrderSceneFieldInfo.HbFqSceneInfo();
        hbFqSceneInfo.setHbFqNum("3");
        hbFqSceneInfo.setHbFqSellerPercent("0");
        info.setSceneInfo(JsonUtils.toJSONString(hbFqSceneInfo));
        info.setOrderSceneType("HB_FQ");
        req.setOrderSceneField(info);*/
        System.out.println("请求参数："+JSONObject.toJSONString(req));
        //3. 发送请求
        String response = LKLSDK.httpPost(req);
        return  response;
    }


    /**
     * 收银台订单创建
     * @return
     * @throws Exception
     */
    public String v3CcssCounterOrderQueryRequest (V3CcssCounterOrderQueryReq resq) throws Exception {

        V3CcssCounterOrderQueryRequest req = new V3CcssCounterOrderQueryRequest();
        req.setOutOrderNo(resq.getOutOrderNo());//商户订单号
        req.setMerchantNo(resq.getMerchantNo());//银联商户号
        System.out.println("请求参数："+JSONObject.toJSONString(req));
        //3. 发送请求
        String response = LKLSDK.httpPost(req);
        return  response;
    }

}
