package com.ruoyi.project.payUtils.lakala.entity.req;
import lombok.Data;

import java.io.Serializable;

/**
 * 创建支付订单
 */
@Data
public class V3CcssCounterOrderCreateReq implements Serializable {
    /**
     * 用户id
     */
    private String userId;
    /**
     *商户订单号
     */
    private String outOrderNo;
    /**
     *银联商户号
     */
    private String merchantNo;
    /**
     *订单金额，单位：分
     */
    private Long totalAmount;
    /**
     *	收银台参数
     *{\“pay_mode\“ : \“ALIPAY\“} ，指定支付方式为支付宝
     * ALIPAY：支付宝
     * WECHAT：微信
     * UNION：银联云闪付
     * CARD：POS刷卡交易
     * LKLAT：线上转帐
     * QUICK_PAY：快捷支付
     * EBANK：网银支付
     * UNION_CC：银联支付
     */
    private String counterParam;
}
