package com.ruoyi.project.payUtils.lakala.config;

import com.alipay.api.internal.util.file.IOUtils;
import com.lkl.laop.sdk.Config;
import com.lkl.laop.sdk.LKLSDK;
import com.lkl.laop.sdk.exception.SDKException;
import com.ruoyi.project.payUtils.wx.sdk.IWXPayDomain;
import com.ruoyi.project.payUtils.wx.sdk.WXPayConfig;
import com.ruoyi.project.payUtils.wx.sdk.WXPayConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.ByteArrayInputStream;
import java.io.InputStream;

@Component
public class LakalaConfig {

    private static final Logger log = LoggerFactory.getLogger(LakalaConfig.class);


    @Value("${pay.lakala.appId}")
    private   String appId ; // 拉卡拉appId
    @Value("${pay.lakala.serialNo}")
    private   String serialNo ; // 你的证书序列号
    /**
     * 商户私钥信息地址
     */
    @Value("${pay.lakala.priKeyPath}")
    private   String priKeyPath ;
    /**
     * 拉卡拉支付平台证书地址
     */
    @Value("${pay.lakala.lklCerPath}")
    private   String lklCerPath ;
    /**
     * 拉卡拉支付平台证书地址2(用于拉卡拉通知验签)
     */
    @Value("${pay.lakala.lklNotifyCerPath}")
    private   String lklNotifyCerPath ;
    /**
     * 拉卡拉报文加密对称性密钥
     */
    @Value("${pay.lakala.sm4Key}")
    private   String sm4Key ;
    /**
     * 	服务地址
     */
    @Value("${pay.lakala.serverUrl}")
    private   String serverUrl ;

    private static volatile boolean init = false;

    /***
     * @Description: 初始化设置商户公共参数（全局只需设置一次）
     * @throws Exception
     */
    @PostConstruct
    public  void doInit() throws Exception {
        if(!init) {
            init = initByJava();
        }
    }

    public  boolean initByJava() throws SDKException {
        log.info("初始化拉卡拉配置开始");
        Config config = new Config();
        config.setAppId(appId);
        config.setSerialNo(serialNo);
        config.setPriKeyPath(priKeyPath);
        config.setLklCerPath(lklCerPath);
        config.setLklNotifyCerPath(lklNotifyCerPath);
        config.setServerUrl(serverUrl);
        config.setSm4Key(sm4Key);
        boolean init = LKLSDK.init(config);
        //boolean init =false;
        log.info("初始化拉卡拉配置结束");
        return init;
    }
}
