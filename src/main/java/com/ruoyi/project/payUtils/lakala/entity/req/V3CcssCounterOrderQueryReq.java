package com.ruoyi.project.payUtils.lakala.entity.req;
import lombok.Data;

import java.io.Serializable;

/**
 * 创建支付订单
 */
@Data
public class V3CcssCounterOrderQueryReq implements Serializable {

    /**
     *商户订单号
     */
    private String outOrderNo;
    /**
     *银联商户号
     */
    private String merchantNo;

}
