package com.ruoyi.project.payUtils.wx.sdk;

import com.alipay.api.internal.util.file.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.ByteArrayInputStream;
import java.io.InputStream;

@Component
public class WxConfig extends WXPayConfig {

    private static final Logger log = LoggerFactory.getLogger(WxConfig.class);


    private byte[] certData;

    @Value("${pay.wx.app.appid}")
    private String appid;
    @Value("${pay.wx.mchid}")
    private String mchid;
    @Value("${pay.wx.key}")
    private String key;

    @PostConstruct
    public void init() {
        try {
            InputStream certStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(
                    "cert/wx/pro/apiclient_cert.p12");
            this.certData = IOUtils.toByteArray(certStream);
            certStream.close();
        }catch (Exception e){
            log.error("用户端初始化失败",e);
        }

    }

    @Override
    public String getAppID() {
        return appid;
    }

    @Override
    public String getMchID() {
        return mchid;
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public InputStream getCertStream() {
        ByteArrayInputStream certBis = new ByteArrayInputStream(this.certData);
        return certBis;
    }

    @Override
    public IWXPayDomain getWXPayDomain() {
        // TODO Auto-generated method stub 
        return new IWXPayDomain() {
            @Override
            public void report(String domain, long elapsedTimeMillis, Exception ex) {

            }

            @Override
            public DomainInfo getDomain(WXPayConfig config) {
                return new IWXPayDomain.DomainInfo(WXPayConstants.DOMAIN_API, true);
            }
        };
    }
}
