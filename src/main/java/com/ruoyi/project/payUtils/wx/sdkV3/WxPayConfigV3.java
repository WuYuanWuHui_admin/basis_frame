/*
 * ...
 */

package com.ruoyi.project.payUtils.wx.sdkV3;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * api v3 配置
 *
 * @author 挺好的 2023年02月18日 下午 16:16
 */
@Component ("wxPayConfigV3")
@ConfigurationProperties (prefix = "wx-pay-v3")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class WxPayConfigV3 {

    /**
     * 直连商户号
     */
    private String merchantId;

    /**
     * 证书路径
     */
    private String privateKeyPath;

    /**
     * 微信支付平台证书
     */
    private String wechatPayCertPath;

    /**
     * 证书序列号
     */
    private String merchantSerialNumber;

    /**
     * api v3 key
     */
    private String apiV3Key;

    /**
     * app id
     */
    private String appId;

    /**
     * 微信小程序 id
     */
    private String miniAppId;

    /**
     * 付款结果通知url
     */
    private String notifyUrl;

    /**
     * 退款通知url
     */
    private String refundNotifyUrl;


}
