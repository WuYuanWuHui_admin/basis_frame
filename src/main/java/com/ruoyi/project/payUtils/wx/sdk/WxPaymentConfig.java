package com.ruoyi.project.payUtils.wx.sdk;

import com.alipay.api.internal.util.file.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.ByteArrayInputStream;
import java.io.InputStream;

@Component
public class WxPaymentConfig extends WXPayConfig {

    private static final Logger log = LoggerFactory.getLogger(WxPaymentConfig.class);


    private byte[] certData;

    @Value("${pay.wx.zf.appid}")
    private String appid;
    @Value("${pay.wx.zf.mchid}")
    private String mchid;
    @Value("${pay.wx.zf.key}")
    private String key;

    @Value("${pay.wx.zf.xcxAppid}")
    private String xcxAppid;

    @Value ("${pay.wx.zf.secretid}")
    private String secretid;

    @Value ("${pay.wx.zf.notifyUrl}")
    public String notifyUrl;

    @Value ("${pay.wx.zf.refundNotifyUrl}")
    public String refundNotifyUrl;

    @PostConstruct
    public void init() {
        try {
            InputStream certStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(
                    "cert/wx/pro/apiclient_zf_cert.p12");
            this.certData = IOUtils.toByteArray(certStream);
            certStream.close();
        }catch (Exception e){
            log.error("用户端初始化失败",e);
        }

    }

    @Override
    public String getAppID() {
        return appid;
    }

    public String getXcxAppid() {
        return xcxAppid;
    }

    @Override
    public String getMchID() {
        return mchid;
    }

    @Override
    public String getKey() {
        return key;
    }

    public String getAppid() {
        return appid;
    }

    public String getMchid() {
        return mchid;
    }

    public String getSecretid() {
        return secretid;
    }

    public String getNotifyUrl() {
        return notifyUrl;
    }

    public String getRefundNotifyUrl() {
        return refundNotifyUrl;
    }

    @Override
    public InputStream getCertStream() {
        ByteArrayInputStream certBis = new ByteArrayInputStream(this.certData);
        return certBis;
    }

    @Override
    public IWXPayDomain getWXPayDomain() {
        // TODO Auto-generated method stub 
        return new IWXPayDomain() {
            @Override
            public void report(String domain, long elapsedTimeMillis, Exception ex) {

            }
            @Override
            public DomainInfo getDomain(WXPayConfig config) {
                return new DomainInfo(WXPayConstants.DOMAIN_API, true);
            }
        };
    }
}
