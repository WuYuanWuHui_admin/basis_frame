package com.ruoyi.project.bajiaostar.userAccount.service.impl;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.text.Convert;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.project.bajiaostar.api.entity.dto.user.ExecuteUserAccountDto;
import com.ruoyi.project.bajiaostar.userAccoundetail.domain.UserAccoundetail;
import com.ruoyi.project.bajiaostar.userAccoundetail.service.IUserAccoundetailService;
import com.ruoyi.project.bajiaostar.userAccount.domain.UserAccount;
import com.ruoyi.project.bajiaostar.userAccount.mapper.UserAccountMapper;
import com.ruoyi.project.bajiaostar.userAccount.service.IUserAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.List;

/**
 * 用户账户信息Service业务层处理
 *
 * @author 谢少辉
 * @date 2024-03-31
 */
@Service
public class UserAccountServiceImpl implements IUserAccountService {
    @Autowired
    private UserAccountMapper userAccountMapper;
    @Autowired
    IUserAccoundetailService userAccoundetailService;

    /**
     * 查询用户账户信息
     *
     * @param id 用户账户信息ID
     * @return 用户账户信息
     */
    @Override
    public UserAccount selectUserAccountById(Long id) {
        return userAccountMapper.selectUserAccountById(id);
    }

    /**
     * 查询用户账户信息列表
     *
     * @param userAccount 用户账户信息
     * @return 用户账户信息
     */
    @Override
    public List<UserAccount> selectUserAccountList(UserAccount userAccount) {
        return userAccountMapper.selectUserAccountList(userAccount);
    }

    /**
     * 新增用户账户信息
     *
     * @param userAccount 用户账户信息
     * @return 结果
     */
    @Override
    public int insertUserAccount(UserAccount userAccount) {
        userAccount.setCreateTime(DateUtils.getNowDate());
        return userAccountMapper.insertUserAccount(userAccount);
    }

    /**
     * 修改用户账户信息
     *
     * @param userAccount 用户账户信息
     * @return 结果
     */
    @Override
    public int updateUserAccount(UserAccount userAccount) {
        userAccount.setUpdateTime(DateUtils.getNowDate());
        return userAccountMapper.updateUserAccount(userAccount);
    }

    /**
     * 删除用户账户信息对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteUserAccountByIds(String ids) {
        return userAccountMapper.deleteUserAccountByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除用户账户信息信息
     *
     * @param id 用户账户信息ID
     * @return 结果
     */
    @Override
    public int deleteUserAccountById(Long id) {
        return userAccountMapper.deleteUserAccountById(id);
    }

    /**
     * 查询用户账户信息
     *
     * @param userId 用户账户信息ID
     * @return 用户账户信息
     */
    @Override
    public UserAccount selectUserAccountByUserId(Long userId) {
        return userAccountMapper.selectUserAccountByUserId(userId);
    }

    /**
     * 操作用户账户信息
     *
     * @param dto
     * @return
     */
    @Override
    public AjaxResult executeUserAccount(ExecuteUserAccountDto dto) {
        //获取账户信息
        UserAccount userAccount = selectUserAccountByUserId(dto.getUserId());
        if (ObjectUtils.isEmpty(userAccount)) {
            throw new RuntimeException("账户不存在");
        }
        UserAccoundetail detail = new UserAccoundetail();
        detail.setUserId(dto.getUserId());
        detail.setUserIdentity(dto.getUserType());
        detail.setOperationAccount(dto.getUserAccount());
        detail.setOperationType(dto.getAccountType());
        detail.setAccountType(dto.getAccountSysType());
        detail.setOperationExplain(dto.getAccountName());
        //增加余额
        if (dto.getAccountType().equals(0)) {
            //用户
            if (dto.getUserType().equals(0)) {
                if (dto.getAccountSysType().equals(0)) {
                    userAccount.setUserAccount(userAccount.getUserAccount() + dto.getUserAccount());
                    detail.setOperationAfterAccount(userAccount.getUserAccount());
                } else {
                    userAccount.setUserCashAccount(userAccount.getUserCashAccount() + dto.getUserAccount());
                    detail.setOperationAfterAccount(userAccount.getUserCashAccount());
                }
            } else {
                //商户
                if (dto.getAccountSysType().equals(0)) {
                    userAccount.setMerchantAccount(userAccount.getMerchantAccount() + dto.getUserAccount());
                    detail.setOperationAfterAccount(userAccount.getMerchantAccount());
                } else {
                    userAccount.setMerchantCashAccount(userAccount.getMerchantCashAccount() + dto.getUserAccount());
                    detail.setOperationAfterAccount(userAccount.getMerchantCashAccount());
                }
            }
        } else {//扣减余额

            if (dto.getUserType().equals(0)) {
                if (dto.getAccountSysType().equals(0)) {
                    if (userAccount.getUserAccount() < dto.getUserAccount()) {
                        throw new RuntimeException("账户余额不足");
                    }
                    userAccount.setUserAccount(userAccount.getUserAccount() - dto.getUserAccount());
                    detail.setOperationAfterAccount(userAccount.getUserAccount());
                } else {
                    if (userAccount.getUserCashAccount() < dto.getUserAccount()) {
                        throw new RuntimeException("账户余额不足");
                    }
                    userAccount.setUserCashAccount(userAccount.getUserCashAccount() - dto.getUserAccount());
                    detail.setOperationAfterAccount(userAccount.getUserCashAccount());
                }
            } else {
                if (dto.getAccountSysType().equals(0)) {
                    if (userAccount.getMerchantAccount() < dto.getUserAccount()) {
                        throw new RuntimeException("账户余额不足");
                    }
                    userAccount.setMerchantAccount(userAccount.getMerchantAccount() + dto.getUserAccount());
                    detail.setOperationAfterAccount(userAccount.getMerchantAccount());
                } else {
                    if (userAccount.getMerchantCashAccount() < dto.getUserAccount()) {
                        throw new RuntimeException("账户余额不足");
                    }
                    userAccount.setMerchantCashAccount(userAccount.getMerchantCashAccount() - dto.getUserAccount());
                    detail.setOperationAfterAccount(userAccount.getMerchantCashAccount());
                }
            }
        }
        this.updateUserAccount(userAccount);
        userAccoundetailService.insertUserAccoundetail(detail);
        return AjaxResult.success();
    }
}
