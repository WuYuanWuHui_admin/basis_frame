package com.ruoyi.project.bajiaostar.userAccount.service;

import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.project.bajiaostar.api.entity.dto.user.ExecuteUserAccountDto;
import com.ruoyi.project.bajiaostar.userAccount.domain.UserAccount;
import java.util.List;

/**
 * 用户账户信息Service接口
 * 
 * @author 谢少辉
 * @date 2024-03-31
 */
public interface IUserAccountService 
{
    /**
     * 查询用户账户信息
     * 
     * @param id 用户账户信息ID
     * @return 用户账户信息
     */
    public UserAccount selectUserAccountById(Long id);

    /**
     * 查询用户账户信息列表
     * 
     * @param userAccount 用户账户信息
     * @return 用户账户信息集合
     */
    public List<UserAccount> selectUserAccountList(UserAccount userAccount);

    /**
     * 新增用户账户信息
     * 
     * @param userAccount 用户账户信息
     * @return 结果
     */
    public int insertUserAccount(UserAccount userAccount);

    /**
     * 修改用户账户信息
     * 
     * @param userAccount 用户账户信息
     * @return 结果
     */
    public int updateUserAccount(UserAccount userAccount);

    /**
     * 批量删除用户账户信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteUserAccountByIds(String ids);

    /**
     * 删除用户账户信息信息
     * 
     * @param id 用户账户信息ID
     * @return 结果
     */
    public int deleteUserAccountById(Long id);

    /**
     * 查询用户账户信息
     *
     * @param userId 用户账户信息ID
     * @return 用户账户信息
     */
    public UserAccount selectUserAccountByUserId(Long userId);

    /**
     * 操作用户账户信息
     * @return
     */
    AjaxResult executeUserAccount(ExecuteUserAccountDto dto);
}
