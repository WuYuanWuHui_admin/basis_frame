package com.ruoyi.project.bajiaostar.userAccount.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.bajiaostar.userAccount.domain.UserAccount;
import com.ruoyi.project.bajiaostar.userAccount.service.IUserAccountService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 用户账户信息Controller
 * 
 * @author 谢少辉
 * @date 2024-03-31
 */
@Controller
@RequestMapping("/bajiaostar/userAccount")
public class UserAccountController extends BaseController
{
    private String prefix = "bajiaostar/userAccount";

    @Autowired
    private IUserAccountService userAccountService;

    @RequiresPermissions("bajiaostar:userAccount:view")
    @GetMapping()
    public String userAccount()
    {
        return prefix + "/userAccount";
    }

    /**
     * 查询用户账户信息列表
     */
    @RequiresPermissions("bajiaostar:userAccount:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(UserAccount userAccount)
    {
        startPage();
        List<UserAccount> list = userAccountService.selectUserAccountList(userAccount);
        return getDataTable(list);
    }

    /**
     * 导出用户账户信息列表
     */
    @RequiresPermissions("bajiaostar:userAccount:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(UserAccount userAccount)
    {
        List<UserAccount> list = userAccountService.selectUserAccountList(userAccount);
        ExcelUtil<UserAccount> util = new ExcelUtil<UserAccount>(UserAccount.class);
        return util.exportExcel(list, "userAccount");
    }

    /**
     * 新增用户账户信息
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存用户账户信息
     */
    @RequiresPermissions("bajiaostar:userAccount:add")
    @Log(title = "用户账户信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(UserAccount userAccount)
    {
        return toAjax(userAccountService.insertUserAccount(userAccount));
    }

    /**
     * 修改用户账户信息
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        UserAccount userAccount = userAccountService.selectUserAccountById(id);
        mmap.put("userAccount", userAccount);
        return prefix + "/edit";
    }

    /**
     * 修改保存用户账户信息
     */
    @RequiresPermissions("bajiaostar:userAccount:edit")
    @Log(title = "用户账户信息", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(UserAccount userAccount)
    {
        return toAjax(userAccountService.updateUserAccount(userAccount));
    }

    /**
     * 删除用户账户信息
     */
    @RequiresPermissions("bajiaostar:userAccount:remove")
    @Log(title = "用户账户信息", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(userAccountService.deleteUserAccountByIds(ids));
    }
}
