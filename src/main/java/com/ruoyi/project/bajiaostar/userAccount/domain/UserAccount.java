package com.ruoyi.project.bajiaostar.userAccount.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 用户账户信息对象 t_user_account
 * 
 * @author 谢少辉
 * @date 2024-03-31
 */
public class UserAccount extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 用户账户id */
    private Long id;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 商户爱豆余额 */
    @Excel(name = "商户爱豆余额")
    private Double merchantAccount;

    /** 商户现金余额 */
    @Excel(name = "商户现金余额")
    private Double merchantCashAccount;

    /** 用户爱豆余额 */
    @Excel(name = "用户爱豆余额")
    private Double userAccount;

    /** 用户爱豆余额 */
    @Excel(name = "用户账户余额")
    private Double userCashAccount;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setMerchantAccount(Double merchantAccount) 
    {
        this.merchantAccount = merchantAccount;
    }

    public Double getMerchantAccount() 
    {
        return merchantAccount;
    }
    public void setMerchantCashAccount(Double merchantCashAccount) 
    {
        this.merchantCashAccount = merchantCashAccount;
    }

    public Double getMerchantCashAccount() 
    {
        return merchantCashAccount;
    }
    public void setUserAccount(Double userAccount) 
    {
        this.userAccount = userAccount;
    }

    public Double getUserAccount() 
    {
        return userAccount;
    }

    public Double getUserCashAccount() {
        return userCashAccount;
    }

    public void setUserCashAccount(Double userCashAccount) {
        this.userCashAccount = userCashAccount;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("merchantAccount", getMerchantAccount())
            .append("merchantCashAccount", getMerchantCashAccount())
            .append("userAccount", getUserAccount())
            .append("createTime", getCreateTime())
            .toString();
    }
}
