package com.ruoyi.project.bajiaostar.userExcitationTask.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;
import java.util.Date;

/**
 * 用户/商家返还任务对象 t_user_excitation_task
 * 
 * @author 谢少辉
 * @date 2024-03-31
 */
public class UserExcitationTask extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 任务表id */
    private Long id;

    /** 任务状态 0 执行中 1 执行结束  2 暂停执行 */
    @Excel(name = "任务状态 0 执行中 1 执行结束  2 暂停执行")
    private Integer taskStatus;

    /** 执行数据 */
    @Excel(name = "执行数据")
    private Double taskAccount;

    /** 执行结束时间 */
    @Excel(name = "执行结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date taskEndTime;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 用户身份 0 用户  1 商家 */
    @Excel(name = "用户身份 0 用户  1 商家")
    private Integer userIdentity;

    /** 任务名称 */
    @Excel(name = "任务名称")
    private String taskName;

    /** 任务类型: 0 激励任务   */
    @Excel(name = "任务类型: 0 激励任务  ")
    private Integer taskType;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setTaskStatus(Integer taskStatus) 
    {
        this.taskStatus = taskStatus;
    }

    public Integer getTaskStatus() 
    {
        return taskStatus;
    }
    public void setTaskAccount(Double taskAccount) 
    {
        this.taskAccount = taskAccount;
    }

    public Double getTaskAccount() 
    {
        return taskAccount;
    }
    public void setTaskEndTime(Date taskEndTime) 
    {
        this.taskEndTime = taskEndTime;
    }

    public Date getTaskEndTime() 
    {
        return taskEndTime;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setUserIdentity(Integer userIdentity) 
    {
        this.userIdentity = userIdentity;
    }

    public Integer getUserIdentity() 
    {
        return userIdentity;
    }
    public void setTaskName(String taskName) 
    {
        this.taskName = taskName;
    }

    public String getTaskName() 
    {
        return taskName;
    }
    public void setTaskType(Integer taskType) 
    {
        this.taskType = taskType;
    }

    public Integer getTaskType() 
    {
        return taskType;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("taskStatus", getTaskStatus())
            .append("taskAccount", getTaskAccount())
            .append("taskEndTime", getTaskEndTime())
            .append("createTime", getCreateTime())
            .append("userId", getUserId())
            .append("userIdentity", getUserIdentity())
            .append("taskName", getTaskName())
            .append("taskType", getTaskType())
            .toString();
    }
}
