package com.ruoyi.project.bajiaostar.userExcitationTask.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.bajiaostar.userExcitationTask.domain.UserExcitationTask;
import com.ruoyi.project.bajiaostar.userExcitationTask.service.IUserExcitationTaskService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 用户/商家返还任务Controller
 * 
 * @author 谢少辉
 * @date 2024-03-31
 */
@Controller
@RequestMapping("/bajiaostar/userExcitationTask")
public class UserExcitationTaskController extends BaseController
{
    private String prefix = "bajiaostar/userExcitationTask";

    @Autowired
    private IUserExcitationTaskService userExcitationTaskService;

    @RequiresPermissions("bajiaostar:userExcitationTask:view")
    @GetMapping()
    public String userExcitationTask()
    {
        return prefix + "/userExcitationTask";
    }

    /**
     * 查询用户/商家返还任务列表
     */
    @RequiresPermissions("bajiaostar:userExcitationTask:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(UserExcitationTask userExcitationTask)
    {
        startPage();
        List<UserExcitationTask> list = userExcitationTaskService.selectUserExcitationTaskList(userExcitationTask);
        return getDataTable(list);
    }

    /**
     * 导出用户/商家返还任务列表
     */
    @RequiresPermissions("bajiaostar:userExcitationTask:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(UserExcitationTask userExcitationTask)
    {
        List<UserExcitationTask> list = userExcitationTaskService.selectUserExcitationTaskList(userExcitationTask);
        ExcelUtil<UserExcitationTask> util = new ExcelUtil<UserExcitationTask>(UserExcitationTask.class);
        return util.exportExcel(list, "userExcitationTask");
    }

    /**
     * 新增用户/商家返还任务
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存用户/商家返还任务
     */
    @RequiresPermissions("bajiaostar:userExcitationTask:add")
    @Log(title = "用户/商家返还任务", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(UserExcitationTask userExcitationTask)
    {
        return toAjax(userExcitationTaskService.insertUserExcitationTask(userExcitationTask));
    }

    /**
     * 修改用户/商家返还任务
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        UserExcitationTask userExcitationTask = userExcitationTaskService.selectUserExcitationTaskById(id);
        mmap.put("userExcitationTask", userExcitationTask);
        return prefix + "/edit";
    }

    /**
     * 修改保存用户/商家返还任务
     */
    @RequiresPermissions("bajiaostar:userExcitationTask:edit")
    @Log(title = "用户/商家返还任务", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(UserExcitationTask userExcitationTask)
    {
        return toAjax(userExcitationTaskService.updateUserExcitationTask(userExcitationTask));
    }

    /**
     * 删除用户/商家返还任务
     */
    @RequiresPermissions("bajiaostar:userExcitationTask:remove")
    @Log(title = "用户/商家返还任务", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(userExcitationTaskService.deleteUserExcitationTaskByIds(ids));
    }
}
