package com.ruoyi.project.bajiaostar.userExcitationTask.service.impl;

import java.util.Date;
import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.project.bajiaostar.api.entity.dto.user.ExecuteUserAccountDto;
import com.ruoyi.project.bajiaostar.userAccount.service.IUserAccountService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.bajiaostar.userExcitationTask.mapper.UserExcitationTaskMapper;
import com.ruoyi.project.bajiaostar.userExcitationTask.domain.UserExcitationTask;
import com.ruoyi.project.bajiaostar.userExcitationTask.service.IUserExcitationTaskService;
import com.ruoyi.common.utils.text.Convert;
import org.springframework.util.ObjectUtils;

/**
 * 用户/商家返还任务Service业务层处理
 * 
 * @author 谢少辉
 * @date 2024-03-31
 */
@Service
@Slf4j
public class UserExcitationTaskServiceImpl implements IUserExcitationTaskService 
{
    @Autowired
    private UserExcitationTaskMapper userExcitationTaskMapper;
    @Autowired
    IUserAccountService userAccountService;

    /**
     * 查询用户/商家返还任务
     * 
     * @param id 用户/商家返还任务ID
     * @return 用户/商家返还任务
     */
    @Override
    public UserExcitationTask selectUserExcitationTaskById(Long id)
    {
        return userExcitationTaskMapper.selectUserExcitationTaskById(id);
    }

    /**
     * 查询用户/商家返还任务列表
     * 
     * @param userExcitationTask 用户/商家返还任务
     * @return 用户/商家返还任务
     */
    @Override
    public List<UserExcitationTask> selectUserExcitationTaskList(UserExcitationTask userExcitationTask)
    {
        return userExcitationTaskMapper.selectUserExcitationTaskList(userExcitationTask);
    }

    /**
     * 新增用户/商家返还任务
     * 
     * @param userExcitationTask 用户/商家返还任务
     * @return 结果
     */
    @Override
    public int insertUserExcitationTask(UserExcitationTask userExcitationTask)
    {
        userExcitationTask.setCreateTime(DateUtils.getNowDate());
        return userExcitationTaskMapper.insertUserExcitationTask(userExcitationTask);
    }

    /**
     * 修改用户/商家返还任务
     * 
     * @param userExcitationTask 用户/商家返还任务
     * @return 结果
     */
    @Override
    public int updateUserExcitationTask(UserExcitationTask userExcitationTask)
    {
        userExcitationTask.setUpdateTime(DateUtils.getNowDate());
        return userExcitationTaskMapper.updateUserExcitationTask(userExcitationTask);
    }

    /**
     * 删除用户/商家返还任务对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteUserExcitationTaskByIds(String ids)
    {
        return userExcitationTaskMapper.deleteUserExcitationTaskByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除用户/商家返还任务信息
     * 
     * @param id 用户/商家返还任务ID
     * @return 结果
     */
    @Override
    public int deleteUserExcitationTaskById(Long id)
    {
        return userExcitationTaskMapper.deleteUserExcitationTaskById(id);
    }

    /**
     * 执行当天激励任务
     *
     * @return
     */
    @Override
    public AjaxResult executeUserExcitationTask() {
        //查询当天可执行数据
        List<UserExcitationTask> list = userExcitationTaskMapper.executeUserExcitationTask();
        if(ObjectUtils.isEmpty(list)){
            return  AjaxResult.error("暂无可执行数据");
        }
        log.info("执行激励任务奖励开始");
        list.forEach(task->{
            //账户信息处理
            ExecuteUserAccountDto dto=new ExecuteUserAccountDto();
            dto.setAccountName("激励任务");
            dto.setUserId(task.getUserId());
            dto.setUserAccount(task.getTaskAccount());
            dto.setUserType(task.getUserIdentity());
            dto.setAccountType(0);
            dto.setAccountSysType(0);
            userAccountService.executeUserAccount(dto);
            //时间如果是今日，则更新任务状态
            if(DateUtils.isToday(new Date(),task.getTaskEndTime())){
                task.setTaskStatus(1);
                userExcitationTaskMapper.updateUserExcitationTask(task);
            }
        });
        log.info("执行激励任务奖励结束");
        return AjaxResult.success();
    }
}
