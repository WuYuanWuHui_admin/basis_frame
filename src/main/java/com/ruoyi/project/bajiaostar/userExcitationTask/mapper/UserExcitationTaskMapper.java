package com.ruoyi.project.bajiaostar.userExcitationTask.mapper;

import com.ruoyi.project.bajiaostar.userExcitationTask.domain.UserExcitationTask;
import java.util.List;

/**
 * 用户/商家返还任务Mapper接口
 * 
 * @author 谢少辉
 * @date 2024-03-31
 */
public interface UserExcitationTaskMapper 
{
    /**
     * 查询用户/商家返还任务
     * 
     * @param id 用户/商家返还任务ID
     * @return 用户/商家返还任务
     */
    public UserExcitationTask selectUserExcitationTaskById(Long id);

    /**
     * 查询用户/商家返还任务列表
     * 
     * @param userExcitationTask 用户/商家返还任务
     * @return 用户/商家返还任务集合
     */
    public List<UserExcitationTask> selectUserExcitationTaskList(UserExcitationTask userExcitationTask);

    /**
     * 新增用户/商家返还任务
     * 
     * @param userExcitationTask 用户/商家返还任务
     * @return 结果
     */
    public int insertUserExcitationTask(UserExcitationTask userExcitationTask);

    /**
     * 修改用户/商家返还任务
     * 
     * @param userExcitationTask 用户/商家返还任务
     * @return 结果
     */
    public int updateUserExcitationTask(UserExcitationTask userExcitationTask);

    /**
     * 删除用户/商家返还任务
     * 
     * @param id 用户/商家返还任务ID
     * @return 结果
     */
    public int deleteUserExcitationTaskById(Long id);

    /**
     * 批量删除用户/商家返还任务
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteUserExcitationTaskByIds(String[] ids);

    /**
     * 获取当天可自行数据
     * @return
     */
   List<UserExcitationTask> executeUserExcitationTask();
}
