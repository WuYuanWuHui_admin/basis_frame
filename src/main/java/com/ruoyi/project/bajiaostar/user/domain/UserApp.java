package com.ruoyi.project.bajiaostar.user.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;
import java.util.Date;

/**
 * 用户信息对象 t_user
 * 
 * @author 谢少辉
 * @date 2024-03-31
 */
public class UserApp extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 用户id */
    private Long id;

    /** 用户名称 */
    @Excel(name = "用户名称")
    private String userName;

    /** 用户密码 */
    @Excel(name = "用户密码")
    private String userPassword;

    /** 手机号码 */
    @Excel(name = "手机号码")
    private String moble;

    /** 创建ip */
    @Excel(name = "创建ip")
    private String createIp;

    /** 推荐人id */
    @Excel(name = "推荐人id")
    private Long pushUserId;

    /** 上级用户集合 */
    @Excel(name = "上级用户集合")
    private String pushUserIds;

    /** 推广码 */
    @Excel(name = "推广码")
    private String pushCode;

    /** 用户状态 0:正常  1 锁定  2 拉黑  3 删除 */
    @Excel(name = "用户状态 0:正常  1 锁定  2 拉黑  3 删除")
    private Integer userStatus;

    /** 用户身份 0:用户  1 商家   2 商家资料待审核 */
    @Excel(name = "用户身份 0:用户  1 商家   2 商家资料待审核")
    private Integer userIdentity;

    /** 用户身份：
0： 普通
1：  1星
2 ： 2星
3 ：3星
4 ： 4星
5： 5星
6  ：6星
7 ： 7 星
8： 8星 
9 ：9星
10 ：10星 */
    @Excel(name = "用户身份：0： 普通 1：  1星 2 ： 2星 3 ：3星 4 ： 4星 5： 5星 6  ：6星 7 ： 7星 8： 8星 9 ：9星 10 ：10星")
    private Integer userIdentityLevel;

    /** 商家身份：
0： 普通商家
1：  v1商家
2 ： v2商家
3 ：v3商家
4 ： v4商家
5： v5商家
6  ：v6商家
7 ： v7 商家
8： v8商家 
9 ：v9商家
10 ：v10商家 */
    @Excel(name = "商家身份：0： 普通商家1：  v1商家2 ： v2商家3 ：v3商家4 ： v4商家5： v5商家6  ：v6商家7 ： v7 商家8： v8商家 9 ：v9商家10 ：v10商家")
    private Integer userIdentityLevel1;

    /** 是否购买年卡 0 未购买 1 已购买 */
    @Excel(name = "是否购买年卡 0 未购买 1 已购买")
    private Integer cardFlag;

    /** 会员卡到期时间 */
    @Excel(name = "会员卡到期时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date cardEndDate;

    /** 真实姓名 */
    @Excel(name = "真实姓名")
    private String reanlName;

    /** 身份证号码 */
    @Excel(name = "身份证号码")
    private String reanlIdnum;

    /** 商家身份： 0 个人  1 个体店铺 2 企业店铺 */
    @Excel(name = "商家身份： 0 个人  1 个体店铺 2 企业店铺")
    private Integer storeIdentity;

    /** 营业执照地址 */
    @Excel(name = "营业执照地址")
    private String businessLicenseUrl;

    /** 门头照 */
    @Excel(name = "门头照")
    private String storefrontPhotoUrl;
    @Excel(name = "店铺名称")
    private String storeName;
    //销量
    private Long salesVolume;
    //粉丝
    private Long fansCount;
    //佣金
    private Long commissionAmount;
    /**
     * 是否渠道推广 0 否  1 是
     */
    private Integer channelFlag;
    /**
     * 支付密码
     */
    private String payPassWord;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUserName(String userName) 
    {
        this.userName = userName;
    }

    public String getUserName() 
    {
        return userName;
    }
    public void setUserPassword(String userPassword) 
    {
        this.userPassword = userPassword;
    }

    public String getUserPassword() 
    {
        return userPassword;
    }
    public void setMoble(String moble) 
    {
        this.moble = moble;
    }

    public String getMoble() 
    {
        return moble;
    }
    public void setCreateIp(String createIp) 
    {
        this.createIp = createIp;
    }

    public String getCreateIp() 
    {
        return createIp;
    }
    public void setPushUserId(Long pushUserId) 
    {
        this.pushUserId = pushUserId;
    }

    public Long getPushUserId() 
    {
        return pushUserId;
    }
    public void setPushUserIds(String pushUserIds) 
    {
        this.pushUserIds = pushUserIds;
    }

    public String getPushUserIds() 
    {
        return pushUserIds;
    }
    public void setPushCode(String pushCode) 
    {
        this.pushCode = pushCode;
    }

    public String getPushCode() 
    {
        return pushCode;
    }
    public void setUserStatus(Integer userStatus) 
    {
        this.userStatus = userStatus;
    }

    public Integer getUserStatus() 
    {
        return userStatus;
    }
    public void setUserIdentity(Integer userIdentity) 
    {
        this.userIdentity = userIdentity;
    }

    public Integer getUserIdentity() 
    {
        return userIdentity;
    }
    public void setUserIdentityLevel(Integer userIdentityLevel) 
    {
        this.userIdentityLevel = userIdentityLevel;
    }

    public Integer getUserIdentityLevel() 
    {
        return userIdentityLevel;
    }
    public void setUserIdentityLevel1(Integer userIdentityLevel1) 
    {
        this.userIdentityLevel1 = userIdentityLevel1;
    }

    public Integer getUserIdentityLevel1() 
    {
        return userIdentityLevel1;
    }
    public void setCardFlag(Integer cardFlag) 
    {
        this.cardFlag = cardFlag;
    }

    public Integer getCardFlag() 
    {
        return cardFlag;
    }
    public void setCardEndDate(Date cardEndDate) 
    {
        this.cardEndDate = cardEndDate;
    }

    public Date getCardEndDate() 
    {
        return cardEndDate;
    }
    public void setReanlName(String reanlName) 
    {
        this.reanlName = reanlName;
    }

    public String getReanlName() 
    {
        return reanlName;
    }
    public void setReanlIdnum(String reanlIdnum) 
    {
        this.reanlIdnum = reanlIdnum;
    }

    public String getReanlIdnum() 
    {
        return reanlIdnum;
    }
    public void setStoreIdentity(Integer storeIdentity) 
    {
        this.storeIdentity = storeIdentity;
    }

    public Integer getStoreIdentity() 
    {
        return storeIdentity;
    }
    public void setBusinessLicenseUrl(String businessLicenseUrl) 
    {
        this.businessLicenseUrl = businessLicenseUrl;
    }

    public String getBusinessLicenseUrl() 
    {
        return businessLicenseUrl;
    }
    public void setStorefrontPhotoUrl(String storefrontPhotoUrl) 
    {
        this.storefrontPhotoUrl = storefrontPhotoUrl;
    }

    public String getStorefrontPhotoUrl() 
    {
        return storefrontPhotoUrl;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public Long getSalesVolume() {
        return salesVolume;
    }

    public void setSalesVolume(Long salesVolume) {
        this.salesVolume = salesVolume;
    }

    public Long getFansCount() {
        return fansCount;
    }

    public void setFansCount(Long fansCount) {
        this.fansCount = fansCount;
    }

    public Long getCommissionAmount() {
        return commissionAmount;
    }

    public void setCommissionAmount(Long commissionAmount) {
        this.commissionAmount = commissionAmount;
    }

    public Integer getChannelFlag() {
        return channelFlag;
    }

    public void setChannelFlag(Integer channelFlag) {
        this.channelFlag = channelFlag;
    }

    public String getPayPassWord() {
        return payPassWord;
    }

    public void setPayPassWord(String payPassWord) {
        this.payPassWord = payPassWord;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userName", getUserName())
            .append("userPassword", getUserPassword())
            .append("moble", getMoble())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("createIp", getCreateIp())
            .append("pushUserId", getPushUserId())
            .append("pushUserIds", getPushUserIds())
            .append("pushCode", getPushCode())
            .append("userStatus", getUserStatus())
            .append("userIdentity", getUserIdentity())
            .append("userIdentityLevel", getUserIdentityLevel())
            .append("userIdentityLevel1", getUserIdentityLevel1())
            .append("cardFlag", getCardFlag())
            .append("cardEndDate", getCardEndDate())
            .append("reanlName", getReanlName())
            .append("reanlIdnum", getReanlIdnum())
            .append("storeIdentity", getStoreIdentity())
            .append("businessLicenseUrl", getBusinessLicenseUrl())
            .append("storefrontPhotoUrl", getStorefrontPhotoUrl())
            .toString();
    }
}
