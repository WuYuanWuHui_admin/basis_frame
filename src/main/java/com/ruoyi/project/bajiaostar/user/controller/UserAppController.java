package com.ruoyi.project.bajiaostar.user.controller;

import java.util.List;

import com.ruoyi.project.bajiaostar.user.domain.UserApp;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.bajiaostar.user.service.IUserAppService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 用户信息Controller
 * 
 * @author 谢少辉
 * @date 2024-03-31
 */
@Controller
@RequestMapping("/bajiaostar/user")
public class UserAppController extends BaseController
{
    private String prefix = "bajiaostar/user";

    @Autowired
    private IUserAppService userService;

    @RequiresPermissions("bajiaostar:userApp:view")
    @GetMapping()
    public String user()
    {
        return prefix + "/user";
    }

    /**
     * 查询用户信息列表
     */
    @RequiresPermissions("bajiaostar:user:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(UserApp userApp)
    {
        startPage();
        List<UserApp> list = userService.selectUserList(userApp);
        return getDataTable(list);
    }

    /**
     * 导出用户信息列表
     */
    @RequiresPermissions("bajiaostar:user:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(UserApp userApp)
    {
        List<UserApp> list = userService.selectUserList(userApp);
        ExcelUtil<UserApp> util = new ExcelUtil<UserApp>(UserApp.class);
        return util.exportExcel(list, "userApp");
    }

    /**
     * 新增用户信息
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存用户信息
     */
    @RequiresPermissions("bajiaostar:user:add")
    @Log(title = "用户信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(UserApp userApp)
    {
        return toAjax(userService.insertUser(userApp));
    }

    /**
     * 修改用户信息
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        UserApp userApp = userService.selectUserById(id);
        mmap.put("userApp", userApp);
        return prefix + "/edit";
    }

    /**
     * 修改保存用户信息
     */
    @RequiresPermissions("bajiaostar:user:edit")
    @Log(title = "用户信息", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(UserApp userApp)
    {
        return toAjax(userService.updateUser(userApp));
    }

    /**
     * 删除用户信息
     */
    @RequiresPermissions("bajiaostar:user:remove")
    @Log(title = "用户信息", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(userService.deleteUserByIds(ids));
    }
}
