package com.ruoyi.project.bajiaostar.user.service.impl;

import com.github.pagehelper.PageHelper;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.project.bajiaostar.user.domain.UserApp;
import com.ruoyi.project.bajiaostar.user.service.ITeamService;
import com.ruoyi.project.bajiaostar.user.service.IUserAppService;
import com.ruoyi.project.utils.RedisUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.RejectedExecutionException;
import java.util.stream.Collectors;

/**
 * @author luoyonghui
 * @date 2024/4/25
 * @description:
 */
@Service
@Slf4j
public class TeamServiceImpl implements ITeamService {
    @Autowired
    private IUserAppService userAppService;
    @Autowired
    private RedisUtils redisUtils;
    @Autowired
    private ApplicationContext applicationContext;

    @Override
    public List<UserApp> getAllParentList(Long userId) {
        List<UserApp> resultList = new ArrayList<>();
        UserApp user = userId == null ? null : userAppService.selectUserById(userId);
        if(user != null) {
            List<String> parentIdList = new ArrayList<>();
            UserApp parent = user.getPushUserId() == null ? null : userAppService.selectUserById(user.getPushUserId());
            while(parent != null && !parentIdList.contains(parent.getId().toString())) {
                parentIdList.add(parent.getId().toString());
                resultList.add(0, parent);
                parent = parent.getPushUserId() == null ? null : userAppService.selectUserById(parent.getPushUserId());
            }
        }
        return resultList;
    }

    @Override
    public Integer getDirectMemberCount(Long userId, boolean isActive) {
        return isActive ? userAppService.findUserMerchantDirectPush(userId) : userAppService.findUserDirectPush(userId);
    }

    @Override
    public Integer getTeamMemberCount(Long userId, boolean isActive) {
        return this.getTeamMemberCount(userAppService.selectUserById(userId), isActive);
    }

    @Override
    public Integer getTeamMemberCount(UserApp user, boolean isActive) {
        if(user == null) {
            return null;
        }
        String sumCacheKeyPrefix = "xiyi:cache:team:sum:";
        if(isActive) {
            String cacheKey = sumCacheKeyPrefix + "count:" + user.getId();
            //活跃用户
            if(!redisUtils.hasKey(cacheKey)) {
                this.refreshTeamSumCache(user);
            }
            String cacheValue = redisUtils.get(cacheKey);
            return StringUtils.isNumeric(cacheValue) ? Integer.parseInt(cacheValue) : 0;
        } else {
            String cacheKey = sumCacheKeyPrefix + "activeCount:" + user.getId();
            //活跃用户
            if(!redisUtils.hasKey(cacheKey)) {
                this.refreshTeamSumCache(user);
            }
            String cacheValue = redisUtils.get(cacheKey);
            return StringUtils.isNumeric(cacheValue) ? Integer.parseInt(cacheValue) : 0;
        }
    }

    @Override
    @Async("myTaskExecutor")
    public void refreshAllTeamInfo() {
        ITeamService that = applicationContext.getBean(ITeamService.class);
        Integer userCount = userAppService.selectUserCount();
        Integer pageSize = 1000;
        Integer totalPage = userCount == null ? 0 : Double.valueOf(Math.ceil(userCount*1.0/pageSize)).intValue();
        UserApp userParam = new UserApp();
        for(int pageIndex = 1;pageIndex <= totalPage;pageIndex++) {
            PageHelper.startPage(pageIndex, pageSize);
            List<UserApp> userList = userAppService.selectUserList(userParam);
            if(userList != null) {
                userList.forEach(userItem -> {
                    try {
                        that.refreshTeamInfo(userItem);
                    } catch (RejectedExecutionException e) {
                        //任务池满了，当前线程执行
                        this.refreshTeamInfo(userItem);
                    }
                });
            }
        }
    }

    @Override
    @Async("myTaskExecutor")
    public void refreshTeamInfo(Long userId) {
        this.refreshTeamInfo(userAppService.selectUserById(userId));
    }

    @Override
    @Async("myTaskExecutor")
    public void refreshTeamInfo(UserApp user) {
        if(user != null) {
            log.info("刷新用户团队信息，userId = " + user.getId());
            String teamUserCacheKeyPrefix = "xiyi:cache:team:user:";
            String teamActiveUserCacheKeyPrefix = "xiyi:cache:team:activeUser:";
            String userTeamCacheKeyPrefix = "xiyi:cache:user:team:";
            List<UserApp> allParentList = this.getAllParentList(user.getId());
            if(allParentList != null && !allParentList.isEmpty()) {
                List<String> allParentIdList = allParentList.stream().map(allParentItem -> allParentItem.getId().toString()).collect(Collectors.toList());
                redisUtils.set(teamUserCacheKeyPrefix + String.join("-", allParentIdList) + "-" + user.getId(), user.getId());
                if(StringUtils.isNotEmpty(user.getReanlIdnum())) {
                    //活跃用户
                    redisUtils.set(teamActiveUserCacheKeyPrefix + String.join("-", allParentIdList) + "-" + user.getId(), user.getId());
                }
                redisUtils.set(userTeamCacheKeyPrefix + user.getId().toString(), String.join("-", allParentIdList));
            } else {
                redisUtils.set(teamUserCacheKeyPrefix + "-" + user.getId(), user.getId());
                if(StringUtils.isNotEmpty(user.getReanlIdnum())) {
                    //活跃用户
                    redisUtils.set(teamActiveUserCacheKeyPrefix + "-" + user.getId(), user.getId());
                }
                redisUtils.set(userTeamCacheKeyPrefix + user.getId().toString(), "");
            }
        }
    }

    @Override
    @Async("myTaskExecutor")
    public void initTeamSumCache() {
        ITeamService that = applicationContext.getBean(ITeamService.class);
        Integer userCount = userAppService.selectUserCount();
        Integer pageSize = 1000;
        Integer totalPage = userCount == null ? 0 : Double.valueOf(Math.ceil(userCount*1.0/pageSize)).intValue();
        UserApp userParam = new UserApp();
        for(int pageIndex = 1;pageIndex <= totalPage;pageIndex++) {
            PageHelper.startPage(pageIndex, pageSize);
            List<UserApp> userList = userAppService.selectUserList(userParam);
            if(userList != null) {
                userList.forEach(userItem -> {
                    try {
                        that.refreshTeamSumCache(userItem);
                    } catch (RejectedExecutionException e) {
                        //任务池满了，当前线程执行
                        this.refreshTeamSumCache(userItem);
                    }
                });
            }
        }
    }

    @Override
    @Async("myTaskExecutor")
    public void refreshTeamSumCache(Long userId) {
        UserApp user = userId == null ? null : userAppService.selectUserById(userId);
        this.refreshTeamSumCache(user);
    }

    @Override
    @Async("myTaskExecutor")
    public void refreshTeamSumCache(UserApp user) {
        if(user == null) {
            return;
        }
        log.info("刷新用户团队统计信息，userId = " + user.getId());
        String teamUserCacheKeyPrefix = "xiyi:cache:team:user:";
        String teamActiveUserCacheKeyPrefix = "xiyi:cache:team:activeUser:";
        String userTeamCacheKeyPrefix = "xiyi:cache:user:team:";
        String sumCacheKeyPrefix = "xiyi:cache:team:sum:";
        String userTeamKey = redisUtils.get(userTeamCacheKeyPrefix + user.getId().toString());

        String teamPattern = teamUserCacheKeyPrefix + (StringUtils.isEmpty(userTeamKey) ? user.getId().toString() + "-*" : (userTeamKey + "-" + user.getId().toString() + "-*"));
        Set<String> teamMemberKeySet = redisUtils.keys(teamPattern);
        redisUtils.set(sumCacheKeyPrefix + "count:" + user.getId(), teamMemberKeySet == null ? 0 : teamMemberKeySet.size());

        String teamActivePattern = teamActiveUserCacheKeyPrefix + (StringUtils.isEmpty(userTeamKey) ? user.getId().toString() + "-*" : (userTeamKey + "-" + user.getId().toString() + "-*"));
        Set<String> teamActiveMemberKeySet = redisUtils.keys(teamActivePattern);
        redisUtils.set(sumCacheKeyPrefix + "activeCount:" + user.getId(), teamActiveMemberKeySet == null ? 0 : teamActiveMemberKeySet.size());
    }
}
