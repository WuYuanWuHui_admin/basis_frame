package com.ruoyi.project.bajiaostar.user.service.impl;

import com.github.pagehelper.PageHelper;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.Md5Utils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.text.Convert;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.mongodb.bajiaostar.user.service.UserAppEntityService;
import com.ruoyi.project.bajiaostar.api.entity.dto.user.ExecuteUserAccountDto;
import com.ruoyi.project.bajiaostar.api.entity.dto.user.FindMerchantListDto;
import com.ruoyi.project.bajiaostar.api.entity.vo.MyTeamPushVo;
import com.ruoyi.project.bajiaostar.api.entity.vo.MyTeamVo;
import com.ruoyi.project.bajiaostar.api.entity.vo.MyUserPushVo;
import com.ruoyi.project.bajiaostar.user.domain.UserApp;
import com.ruoyi.project.bajiaostar.user.mapper.UserAppMapper;
import com.ruoyi.project.bajiaostar.user.service.ITeamService;
import com.ruoyi.project.bajiaostar.user.service.IUserAppService;
import com.ruoyi.project.bajiaostar.userAccount.service.IUserAccountService;
import com.ruoyi.project.bajiaostar.userShopFans.service.IUserShopFansService;
import com.ruoyi.project.bajiaostar.userSignIn.domain.UserSignIn;
import com.ruoyi.project.bajiaostar.userSignIn.service.IUserSignInService;
import com.ruoyi.project.bajiaostar.userSignInTask.service.IUserSignInTaskService;
import com.ruoyi.project.utils.RedisUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 用户信息Service业务层处理
 * 
 * @author 谢少辉
 * @date 2024-03-31
 */
@Service
@Slf4j
public class UserAppServiceImpl implements IUserAppService
{
    @Autowired
    private UserAppMapper userMapper;
    @Autowired
    IUserAccountService userAccountService;
    @Autowired
    IUserShopFansService userShopFansService;
    @Autowired
    IUserSignInService userSignInService;
    @Autowired
    IUserSignInTaskService userSignInTaskService;

    @Autowired
    UserAppEntityService userAppEntityService;
    @Autowired
    private ITeamService teamService;

    @Autowired
    private RedisUtils redisUtils;


    /**
     * 查询用户信息
     * 
     * @param id 用户信息ID
     * @return 用户信息
     */
    @Override
    public UserApp selectUserById(Long id)
    {
        return userMapper.selectUserById(id);
    }

    /**
     * 查询用户信息列表
     * 
     * @param userApp 用户信息
     * @return 用户信息
     */
    @Override
    public List<UserApp> selectUserList(UserApp userApp)
    {
        return userMapper.selectUserList(userApp);
    }

    /**
     * 新增用户信息
     * 
     * @param userApp 用户信息
     * @return 结果
     */
    @Override
    public int insertUser(UserApp userApp)
    {
        userApp.setCreateTime(DateUtils.getNowDate());
        return userMapper.insertUser(userApp);
    }

    /**
     * 修改用户信息
     * 
     * @param userApp 用户信息
     * @return 结果
     */
    @Override
    public int updateUser(UserApp userApp)
    {
        userApp.setUpdateTime(DateUtils.getNowDate());
        return userMapper.updateUser(userApp);
    }

    /**
     * 删除用户信息对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteUserByIds(String ids)
    {
        return userMapper.deleteUserByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除用户信息信息
     * 
     * @param id 用户信息ID
     * @return 结果
     */
    @Override
    public int deleteUserById(Long id)
    {
        return userMapper.deleteUserById(id);
    }

    /**
     * 根据手机号码查询信息
     *
     * @param moble
     */
    @Override
    public UserApp findUserByMoble(String moble) {
        return userMapper.findUserByMoble(moble);
    }

    /**
     * 根据推荐码查询推荐人
     *
     * @param pushUserCode
     */
    @Override
    public UserApp findUserByPushUserCode(String pushUserCode) {
        return userMapper.findUserByPushUserCode(pushUserCode);
    }

    /**
     * 根据条件查找商家
     *
     * @param dto
     * @return
     */
    @Override
    public List<UserApp> findMerchantList(FindMerchantListDto dto) {
        return userMapper.findMerchantList(dto);
    }

    /**
     * 修改会员购买状态
     *
     * @param userId
     * @param account
     * @return
     */
    @Override
    public AjaxResult updateUserCard(Long userId, BigDecimal account) {
        UserApp userApp = selectUserById(userId);
        if(ObjectUtils.isEmpty(userApp)){
            return AjaxResult.error("账户信息消失啦");
        }
        if(!userApp.getCardFlag().equals(0)){
            return AjaxResult.error("会员状态有误，请联系客服人员");
        }
        userApp.setCardFlag(1);
        userApp.setCardEndDate(DateUtils.addDayTime(new Date(),30));
        updateUser(userApp);
        //异步执行分润
        userDistribution(userId,new BigDecimal(20));
        return AjaxResult.success();
    }

    /**
     * 用户身份判断
     *
     * @param userId
     * @return
     */
    @Override
    public AjaxResult userAuthCheck(Long userId) {
        UserApp userApp = selectUserById(userId);
        if(ObjectUtils.isEmpty(userApp)){
            return AjaxResult.error("账户信息消失啦");
        }
        if(userApp.getUserIdentity().equals(1)){
            //商户身份
            return merchantAuthCheck(userApp);
        }
        if(userApp.getUserIdentityLevel().equals(10)){
            return AjaxResult.error("账户已到最高等级,无需更新等级");
        }
        //用户是否开通会员
        Integer cardFlag = userApp.getCardFlag();
        if(!cardFlag.equals(0)){
            return AjaxResult.error("未购买卡片");
        }
        //查询直推人员
        int userDirectPushCount = userMapper.findUserDirectPush(userId);
        //查询直推商户人员
        int userMerchantDirectPush = userMapper.findUserMerchantDirectPush(userId);
        //市场架构
        int userLevelDirectPush = userMapper.findUserLevelDirectPush(userId, userApp.getUserIdentityLevel() + 1);
        //个人持有
        int userTaskCount = userSignInTaskService.findUserSignTaskByUserIdAndTaskId(userId, userApp.getUserIdentityLevel());
        if(userTaskCount > 1){
            return AjaxResult.error("礼包持有不足");
        }
        //总活跃度
       // Integer activityTotal = userMapper.finduserPushCount(userId, "1");
        //小区活跃度
        Integer activityCommunity = 0;
        //大区活跃度
        Integer activityRegion = 0;
        //获取所有直推用户
        List<UserApp> userPushList = userMapper.selectUserPushListByUserId(userId);
        //获取所有直推人员信息、
        if(!ObjectUtils.isEmpty(userPushList)){
            Integer pushUserCount=0;
            List<Integer> list=new ArrayList<>();
            for (UserApp u:userPushList) {
                //Integer pushCount = userMapper.finduserPushCount(u.getId(), "1");
                //Integer pushCount = userAppEntityService.findUserLevelDirectPushCount(u.getId() + "", true);
                Integer pushCount = this.getTeamMemberCount(u.getId(), true);
                list.add(pushCount);
                pushUserCount=pushUserCount+pushCount;
            }
            activityRegion = Collections.max(list);
            activityCommunity=pushUserCount-activityRegion;
        }

        //查询
        if(userApp.getUserIdentityLevel().equals(0)){
            if(userDirectPushCount < 5){
                return AjaxResult.error("直推人员不满足");
            }
            if(userMerchantDirectPush < 1){
                return AjaxResult.error("邀请商户人员不满足");
            }
            if(activityRegion < 30 ){
                return AjaxResult.error("总活跃度不满足");
            }
            if(activityCommunity < 10 ){
                return AjaxResult.error("小区活跃度不满足");
            }
            userApp.setUserIdentityLevel(1);
        }else if(userApp.getUserIdentityLevel().equals(1)){
            if(userDirectPushCount < 9){
                return AjaxResult.error("直推人员不满足");
            }
            if(userMerchantDirectPush < 3){
                return AjaxResult.error("邀请商户人员不满足");
            }
            if(userLevelDirectPush<3){
                return AjaxResult.error("市场架构人员人员不满足");
            }
            if(activityRegion < 300 ){
                return AjaxResult.error("总活跃度不满足");
            }
            if(activityCommunity < 50 ){
                return AjaxResult.error("小区活跃度不满足");
            }
            userApp.setUserIdentityLevel(2);

        }else if(userApp.getUserIdentityLevel().equals(2)){
            if(userDirectPushCount < 12){
                return AjaxResult.error("直推人员不满足");
            }
            if(userMerchantDirectPush < 5){
                return AjaxResult.error("邀请商户人员不满足");
            }
            if(userLevelDirectPush<2){
                return AjaxResult.error("市场架构人员人员不满足");
            }
            if(activityRegion < 3000 ){
                return AjaxResult.error("总活跃度不满足");
            }
            if(activityCommunity < 800 ){
                return AjaxResult.error("小区活跃度不满足");
            }
            userApp.setUserIdentityLevel(3);

        }else if(userApp.getUserIdentityLevel().equals(3)){
            if(userDirectPushCount < 16){
                return AjaxResult.error("直推人员不满足");
            }
            if(userMerchantDirectPush < 10){
                return AjaxResult.error("邀请商户人员不满足");
            }
            if(userLevelDirectPush<2){
                return AjaxResult.error("市场架构人员人员不满足");
            }
            if(activityRegion < 6000 ){
                return AjaxResult.error("总活跃度不满足");
            }
            if(activityCommunity < 1200 ){
                return AjaxResult.error("小区活跃度不满足");
            }
            userApp.setUserIdentityLevel(4);
        }else if(userApp.getUserIdentityLevel().equals(4)){
            if(userDirectPushCount < 20){
                return AjaxResult.error("直推人员不满足");
            }
            if(userMerchantDirectPush < 20){
                return AjaxResult.error("邀请商户人员不满足");
            }
            if(userLevelDirectPush<2){
                return AjaxResult.error("市场架构人员人员不满足");
            }
            if(activityRegion < 9000 ){
                return AjaxResult.error("总活跃度不满足");
            }
            if(activityCommunity < 2000 ){
                return AjaxResult.error("小区活跃度不满足");
            }
            userApp.setUserIdentityLevel(5);
        }else if(userApp.getUserIdentityLevel().equals(5)){
            if(userDirectPushCount < 24){
                return AjaxResult.error("直推人员不满足");
            }
            if(userMerchantDirectPush < 20){
                return AjaxResult.error("邀请商户人员不满足");
            }
            if(userLevelDirectPush<2){
                return AjaxResult.error("市场架构人员人员不满足");
            }
            if(activityRegion < 20000 ){
                return AjaxResult.error("总活跃度不满足");
            }
            if(activityCommunity < 3000 ){
                return AjaxResult.error("小区活跃度不满足");
            }
            userApp.setUserIdentityLevel(6);
        }else if(userApp.getUserIdentityLevel().equals(6)){

            if(userDirectPushCount < 32){
                return AjaxResult.error("直推人员不满足");
            }
            if(userMerchantDirectPush < 20){
                return AjaxResult.error("邀请商户人员不满足");
            }
            if(userLevelDirectPush<2){
                return AjaxResult.error("市场架构人员人员不满足");
            }
            if(activityRegion < 50000 ){
                return AjaxResult.error("总活跃度不满足");
            }
            if(activityCommunity < 10000 ){
                return AjaxResult.error("小区活跃度不满足");
            }
            userApp.setUserIdentityLevel(7);
        }else if(userApp.getUserIdentityLevel().equals(7)){
            if(userDirectPushCount < 25){
                return AjaxResult.error("直推人员不满足");
            }
            if(userMerchantDirectPush < 21){
                return AjaxResult.error("邀请商户人员不满足");
            }
            if(userLevelDirectPush<3){
                return AjaxResult.error("市场架构人员人员不满足");
            }
            if(activityRegion < 120000 ){
                return AjaxResult.error("总活跃度不满足");
            }
            if(activityCommunity < 30000 ){
                return AjaxResult.error("小区活跃度不满足");
            }
            userApp.setUserIdentityLevel(8);
        }else if(userApp.getUserIdentityLevel().equals(8)){
            if(userDirectPushCount < 36){
                return AjaxResult.error("直推人员不满足");
            }
            if(userMerchantDirectPush < 20){
                return AjaxResult.error("邀请商户人员不满足");
            }
            if(userLevelDirectPush<2){
                return AjaxResult.error("市场架构人员人员不满足");
            }
            if(activityRegion < 180000 ){
                return AjaxResult.error("总活跃度不满足");
            }
            if(activityCommunity < 60000 ){
                return AjaxResult.error("小区活跃度不满足");
            }
            userApp.setUserIdentityLevel(9);
        }else if(userApp.getUserIdentityLevel().equals(9)){
            if(userDirectPushCount < 40){
                return AjaxResult.error("直推人员不满足");
            }
            if(userMerchantDirectPush < 20){
                return AjaxResult.error("邀请商户人员不满足");
            }
            if(userLevelDirectPush<2){
                return AjaxResult.error("市场架构人员人员不满足");
            }
            if(activityRegion < 300000 ){
                return AjaxResult.error("总活跃度不满足");
            }
            if(activityCommunity < 100000 ){
                return AjaxResult.error("小区活跃度不满足");
            }
            userApp.setUserIdentityLevel(10);
        }
        userMapper.updateUser(userApp);
        return AjaxResult.success();
    }

    /**
     * 分销
     *
     * @param userId
     * @param totalAmount
     * @return
     */
    @Override
    public AjaxResult userDistribution(Long userId, BigDecimal totalAmount) {
        UserApp userApp = selectUserById(userId);
        if(ObjectUtils.isEmpty(userApp)){
            return AjaxResult.error("账户信息消失啦");
        }
        //分润总金额
        BigDecimal totalAmount1=totalAmount;
        //上一次分润基数
        BigDecimal lastTimeAmount = BigDecimal.ZERO;
        //上一次用户
        Long lastTimePsuhUserId = userApp.getPushUserId();
        distribution(userApp,totalAmount1,lastTimeAmount,lastTimePsuhUserId,true);
        return AjaxResult.success();
    }

    /**
     * 查询用户最后一个推荐人
     *
     * @param userId
     */
    @Override
    public UserApp selectUserPushByUserId(Long userId) {
        return userMapper.selectUserPushByUserId(userId);
    }

    /**
     * 我的团队
     *
     * @param userId
     * @return
     */
    @Override
    public AjaxResult  myTeam(Long userId) {
        MyTeamVo vo =new MyTeamVo();
        //获取当前用户信息
        UserApp userApp = userMapper.selectUserById(userId);
        if(ObjectUtils.isEmpty(userApp)){
            return AjaxResult.error("用户不存在");
        }
        vo.setCardFlag(userApp.getCardFlag());
        vo.setUserIdentityLevel(userApp.getUserIdentityLevel());
        vo.setLowerUserIdentityLevel(userApp.getUserIdentityLevel()>10?userApp.getUserIdentityLevel():userApp.getUserIdentityLevel()+1);
        if(ObjectUtils.isEmpty(userApp.getReanlIdnum())){
            vo.setRealNameAuthFlag(0);
        }else {
            vo.setRealNameAuthFlag(1);
        }
        int userMerchantDirectPush = this.findUserMerchantDirectPush(userId);

        vo.setUserMerchantDirectPush(userMerchantDirectPush);
        //下一身份所需条件
        if(vo.getLowerUserIdentityLevel().equals(1)){
            vo.setLowerAuthActivityTotal(30);
            vo.setLowerAuthActivityCommunity(10);
            vo.setLowerAuthMyDirectPush(5);
            vo.setLowerUserMerchantDirectPush(1);
        }else if(vo.getLowerUserIdentityLevel().equals(2)){
            vo.setLowerAuthActivityTotal(300);
            vo.setLowerAuthActivityCommunity(50);
            vo.setLowerAuthMyDirectPush(8);
            vo.setLowerUserMerchantDirectPush(2);
        }else if(vo.getLowerUserIdentityLevel().equals(3)){
            vo.setLowerAuthActivityTotal(3000);
            vo.setLowerAuthActivityCommunity(800);
            vo.setLowerAuthMyDirectPush(12);
            vo.setLowerUserMerchantDirectPush(5);
        }else if(vo.getLowerUserIdentityLevel().equals(4)){
            vo.setLowerAuthActivityTotal(6000);
            vo.setLowerAuthActivityCommunity(1200);
            vo.setLowerAuthMyDirectPush(16);
            vo.setLowerUserMerchantDirectPush(10);
        }else if(vo.getLowerUserIdentityLevel().equals(5)){
            vo.setLowerAuthActivityTotal(9000);
            vo.setLowerAuthActivityCommunity(2000);
            vo.setLowerAuthMyDirectPush(20);
            vo.setLowerUserMerchantDirectPush(20);
        }else if(vo.getLowerUserIdentityLevel().equals(6)){
            vo.setLowerAuthActivityTotal(20000);
            vo.setLowerAuthActivityCommunity(3000);
            vo.setLowerAuthMyDirectPush(24);
            vo.setLowerUserMerchantDirectPush(20);
        }else if(vo.getLowerUserIdentityLevel().equals(7)){
            vo.setLowerAuthActivityTotal(50000);
            vo.setLowerAuthActivityCommunity(10000);
            vo.setLowerAuthMyDirectPush(28);
            vo.setLowerUserMerchantDirectPush(20);
        }else if(vo.getLowerUserIdentityLevel().equals(8)){
            vo.setLowerAuthActivityTotal(120000);
            vo.setLowerAuthActivityCommunity(30000);
            vo.setLowerAuthMyDirectPush(32);
            vo.setLowerUserMerchantDirectPush(20);
        }else if(vo.getLowerUserIdentityLevel().equals(9)){
            vo.setLowerAuthActivityTotal(180000);
            vo.setLowerAuthActivityCommunity(60000);
            vo.setLowerAuthMyDirectPush(36);
            vo.setLowerUserMerchantDirectPush(20);
        }else if(vo.getLowerUserIdentityLevel().equals(10)){
            vo.setLowerAuthActivityTotal(300000);
            vo.setLowerAuthActivityCommunity(100000);
            vo.setLowerAuthMyDirectPush(40);
            vo.setLowerUserMerchantDirectPush(20);
        }
        //获取所有直推用户
        List<UserApp> userPushList = userMapper.selectUserPushListByUserId(userId);
        if(ObjectUtils.isEmpty(userPushList)){
            vo.setMyDirectPush(0);
        }else{
            vo.setMyDirectPush(userPushList.size());
        }
        //获取所有直推人员信息、
        if(ObjectUtils.isEmpty(userPushList)){
            vo.setActivityCommunity(0);
        }else {
            Integer pushUserCount=0;
            List<Integer> list=new ArrayList<>();
            for (UserApp u:userPushList) {
                //数据库直接查
                //Integer pushCount = userMapper.finduserPushCount(u.getId(), "1");
                //mangodb查
                //Integer pushCount = userAppEntityService.findUserLevelDirectPushCount(u.getId() + "", true);
                //Redis查
                Integer pushCount = teamService.getTeamMemberCount(u, true);
                list.add(pushCount);
                pushUserCount=pushUserCount+pushCount;
            }
            int max = Collections.max(list);
            vo.setActivityCommunity(pushUserCount-max);
        }
        //个人持有
        int userTaskCount = userSignInTaskService.findUserSignTaskByUserIdAndTaskId(userId, userApp.getUserIdentityLevel());
        vo.setUserTaskCount(userTaskCount);
        //数据库查
        //vo.setActivityTotal(userMapper.finduserPushCount(userId,"1"));
        //vo.setTeamTotal(userMapper.finduserPushCount(userId,null));
        //mangodb查
        //vo.setActivityTotal(userAppEntityService.findUserLevelDirectPushCount(userId+"",true));
        //vo.setTeamTotal(userAppEntityService.findUserLevelDirectPushCount(userId+"",false));
        //Redis查
        vo.setActivityTotal(teamService.getTeamMemberCount(userApp, true));
        vo.setTeamTotal(teamService.getTeamMemberCount(userApp, false));

        vo.setTeamActivityTotal(userSignInService.findUserSignInDataByUserId(userId));
        if(!ObjectUtils.isEmpty(userApp.getPushUserId())){
            //获取我的上级用户信息
            UserApp pushUserApp = userMapper.selectUserById(userApp.getPushUserId());
            if(!ObjectUtils.isEmpty(pushUserApp)){
                MyUserPushVo userPushVo=new MyUserPushVo();
                //userPushVo.setUserPhto(pushUserApp.);
                userPushVo.setUserName(pushUserApp.getUserName());
                userPushVo.setUserPhone(pushUserApp.getMoble());
                userPushVo.setUserPushCode(pushUserApp.getPushCode());
                vo.setUserPushVo(userPushVo);
            }
        }

        //市场架构
        int userLevelDirectPush = userMapper.findUserLevelDirectPush(userId, userApp.getUserIdentityLevel() + 1);
        vo.setUserLevelDirectPush(userLevelDirectPush);
        vo.setLowerUserLevelDirectPush(1);


        return AjaxResult.success(vo);
    }

    /**
     * 我的团队(我的直推)
     *
     * @param userId
     * @return
     */
    @Override
    public AjaxResult myTeamPush(Long userId,String phone) {
        //获取所有直推用户
        List<UserApp> userPushList = userMapper.selectUserPushListByUserIdOrPhone(userId,phone);
        if(ObjectUtils.isEmpty(userPushList)){
            return AjaxResult.success();
        }
        List<MyTeamPushVo> list=new ArrayList<>();
        for (UserApp userApp:userPushList) {
            MyTeamPushVo vo = new MyTeamPushVo();
            vo.setUserName(userApp.getUserName());
            vo.setUserPhone(userApp.getMoble());
            vo.setRegisterDate(DateUtils.parseDateToStr(DateUtils.YYYY_MM_DD,userApp.getCreateTime()));
            if(ObjectUtils.isEmpty(userApp.getReanlIdnum())){
                vo.setRealNameAuthFlag(0);
            }else{
                vo.setRealNameAuthFlag(1);
            }
            //查询当日是否签到
            UserSignIn userSignInData = userSignInService.findUserSignInData(userApp.getId());
            vo.setValidFlag(0);
            vo.setUserActivity(0);
            if(!ObjectUtils.isEmpty(userSignInData)){
                vo.setValidFlag(1);
                vo.setUserActivity(1);
            }
            //数据库直接查
            //vo.setTeamTotal(userMapper.finduserPushCount(userApp.getId(),null));
            //vo.setTeamActivityTotal(userMapper.finduserPushCount(userApp.getId(),"1"));
            //mangodb查
            //vo.setTeamTotal(userAppEntityService.findUserLevelDirectPushCount(userApp.getId()+"",false));
            //vo.setTeamActivityTotal(userAppEntityService.findUserLevelDirectPushCount(userApp.getId()+"",true));
            //Redis查
            vo.setTeamTotal(teamService.getTeamMemberCount(userApp, false));
            vo.setTeamActivityTotal(teamService.getTeamMemberCount(userApp, true));

            vo.setTeamActivity(userSignInService.findUserSignInDataByUserId(userApp.getId()));
            list.add(vo);
        }
        return AjaxResult.success(list);
    }

    @Override
    public int findUserDirectPush(Long userId) {
        return userMapper.findUserDirectPush(userId);
    }

    @Override
    public int findUserMerchantDirectPush(Long userId) {
        return userMapper.findUserMerchantDirectPush(userId);
    }

    /**
     * 获取所有直推人员
     *
     * @param userId
     * @return
     */
    @Override
    public List<UserApp> selectUserPushListByUserId(Long userId) {
        return userMapper.selectUserPushListByUserId(userId);
    }

    /**
     * 统计推广人数
     *
     * @param userId
     * @param idNumberFlag
     * @return
     */
    @Override
    public int finduserPushCount(Long userId, String idNumberFlag) {
        return userMapper.finduserPushCount(userId,idNumberFlag);
    }

    /**
     * 获取用户最后的一个推荐人
     *
     * @param userId
     * @return
     */
    @Override
    public Long finduserPushMaxUserId(Long userId) {
        return userMapper.finduserPushMaxUserId(userId);
    }

    /**
     * 设置交易密码
     *
     * @param userId
     * @param passWord
     * @return
     */
    @Override
    public AjaxResult setPassword(Long userId, String passWord) {
        UserApp userApp = selectUserById(userId);
        if(ObjectUtils.isEmpty(userApp)){
            return AjaxResult.error("用户不存在");
        }
        if(ObjectUtils.isEmpty(userApp.getReanlIdnum())){
            return AjaxResult.error("请先实名认证");
        }
        if(!ObjectUtils.isEmpty(userApp.getPayPassWord())){
            return AjaxResult.error("请不要重复设置交易密码");
        }
        userApp.setPayPassWord(Md5Utils.hash(passWord));
        updateUser(userApp);
        return AjaxResult.success();
    }

    /**
     * 重置交易密码
     *
     * @param userId
     * @param passWord
     * @param idCard   身份证号码后四位
     * @return
     */
    @Override
    public AjaxResult resetPassword(Long userId, String passWord, String idCard) {
        UserApp userApp = selectUserById(userId);
        if(ObjectUtils.isEmpty(userApp)){
            return AjaxResult.error("用户不存在");
        }
        if(!userApp.getReanlIdnum().contains(idCard)){
            return AjaxResult.error("身份证后四位不正确");
        }
        String hash = Md5Utils.hash(passWord);
        if(userApp.getPayPassWord().equals(hash)){
            return AjaxResult.error("重置密码不能与前一次密码相同");
        }
        userApp.setPayPassWord(Md5Utils.hash(passWord));
        updateUser(userApp);
        return AjaxResult.success();
    }

    /**
     * 分润
     * @param userApp
     * @param totalAmount1
     * @param lastTimeAmount
     * @param lastTimePsuhUserId
     * @param directPushFlag 是否直推
     */
    private void distribution(UserApp userApp, BigDecimal totalAmount1,
                              BigDecimal lastTimeAmount,Long lastTimePsuhUserId,
                              boolean directPushFlag){
        if(userApp.getUserIdentityLevel().equals(10)){
            log.error("用户级别达到10级，无法继续分润");
            return;
        }
        if(ObjectUtils.isEmpty(userApp.getPushUserId())){
            log.error("推荐人不存在，无法继续分润");
            return;
        }
        //获取推荐人信息
        UserApp pushuser = selectUserById(lastTimePsuhUserId);
        if(ObjectUtils.isEmpty(pushuser)){
            log.error("推荐人不存在，无法继续分润");
            return;
        }
        if(userApp.getUserIdentityLevel() >= pushuser.getUserIdentityLevel() && !directPushFlag){
            log.error("推荐人级别小于当前用户，无法继续分润");
            //继续执行下一个分润
             distribution(pushuser,totalAmount1,lastTimeAmount,pushuser.getPushUserId(),false);
            return;
        }
        BigDecimal amount=BigDecimal.ZERO;
        //获取分润金额
        if(directPushFlag){
            amount = new BigDecimal(10);
        }else if(userApp.getUserIdentityLevel().equals(0)){
            amount = new BigDecimal(10);
        }else if(userApp.getUserIdentityLevel().equals(1)){
            amount = new BigDecimal(11);
        }else if(userApp.getUserIdentityLevel().equals(2)){
            amount = new BigDecimal(12);
        }else if(userApp.getUserIdentityLevel().equals(3)){
            amount = new BigDecimal(13);
        }else if(userApp.getUserIdentityLevel().equals(4)){
            amount = new BigDecimal(14);
        }else if(userApp.getUserIdentityLevel().equals(5)){
            amount = new BigDecimal(15);
        }else if(userApp.getUserIdentityLevel().equals(6)){
            amount = new BigDecimal(16);
        }else if(userApp.getUserIdentityLevel().equals(7)){
            amount = new BigDecimal(17);
        }else if(userApp.getUserIdentityLevel().equals(8)){
            amount = new BigDecimal(18);
        }else if(userApp.getUserIdentityLevel().equals(9)){
            amount = new BigDecimal(19);
        }else if(userApp.getUserIdentityLevel().equals(10)){
            amount = new BigDecimal(20);
        }
        //判断应分润金额（得到分润信息-上一次分润金额）
        BigDecimal userAmount = amount.subtract(lastTimeAmount);
        lastTimeAmount=amount;
        //添加账户余额明细
        ExecuteUserAccountDto dto=new ExecuteUserAccountDto();
        dto.setUserId(pushuser.getId());
        dto.setUserAccount(userAmount.doubleValue());
        dto.setUserType(0);
        dto.setAccountSysType(1);
        dto.setAccountName("分润奖励");
        dto.setAccountType(0);
        userAccountService.executeUserAccount(dto);

        if(totalAmount1.compareTo(lastTimeAmount) !=0 ){
            //继续执行下一个分润
            distribution(pushuser,totalAmount1,lastTimeAmount,pushuser.getPushUserId(),false);
            return;
        }
    }



    /**
     * 商户身份判断
     *
     * @param userApp
     * @return
     */
    private AjaxResult merchantAuthCheck(UserApp userApp) {
        if(userApp.getUserIdentityLevel1().equals(10)){
            return AjaxResult.error("账户已到最高等级,无需更新等级");
        }
        //查询商户粉丝数
        int count = userShopFansService.findUserByMerchantUserId(userApp.getId());
        if(userApp.getUserIdentityLevel1().equals(0)){
            if(count < 50){
                return AjaxResult.error("邀请商户人员不满足");
            }
            userApp.setUserIdentityLevel1(1);
        }else if(userApp.getUserIdentityLevel1().equals(1)){

            if(count<200){
                return AjaxResult.error("市场架构人员人员不满足");
            }
            userApp.setUserIdentityLevel1(2);

        }else if(userApp.getUserIdentityLevel1().equals(2)){

            if(count<1000){
                return AjaxResult.error("市场架构人员人员不满足");
            }
            userApp.setUserIdentityLevel1(3);

        }else if(userApp.getUserIdentityLevel1().equals(3)){

            if(count<3000){
                return AjaxResult.error("市场架构人员人员不满足");
            }
            userApp.setUserIdentityLevel1(4);
        }else if(userApp.getUserIdentityLevel1().equals(4)){

            if(count<8000){
                return AjaxResult.error("市场架构人员人员不满足");
            }
            userApp.setUserIdentityLevel1(5);
        }else if(userApp.getUserIdentityLevel1().equals(5)){

            if(count<12000){
                return AjaxResult.error("市场架构人员人员不满足");
            }
            userApp.setUserIdentityLevel1(6);
        }else if(userApp.getUserIdentityLevel1().equals(6)){


            if(count<18000){
                return AjaxResult.error("市场架构人员人员不满足");
            }
            userApp.setUserIdentityLevel1(7);
        }else if(userApp.getUserIdentityLevel1().equals(7)){

            if(count<32000){
                return AjaxResult.error("市场架构人员人员不满足");
            }
            userApp.setUserIdentityLevel1(8);
        }else if(userApp.getUserIdentityLevel1().equals(8)){

            if(count<100000){
                return AjaxResult.error("市场架构人员人员不满足");
            }
            userApp.setUserIdentityLevel1(9);
        }else if(userApp.getUserIdentityLevel1().equals(9)){

            if(count<300000){
                return AjaxResult.error("市场架构人员人员不满足");
            }
            userApp.setUserIdentityLevel1(10);
        }
        userMapper.updateUser(userApp);
        return AjaxResult.success();
    }

    public void refreshAllTeamInfo() {
        Integer userCount = userMapper.selectUserCount();
        Integer pageSize = 1000;
        Integer totalPage = userCount == null ? 0 : Double.valueOf(Math.ceil(userCount*1.0/pageSize)).intValue();
        UserApp userParam = new UserApp();
        for(int pageIndex = 1;pageIndex <= totalPage;pageIndex++) {
            PageHelper.startPage(pageIndex, pageSize);
            List<UserApp> userList = userMapper.selectUserList(userParam);
            if(userList != null) {
                userList.forEach(userItem -> {
                    this.refreshTeamInfo(userItem.getId());
                });
            }
        }
    }

    public void refreshTeamInfo(Long userId) {
        String teamUserCacheKeyPrefix = "xiyi:cache:team:user:";
        String teamActiveUserCacheKeyPrefix = "xiyi:cache:team:activeUser:";
        String userTeamCacheKeyPrefix = "xiyi:cache:user:team:";
        UserApp user = userId == null ? null : userMapper.selectUserById(userId);
        if(user != null) {
            List<UserApp> allParentList = this.getAllParentList(userId);
            if(allParentList != null && !allParentList.isEmpty()) {
                List<String> allParentIdList = allParentList.stream().map(allParentItem -> allParentItem.getId().toString()).collect(Collectors.toList());
                redisUtils.set(teamUserCacheKeyPrefix + String.join("-", allParentIdList) + "-" + user.getId(), user.getId());
                if(StringUtils.isNotEmpty(user.getReanlIdnum())) {
                    //活跃用户
                    redisUtils.set(teamActiveUserCacheKeyPrefix + String.join("-", allParentIdList) + "-" + user.getId(), user.getId());
                }
                redisUtils.set(userTeamCacheKeyPrefix + user.getId().toString(), String.join("-", allParentIdList));
            }
        }
    }

    private List<UserApp> getAllParentList(Long userId) {
        List<UserApp> resultList = new ArrayList<>();
        UserApp user = userId == null ? null : userMapper.selectUserById(userId);
        if(user != null) {
            List<String> parentIdList = new ArrayList<>();
            UserApp parent = user.getPushUserId() == null ? null : userMapper.selectUserById(user.getPushUserId());
            while(parent != null && !parentIdList.contains(parent.getId().toString())) {
                parentIdList.add(parent.getId().toString());
                resultList.add(0, parent);
                parent = parent.getPushUserId() == null ? null : userMapper.selectUserById(parent.getPushUserId());
            }
        }
        return resultList;
    }

    private Integer getTeamMemberCount(Long userId, boolean isActive) {
        String teamUserCacheKeyPrefix = "xiyi:cache:team:user:";
        String teamActiveUserCacheKeyPrefix = "xiyi:cache:team:activeUser:";
        String userTeamCacheKeyPrefix = "xiyi:cache:user:team:";
        UserApp user = userId == null ? null : userMapper.selectUserById(userId);
        String userTeamKey = user == null ? null : redisUtils.get(userTeamCacheKeyPrefix + user.getId().toString());
        String matchTeamPattern = userTeamKey == null ? null : ((isActive ? teamActiveUserCacheKeyPrefix : teamUserCacheKeyPrefix) + userTeamKey + "-" + user.getId().toString() + "-*");
        Set<String> teamMemberKeySet = matchTeamPattern == null ? null : redisUtils.keys(matchTeamPattern);
        return teamMemberKeySet == null ? 0 : teamMemberKeySet.size();
    }

    @Override
    public Integer selectUserCount() {
        return userMapper.selectUserCount();
    }
}
