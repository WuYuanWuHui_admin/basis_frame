package com.ruoyi.project.bajiaostar.user.service;

import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.project.bajiaostar.api.entity.dto.user.FindMerchantListDto;
import com.ruoyi.project.bajiaostar.api.entity.vo.MyTeamVo;
import com.ruoyi.project.bajiaostar.user.domain.UserApp;
import io.lettuce.core.dynamic.annotation.Param;

import java.math.BigDecimal;
import java.util.List;

/**
 * 用户信息Service接口
 * 
 * @author 谢少辉
 * @date 2024-03-31
 */
public interface IUserAppService
{
    /**
     * 查询用户信息
     * 
     * @param id 用户信息ID
     * @return 用户信息
     */
    public UserApp selectUserById(Long id);

    /**
     * 查询用户信息列表
     * 
     * @param userApp 用户信息
     * @return 用户信息集合
     */
    public List<UserApp> selectUserList(UserApp userApp);

    /**
     * 新增用户信息
     * 
     * @param userApp 用户信息
     * @return 结果
     */
    public int insertUser(UserApp userApp);

    /**
     * 修改用户信息
     * 
     * @param userApp 用户信息
     * @return 结果
     */
    public int updateUser(UserApp userApp);

    /**
     * 批量删除用户信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteUserByIds(String ids);

    /**
     * 删除用户信息信息
     * 
     * @param id 用户信息ID
     * @return 结果
     */
    public int deleteUserById(Long id);

    /**
     * 根据手机号码查询信息
     * @param moble
     */
    UserApp findUserByMoble(String moble);

    /**
     * 根据推荐码查询推荐人
     * @param pushUserCode
     */
    UserApp findUserByPushUserCode(String pushUserCode);

    /**
     * 根据条件查找商家
     * @return
     */
    List<UserApp>  findMerchantList(FindMerchantListDto dto);

    /**
     * 修改会员购买状态
     * @param userId
     * @param account
     * @return
     */
    AjaxResult updateUserCard(Long userId, BigDecimal account);

    /**
     * 用户身份判断
     * @return
     */
    AjaxResult userAuthCheck(Long userId);

    /**
     * 分销
     * @param userId
     * @param totalAmount
     * @return
     */
    AjaxResult userDistribution(Long userId,BigDecimal totalAmount);


    /**
     * 查询用户最后一个推荐人
     */
    public UserApp selectUserPushByUserId(Long userId);

    /**
     * 我的团队
     * @param userId
     * @return
     */
    AjaxResult myTeam(Long userId);


    /**
     * 我的团队(我的直推)
     * @param userId
     * @return
     */
    AjaxResult myTeamPush(Long userId,String phone);

    /**
     * 获取所有直推人员
     * @param userId
     * @return
     */
    List<UserApp> selectUserPushListByUserId(Long userId);

    /**
     * 统计推广人数
     * @param userId
     * @param idNumberFlag
     * @return
     */
    int finduserPushCount(Long userId, String idNumberFlag);

    /**
     * 获取用户最后的一个推荐人
     * @param userId
     * @return
     */
    Long finduserPushMaxUserId(Long userId);

    /**
     * 设置交易密码
     * @param userId
     * @param passWord
     * @return
     */
    AjaxResult setPassword(Long userId,String passWord);

    /**
     * 重置交易密码
     * @param userId
     * @param passWord
     * @param idCard 身份证号码后四位
     * @return
     */
    AjaxResult resetPassword(Long userId,String passWord,String idCard);

    Integer selectUserCount();

    /**
     * 查询用户直推人员
     * @param userId
     * @return
     */
    int findUserDirectPush(Long userId);

    /**
     * 查询用户直推人员(商户)
     * @param userId
     * @return
     */
    int findUserMerchantDirectPush(Long userId);
}
