package com.ruoyi.project.bajiaostar.user.mapper;

import com.ruoyi.project.bajiaostar.api.entity.dto.user.FindMerchantListDto;
import com.ruoyi.project.bajiaostar.user.domain.UserApp;
import io.lettuce.core.dynamic.annotation.Param;

import java.util.List;

/**
 * 用户信息Mapper接口
 * 
 * @author 谢少辉
 * @date 2024-03-31
 */
public interface UserAppMapper
{
    /**
     * 查询用户数量
     * @return
     */
    Integer selectUserCount();
    /**
     * 查询用户信息
     * 
     * @param id 用户信息ID
     * @return 用户信息
     */
    public UserApp selectUserById(Long id);

    /**
     * 查询用户信息列表
     * 
     * @param userApp 用户信息
     * @return 用户信息集合
     */
    public List<UserApp> selectUserList(UserApp userApp);

    /**
     * 查询直推
     * @param pushUserId
     * @return
     */
    List<UserApp> selectDirectUserList(Long pushUserId);

    /**
     * 新增用户信息
     * 
     * @param userApp 用户信息
     * @return 结果
     */
    public int insertUser(UserApp userApp);

    /**
     * 修改用户信息
     * 
     * @param userApp 用户信息
     * @return 结果
     */
    public int updateUser(UserApp userApp);

    /**
     * 删除用户信息
     * 
     * @param id 用户信息ID
     * @return 结果
     */
    public int deleteUserById(Long id);

    /**
     * 批量删除用户信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteUserByIds(String[] ids);


    /**
     * 根据手机号码查询信息
     * @param moble
     */
    UserApp findUserByMoble(String moble);

    /**
     * 根据推荐码查询推荐人
     * @param pushUserCode
     */
    UserApp findUserByPushUserCode(String pushUserCode);


    /**
     * 根据条件查找商家
     * @return
     */
    List<UserApp>  findMerchantList(FindMerchantListDto dto);

    /**
     * 查询用户直推人员
     * @param userId
     * @return
     */
    int findUserDirectPush(Long userId);

    /**
     * 查询用户直推人员(商户)
     * @param userId
     * @return
     */
    int findUserMerchantDirectPush(Long userId);

    /**
     * 查询用户人员（市场架构）
     * @param userId
     * @return
     */
    int findUserLevelDirectPush(@Param("userId") Long userId,@Param("level") Integer level);

    /**
     * 获取最后一个推荐人
     * @param userId
     * @return
     */
    UserApp selectUserPushByUserId(Long userId);
    /**
     *获取所有直推用户信息
     * @param userId
     * @return
     */
    List<UserApp> selectUserPushListByUserId(Long userId);

    /**
     *获取所有直推用户信息
     * @param userId
     * @return
     */
    List<UserApp> selectUserPushListByUserIdOrPhone(@Param("userId")Long userId,@Param("phone")String phone);

    /**
     * 统计推广人数
     * @param userId
     * @param idNumberFlag
     * @return
     */
    int finduserPushCount(@Param("userId")Long userId,@Param("idNumberFlag")String idNumberFlag);

    /**
     * 更新用户推荐人信息
     * @param userId
     * @param shangjiUserId
     * @return
     */
    public int updateUserPushCodeids(@Param("userId")Long userId,@Param("shangjiUserId")Long shangjiUserId);

    /**
     * 获取用户最后的一个推荐人
     * @param userId
     * @return
     */
    Long finduserPushMaxUserId(@Param("userId")Long userId);


}
