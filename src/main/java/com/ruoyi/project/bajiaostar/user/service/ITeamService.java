package com.ruoyi.project.bajiaostar.user.service;

import com.ruoyi.project.bajiaostar.user.domain.UserApp;

import java.util.List;

/**
 * 团队service
 * @author luoyonghui
 * @date 2024/4/25
 * @description:
 */
public interface ITeamService {
    /**
     * 查询所有上级
     * @param userId
     * @return
     */
    List<UserApp> getAllParentList(Long userId);

    /**
     * 查询直推成员数量
     * @param userId
     * @param isActive 是有活跃用户
     * @return
     */
    Integer getDirectMemberCount(Long userId, boolean isActive);

    /**
     * 查询团队成员数量
     * @param userId
     * @param isActive 是有活跃用户
     * @return
     */
    Integer getTeamMemberCount(Long userId, boolean isActive);

    /**
     * 查询团队成员数量
     * @param user
     * @param isActive 是有活跃用户
     * @return
     */
    Integer getTeamMemberCount(UserApp user, boolean isActive);

    /**
     * 刷新团队信息
     */
    void refreshAllTeamInfo();
    /**
     * 刷新用户团队信息
     * @param userId
     */
    void refreshTeamInfo(Long userId);
    /**
     * 刷新用户团队信息
     * @param user
     */
    void refreshTeamInfo(UserApp user);

    /**
     * 初始化团队汇总统计
     */
    void initTeamSumCache();

    /**
     * 刷新用户团队汇总统计
     */
    void refreshTeamSumCache(Long userId);

    /**
     * 刷新用户团队汇总统计
     */
    void refreshTeamSumCache(UserApp user);
}
