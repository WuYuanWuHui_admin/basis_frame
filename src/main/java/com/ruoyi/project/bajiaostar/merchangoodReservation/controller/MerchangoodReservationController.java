package com.ruoyi.project.bajiaostar.merchangoodReservation.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.bajiaostar.merchangoodReservation.domain.MerchangoodReservation;
import com.ruoyi.project.bajiaostar.merchangoodReservation.service.IMerchangoodReservationService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 用户商品预约Controller
 * 
 * @author 谢少辉
 * @date 2024-03-31
 */
@Controller
@RequestMapping("/bajiaostar/merchangoodReservation")
public class MerchangoodReservationController extends BaseController
{
    private String prefix = "bajiaostar/merchangoodReservation";

    @Autowired
    private IMerchangoodReservationService merchangoodReservationService;

    @RequiresPermissions("bajiaostar:merchangoodReservation:view")
    @GetMapping()
    public String merchangoodReservation()
    {
        return prefix + "/merchangoodReservation";
    }

    /**
     * 查询用户商品预约列表
     */
    @RequiresPermissions("bajiaostar:merchangoodReservation:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(MerchangoodReservation merchangoodReservation)
    {
        startPage();
        List<MerchangoodReservation> list = merchangoodReservationService.selectMerchangoodReservationList(merchangoodReservation);
        return getDataTable(list);
    }

    /**
     * 导出用户商品预约列表
     */
    @RequiresPermissions("bajiaostar:merchangoodReservation:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(MerchangoodReservation merchangoodReservation)
    {
        List<MerchangoodReservation> list = merchangoodReservationService.selectMerchangoodReservationList(merchangoodReservation);
        ExcelUtil<MerchangoodReservation> util = new ExcelUtil<MerchangoodReservation>(MerchangoodReservation.class);
        return util.exportExcel(list, "merchangoodReservation");
    }

    /**
     * 新增用户商品预约
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存用户商品预约
     */
    @RequiresPermissions("bajiaostar:merchangoodReservation:add")
    @Log(title = "用户商品预约", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(MerchangoodReservation merchangoodReservation)
    {
        return toAjax(merchangoodReservationService.insertMerchangoodReservation(merchangoodReservation));
    }

    /**
     * 修改用户商品预约
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        MerchangoodReservation merchangoodReservation = merchangoodReservationService.selectMerchangoodReservationById(id);
        mmap.put("merchangoodReservation", merchangoodReservation);
        return prefix + "/edit";
    }

    /**
     * 修改保存用户商品预约
     */
    @RequiresPermissions("bajiaostar:merchangoodReservation:edit")
    @Log(title = "用户商品预约", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(MerchangoodReservation merchangoodReservation)
    {
        return toAjax(merchangoodReservationService.updateMerchangoodReservation(merchangoodReservation));
    }

    /**
     * 删除用户商品预约
     */
    @RequiresPermissions("bajiaostar:merchangoodReservation:remove")
    @Log(title = "用户商品预约", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(merchangoodReservationService.deleteMerchangoodReservationByIds(ids));
    }
}
