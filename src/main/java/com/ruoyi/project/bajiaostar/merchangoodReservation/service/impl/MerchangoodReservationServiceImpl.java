package com.ruoyi.project.bajiaostar.merchangoodReservation.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.bajiaostar.merchangoodReservation.mapper.MerchangoodReservationMapper;
import com.ruoyi.project.bajiaostar.merchangoodReservation.domain.MerchangoodReservation;
import com.ruoyi.project.bajiaostar.merchangoodReservation.service.IMerchangoodReservationService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 用户商品预约Service业务层处理
 * 
 * @author 谢少辉
 * @date 2024-03-31
 */
@Service
public class MerchangoodReservationServiceImpl implements IMerchangoodReservationService 
{
    @Autowired
    private MerchangoodReservationMapper merchangoodReservationMapper;

    /**
     * 查询用户商品预约
     * 
     * @param id 用户商品预约ID
     * @return 用户商品预约
     */
    @Override
    public MerchangoodReservation selectMerchangoodReservationById(Long id)
    {
        return merchangoodReservationMapper.selectMerchangoodReservationById(id);
    }

    /**
     * 查询用户商品预约列表
     * 
     * @param merchangoodReservation 用户商品预约
     * @return 用户商品预约
     */
    @Override
    public List<MerchangoodReservation> selectMerchangoodReservationList(MerchangoodReservation merchangoodReservation)
    {
        return merchangoodReservationMapper.selectMerchangoodReservationList(merchangoodReservation);
    }

    /**
     * 新增用户商品预约
     * 
     * @param merchangoodReservation 用户商品预约
     * @return 结果
     */
    @Override
    public int insertMerchangoodReservation(MerchangoodReservation merchangoodReservation)
    {
        merchangoodReservation.setCreateTime(DateUtils.getNowDate());
        return merchangoodReservationMapper.insertMerchangoodReservation(merchangoodReservation);
    }

    /**
     * 修改用户商品预约
     * 
     * @param merchangoodReservation 用户商品预约
     * @return 结果
     */
    @Override
    public int updateMerchangoodReservation(MerchangoodReservation merchangoodReservation)
    {
        merchangoodReservation.setUpdateTime(DateUtils.getNowDate());
        return merchangoodReservationMapper.updateMerchangoodReservation(merchangoodReservation);
    }

    /**
     * 删除用户商品预约对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteMerchangoodReservationByIds(String ids)
    {
        return merchangoodReservationMapper.deleteMerchangoodReservationByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除用户商品预约信息
     * 
     * @param id 用户商品预约ID
     * @return 结果
     */
    @Override
    public int deleteMerchangoodReservationById(Long id)
    {
        return merchangoodReservationMapper.deleteMerchangoodReservationById(id);
    }
}
