package com.ruoyi.project.bajiaostar.merchangoodReservation.domain;

import com.ruoyi.project.bajiaostar.merchangood.domain.Merchangood;
import com.ruoyi.project.bajiaostar.user.domain.UserApp;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;
import java.util.Date;

/**
 * 用户商品预约对象 t_merchant_good_reservation
 * 
 * @author 谢少辉
 * @date 2024-03-31
 */
public class MerchangoodReservation extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 预约id */
    private Long id;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 商品id */
    @Excel(name = "商品id")
    private Long goodId;

    /** 核销时间 */
    @Excel(name = "核销时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date writeOffTime;

    /** 核销用户id */
    @Excel(name = "核销用户id")
    private Long writeOffUserId;

    /** 核销状态：0 待核销  1 已核销  2 退款 */
    @Excel(name = "核销状态：0 待核销  1 已核销  2 退款")
    private Integer writeOffStatus;
    //商品信息
    private Merchangood merchangood;
    //用户信息
    private UserApp userApp;
    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setGoodId(Long goodId) 
    {
        this.goodId = goodId;
    }

    public Long getGoodId() 
    {
        return goodId;
    }
    public void setWriteOffTime(Date writeOffTime) 
    {
        this.writeOffTime = writeOffTime;
    }

    public Date getWriteOffTime() 
    {
        return writeOffTime;
    }
    public void setWriteOffUserId(Long writeOffUserId) 
    {
        this.writeOffUserId = writeOffUserId;
    }

    public Long getWriteOffUserId() 
    {
        return writeOffUserId;
    }
    public void setWriteOffStatus(Integer writeOffStatus) 
    {
        this.writeOffStatus = writeOffStatus;
    }

    public Integer getWriteOffStatus() 
    {
        return writeOffStatus;
    }

    public Merchangood getMerchangood() {
        return merchangood;
    }

    public void setMerchangood(Merchangood merchangood) {
        this.merchangood = merchangood;
    }

    public UserApp getUserApp() {
        return userApp;
    }

    public void setUserApp(UserApp userApp) {
        this.userApp = userApp;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("goodId", getGoodId())
            .append("createTime", getCreateTime())
            .append("writeOffTime", getWriteOffTime())
            .append("writeOffUserId", getWriteOffUserId())
            .append("writeOffStatus", getWriteOffStatus())
            .toString();
    }
}
