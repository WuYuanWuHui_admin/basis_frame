package com.ruoyi.project.bajiaostar.merchangoodReservation.mapper;

import com.ruoyi.project.bajiaostar.merchangoodReservation.domain.MerchangoodReservation;
import java.util.List;

/**
 * 用户商品预约Mapper接口
 * 
 * @author 谢少辉
 * @date 2024-03-31
 */
public interface MerchangoodReservationMapper 
{
    /**
     * 查询用户商品预约
     * 
     * @param id 用户商品预约ID
     * @return 用户商品预约
     */
    public MerchangoodReservation selectMerchangoodReservationById(Long id);

    /**
     * 查询用户商品预约列表
     * 
     * @param merchangoodReservation 用户商品预约
     * @return 用户商品预约集合
     */
    public List<MerchangoodReservation> selectMerchangoodReservationList(MerchangoodReservation merchangoodReservation);

    /**
     * 新增用户商品预约
     * 
     * @param merchangoodReservation 用户商品预约
     * @return 结果
     */
    public int insertMerchangoodReservation(MerchangoodReservation merchangoodReservation);

    /**
     * 修改用户商品预约
     * 
     * @param merchangoodReservation 用户商品预约
     * @return 结果
     */
    public int updateMerchangoodReservation(MerchangoodReservation merchangoodReservation);

    /**
     * 删除用户商品预约
     * 
     * @param id 用户商品预约ID
     * @return 结果
     */
    public int deleteMerchangoodReservationById(Long id);

    /**
     * 批量删除用户商品预约
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteMerchangoodReservationByIds(String[] ids);
}
