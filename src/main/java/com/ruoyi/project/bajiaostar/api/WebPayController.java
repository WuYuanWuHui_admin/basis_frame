package com.ruoyi.project.bajiaostar.api;


import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.IpUtils;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.project.bajiaostar.api.entity.dto.PlaceOrderPayDto;
import com.ruoyi.project.bajiaostar.paymenrequest.domain.Paymenrequest;
import com.ruoyi.project.bajiaostar.paymenrequest.service.IPaymenrequestService;
import com.ruoyi.project.bajiaostar.user.domain.UserApp;
import com.ruoyi.project.bajiaostar.user.service.IUserAppService;
import com.ruoyi.project.payUtils.lakala.LakalaPayUtil;
import com.ruoyi.project.payUtils.lakala.entity.req.V3CcssCounterOrderCreateReq;
import com.ruoyi.project.payUtils.lakala.entity.req.V3CcssCounterOrderQueryReq;
import com.ruoyi.project.payUtils.wx.WxPayUtils;
import com.ruoyi.project.utils.SnowFlakeUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.Map;

/**
 * 用户账户信息
 */
@RestController
@RequestMapping("/nt/pay/")
@Slf4j
public class WebPayController extends BaseController {

    @Autowired
    WxPayUtils wxPayUtils;
    @Autowired
    LakalaPayUtil lakalaPayUtil;
    @Autowired
    IPaymenrequestService paymenrequestService;
    @Autowired
    IUserAppService userAppService;



    @PostMapping("/placeOrderPay")
    @ResponseBody
    public AjaxResult placeOrderPay(@RequestBody PlaceOrderPayDto dto) {

        try {
           /* UserApp userApp = userAppService.selectUserById(getAppUserId());
            if(ObjectUtils.isEmpty(userApp)){
                return AjaxResult.error("账户信息消失啦");
            }
            if(!userApp.getCardFlag().equals(0)){
                return AjaxResult.error("会员状态有误，请联系客服人员");
            }
            if(dto.getTotalAmount().longValue() < 29){
                return AjaxResult.error("金额有误，请联系客服人员");
            }*/
            String orderNo =  SnowFlakeUtil.getDefaultSnowFlakeId()+"";
            V3CcssCounterOrderCreateReq req=new V3CcssCounterOrderCreateReq();
            req.setOutOrderNo(orderNo);
            req.setUserId(dto.getUserId());
            req.setTotalAmount(dto.getTotalAmount().multiply(new BigDecimal(100)).longValue());
            req.setCounterParam(dto.getCounterParam());
            req.setMerchantNo("822290059430BCW");
            String resp = lakalaPayUtil.v3CcssCounterOrderCreateRequest(req);
            Paymenrequest paymentRequest=new Paymenrequest();
            paymentRequest.setUserId(Long.parseLong(dto.getUserId()));
            paymentRequest.setServiceOrderNo(orderNo);
            paymentRequest.setServiceType(1);//洗护支付
            paymentRequest.setOrderNo(orderNo);
            paymentRequest.setStatus(0);
            paymentRequest.setReqParams(JSONObject.toJSONString(resp));
            paymenrequestService.insertPaymenrequest(paymentRequest);
            return AjaxResult.success("成功",resp);
        }catch (Exception e){
            log.error("下单异常：{}",e);
            throw new RuntimeException("系统下单异常，请稍后再试");
        }
    }

    @PostMapping("/placeOrderPayQuiery")
    @ResponseBody
    public AjaxResult placeOrderPay() {

        try {
            V3CcssCounterOrderQueryReq req=new V3CcssCounterOrderQueryReq();
            req.setOutOrderNo("GHSNVDY8033038232443530");
            req.setMerchantNo("822290059430BCW");
            String resp = lakalaPayUtil.v3CcssCounterOrderQueryRequest(req);
            return AjaxResult.success("成功",resp);
        }catch (Exception e){
            log.error("下单异常：{}",e);
            throw new RuntimeException("系统下单异常，请稍后再试");
        }
    }

}
