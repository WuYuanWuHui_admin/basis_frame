package com.ruoyi.project.bajiaostar.api.entity.dto.user;

import lombok.Data;

import java.io.Serializable;

@Data
public class LoginDto implements Serializable {

    /** 用户密码 */
    private String userPassword;

    /** 手机号码 */
    private String moble;
}
