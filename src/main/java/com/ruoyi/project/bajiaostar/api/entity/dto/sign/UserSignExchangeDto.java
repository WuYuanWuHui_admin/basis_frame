package com.ruoyi.project.bajiaostar.api.entity.dto.sign;

import lombok.Data;

import java.io.Serializable;

@Data
public class UserSignExchangeDto implements Serializable {
    //任务id
    private Long taskId;
}
