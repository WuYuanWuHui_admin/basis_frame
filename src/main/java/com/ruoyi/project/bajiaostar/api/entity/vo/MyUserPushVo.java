package com.ruoyi.project.bajiaostar.api.entity.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * 推荐我的人
 */
@Data
public class MyUserPushVo implements Serializable {
    //头像
    private String userPhto;
    //用户名
    private String userName;
    //手机号码
    private String userPhone;
    //推荐码
    private String userPushCode;

}
