package com.ruoyi.project.bajiaostar.api.entity.dto.user;

import lombok.Data;

import java.io.Serializable;

@Data
public class MerchangoodReservationWriteOffDto implements Serializable {
    //预约id
    private Long merchangoodReservationId;
}
