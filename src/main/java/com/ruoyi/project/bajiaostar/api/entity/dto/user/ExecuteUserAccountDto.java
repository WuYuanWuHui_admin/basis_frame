package com.ruoyi.project.bajiaostar.api.entity.dto.user;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class ExecuteUserAccountDto implements Serializable {
    //账户id
    private Long userId;
    //用户账户操作金额
    private Double userAccount;
    //用户身份 0 用户  1 商家
    private Integer userType;
    //资金操作类型 0 爱豆  1 现金
    private Integer accountSysType;
    //操作事由
    private String accountName;
    //操作类型 0 增加余额  1 减少余额
    private Integer accountType;


}
