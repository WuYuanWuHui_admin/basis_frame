package com.ruoyi.project.bajiaostar.api.entity.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class SplitInfo implements Serializable {

    private String amount;
    private String merchant_no;
    private String out_sub_order_no;
    private String sub_log_no;
    private String sub_trade_no;
    private String term_no;
}
