package com.ruoyi.project.bajiaostar.api.entity.dto.transHall;

import lombok.Data;

import java.io.Serializable;

/**
 * 交易大厅操作
 */
@Data
public class ExecuteUserTransHallDto implements Serializable {
    //需求id
    private Long  transHallId;
}
