package com.ruoyi.project.bajiaostar.api.entity.dto.user;

import lombok.Data;

import java.io.Serializable;

@Data
public class FindGoodDto implements Serializable {
    //商家用户id
    private Long merchantUserId;
    //产品id
    private Long goodId;
}
