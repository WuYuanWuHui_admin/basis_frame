package com.ruoyi.project.bajiaostar.api.entity.dto.user;

import lombok.Data;

import java.io.Serializable;

@Data
public class UpdateUserPasswordDto implements Serializable {
    //用户密码
    private String userPassword;
    //用户手机号码
    private String userMoble;
    //身份证号码后四位
    private String idCard;

}
