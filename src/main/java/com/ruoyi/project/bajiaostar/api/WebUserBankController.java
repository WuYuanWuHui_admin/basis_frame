package com.ruoyi.project.bajiaostar.api;


import com.ruoyi.common.constant.SystemContstant;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.IpUtils;
import com.ruoyi.common.utils.Md5Utils;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.framework.aop.NoRepeatSubmit;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.bajiaostar.api.entity.dto.user.*;
import com.ruoyi.project.bajiaostar.merchangood.domain.Merchangood;
import com.ruoyi.project.bajiaostar.merchangood.service.IMerchangoodService;
import com.ruoyi.project.bajiaostar.merchangoodReservation.domain.MerchangoodReservation;
import com.ruoyi.project.bajiaostar.merchangoodReservation.service.IMerchangoodReservationService;
import com.ruoyi.project.bajiaostar.user.domain.UserApp;
import com.ruoyi.project.bajiaostar.user.service.IUserAppService;
import com.ruoyi.project.bajiaostar.userAccount.domain.UserAccount;
import com.ruoyi.project.bajiaostar.userAccount.service.IUserAccountService;
import com.ruoyi.project.bajiaostar.userBank.domain.UserBank;
import com.ruoyi.project.bajiaostar.userBank.service.IUserBankService;
import com.ruoyi.project.bajiaostar.userExcitationTask.domain.UserExcitationTask;
import com.ruoyi.project.bajiaostar.userExcitationTask.service.IUserExcitationTaskService;
import com.ruoyi.project.bajiaostar.userShopFans.domain.UserShopFans;
import com.ruoyi.project.bajiaostar.userShopFans.service.IUserShopFansService;
import com.ruoyi.project.utils.RedisUtils;
import com.ruoyi.project.utils.TokenProccessor;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * 用户绑卡信息
 */
@RestController
@RequestMapping("/nt/user/bank/")
@Slf4j
public class WebUserBankController extends BaseController {

    @Autowired
    IUserBankService userBankService;

    @PostMapping("/list")
    @ResponseBody
    public AjaxResult list(UserBank userBank)
    {
        List<UserBank> list = userBankService.selectUserBankList(userBank);
        return AjaxResult.success(list);
    }
    /**
     * 新增保存用户提现卡管理
     */
    @PostMapping("/addUserBank")
    @ResponseBody
    public AjaxResult addSave(@RequestBody  UserBank userBank)
    {
        if(ObjectUtils.isEmpty(userBank)){
            return AjaxResult.error("信息不能为空");
        }
        userBank.setUserId(getAppUserId());
        return toAjax(userBankService.insertUserBank(userBank));
    }

    /**
     * 查询单个绑卡信息
     */
    @GetMapping("/edit/{id}")
    public AjaxResult edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        UserBank userBank = userBankService.selectUserBankById(id);
        return AjaxResult.success(userBank);
    }

    /**
     * 修改保存用户提现卡管理
     */
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(@RequestBody  UserBank userBank)
    {
        return toAjax(userBankService.updateUserBank(userBank));
    }


}
