package com.ruoyi.project.bajiaostar.api.entity.dto.user;

import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;

@Data
public class FindMerchantListDto implements Serializable {

    /**
     * 店铺名称
     */
    private String storeName;
}
