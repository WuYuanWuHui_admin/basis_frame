package com.ruoyi.project.bajiaostar.api.entity.dto.draw;

import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class ExecuteUserDrawDto implements Serializable {
    /**
     * 支付密码
     */
    private String payPassword;
    /**
     * 提现金额
     */
    private BigDecimal drawAmount;

   //提现到 0:支付宝 1：微信 2：银行卡
    private Integer type;
    //提现账户(阉割版)
    private String amount;
}
