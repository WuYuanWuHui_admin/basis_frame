package com.ruoyi.project.bajiaostar.api;


import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.framework.aop.NoRepeatSubmit;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.bajiaostar.api.entity.dto.sign.FindUserSignTaskListDto;
import com.ruoyi.project.bajiaostar.api.entity.dto.sign.UserSignExchangeDto;
import com.ruoyi.project.bajiaostar.api.entity.dto.transHall.AddUserTransHallDto;
import com.ruoyi.project.bajiaostar.api.entity.dto.transHall.ExecuteUserTransHallDto;
import com.ruoyi.project.bajiaostar.api.entity.dto.transHall.FindTransHallPageDto;
import com.ruoyi.project.bajiaostar.api.entity.dto.user.ExecuteUserAccountDto;
import com.ruoyi.project.bajiaostar.systemSignTask.domain.SystemSignTask;
import com.ruoyi.project.bajiaostar.systemSignTask.service.ISystemSignTaskService;
import com.ruoyi.project.bajiaostar.transHall.domain.TransHall;
import com.ruoyi.project.bajiaostar.transHall.service.ITransHallService;
import com.ruoyi.project.bajiaostar.user.service.IUserAppService;
import com.ruoyi.project.bajiaostar.userAccount.domain.UserAccount;
import com.ruoyi.project.bajiaostar.userAccount.service.IUserAccountService;
import com.ruoyi.project.bajiaostar.userSignIn.domain.UserSignIn;
import com.ruoyi.project.bajiaostar.userSignIn.service.IUserSignInService;
import com.ruoyi.project.bajiaostar.userSignInTask.domain.UserSignInTask;
import com.ruoyi.project.bajiaostar.userSignInTask.service.IUserSignInTaskService;
import com.ruoyi.project.utils.RedisUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 交易
 */
@RestController
@RequestMapping("/nt/user/transHall")
@Slf4j
public class WebUserTransHallController extends BaseController {


    @Autowired
    ITransHallService transHallService;
    @Autowired
    RedisUtils redisUtils;


    @PostMapping("/findTransHallPage")
    @ResponseBody
    public TableDataInfo findTransHallPage(@RequestBody FindTransHallPageDto dto) {
        try {
            if(ObjectUtils.isEmpty(dto) || ObjectUtils.isEmpty(dto.getPageNum()) || ObjectUtils.isEmpty(dto.getPageSize())){
                throw new RuntimeException("数据有误");
            }
            startPage(dto.getPageNum(),dto.getPageSize());
            TransHall transHall=new TransHall();
            transHall.setTransType(dto.getTransType());
            transHall.setTransStatus(0);
            List<TransHall> list = transHallService.selectTransHallList(transHall);
            return getDataTable(list);
        }catch (Exception e){
            log.error("查询交易列表-交易大厅异常：{}",e);
            throw new RuntimeException("查询交易列表-交易大厅异常，请稍后再试");
        }
    }


    @PostMapping("/findUserTransHallReleasePage")
    @ResponseBody
    public TableDataInfo findUserTransHallReleasePage(@RequestBody FindTransHallPageDto dto) {
        try {
            if(ObjectUtils.isEmpty(dto) || ObjectUtils.isEmpty(dto.getPageNum()) || ObjectUtils.isEmpty(dto.getPageSize())){
                throw new RuntimeException("数据有误");
            }
            startPage(dto.getPageNum(),dto.getPageSize());
            TransHall transHall=new TransHall();
            transHall.setTransType(dto.getTransType());
            transHall.setUserId(getAppUserId());
            List<TransHall> list = transHallService.selectTransHallList(transHall);
            return getDataTable(list);
        }catch (Exception e){
            log.error("查询交易列表-发布交易异常：{}",e);
            throw new RuntimeException("查询交易列表-发布交易异常，请稍后再试");
        }
    }

    @PostMapping("/findUserTransHallBuyPage")
    @ResponseBody
    public TableDataInfo findUserTransHallBuyPage(@RequestBody FindTransHallPageDto dto) {
        try {
            if(ObjectUtils.isEmpty(dto) || ObjectUtils.isEmpty(dto.getPageNum()) || ObjectUtils.isEmpty(dto.getPageSize())){
                throw new RuntimeException("数据有误");
            }
            startPage(dto.getPageNum(),dto.getPageSize());
            TransHall transHall=new TransHall();
            transHall.setTransType(dto.getTransType());
            transHall.setTransUserId(getAppUserId());
            List<TransHall> list = transHallService.selectTransHallList(transHall);
            return getDataTable(list);
        }catch (Exception e){
            log.error("查询交易列表-购买交易异常：{}",e);
            throw new RuntimeException("查询交易列表-购买交易异常，请稍后再试");
        }
    }


    @PostMapping("/addUserTransHall")
    @ResponseBody
    @NoRepeatSubmit
    public AjaxResult addUserTransHall(@RequestBody AddUserTransHallDto dto) {
        try {
            if(ObjectUtils.isEmpty(dto)){
                return AjaxResult.error("发布信息有误");
            }
            if(ObjectUtils.isEmpty(dto.getTransType())){
                return AjaxResult.error("请选择交易类型");
            }
            if(ObjectUtils.isEmpty(dto.getEmdTime())){
                return AjaxResult.error("截止时间不能为空");
            }
            if(ObjectUtils.isEmpty(dto.getYidouAccount()) || dto.getYidouAccount()< 0.1d){
                return AjaxResult.error("亦豆不能为空活小于0.1");
            }
            if(ObjectUtils.isEmpty(dto.getAccount()) || dto.getAccount()< 0.1d){
                return AjaxResult.error("所需金额不能为空或者小于0.1");
            }
            Long appUserId = getAppUserId();
            boolean b = redisUtils.setLock("user:addUserTransHall:" + appUserId, 5);
            if(!b){
               return AjaxResult.error("请不要重复操作");
            }
            dto.setUserId(appUserId);
            return transHallService.addUserTransHall(dto);
        }catch (Exception e){
            log.error("查询交易列表-购买交易异常：{}",e);
            throw new RuntimeException("查询交易列表-购买交易异常，请稍后再试");
        }
    }


    @PostMapping("/withdrawUserTransHall")
    @ResponseBody
    @NoRepeatSubmit
    public AjaxResult withdrawUserTransHall(@RequestBody ExecuteUserTransHallDto dto) {
        try {

            Long appUserId = getAppUserId();
            boolean b = redisUtils.setLock("user:addUserTransHall:" + dto.getTransHallId(), 5);
            if(!b){
                return AjaxResult.error("请不要重复操作");
            }
            return transHallService.withdrawUserTransHall(dto.getTransHallId(),appUserId);
        }catch (Exception e){
            log.error("撤回交易信息异常：{}",e);
            throw new RuntimeException("撤回交易信息异常，请稍后再试");
        }
    }
    @PostMapping("/buyUserTransHall")
    @ResponseBody
    @NoRepeatSubmit
    public AjaxResult buyUserTransHall(@RequestBody ExecuteUserTransHallDto dto) {
        try {

            Long appUserId = getAppUserId();
            boolean b = redisUtils.setLock("user:addUserTransHall:" + dto.getTransHallId(), 5);
            if(!b){
                return AjaxResult.error("请不要重复操作");
            }
            return transHallService.withdrawUserTransHall(dto.getTransHallId(),appUserId);
        }catch (Exception e){
            log.error("购买交易信息异常：{}",e);
            throw new RuntimeException("购买交易信息异常，请稍后再试");
        }
    }

}
