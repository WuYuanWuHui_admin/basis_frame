package com.ruoyi.project.bajiaostar.api.entity.dto;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class PlaceOrderPayDto implements Serializable {

    /**
     * 用户id
     */
    private String userId;
    /**
     *订单金额，单位：元
     *  */

    private BigDecimal totalAmount;
    /**
     *	收银台参数
     */
    private String counterParam;
}
