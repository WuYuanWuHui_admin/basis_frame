package com.ruoyi.project.bajiaostar.api;


import cn.hutool.core.bean.BeanUtil;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.constant.SystemContstant;
import com.ruoyi.common.utils.*;
import com.ruoyi.framework.aop.NoRepeatSubmit;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.mongodb.bajiaostar.user.entity.UserAppEntity;
import com.ruoyi.mongodb.bajiaostar.user.service.UserAppEntityService;
import com.ruoyi.project.bajiaostar.api.entity.dto.user.*;
import com.ruoyi.project.bajiaostar.merchangood.domain.Merchangood;
import com.ruoyi.project.bajiaostar.merchangood.service.IMerchangoodService;
import com.ruoyi.project.bajiaostar.merchangoodReservation.domain.MerchangoodReservation;
import com.ruoyi.project.bajiaostar.merchangoodReservation.service.IMerchangoodReservationService;
import com.ruoyi.project.bajiaostar.user.domain.UserApp;
import com.ruoyi.project.bajiaostar.user.service.IUserAppService;
import com.ruoyi.project.bajiaostar.userAccount.domain.UserAccount;
import com.ruoyi.project.bajiaostar.userAccount.service.IUserAccountService;
import com.ruoyi.project.bajiaostar.userExcitationTask.domain.UserExcitationTask;
import com.ruoyi.project.bajiaostar.userExcitationTask.service.IUserExcitationTaskService;
import com.ruoyi.project.bajiaostar.userShopFans.domain.UserShopFans;
import com.ruoyi.project.bajiaostar.userShopFans.service.IUserShopFansService;
import com.ruoyi.project.bajiaostar.userSignInTask.domain.UserSignInTask;
import com.ruoyi.project.bajiaostar.userSignInTask.service.IUserSignInTaskService;
import com.ruoyi.project.bajiaostar.version.domain.AppVersion;
import com.ruoyi.project.bajiaostar.version.service.IAppVersionService;
import com.ruoyi.project.third.numericalPulse.NumericalPulseUtil;
import com.ruoyi.project.utils.JsonUtils;
import com.ruoyi.project.utils.RedisUtils;
import com.ruoyi.project.utils.TokenProccessor;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * 用户信息
 */
@RestController
@RequestMapping("/nt/user/app/")
@Slf4j
public class WebAppController extends BaseController {
    @Autowired
    RedisUtils redisUtils;

    @Autowired
    IAppVersionService appVersionService;

    @ApiOperation("获取最新版本号")
    @GetMapping ("/getNewVersion")
    @ResponseBody
    public AjaxResult getNewVersion (@RequestParam ("type") Integer type) {
        Object appVersionStr = this.redisUtils.getObjKey("xiyi:app:app_version_" + type);
        if (!ObjectUtils.isEmpty(appVersionStr)) {
            return AjaxResult.success(appVersionStr);
        } else {
            AppVersion appVersion = new AppVersion();
            appVersion.setAppType(type);
            AppVersion appVersionTemp = this.appVersionService.selectNewAppVersionByType(appVersion);
            if (appVersionTemp != null) {
                this.redisUtils.set("xiyi:app:app_version_" + appVersionTemp.getAppType(),
                        JsonUtils.objectToJson(appVersionTemp), 7200);
                appVersionStr = this.redisUtils.get("xiyi:app:app_version_" + type);
            }
            return AjaxResult.success(appVersionStr);
        }
    }

    @ApiOperation ("获取最新H5版本")
    @GetMapping ("/getNewApp")
    @ResponseBody
    public AjaxResult getNewApp (@RequestParam ("type") Integer type) {
        String appVersionStr = this.redisUtils.get("xiyi:app:app_version_" + type + "_1");
        if (StringUtils.isNotBlank(appVersionStr)) {
            return AjaxResult.success(appVersionStr);
        } else {
            AppVersion appVersion = new AppVersion();
            appVersion.setAppType(type);
            appVersion.setBigUpdFlag(1);
            AppVersion appVersionTemp = this.appVersionService.selectNewAppVersionByType(appVersion);
            if (appVersionTemp != null) {
                this.redisUtils.set("xiyi:app:app_version_" + appVersionTemp.getAppType() + "_1",
                        JsonUtils.objectToJson(appVersionTemp), 7200
                );
                appVersionStr = this.redisUtils.get("xiyi:app:app_version_" + type + "_1");
            }
            return AjaxResult.success(appVersionStr);
        }
    }


    @Autowired
    UserAppEntityService userAppEntityService;
    @Autowired
    IUserAppService userAppService;

    @GetMapping ("/findById")
    @ResponseBody
    public AjaxResult findById (@RequestParam ("id") String id) {
        UserAppEntity byId = userAppEntityService.findById(id);
        return AjaxResult.success(byId);
    }


    @GetMapping ("/findAll")
    @ResponseBody
    public AjaxResult findById () {
        List<UserApp> userApps = userAppService.selectUserList(new UserApp());
        for (UserApp app:userApps) {
            System.out.println("appid=>"+app.getId());
            UserAppEntity entity=new UserAppEntity();
            entity.setId(app.getId()+"");
            entity.setMoble(app.getMoble());
            entity.setUsername(app.getUserName());
            entity.setPushuserids(app.getPushUserIds());
            entity.setSfz(app.getReanlIdnum());
            entity.setPushuserid(app.getPushUserId()+"");
            entity.setPushcode(app.getPushCode());
            userAppEntityService.add(entity);
        }
        return AjaxResult.success();
    }

    @GetMapping ("/add")
    @ResponseBody
    public AjaxResult add (@RequestParam ("id") Long id) {
        UserApp userApp = userAppService.selectUserById(id);
        UserAppEntity entity=new UserAppEntity();
        BeanUtil.copyProperties(userApp,entity);
        UserAppEntity byId = userAppEntityService.add(entity);
        return AjaxResult.success(byId);
    }

    @GetMapping ("/findUsersByAgeGreaterThan25")
    @ResponseBody
    public AjaxResult findUsersByAgeGreaterThan25 (@RequestParam ("id") String id) {
        List<UserAppEntity> byId = userAppEntityService.findUserLevelDirectPush(id,false);
        return AjaxResult.success(byId);
    }
}
