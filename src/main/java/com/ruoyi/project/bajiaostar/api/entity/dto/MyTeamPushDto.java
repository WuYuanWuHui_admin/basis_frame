package com.ruoyi.project.bajiaostar.api.entity.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class MyTeamPushDto implements Serializable {

    private String phone;
}
