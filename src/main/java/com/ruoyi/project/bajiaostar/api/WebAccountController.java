package com.ruoyi.project.bajiaostar.api;


import com.ruoyi.common.constant.SystemContstant;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.IpUtils;
import com.ruoyi.common.utils.Md5Utils;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.framework.aop.NoRepeatSubmit;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.project.bajiaostar.api.entity.dto.ExchangeDto;
import com.ruoyi.project.bajiaostar.api.entity.dto.user.*;
import com.ruoyi.project.bajiaostar.merchangood.domain.Merchangood;
import com.ruoyi.project.bajiaostar.merchangood.service.IMerchangoodService;
import com.ruoyi.project.bajiaostar.merchangoodReservation.domain.MerchangoodReservation;
import com.ruoyi.project.bajiaostar.merchangoodReservation.service.IMerchangoodReservationService;
import com.ruoyi.project.bajiaostar.user.domain.UserApp;
import com.ruoyi.project.bajiaostar.user.service.IUserAppService;
import com.ruoyi.project.bajiaostar.userAccount.domain.UserAccount;
import com.ruoyi.project.bajiaostar.userAccount.service.IUserAccountService;
import com.ruoyi.project.bajiaostar.userExcitationTask.domain.UserExcitationTask;
import com.ruoyi.project.bajiaostar.userExcitationTask.service.IUserExcitationTaskService;
import com.ruoyi.project.bajiaostar.userShopFans.domain.UserShopFans;
import com.ruoyi.project.bajiaostar.userShopFans.service.IUserShopFansService;
import com.ruoyi.project.utils.RedisUtils;
import com.ruoyi.project.utils.TokenProccessor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.*;

/**
 * 用户信息
 */
@RestController
@RequestMapping("/nt/account/")
@Slf4j
public class WebAccountController extends BaseController {

    @Autowired
    IUserAppService userService;

    @Autowired
    IUserAccountService userAccountService;
    @Autowired
    RedisUtils redisUtils;


    @PostMapping("/exchange")
    @ResponseBody
    @NoRepeatSubmit
    public AjaxResult exchange(@RequestBody ExchangeDto dto, HttpServletRequest request) {

        try {
            //获取当前用户身份
            UserApp userApp = userService.selectUserById(getAppUserId());
            if(ObjectUtils.isEmpty(userApp)){
                return AjaxResult.error("用户不存在");
            }
            BigDecimal free= BigDecimal.ONE;
            //计算到账数据
            if(userApp.getUserIdentity().equals(0)){
                if(userApp.getUserIdentityLevel().equals(0)){
                    free=new BigDecimal("0.1");
                }else if(userApp.getUserIdentityLevel().equals(1)){
                    free=new BigDecimal("0.2");
                }else if(userApp.getUserIdentityLevel().equals(2)){
                    free=new BigDecimal("0.4");
                }else if(userApp.getUserIdentityLevel().equals(3)){
                    free=new BigDecimal("0.5");
                }else if(userApp.getUserIdentityLevel().equals(4)){
                    free=new BigDecimal("0.55");
                }else if(userApp.getUserIdentityLevel().equals(5)){
                    free=new BigDecimal("0.60");
                }else if(userApp.getUserIdentityLevel().equals(6)){
                    free=new BigDecimal("0.65");
                }else if(userApp.getUserIdentityLevel().equals(7)){
                    free=new BigDecimal("0.70");
                }else if(userApp.getUserIdentityLevel().equals(8)){
                    free=new BigDecimal("0.75");
                }else if(userApp.getUserIdentityLevel().equals(9)){
                    free=new BigDecimal("0.80");
                }else if(userApp.getUserIdentityLevel().equals(10)){
                    free=new BigDecimal("0.85");
                }

            }else if(userApp.getUserIdentity().equals(1)){
                if(userApp.getUserIdentityLevel1().equals(0)){
                    free=new BigDecimal("0.1");
                }else if(userApp.getUserIdentityLevel1().equals(1)){
                    free=new BigDecimal("0.2");
                }else if(userApp.getUserIdentityLevel1().equals(2)){
                    free=new BigDecimal("0.4");
                }else if(userApp.getUserIdentityLevel1().equals(3)){
                    free=new BigDecimal("0.5");
                }else if(userApp.getUserIdentityLevel1().equals(4)){
                    free=new BigDecimal("0.55");
                }else if(userApp.getUserIdentityLevel1().equals(5)){
                    free=new BigDecimal("0.60");
                }else if(userApp.getUserIdentityLevel1().equals(6)){
                    free=new BigDecimal("0.65");
                }else if(userApp.getUserIdentityLevel1().equals(7)){
                    free=new BigDecimal("0.70");
                }else if(userApp.getUserIdentityLevel1().equals(8)){
                    free=new BigDecimal("0.75");
                }else if(userApp.getUserIdentityLevel1().equals(9)){
                    free=new BigDecimal("0.80");
                }else if(userApp.getUserIdentityLevel1().equals(10)){
                    free=new BigDecimal("0.85");
                }
            }else{
                return AjaxResult.error("当前身份状态异常");
            }
            //扣减当前用户信息
            ExecuteUserAccountDto dto1=new ExecuteUserAccountDto();
            dto1.setUserId(getAppUserId());
            dto1.setUserAccount(dto.getSufferAccount().doubleValue());
            dto1.setUserType(userApp.getUserIdentity());
            dto1.setAccountSysType(0);
            dto1.setAccountName("相互交易");
            dto1.setAccountType(1);
            AjaxResult ajaxResult = userAccountService.executeUserAccount(dto1);
            if(!ajaxResult.get("code").equals(0)){
                return ajaxResult;
            }

            //增加接收用户信息
            ExecuteUserAccountDto dto2=new ExecuteUserAccountDto();
            dto2.setUserId(dto.getSufferUserId());
            dto2.setUserAccount(dto.getSufferAccount().multiply(free).doubleValue());
            dto2.setUserType(dto.getUserType());
            dto2.setAccountSysType(0);
            dto2.setAccountName("相互交易");
            dto2.setAccountType(0);
            AjaxResult ajaxResult1 = userAccountService.executeUserAccount(dto1);
            if(!ajaxResult1.get("code").equals(0)){
                return ajaxResult1;
            }
            return AjaxResult.success("成功");
        }catch (Exception e){
            log.error("用户交换亦豆异常：{}",e);
            throw new RuntimeException("用户交换亦豆异常，请稍后再试");
        }
    }



}
