package com.ruoyi.project.bajiaostar.api;


import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.framework.aop.NoRepeatSubmit;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.project.bajiaostar.api.entity.dto.UpdateUserPushDto;
import com.ruoyi.project.bajiaostar.api.entity.dto.sign.FindUserSignTaskListDto;
import com.ruoyi.project.bajiaostar.api.entity.dto.sign.UserSignExchangeDto;
import com.ruoyi.project.bajiaostar.api.entity.dto.sign.UserSignInDto;
import com.ruoyi.project.bajiaostar.api.entity.dto.user.ExecuteUserAccountDto;
import com.ruoyi.project.bajiaostar.systemSignTask.domain.SystemSignTask;
import com.ruoyi.project.bajiaostar.systemSignTask.service.ISystemSignTaskService;
import com.ruoyi.project.bajiaostar.user.domain.UserApp;
import com.ruoyi.project.bajiaostar.user.mapper.UserAppMapper;
import com.ruoyi.project.bajiaostar.user.service.IUserAppService;
import com.ruoyi.project.bajiaostar.userAccount.domain.UserAccount;
import com.ruoyi.project.bajiaostar.userAccount.service.IUserAccountService;
import com.ruoyi.project.bajiaostar.userSignIn.domain.UserSignIn;
import com.ruoyi.project.bajiaostar.userSignIn.service.IUserSignInService;
import com.ruoyi.project.bajiaostar.userSignInTask.domain.UserSignInTask;
import com.ruoyi.project.bajiaostar.userSignInTask.service.IUserSignInTaskService;
import com.ruoyi.project.utils.RedisUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.*;

/**
 * 用户签到信息
 */
@RestController
@RequestMapping("/nt/user/test")
@Slf4j
public class WebTestController extends BaseController {


    @Autowired
    IUserAppService userService;
    @Autowired
    UserAppMapper userAppMapper;


    @Log(title = "查询用户推荐信息", businessType = BusinessType.INSERT)
    @PostMapping("/findUserStatic/{id}")
    @ResponseBody
    public AjaxResult findUserStatic(@PathVariable("id")Long id) {
        try {
            List<Map<String,Object>> list = new ArrayList<>();
            //获取所有直推用户
            List<UserApp> userPushList = userAppMapper.selectUserPushListByUserId(id);
            for (UserApp u:userPushList) {
                Integer pushCount = userAppMapper.finduserPushCount(u.getId(), "1");
                Map<String,Object> map=new HashMap<>();
                map.put(u.getId()+"",pushCount);
            }
            return AjaxResult.success("成功",list);
        }catch (Exception e){
            log.error("查询用户推荐信息异常：{}",e);
            throw new RuntimeException("查询用户推荐信息异常，请稍后再试");
        }
    }

    @Log(title = "更改用户推荐信息", businessType = BusinessType.INSERT)
    @PostMapping("/updateUserPush")
    @ResponseBody
    public AjaxResult updateUserPush(@RequestBody UpdateUserPushDto dto) {
        try {
            if(ObjectUtils.isEmpty(dto) || ObjectUtils.isEmpty(dto.getGenggaiId()) || ObjectUtils.isEmpty(dto.getXiajiId())){
                return AjaxResult.error("信息有误");
            }
            //获取更改用户和上级用户信息
            //需要更改的用户
            UserApp daqianUserApp = userAppMapper.selectUserById(dto.getGenggaiId());
            //下级用户id
            UserApp shangjiUserApp = userAppMapper.selectUserById(dto.getXiajiId());
            if(ObjectUtils.isEmpty(daqianUserApp) || ObjectUtils.isEmpty(shangjiUserApp)){
                return AjaxResult.error("上级用户或更改用户信息不存在");
            }

            //更新用户下面推荐人联系信息
userAppMapper.updateUserPushCodeids(shangjiUserApp.getId(),daqianUserApp.getId());

            String pushUserIds=shangjiUserApp.getPushUserIds()+daqianUserApp.getId()+",";
            shangjiUserApp.setPushUserId(daqianUserApp.getId());
            shangjiUserApp.setPushUserIds(pushUserIds);
            userAppMapper.updateUser(shangjiUserApp);
            //推荐人
            daqianUserApp.setPushUserId(shangjiUserApp.getPushUserId());
            daqianUserApp.setPushUserIds(shangjiUserApp.getPushUserIds());
            userAppMapper.updateUser(daqianUserApp);



            return AjaxResult.success("成功");
        }catch (Exception e){
            log.error("查询用户推荐信息异常：{}",e);
            throw new RuntimeException("查询用户推荐信息异常，请稍后再试");
        }
    }

}
