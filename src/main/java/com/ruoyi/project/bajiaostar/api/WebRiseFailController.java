package com.ruoyi.project.bajiaostar.api;


import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.framework.aop.NoRepeatSubmit;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.project.bajiaostar.api.entity.dto.sign.FindUserSignTaskListDto;
import com.ruoyi.project.bajiaostar.api.entity.dto.sign.UserSignExchangeDto;
import com.ruoyi.project.bajiaostar.api.entity.dto.user.ExecuteUserAccountDto;
import com.ruoyi.project.bajiaostar.systemRiseFail.domain.SystemRiseFail;
import com.ruoyi.project.bajiaostar.systemRiseFail.service.ISystemRiseFailService;
import com.ruoyi.project.bajiaostar.systemSignTask.domain.SystemSignTask;
import com.ruoyi.project.bajiaostar.systemSignTask.service.ISystemSignTaskService;
import com.ruoyi.project.bajiaostar.user.service.IUserAppService;
import com.ruoyi.project.bajiaostar.userAccount.domain.UserAccount;
import com.ruoyi.project.bajiaostar.userAccount.service.IUserAccountService;
import com.ruoyi.project.bajiaostar.userSignIn.domain.UserSignIn;
import com.ruoyi.project.bajiaostar.userSignIn.service.IUserSignInService;
import com.ruoyi.project.bajiaostar.userSignInTask.domain.UserSignInTask;
import com.ruoyi.project.bajiaostar.userSignInTask.service.IUserSignInTaskService;
import com.ruoyi.project.utils.RedisUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 系统涨幅信息
 */
@RestController
@RequestMapping("/nt/user/risefail")
@Slf4j
public class WebRiseFailController extends BaseController {


    @Autowired
    ISystemRiseFailService systemRiseFailService;

    @PostMapping("/finfSystemRiseFailList")
    @ResponseBody
    public AjaxResult finfSystemRiseFailList() {

        try {
            SystemRiseFail systemRiseFail=new SystemRiseFail();
            List<SystemRiseFail> systemRiseFails = systemRiseFailService.finfSystemRiseFailList(systemRiseFail);
            return AjaxResult.success("成功",systemRiseFails);
        }catch (Exception e){
            log.error("系统涨幅看板七天数据异常：{}",e);
            throw new RuntimeException("系统涨幅看板七天数据异常，请稍后再试");
        }
    }

}
