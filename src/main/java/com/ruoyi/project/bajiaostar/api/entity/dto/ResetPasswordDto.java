package com.ruoyi.project.bajiaostar.api.entity.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class ResetPasswordDto implements Serializable {

    private String passWord;
    private String idCard;

}
