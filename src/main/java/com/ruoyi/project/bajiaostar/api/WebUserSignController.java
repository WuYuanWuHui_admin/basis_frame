package com.ruoyi.project.bajiaostar.api;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.framework.aop.NoRepeatSubmit;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.project.bajiaostar.api.entity.dto.sign.FindUserSignTaskListDto;
import com.ruoyi.project.bajiaostar.api.entity.dto.sign.UserSignExchangeDto;
import com.ruoyi.project.bajiaostar.api.entity.dto.sign.UserSignInDto;
import com.ruoyi.project.bajiaostar.api.entity.dto.user.ExecuteUserAccountDto;
import com.ruoyi.project.bajiaostar.systemSignTask.domain.SystemSignTask;
import com.ruoyi.project.bajiaostar.systemSignTask.service.ISystemSignTaskService;
import com.ruoyi.project.bajiaostar.user.service.IUserAppService;
import com.ruoyi.project.bajiaostar.userAccount.domain.UserAccount;
import com.ruoyi.project.bajiaostar.userAccount.service.IUserAccountService;
import com.ruoyi.project.bajiaostar.userSignIn.domain.UserSignIn;
import com.ruoyi.project.bajiaostar.userSignIn.service.IUserSignInService;
import com.ruoyi.project.bajiaostar.userSignInTask.domain.UserSignInTask;
import com.ruoyi.project.bajiaostar.userSignInTask.service.IUserSignInTaskService;
import com.ruoyi.project.utils.RedisUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 用户签到信息
 */
@RestController
@RequestMapping("/nt/user/sgin")
@Slf4j
public class WebUserSignController extends BaseController {
    @Autowired
    ISystemSignTaskService systemSignTaskService;
    @Autowired
    IUserSignInTaskService userSignInTaskService;
    @Autowired
    IUserSignInService userSignInService;
    @Autowired
    IUserAppService userService;

    @Autowired
    IUserAccountService userAccountService;
    @Autowired
    RedisUtils redisUtils;

    @PostMapping("/findSystemSignTaskList")
    @ResponseBody
    public AjaxResult findSystemSignTaskList() {

        try {
            Object objKey = redisUtils.getObjKey("xiyi:SystemSignTask:" + DateUtils.dateTimeNow(DateUtils.YYYY_MM_DD));
            if(!ObjectUtils.isEmpty(objKey)){
                return AjaxResult.success("成功",objKey);
            }
            SystemSignTask systemSignTask=new SystemSignTask();
            systemSignTask.setTaskStatus(0L);
            List<SystemSignTask> taskList = systemSignTaskService.selectSystemSignTaskList(systemSignTask);
            redisUtils.set("xiyi:SystemSignTask:" + DateUtils.dateTimeNow(DateUtils.YYYY_MM_DD),taskList,14400L);
            return AjaxResult.success("成功",taskList);
        }catch (Exception e){
            log.error("系统任务列表异常：{}",e);
            throw new RuntimeException("系统任务列表异常，请稍后再试");
        }
    }
    @PostMapping("/findUserSignTaskList")
    @ResponseBody
    public AjaxResult findUserSignTaskList(@RequestBody FindUserSignTaskListDto dto) {
        try {
            Long appUserId = getAppUserId();
            if(ObjectUtils.isEmpty(appUserId)){
                return AjaxResult.error("获取用户信息失败");
            }
            Object objKey = redisUtils.getObjKey("xiyi:UserSignInTask:" +appUserId);
            if(!ObjectUtils.isEmpty(objKey)){
                return AjaxResult.success("成功",objKey);
            }
            UserSignInTask userSignInTask=new UserSignInTask();
            userSignInTask.setTaskStatus(dto.getTaskStatus());
            userSignInTask.setUserId(appUserId);
            List<UserSignInTask> taskList = userSignInTaskService.selectUserSignInTaskList(userSignInTask);
           redisUtils.set("xiyi:UserSignInTask:" +appUserId,taskList,86400L);
            return AjaxResult.success("成功",taskList);
        }catch (Exception e){
            log.error("用户任务列表列表异常：{}",e);
            throw new RuntimeException("用户任务列表异常，请稍后再试");
        }
    }
    @PostMapping("/selectUserSignInForData")
    @ResponseBody
    public AjaxResult selectUserSignInForData() {
        try {
            List<UserSignIn> taskList = userSignInService.selectUserSignInForData(getAppUserId());
            return AjaxResult.success("成功",taskList);
        }catch (Exception e){
            log.error("用户当月签到列表异常：{}",e);
            throw new RuntimeException("用户当月签到列表异常，请稍后再试");
        }
    }

    @PostMapping("/findDaySignIn")
    @ResponseBody
    public AjaxResult findDaySignIn() {
            List<UserSignIn> taskList = userSignInService.findDaySignIn();
            if(!ObjectUtils.isEmpty(taskList)){
                for (UserSignIn in:taskList) {
                    try {
                    UserSignInTask task = userSignInTaskService.selectUserSignInTaskById(in.getUserSigninId());
                    if(ObjectUtils.isEmpty(task)){
                        continue;
                    }
                    //计算发放金额
                    BigDecimal divide = new BigDecimal(task.getTotalReward() + "").divide(new BigDecimal(task.getTaskCycleCount() + ""), 2, BigDecimal.ROUND_HALF_UP);
                    if (divide.compareTo(BigDecimal.ZERO) == 0) {
                       continue;
                    }
                    //签到发放上级奖励
                    userSignInTaskService.userPushDistribution(in.getUserId(), divide);
                    }catch (Exception e){
                        log.error("发放签到奖励异常：{}",e);
                        throw new RuntimeException("发放签到奖励异常，请稍后再试");
                    }
                }

            }
            return AjaxResult.success("成功");

    }

    @PostMapping("/userSignIn")
    @ResponseBody
    @NoRepeatSubmit
    public AjaxResult userSignIn(@RequestBody UserSignInDto inDto) {
        try {
            if (ObjectUtils.isEmpty(inDto) || ObjectUtils.isEmpty(inDto.getUserSignInId())) {
                return AjaxResult.error("信息有误");
            }
            UserSignInTask task = userSignInTaskService.selectUserSignInTaskById(inDto.getUserSignInId());
            if (ObjectUtils.isEmpty(task)) {
                return AjaxResult.error("不存在可签到任务");
            }
            Long appUserId = getAppUserId();
            //查询是否已签到
            UserSignIn sign = userSignInService.findUserSignInDataByUserIdAndTaskId(appUserId, inDto.getUserSignInId());
            if(!ObjectUtils.isEmpty(sign)){
                return AjaxResult.error(299,"已签到",null);
            }

            UserSignIn in = new UserSignIn();
            in.setUserId(getAppUserId());
            in.setSignInDate(new Date());
            in.setUserSigninId(inDto.getUserSignInId());
            userSignInService.insertUserSignIn(in);

            //计算发放金额
            BigDecimal divide = new BigDecimal(task.getTotalReward() + "").divide(new BigDecimal(task.getTaskCycleCount() + ""), 2, BigDecimal.ROUND_HALF_UP);
            if (divide.compareTo(BigDecimal.ZERO) == 0) {
                return AjaxResult.success("恭喜签到成功");
            }
            //发放签到奖励
            ExecuteUserAccountDto dto = new ExecuteUserAccountDto();
            dto.setUserId(getAppUserId());
            dto.setUserAccount(divide.doubleValue());
            dto.setUserType(0);
            dto.setAccountSysType(0);
            dto.setAccountName("签到奖励");
            dto.setAccountType(0);
            userAccountService.executeUserAccount(dto);
            //签到发放上级奖励
            userSignInTaskService.userPushDistribution(appUserId, divide);
            redisUtils.del("xiyi:user:UserAccount:" + appUserId);
            return AjaxResult.success("恭喜签到成功");
        }catch (Exception e){
            log.error("用户签到异常：{}",e);
            throw new RuntimeException("您的小手太快了哦");
        }
    }
    @Log(title = "用户兑换", businessType = BusinessType.INSERT)
    @PostMapping("/userSignExchange")
    @ResponseBody
    @NoRepeatSubmit
    public AjaxResult userSignExchange(@RequestBody UserSignExchangeDto dto) {
        try {
            Long appUserId = getAppUserId();
            //查询签到任务是否存在
            SystemSignTask systemSignTask = systemSignTaskService.selectSystemSignTaskById(dto.getTaskId());
            if(ObjectUtils.isEmpty(systemSignTask) || systemSignTask.getTaskStatus().equals(1)){
                return AjaxResult.error("兑换任务已下架");
            }
           //查询用户账户信息
            UserAccount userAccount = userAccountService.selectUserAccountByUserId(appUserId);
            if(ObjectUtils.isEmpty(userAccount)){
                return AjaxResult.error("账户信息有误，请联系管理员");
            }
            if(userAccount.getUserAccount() < systemSignTask.getTaskYidouCount()){
                return AjaxResult.error("账户亦豆不足");
            }
            //扣除亦豆
            ExecuteUserAccountDto dto1=new ExecuteUserAccountDto();
            dto1.setUserId(getAppUserId());
            dto1.setUserAccount(systemSignTask.getTaskYidouCount());
            dto1.setUserType(0);
            dto1.setAccountSysType(0);
            dto1.setAccountName("兑换任务");
            dto1.setAccountType(1);
            userAccountService.executeUserAccount(dto1);
            //生成签到任务
            UserSignInTask userTask=new UserSignInTask();
            userTask.setUserId(appUserId);
            userTask.setTaskId(systemSignTask.getId());
            userTask.setTaskName(systemSignTask.getTaskName());
            userTask.setTaskStartTime(new Date());
            userTask.setTaskEndTime(DateUtils.addDayTime(new Date(),Integer.parseInt(systemSignTask.getTaskCycleCount()+"")));
            userTask.setTaskStatus(0L);
            userTask.setTotalReward(systemSignTask.getTotalReward());
            userTask.setTaskCycleCount(systemSignTask.getTaskCycleCount());
            userTask.setTaskYidouCount(systemSignTask.getTaskYidouCount());
            userTask.setActivityLevel(systemSignTask.getActivityLevel());
            userSignInTaskService.insertUserSignInTask(userTask);
            redisUtils.del("xiyi:UserSignInTask:" +appUserId);
            return AjaxResult.success("恭喜兑换成功");
        }catch (Exception e){
            log.error("用户兑换异常：{}",e);
            throw new RuntimeException("用户兑换异常，请稍后再试");
        }
    }
}
