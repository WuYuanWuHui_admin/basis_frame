package com.ruoyi.project.bajiaostar.api.entity.dto.sign;

import lombok.Data;

import java.io.Serializable;

/**
 * 签到
 */
@Data
public class UserSignInDto implements Serializable {
    //签到任务id
    private Long userSignInId;
}
