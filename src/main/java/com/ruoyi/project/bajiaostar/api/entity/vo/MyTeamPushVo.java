package com.ruoyi.project.bajiaostar.api.entity.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class MyTeamPushVo implements Serializable {

    //头像
    private String userPhto;
    //用户名
    private String userName;
    //手机号码
    private String userPhone;
    //是否实名认证 0 否  1 是
    private Integer realNameAuthFlag;
    //注册时间
    private String registerDate;
    //团队人数
    private Integer teamTotal;
    //团队活跃人数 1
    private Integer teamActivityTotal;
    //个人活跃度
    private  Integer userActivity;
    //团队活跃度
    private  Integer teamActivity;
    //是否有效 0 否 1 是 （是否当日签到）
    private  Integer validFlag;

}
