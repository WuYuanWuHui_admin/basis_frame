package com.ruoyi.project.bajiaostar.api.entity.dto.user;

import lombok.Data;

import java.io.Serializable;

@Data
public class UserAuthDto implements Serializable {
    //真实姓名
    private String realName;
    //身份证号码
    private String idNum;

}
