package com.ruoyi.project.bajiaostar.api;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.XmlUtil;
import cn.hutool.crypto.digest.DigestUtil;
import com.alibaba.druid.support.json.JSONUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.constant.SystemContstant;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.project.bajiaostar.api.entity.dto.LakalaNotifyUrlDto;
import com.ruoyi.project.bajiaostar.paymenrequest.domain.Paymenrequest;
import com.ruoyi.project.bajiaostar.paymenrequest.service.IPaymenrequestService;
import com.ruoyi.project.bajiaostar.user.service.IUserAppService;
import com.ruoyi.project.content.FlowTypeEnum;
import com.ruoyi.project.payUtils.leshua.model.LsNotifyParam;
import com.ruoyi.project.payUtils.wx.WxPayUtils;
import com.ruoyi.project.payUtils.wx.sdk.WXPayUtil;
import com.ruoyi.project.payUtils.zfb.model.AlipayNotifyParam;
import com.ruoyi.project.utils.RedisUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Controller
@RequestMapping ("/nt/pay")
public class PayNotifyController {

    private static final Logger log = LoggerFactory.getLogger(PayNotifyController.class);

    @Value ("${pay.ls.secret}")
    private String SECRET;

    @Autowired
    @Qualifier ("scheduledExecutorService")
    private ScheduledExecutorService executor;

    @Autowired
    private RedisUtils redisUtils;

    @Autowired
    private WxPayUtils wxPayUtils;

    @Autowired
    IPaymenrequestService paymenrequestService;
    @Autowired
    IUserAppService userAppService;




    /**
     * 乐刷支付回调
     *
     * @param request
     *
     * @return
     */
    @RequestMapping ("/lsNotifyUrl")
    @ResponseBody
    public String lsNotify (HttpServletRequest request) throws Exception {
        Map <String, String> params1 = convertRequestParamsToMap(request);
        log.info("乐刷支付回调:{}", JSONObject.toJSONString(params1));
        // 读取请求的输入流
        BufferedReader reader = new BufferedReader(new InputStreamReader(request.getInputStream()));
        StringBuilder requestBody = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            requestBody.append(line);
        }
        reader.close();
        // 处理请求体数据（XML）
        String strXml = requestBody.toString();
        Map <String, Object> xmlMap = XmlUtil.xmlToMap(strXml);
        List <String> linkParam = new ArrayList <>();
        xmlMap.keySet().stream().sorted().forEach(k -> {
            if (!"sign".equals(k)) {
                linkParam.add(StringUtils.format("{}={}", k, xmlMap.get(k)));
            }
        });
        String xmlLink = linkParam.stream().collect(Collectors.joining("&"));
        String sign = DigestUtil.md5Hex(xmlLink + "&key=" + this.SECRET).toUpperCase();
        LsNotifyParam lsNotifyParam = BeanUtil.fillBeanWithMap(xmlMap, LsNotifyParam.builder().build(), false);
        if (!sign.equals(lsNotifyParam.getSign())) {
            throw new RuntimeException("签名不正确");
        }
        String status = this.redisUtils.get(lsNotifyParam.getOut_trade_no());
        if (StringUtils.isNotBlank(status)) {
            if (!"1".equals(status)) {
                throw new RuntimeException("重复请求");
            }
        }
        String outTradeNo = lsNotifyParam.getOut_trade_no();
        String[] nos = outTradeNo.split("-");

        return "success";
    }

    private Integer payType (String nos) {
        Integer paytype = 1;
        switch (FlowTypeEnum.findByValue(Integer.valueOf(nos))) {
            case ALI_PAY_BOND:
            case ALI_PAY_GOLD_MEDAL:
            case ALI_PAY_YUEKA_MEDAL:
            case ALI_PAY_RENSHU_MEDAL:
            case ALI_PAY_SETTLEMENT:
            case ALI_PAY_RECHARGE:
            case ALI_PAY_SINGLE_SETTLEMENT:
            case ALI_PAY_PENALTY:
                paytype = 0;
                break;
        }
        return paytype;
    }

    /**
     * 支付宝支付回调
     *
     * @param request
     *
     * @return
     */
    @RequestMapping ("/notify_url")
    @ResponseBody
    public String articleByTypeName (HttpServletRequest request) throws Exception {

        return null;

    }

    @RequestMapping (value = "/wx_notify_url", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String wxNotifyUrl (HttpServletRequest request, HttpServletResponse response) throws Exception {
        //进入回调成功
        log.info("微信支付回调");
        InputStream inStream = request.getInputStream();
        ByteArrayOutputStream outSteam = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int len = 0;
        while ((len = inStream.read(buffer)) != -1) {
            outSteam.write(buffer, 0, len);
        }
        String resultxml = new String(outSteam.toByteArray(), "utf-8");
        Map <String, String> params = WXPayUtil.xmlToMap(resultxml);
        outSteam.close();
        inStream.close();
        Map <String, String> return_data = new HashMap <>();
        log.info("------------回调数据查看：" + JSONUtils.toJSONString(params));

        Thread.sleep(150);
        if (!this.isTenpaySign(params)) {
            // 支付失败
            return_data.put("return_code", "FAIL");
            return_data.put("return_msg", "return_code不正确");
            return WXPayUtil.mapToXml(return_data);
        }
        String outTradeNo = params.get("out_trade_no");
       /* //检查是否最新支付逻辑
        TPaymentRequest paymentRequest = new TPaymentRequest();
        paymentRequest.setServiceOrderNo(outTradeNo);
        paymentRequest.setStatus(0);
        //检查是否存在支付中信息
        paymentRequest = this.paymentRequestService.selectPaymentRequestByServiceOrderNo(paymentRequest);
        if(ObjectUtils.isEmpty(paymentRequest)){
            return_data.put("return_code", "FAIL");
            return_data.put("return_msg", "订单不存在,或已处理");
            return WXPayUtil.mapToXml(return_data);
        }
        if(paymentRequest.getServiceType().equals(1)){
            JSONObject json = JSONObject.parseObject(paymentRequest.getReqParams());
            TGoodOrder goodOrder= JSON.toJavaObject(json, TGoodOrder.class);
            goodOrderService.GoodOrderPayCallback(goodOrder);
        }
        paymentRequest.setStatus(1);
        paymentRequest.setCompletedTime(new Date());
        paymentRequestService.updateTPaymentRequest(paymentRequest);*/
        return_data.put("return_code", "SUCCESS");
        return_data.put("return_msg", "OK");
        return WXPayUtil.mapToXml(return_data);
    }

    @RequestMapping (value = "/wx_refund_notify_url", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String wxRefundNotifyUrl (HttpServletRequest request, HttpServletResponse response) throws Exception {
        //进入回调成功
        log.info("微信退款回调");
        InputStream inStream = request.getInputStream();
        ByteArrayOutputStream outSteam = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int len = 0;
        while ((len = inStream.read(buffer)) != -1) {
            outSteam.write(buffer, 0, len);
        }
        String resultxml = new String(outSteam.toByteArray(), "utf-8");
        Map <String, String> params = WXPayUtil.xmlToMap(resultxml);
        outSteam.close();
        inStream.close();
        Map <String, String> return_data = new HashMap <String, String>();
        log.info("------------回调数据查看：" + JSONUtils.toJSONString(params));

        //        NotifyRecord notifyRecord=new NotifyRecord();
        //        notifyRecord.setOutTradeNo(params.get("out_trade_no"));
        //        notifyRecord.setNotifyRecord(JSONUtils.toJSONString(params));
        //        notifyRecord.setType(1);
        //        notifyRecordService.insertNotifyRecord(notifyRecord);


        //TODO 不处理回调 直接返回成功

        return_data.put("return_code", "SUCCESS");
        return_data.put("return_msg", "OK");
        return WXPayUtil.mapToXml(return_data);
    }

    /**
     * 将request中的参数转换成Map
     *
     * @param request
     *
     * @return
     */
    private static Map <String, String> convertRequestParamsToMap (HttpServletRequest request) {
        Map <String, String> retMap = new HashMap <>();
        Set <Map.Entry <String, String[]>> entrySet = request.getParameterMap().entrySet();
        for (Map.Entry <String, String[]> entry : entrySet) {
            String name = entry.getKey();
            String[] values = entry.getValue();
            int valLen = values.length;

            if (valLen == 1) {
                retMap.put(name, values[0]);
            } else if (valLen > 1) {
                StringBuilder sb = new StringBuilder();
                for (String val : values) {
                    sb.append(",").append(val);
                }
                retMap.put(name, sb.toString().substring(1));
            } else {
                retMap.put(name, "");
            }
        }
        return retMap;
    }

    private AlipayNotifyParam buildAlipayNotifyParam (Map <String, String> params) {
        String json = JSON.toJSONString(params);
        return JSON.parseObject(json, AlipayNotifyParam.class);
    }



    /**
     * 微信支付验证
     *
     * @param param
     *
     * @return
     */
    private boolean isTenpaySign (Map <String, String> param) {
        String signFromAPIResponse = param.get("sign");
        if (StringUtils.isBlank(signFromAPIResponse)) {
            log.info("API返回的数据签名数据不存在，有可能被第三方篡改!!!");
            return false;
        }
        log.debug("服务器回包里面的签名是:" + signFromAPIResponse);

        String outTradeNo = param.get("out_trade_no");
        //将API返回的数据根据用签名算法进行计算新的签名，用来跟API返回的签名进行比较
        String status = this.redisUtils.get(outTradeNo);
        if (StringUtils.isNotBlank(status)) {
            if (!"1".equals(status)) {
                log.error("微信重复请求");
                return false;
            }
        }
        //算出签名
        try {
            String resultSign = this.wxPayUtils.generateSign(param);
            boolean flag = true;//resultSign.equals(signFromAPIResponse);
            log.info("查看判断返回" + flag);
            return flag;
        } catch (Exception e) {
            log.error("微信签名验证异常", e);
            return false;
        }
    }


    @RequestMapping (value = "/lakala_notify_url", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map <String, String> lakalaNotifyUrl (@RequestBody LakalaNotifyUrlDto dto, HttpServletRequest request, HttpServletResponse response) throws Exception {
        //进入回调成功
        log.info("拉卡拉支付回调");

        //检查是否最新支付逻辑
        Paymenrequest paymentRequest = new Paymenrequest();
        paymentRequest.setServiceOrderNo(dto.getPay_order_no());
        paymentRequest.setStatus(0);
        Map <String, String> return_data = new HashMap <String, String>();
        //检查是否存在支付中信息
        paymentRequest = this.paymenrequestService.selectPaymentRequestByServiceOrderNo(paymentRequest);
        if(ObjectUtils.isEmpty(paymentRequest)){
            return_data.put("return_code", "FAIL");
            return_data.put("return_msg", "订单不存在,或已处理");
            return return_data;
        }
        if(paymentRequest.getServiceType().equals(1)){
            //JSONObject json = JSONObject.parseObject(paymentRequest.getReqParams());
            //更新用户会员状态
            userAppService.updateUserCard(paymentRequest.getUserId(), BigDecimal.ZERO);
        }
        paymentRequest.setStatus(1);
        paymentRequest.setCompletedTime(new Date());
        paymenrequestService.updatePaymenrequest(paymentRequest);

        return_data.put("return_code", "SUCCESS");
        return_data.put("return_msg", "OK");
        return return_data;
    }

}
