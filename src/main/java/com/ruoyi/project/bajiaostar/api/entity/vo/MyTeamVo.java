package com.ruoyi.project.bajiaostar.api.entity.vo;

import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import lombok.Data;

import java.io.Serializable;

/**
 * 我的团队
 */
@Data
public class MyTeamVo implements Serializable {

    //"用户身份：0： 普通1：  1星 2 ： 2星3 ：3星4 ： 4星5： 5星6  ：6星7 ： 7 星8： 8星9 ：9星10 ：10星"
    private Integer userIdentityLevel;
    //是否实名认证 0 否  1 是
    private Integer realNameAuthFlag;
    //总活跃度
    private Integer  activityTotal;
    //小区活跃度 1
    private Integer  activityCommunity;
    //我的直推
    private Integer myDirectPush;
    //团队人数
    private Integer teamTotal;
    //团队活跃人数 1
    private Integer teamActivityTotal;
    //下一身份所需要总活跃度
    private Integer lowerAuthActivityTotal;
    //下一身份所需要小区活跃度
    private Integer  lowerAuthActivityCommunity;
    //下一身份所需要我的直推
    private Integer lowerAuthMyDirectPush;
    //"用户身份：0： 普通1：  1星 2 ： 2星3 ：3星4 ： 4星5： 5星6  ：6星7 ： 7 星8： 8星9 ：9星10 ：10星"
    private Integer lowerUserIdentityLevel;

    //我的上级
    private  MyUserPushVo userPushVo;


    //市场架构
   private Integer userLevelDirectPush;
    //下一级市场架构
    private Integer lowerUserLevelDirectPush;
    //用户礼包
    private int userTaskCount;
    //是否购买年卡 0 未购买 1 已购买
    private Integer cardFlag;

    //直推商户人员
    private int userMerchantDirectPush ;
    //下一级直推商户人员
    private int lowerUserMerchantDirectPush ;


}
