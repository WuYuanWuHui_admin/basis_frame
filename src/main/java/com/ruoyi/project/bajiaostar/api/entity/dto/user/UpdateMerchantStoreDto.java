package com.ruoyi.project.bajiaostar.api.entity.dto.user;

import lombok.Data;

import java.io.Serializable;

@Data
public class UpdateMerchantStoreDto implements Serializable {
    //门店名称
    private String storeName;
}
