package com.ruoyi.project.bajiaostar.api.entity.dto.user;

import lombok.Data;

import java.io.Serializable;

@Data
public class AddMerchangoodReservationDto implements Serializable {
    //产品id
    private Long goodId;
}
