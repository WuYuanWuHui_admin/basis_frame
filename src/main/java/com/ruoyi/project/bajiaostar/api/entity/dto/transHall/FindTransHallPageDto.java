package com.ruoyi.project.bajiaostar.api.entity.dto.transHall;

import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import lombok.Data;

import java.io.Serializable;

@Data
public class FindTransHallPageDto implements Serializable {
    //页数
    private Integer pageNum;
    //条数
    private Integer pageSize;

    /** 交易类型：0 出售 , 1 求购 */
    private Integer transType;
}
