package com.ruoyi.project.bajiaostar.api.entity.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class LakalaNotifyUrlDto implements Serializable {

    /**
     *
     */
    private String channel_id;
    /**
     *
     */
    private String merchant_no;
    /**
     *
     */
    private String order_create_time;
    /**
     *
     */
    private String order_efficient_time;
    /**
     *
     */
    private String order_status;
    /**
     *
     */

    private OrderTradeInfo order_trade_info;
    /**
     *
     */
    private String out_order_no;
    /**
     *
     */
    private String pay_order_no;
    /**
     *
     */
    private List<SplitInfo> split_info;
    /**
     *
     */
    private String split_mark;
    /**
     *
     */
    private String term_no;
    /**
     *
     */
    private String total_amount;
    /**
     *
     */
    private String trans_merchant_no;
    /**
     *
     */
    private String trans_term_no;
}
