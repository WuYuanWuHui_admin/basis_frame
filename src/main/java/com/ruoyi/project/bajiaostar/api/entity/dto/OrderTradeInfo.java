package com.ruoyi.project.bajiaostar.api.entity.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class OrderTradeInfo implements Serializable {

    private String acc_trade_no;
    private String acc_type;
    private String busi_type;
    private String log_no;
    private String pay_mode;
    private String payer_amount;
    private String settle_merchant_no;
    private String settle_term_no;
    private String trade_amount;
    private String trade_no;
    private String trade_status;
    private String trade_time;
    private String trade_type;
    private String user_id1;
    private String user_id2;
}
