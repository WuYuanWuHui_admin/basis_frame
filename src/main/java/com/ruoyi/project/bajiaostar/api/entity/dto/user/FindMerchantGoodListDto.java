package com.ruoyi.project.bajiaostar.api.entity.dto.user;

import lombok.Data;

import java.io.Serializable;

@Data
public class FindMerchantGoodListDto implements Serializable {
    //商户用户id
    private Long merchantUserId;
    //店铺名称
    private String storeName;
}
