package com.ruoyi.project.bajiaostar.api.entity.dto.transHall;

import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class AddUserTransHallDto implements Serializable {

    //用户id
    private Long userId;

    /** 交易类型：0 出售 , 1 求购 */
    private Integer transType;

    /** 截止日期 */
    private Date emdTime;

    /** 亦豆 */
    private Double yidouAccount;

    /** 金额 */
    private Double account;
}
