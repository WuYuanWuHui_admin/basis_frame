package com.ruoyi.project.bajiaostar.api.entity.dto.sign;

import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import lombok.Data;

import java.io.Serializable;

@Data
public class FindUserSignTaskListDto implements Serializable {
    /** 任务状态：0 未结束 1 已结束 */
    private Long taskStatus;
}
