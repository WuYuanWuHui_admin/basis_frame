package com.ruoyi.project.bajiaostar.api;


import com.ruoyi.common.constant.SystemContstant;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.IpUtils;
import com.ruoyi.common.utils.Md5Utils;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.framework.aop.NoRepeatSubmit;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.project.bajiaostar.api.entity.dto.draw.ExecuteUserDrawDto;
import com.ruoyi.project.bajiaostar.api.entity.dto.user.*;
import com.ruoyi.project.bajiaostar.merchangood.domain.Merchangood;
import com.ruoyi.project.bajiaostar.merchangood.service.IMerchangoodService;
import com.ruoyi.project.bajiaostar.merchangoodReservation.domain.MerchangoodReservation;
import com.ruoyi.project.bajiaostar.merchangoodReservation.service.IMerchangoodReservationService;
import com.ruoyi.project.bajiaostar.user.domain.UserApp;
import com.ruoyi.project.bajiaostar.user.service.IUserAppService;
import com.ruoyi.project.bajiaostar.userAccount.domain.UserAccount;
import com.ruoyi.project.bajiaostar.userAccount.service.IUserAccountService;
import com.ruoyi.project.bajiaostar.userBank.domain.UserBank;
import com.ruoyi.project.bajiaostar.userBank.service.IUserBankService;
import com.ruoyi.project.bajiaostar.userDraw.domain.UserDraw;
import com.ruoyi.project.bajiaostar.userDraw.service.IUserDrawService;
import com.ruoyi.project.bajiaostar.userExcitationTask.domain.UserExcitationTask;
import com.ruoyi.project.bajiaostar.userExcitationTask.service.IUserExcitationTaskService;
import com.ruoyi.project.bajiaostar.userShopFans.domain.UserShopFans;
import com.ruoyi.project.bajiaostar.userShopFans.service.IUserShopFansService;
import com.ruoyi.project.utils.RedisUtils;
import com.ruoyi.project.utils.TokenProccessor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.*;

/**
 * 用户信息
 */
@RestController
@RequestMapping("/nt/user/")
@Slf4j
public class WebUserDrawController extends BaseController {

    @Autowired
    IUserAppService userService;

    @Autowired
    IUserAccountService userAccountService;
    @Autowired
    RedisUtils redisUtils;
    @Autowired
    IUserDrawService userDrawService;
    @Autowired
    IUserBankService userBankService;

    @PostMapping("/executeUserDraw")
    @ResponseBody
    @NoRepeatSubmit
    public AjaxResult executeUserDraw(@RequestBody ExecuteUserDrawDto dto, HttpServletRequest request) {

        try {
            Long appUserId = getAppUserId();
            if(ObjectUtils.isEmpty(appUserId)){
                return AjaxResult.error("用户未登录");
            }
            if(ObjectUtils.isEmpty(dto) || ObjectUtils.isEmpty(dto.getDrawAmount())){
                return AjaxResult.error("提现金额有误");
            }
            if(dto.getDrawAmount().compareTo(BigDecimal.ONE) < 1 ){
                return AjaxResult.error("提现金额不能小于1元");
            }
            UserApp userApp = userService.selectUserById(appUserId);
            if(ObjectUtils.isEmpty(userApp)){
                return AjaxResult.error("用户不存在");
            }
            //查询是否绑定体提现信息
            UserBank userBank=new UserBank();
            userBank.setUserId(appUserId);
            userBank.setType(dto.getType());
            List<UserBank> userBankList = userBankService.selectUserBankList(userBank);
            if(ObjectUtils.isEmpty(userBankList)){
                return AjaxResult.error("请先填写提现信息");
            }
            userBank=userBankList.get(0);

            UserAccount userAccount = userAccountService.selectUserAccountByUserId(appUserId);
            if(ObjectUtils.isEmpty(userAccount)){
                return AjaxResult.error("账户异常");
            }
            //扣减余额
            ExecuteUserAccountDto dto1=new ExecuteUserAccountDto();
            if(userApp.getUserIdentity().equals(0)){
                if(userAccount.getUserCashAccount() < dto.getDrawAmount().doubleValue()){
                    return AjaxResult.error("账户余额不足");
                }
                dto1.setUserType(0);
            }else {
                if(userAccount.getMerchantCashAccount() < dto.getDrawAmount().doubleValue()){
                    return AjaxResult.error("账户余额不足");
                }
                dto1.setUserType(1);
            }

            dto1.setUserId(appUserId);
            dto1.setUserAccount(dto.getDrawAmount().doubleValue());
            dto1.setAccountName("提现冻结金额");
            dto1.setAccountSysType(1);
            dto1.setAccountType(1);
           userAccountService.executeUserAccount(dto1);
           //添加审核信息
            UserDraw userDraw=new UserDraw();
            userDraw.setAmountys(0L);
            userDraw.setUserId(appUserId);
            userDraw.setAmount(dto.getDrawAmount().longValue());
            userDraw.setStatus(0);
            userDraw.setType(0);
            userDraw.setAccount(userBank.getAccount());
            userDraw.setUserName(userBank.getUserName());
            userDrawService.insertUserDraw(userDraw);

            return AjaxResult.success("成功");
        }catch (Exception e){
            log.error("下单异常：{}",e);
            throw new RuntimeException("系统下单异常，请稍后再试");
        }
    }


    @PostMapping("/addUserDraw")
    @ResponseBody
    @NoRepeatSubmit
    public AjaxResult addUserDraw(@RequestBody ExecuteUserDrawDto dto, HttpServletRequest request) {

        try {
            Long appUserId = getAppUserId();
            if(ObjectUtils.isEmpty(appUserId)){
                return AjaxResult.error("用户未登录");
            }
            if(ObjectUtils.isEmpty(dto) || ObjectUtils.isEmpty(dto.getDrawAmount())){
                return AjaxResult.error("提现金额有误");
            }
            if(dto.getDrawAmount().compareTo(BigDecimal.ONE) == -1 ){
                return AjaxResult.error("提现金额不能小于1元");
            }
            UserApp userApp = userService.selectUserById(appUserId);
            if(ObjectUtils.isEmpty(userApp)){
                return AjaxResult.error("用户不存在");
            }
            if(ObjectUtils.isEmpty(userApp.getReanlIdnum())){
                return AjaxResult.error("请先实名后提现");
            }
            //查询是否绑定体提现信息
           /* UserBank userBank=new UserBank();
            userBank.setUserId(appUserId);
            userBank.setType(dto.getType());
            List<UserBank> userBankList = userBankService.selectUserBankList(userBank);
            if(ObjectUtils.isEmpty(userBankList)){
                return AjaxResult.error("请先填写提现信息");
            }
            userBank=userBankList.get(0);*/

            UserAccount userAccount = userAccountService.selectUserAccountByUserId(appUserId);
            if(ObjectUtils.isEmpty(userAccount)){
                return AjaxResult.error("账户异常");
            }
            //扣减余额
            ExecuteUserAccountDto dto1=new ExecuteUserAccountDto();
            if(userApp.getUserIdentity().equals(0)){
                if(userAccount.getUserCashAccount() < dto.getDrawAmount().doubleValue()){
                    return AjaxResult.error("账户余额不足");
                }
                dto1.setUserType(0);
            }else {
                if(userAccount.getMerchantCashAccount() < dto.getDrawAmount().doubleValue()){
                    return AjaxResult.error("账户余额不足");
                }
                dto1.setUserType(1);
            }

            dto1.setUserId(appUserId);
            dto1.setUserAccount(dto.getDrawAmount().doubleValue());
            dto1.setAccountName("提现冻结金额");
            dto1.setAccountSysType(1);
            dto1.setAccountType(1);
            userAccountService.executeUserAccount(dto1);
            //添加审核信息
            UserDraw userDraw=new UserDraw();
            userDraw.setCreateBy(userApp.getUserName());
            userDraw.setAmountys(0L);
            userDraw.setUserId(appUserId);
            userDraw.setAmount(dto.getDrawAmount().longValue());
            userDraw.setStatus(0);
            userDraw.setType(0);
            userDraw.setAccount(dto.getAmount());
            userDraw.setUserName(userApp.getReanlName());
            userDraw.setIdCard(userApp.getReanlIdnum());
            userDrawService.insertUserDraw(userDraw);

            return AjaxResult.success("成功");
        }catch (Exception e){
            log.error("下单异常：{}",e);
            throw new RuntimeException("系统下单异常，请稍后再试");
        }
    }


}
