package com.ruoyi.project.bajiaostar.api;


import cn.hutool.core.bean.BeanUtil;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.constant.SystemContstant;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.IpUtils;
import com.ruoyi.common.utils.Md5Utils;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.common.utils.file.FileUtils;
import com.ruoyi.common.utils.file.ImagUtils;
import com.ruoyi.framework.aop.NoRepeatSubmit;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.mongodb.bajiaostar.user.entity.UserAppEntity;
import com.ruoyi.mongodb.bajiaostar.user.service.UserAppEntityService;
import com.ruoyi.project.bajiaostar.api.entity.dto.MyTeamPushDto;
import com.ruoyi.project.bajiaostar.api.entity.dto.ResetPasswordDto;
import com.ruoyi.project.bajiaostar.api.entity.dto.user.*;
import com.ruoyi.project.bajiaostar.merchangood.domain.Merchangood;
import com.ruoyi.project.bajiaostar.merchangood.service.IMerchangoodService;
import com.ruoyi.project.bajiaostar.merchangoodReservation.domain.MerchangoodReservation;
import com.ruoyi.project.bajiaostar.merchangoodReservation.service.IMerchangoodReservationService;
import com.ruoyi.project.bajiaostar.user.domain.UserApp;
import com.ruoyi.project.bajiaostar.user.service.ITeamService;
import com.ruoyi.project.bajiaostar.user.service.IUserAppService;
import com.ruoyi.project.bajiaostar.userAccount.domain.UserAccount;
import com.ruoyi.project.bajiaostar.userAccount.service.IUserAccountService;
import com.ruoyi.project.bajiaostar.userExcitationTask.domain.UserExcitationTask;
import com.ruoyi.project.bajiaostar.userExcitationTask.service.IUserExcitationTaskService;
import com.ruoyi.project.bajiaostar.userShopFans.domain.UserShopFans;
import com.ruoyi.project.bajiaostar.userShopFans.service.IUserShopFansService;
import com.ruoyi.project.bajiaostar.userSignInTask.domain.UserSignInTask;
import com.ruoyi.project.bajiaostar.userSignInTask.service.IUserSignInTaskService;
import com.ruoyi.project.config.PosterMakerConfig;
import com.ruoyi.project.third.numericalPulse.NumericalPulseUtil;
import com.ruoyi.project.utils.PosterMakerUtil;
import com.ruoyi.project.utils.QiniuUtils;
import com.ruoyi.project.utils.RedisUtils;
import com.ruoyi.project.utils.TokenProccessor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.*;

/**
 * 用户信息
 */
@RestController
@RequestMapping("/nt/user/")
@Slf4j
public class WebUserController extends BaseController {

    @Autowired
    IUserAppService userService;
    @Autowired
    IUserAccountService userAccountService;
    @Autowired
    RedisUtils redisUtils;
    @Autowired
    NumericalPulseUtil numericalPulseUtil;
    @Autowired
    IMerchangoodService merchangoodService;
    @Autowired
    IUserShopFansService userShopFansService;
    @Autowired
    IMerchangoodReservationService merchangoodReservationService;
    @Autowired
    IUserExcitationTaskService userExcitationTaskService;
    @Autowired
    IUserSignInTaskService userSignInTaskService;
    @Autowired
    PosterMakerUtil posterMakerUtil;
    @Autowired
    private QiniuUtils qiniuUtils;
    @Autowired
    PosterMakerConfig posterMakerConfig;
    @Autowired
    UserAppEntityService userAppEntityService;
    @Autowired
    private ITeamService teamService;

    /**
     * 刷新团队信息
     */
    @GetMapping("/refreshAllTeamInfo")
    public AjaxResult refreshAllTeamInfo()
    {
        teamService.refreshAllTeamInfo();
        return AjaxResult.success();
    }

    /**
     * 刷新用户团队信息
     */
    @GetMapping("/refreshUserTeamInfo/{userId}")
    public AjaxResult refreshUserTeamInfo(@PathVariable("userId") Long userId)
    {
        teamService.refreshTeamInfo(userId);
        return AjaxResult.success();
    }

    /**
     * 刷新团队统计信息
     */
    @GetMapping("/refreshAllTeamSum")
    public AjaxResult refreshAllTeamSum()
    {
        teamService.initTeamSumCache();
        return AjaxResult.success();
    }

    /**
     * 刷新用户团队统计信息
     */
    @GetMapping("/refreshUserTeamSum/{userId}")
    public AjaxResult refreshUserTeamSum(@PathVariable("userId") Long userId)
    {
        teamService.refreshTeamSumCache(userId);
        return AjaxResult.success();
    }

    @PostMapping("/channel/register")
    @ResponseBody
    @NoRepeatSubmit
    public AjaxResult register(@RequestBody ChannelRegisterDto dto, HttpServletRequest request) {

        try {
            String ip = IpUtils.getIpAddr(request);
            if(ObjectUtils.isEmpty(dto)){
                return AjaxResult.error("参数有误");
            }
            if(ObjectUtils.isEmpty(dto.getMoble())){
                return AjaxResult.error("手机号码有误");
            }
            if(ObjectUtils.isEmpty(dto.getUserPassword())){
                return AjaxResult.error("登陆密码不能为空");
            }
            if(ObjectUtils.isEmpty(dto.getPushUserCode())){
                return AjaxResult.error("邀请码不能为空");
            }
            //检查手机号码是否注册
            UserApp userAppByMoble = userService.findUserByMoble(dto.getMoble());
            if(!ObjectUtils.isEmpty(userAppByMoble)){
                return AjaxResult.error("手机号码已注册");
            }
            //是否排线码
            boolean channelFlag = false;
            //渠道码
            String pushUserCode = null;
            //推荐用户id
            Long   pushUserId =0L;
            if(!ObjectUtils.isEmpty(dto.getPushUserCode())){
                String[] split = dto.getPushUserCode().split("-");
                //大于1 证明是排线码
                if(split.length > 1){
                    channelFlag = true;
                    pushUserId = Long.parseLong(split[0]);
                }
                pushUserCode= split[0];
            }
            UserApp userByPushUserAppCode = null;
            if(!ObjectUtils.isEmpty(pushUserCode)){
                /*
                if(channelFlag){
                    userByPushUserAppCode = userService.findUserByPushUserCode(pushUserCode);
                    if(ObjectUtils.isEmpty(userByPushUserAppCode)){
                        return AjaxResult.error("推荐码有误,请检查");
                    }
                }else {
                    List<UserAppEntity> list = userAppEntityService.findUserLevelDirectPush(pushUserId + "", false);
                   //如果是排线码查找最后一个人
//                    Long userId = userService.finduserPushMaxUserId(pushUserId);
//                    if(ObjectUtils.isEmpty(userId)){
//                        return AjaxResult.error("推荐码有误,请检查");
//                    }
                    userByPushUserAppCode= userService.selectUserById(Long.parseLong(list.get(list.size()-1).getId()));
                }
                */
                userByPushUserAppCode = userService.findUserByPushUserCode(pushUserCode);
                if(ObjectUtils.isEmpty(userByPushUserAppCode)){
                    return AjaxResult.error("推荐码有误,请检查");
                }
            }
            UserApp userApp =new UserApp();
            userApp.setMoble(dto.getMoble());
            userApp.setUserPassword(Md5Utils.hash(dto.getUserPassword()));
            userApp.setUserName(dto.getMoble());
            userApp.setCreateIp(ip);

            if(!ObjectUtils.isEmpty(userByPushUserAppCode)){
                userApp.setPushUserId(userByPushUserAppCode.getId());
                if(ObjectUtils.isEmpty(userByPushUserAppCode.getPushUserIds())){
                    userApp.setPushUserIds(userByPushUserAppCode.getId()+",");
                }else{
                    userApp.setPushUserIds(userByPushUserAppCode.getPushUserIds()+ userByPushUserAppCode.getId()+",");
                }
            }
            userApp.setChannelFlag(0);
            if(channelFlag){
                userApp.setChannelFlag(1);
            }
            userApp.setPushCode(generateRecommendationCode());
            userApp.setUserStatus(0);
            userApp.setUserIdentity(0);
            userApp.setUserIdentityLevel(0);
            userApp.setUserIdentityLevel1(0);
            userApp.setCardFlag(0);
            int i = userService.insertUser(userApp);
            if(i < 1){
                return AjaxResult.error("注册失败");
            }
            //加入mongodb
            UserAppEntity entity=new UserAppEntity();
            BeanUtil.copyProperties(userApp,entity);
            //userAppEntityService.add(entity);

            //加入Redis
            teamService.refreshTeamInfo(userApp);
            teamService.refreshTeamSumCache(userApp);

            //注册用户账户信息
            UserAccount account=new UserAccount();
            account.setUserId(userApp.getId());
            account.setMerchantAccount(0d);
            account.setMerchantCashAccount(0d);
            account.setUserAccount(0d);
            int i1 = userAccountService.insertUserAccount(account);
            if(i1 < 1){
                return AjaxResult.error("注册失败");
            }
            return AjaxResult.success("成功");
        }catch (Exception e){
            log.error("下单异常：{}",e);
            throw new RuntimeException("系统下单异常，请稍后再试");
        }
    }



    @PostMapping("/login")
    @ResponseBody
    @NoRepeatSubmit
    public AjaxResult login(@RequestBody LoginDto dto) {

        try {
            if(ObjectUtils.isEmpty(dto)){
                return AjaxResult.error("参数有误");
            }
            if(ObjectUtils.isEmpty(dto.getMoble())){
                return AjaxResult.error("手机号码有误");
            }
            if(ObjectUtils.isEmpty(dto.getUserPassword())){
                return AjaxResult.error("登陆密码不能为空");
            }
            //检查手机号码
            UserApp userAppByMoble = userService.findUserByMoble(dto.getMoble());
            if(ObjectUtils.isEmpty(userAppByMoble)){
                return AjaxResult.error("账号未注册,请先注册");
            }
            String hashPassword = Md5Utils.hash(dto.getUserPassword());
            if(!hashPassword.equals(userAppByMoble.getUserPassword())){
                return AjaxResult.error("账号密码不正确");
            }
            if(!userAppByMoble.getUserStatus().equals(0)){
                return AjaxResult.error("账号状态异常");
            }
            String token = Md5Utils.hash(TokenProccessor.getInstance().makeToken());
            redisUtils.set(SystemContstant.TOKEN + token, userAppByMoble.getId(),2592000L);
            redisUtils.set(SystemContstant.TOKEN + "user:"+token, userAppByMoble,2592000L);
            Map<String, Object> header = new HashMap<>();
            header.put("token", SystemContstant.TOKEN +token);
            header.put("user", userAppByMoble);
            header.put("userType", userAppByMoble.getUserIdentity());

            return AjaxResult.success("成功",header);
        }catch (Exception e){
            log.error("用户登陆异常：{}",e);
            throw new RuntimeException("用户登陆异常，请稍后再试");
        }
    }



    @PostMapping("/getUserDeatil")
    @ResponseBody
    public AjaxResult getUserDeatil() {
        try {
            Long appUserId = getAppUserId();
            Object objKey = redisUtils.getObjKey("xiyi:user:detail:" + appUserId);
            if(!ObjectUtils.isEmpty(objKey)){
                return AjaxResult.success(objKey);
            }
            UserApp userApp = userService.selectUserById(appUserId);
            redisUtils.set("xiyi:user:detail:" + appUserId,userApp,259200L);
            return AjaxResult.success(userApp);
        }catch (Exception e){
            log.error("用户信息异常：{}",e);
            throw new RuntimeException("用户信息异常，请稍后再试");
        }
    }


    @PostMapping("/getUserAccountDeatil")
    @ResponseBody
    public AjaxResult getUserAccountDeatil() {
        try {
            Long appUserId = getAppUserId();
            Object objKey = redisUtils.getObjKey("xiyi:user:UserAccount:" + appUserId);
            if(!ObjectUtils.isEmpty(objKey)){
                return AjaxResult.success(objKey);
            }
            UserAccount userAccount = userAccountService.selectUserAccountByUserId(getAppUserId());
            redisUtils.set("xiyi:user:UserAccount:" + appUserId,userAccount,259200L);
            return AjaxResult.success(userAccount);
        }catch (Exception e){
            log.error("用户信息详情异常：{}",e);
            throw new RuntimeException("用户信息详情异常，请稍后再试");
        }
    }


    @PostMapping("/userAuth")
    @ResponseBody
    @NoRepeatSubmit
    public AjaxResult userAuth(@RequestBody UserAuthDto dto, HttpServletRequest request) {

        try {
            if(ObjectUtils.isEmpty(dto)){
                return AjaxResult.error("参数有误");
            }
            if(ObjectUtils.isEmpty(dto.getRealName())){
                return AjaxResult.error("认证真实姓名不能为空");
            }
            if(ObjectUtils.isEmpty(dto.getIdNum())){
                return AjaxResult.error("认证身份证号码不能为空");
            }
            UserApp userApp = userService.selectUserById(getAppUserId());
            if(ObjectUtils.isEmpty(userApp)){
                return AjaxResult.error("用户信息不存在");
            }
            //判断是否已认证
            if(!ObjectUtils.isEmpty(userApp.getReanlName())){
                return AjaxResult.error("已认证，请勿重复认证");
            }

            //查询认证是否存在两条以上
            UserApp app=new UserApp();
            app.setReanlIdnum(dto.getIdNum());
            List<UserApp> appList = userService.selectUserList(app);


            userApp.setReanlName(dto.getRealName());
            userApp.setReanlIdnum(dto.getIdNum());
            int i = userService.updateUser(userApp);
            if(i < 1){
                return AjaxResult.error("认证失败，请重新认证");
            }
            UserAppEntity byId = userAppEntityService.findById(userApp.getId() + "");
            byId.setSfz(dto.getIdNum());
            userAppEntityService.add(byId);

            //重复认证不给免费礼包
            if(ObjectUtils.isEmpty(appList)){
                //生成签到任务
                UserSignInTask userTask=new UserSignInTask();
                userTask.setUserId(userApp.getId());
                userTask.setTaskId(0L);
                userTask.setTaskName("免费奖励签到任务");
                userTask.setTaskStartTime(new Date());
                userTask.setTaskEndTime(DateUtils.addDayTime(new Date(),30));
                userTask.setTaskStatus(0L);
                userTask.setTotalReward(12L);
                userTask.setTaskCycleCount(30L);
                userTask.setTaskYidouCount(0d);
                userTask.setActivityLevel(0d);
                userSignInTaskService.insertUserSignInTask(userTask);
            }
            redisUtils.del("xiyi:user:UserAccount:" + userApp.getId());
            return AjaxResult.success("成功", userApp);
        }catch (Exception e){
            log.error("用户认证异常：{}",e);
            throw new RuntimeException("用户认证异常，请稍后再试");
        }
    }
    @PostMapping("/updateMerchantStore")
    @ResponseBody
    @NoRepeatSubmit
    public AjaxResult updateMerchantStore(@RequestBody UpdateMerchantStoreDto dto) {
        try {
            if(ObjectUtils.isEmpty(dto)){
                return AjaxResult.error("参数有误");
            }
            if(ObjectUtils.isEmpty(dto.getStoreName())){
                return AjaxResult.error("门店名称不能为空");
            }

            Long userId = getAppUserId();
            UserApp userApp = userService.selectUserById(userId);
            if(ObjectUtils.isEmpty(userApp)){
                return AjaxResult.error("用户信息不存在");
            }
            userApp.setUserIdentity(1);
            userApp.setStoreName(dto.getStoreName());
            int i = userService.updateUser(userApp);
            if(i < 1){
                return AjaxResult.error("修改门店名称失败，请重新认证");
            }
            redisUtils.del("xiyi:user:UserAccount:" + userApp.getId());
            return AjaxResult.success("成功", userApp);
        }catch (Exception e){
            log.error("修改门店名称失败异常：{}",e);
            throw new RuntimeException("修改门店名称失败异常，请稍后再试");
        }
    }

    @PostMapping("/merchantAuth")
    @ResponseBody
    @NoRepeatSubmit
    public AjaxResult merchantAuth(@RequestBody MerchantAuthDto dto, HttpServletRequest request) {
        try {
            if(ObjectUtils.isEmpty(dto)){
                return AjaxResult.error("参数有误");
            }
            if(ObjectUtils.isEmpty(dto.getRealName())){
                return AjaxResult.error("认证真实姓名不能为空");
            }
            if(ObjectUtils.isEmpty(dto.getIdNum())){
                return AjaxResult.error("认证身份证号码不能为空");
            }
            if(ObjectUtils.isEmpty(dto.getStoreIdentity())){
                return AjaxResult.error("认证商家级别不能为空");
            }
            if(!dto.getStoreIdentity().equals(0)){
                if(ObjectUtils.isEmpty(dto.getBusinessLicenseUrl())){
                    return AjaxResult.error("请上传营业执照");
                }
                if(ObjectUtils.isEmpty(dto.getStorefrontPhotoUrl())){
                    return AjaxResult.error("请上传门头照");
                }
            }
            Long userId = getAppUserId();
            //判断商户亦豆是否足够
            UserAccount userAccount = userAccountService.selectUserAccountByUserId(userId);
            if(userAccount.getMerchantAccount() <100d){
                return AjaxResult.error("商户亦豆余额不足，请购买后重新提交认证");
            }
            //扣减账户余额明细
            ExecuteUserAccountDto dto1=new ExecuteUserAccountDto();
            dto1.setUserId(userId);
            dto1.setUserAccount(100d);
            dto1.setUserType(1);
            dto1.setAccountSysType(0);
            dto1.setAccountName("商户认证费用");
            dto1.setAccountType(1);
            userAccountService.executeUserAccount(dto1);

            UserApp userApp = userService.selectUserById(userId);
            if(ObjectUtils.isEmpty(userApp)){
                return AjaxResult.error("用户信息不存在");
            }
            userApp.setUserIdentity(2);
            userApp.setStoreIdentity(dto.getStoreIdentity());
            userApp.setStorefrontPhotoUrl(dto.getStorefrontPhotoUrl());
            userApp.setBusinessLicenseUrl(dto.getBusinessLicenseUrl());
            userApp.setReanlName(dto.getRealName());
            userApp.setReanlIdnum(dto.getIdNum());
            int i = userService.updateUser(userApp);
            if(i < 1){
                return AjaxResult.error("认证失败，请重新认证");
            }
            redisUtils.del("xiyi:user:UserAccount:" + userApp.getId());
            return AjaxResult.success("成功", userApp);
        }catch (Exception e){
            log.error("商户认证异常：{}",e);
            throw new RuntimeException("商户认证异常，请稍后再试");
        }
    }



    @PostMapping("/findMerchantList")
    @ResponseBody
    public AjaxResult findMerchantList(@RequestBody FindMerchantListDto dto) {
        try {
            Object objKey = redisUtils.getObjKey("xiyi:MerchantList:" + dto.getStoreName());
            if(!ObjectUtils.isEmpty(objKey)){
                return AjaxResult.success("成功",objKey);
            }
            List<UserApp> merchantList = userService.findMerchantList(dto);
            if(!ObjectUtils.isEmpty(merchantList)){
                for (UserApp a:merchantList) {
                    a.setSalesVolume(55L);
                    a.setFansCount(1607L);
                    a.setCommissionAmount(5580L);
                }
            }
            redisUtils.set("xiyi:MerchantList:" + dto.getStoreName(),merchantList,86400L);
            return AjaxResult.success("成功",merchantList);
        }catch (Exception e){
            log.error("用户登陆异常：{}",e);
            throw new RuntimeException("获取商户列表异常，请稍后再试");
        }
    }


    @PostMapping("/findMerchantGoodList")
    @ResponseBody
    public AjaxResult findMerchantGoodList(@RequestBody FindMerchantGoodListDto dto, HttpServletRequest request) {
        try {
            Merchangood merchangood=new Merchangood();
            merchangood.setUserId(dto.getMerchantUserId());
            merchangood.setGoodStatus(1);
            List<Merchangood> merchangoods = merchangoodService.selectMerchangoodList(merchangood);
            if(!ObjectUtils.isEmpty(merchangoods)){
                for (Merchangood a:merchangoods) {
                    a.setGoodPinCount(55L);
                    a.setFansCount(1607L);
                    a.setCommissionAmount(5580L);
                }
            }
            return AjaxResult.success("成功", merchangoods);
        } catch (Exception e) {
            log.error("用户登陆异常：{}", e);
            throw new RuntimeException("获取商户商品列表异常，请稍后再试");
        }
    }


    @PostMapping("/selsectMerchantGoodList")
    @ResponseBody
    public AjaxResult selsectMerchantGoodList() {
        try {

            Merchangood merchangood=new Merchangood();
            merchangood.setUserId(getAppUserId());
            List<Merchangood> merchangoods = merchangoodService.selectMerchangoodList(merchangood);
            return AjaxResult.success("成功", merchangoods);
        } catch (Exception e) {
            log.error("用户登陆异常：{}", e);
            throw new RuntimeException("获取商户商品列表异常，请稍后再试");
        }
    }
    @PostMapping("/findGood")
    @ResponseBody
    public AjaxResult findGood(@RequestBody FindGoodDto dto, HttpServletRequest request) {
        try {
            if (ObjectUtils.isEmpty(dto) || ObjectUtils.isEmpty(dto.getMerchantUserId())) {
                return AjaxResult.error("参数信息有误");
            }

            UserApp userApp = userService.selectUserById(dto.getMerchantUserId());
            if (ObjectUtils.isEmpty(userApp)) {
                return AjaxResult.error("用户信息不存在");
            }
            List<Merchangood> merchangoodList = new ArrayList<>();
            if (ObjectUtils.isEmpty(dto.getGoodId())) {
                Merchangood merchangood = new Merchangood();
                merchangood.setUserId(dto.getMerchantUserId());
                merchangood.setGoodStatus(1);
                List<Merchangood> merchangoods = merchangoodService.selectMerchangoodList(merchangood);
                merchangoodList.addAll(merchangoods);
            } else {
                Merchangood merchangood = merchangoodService.selectMerchangoodById(dto.getGoodId());
                merchangoodList.add(merchangood);
            }

            try {
                UserShopFans userShopFans = new UserShopFans();
                userShopFans.setShopUserId(dto.getMerchantUserId());
                userShopFans.setUserId(getAppUserId());
                userShopFansService.insertUserShopFans(userShopFans);
            } catch (Exception e) {
                log.info("添加关注失败:{}", e);
            }
            Map<String,Object> hash=new HashMap<>();
            hash.put("userApp",userApp);
            hash.put("merchangoodList",merchangoodList);
            return AjaxResult.success("成功", hash);
        } catch (Exception e) {
            log.error("用户登陆异常：{}", e);
            throw new RuntimeException("获取商户商品列表异常，请稍后再试");
        }
    }


    @PostMapping("/findGoodById/{id}")
    @ResponseBody
    public AjaxResult findGoodById(@PathVariable("id") Long id, HttpServletRequest request) {
        try {
            Merchangood merchangood = merchangoodService.selectMerchangoodById(id);
            if (!ObjectUtils.isEmpty(id)) {
                try {
                    UserShopFans userShopFans = new UserShopFans();
                    userShopFans.setShopUserId(merchangood.getUserId());
                    userShopFans.setUserId(getAppUserId());
                    userShopFansService.insertUserShopFans(userShopFans);
                }catch (Exception e){
                    log.info("添加关注失败:{}",e);
                }
            }
            return AjaxResult.success("成功",merchangood);
        }catch (Exception e){
            log.error("用户登陆异常：{}",e);
            throw new RuntimeException("获取商户商品列表异常，请稍后再试");
        }
    }


    @PostMapping("/updateMerchangood")
    @ResponseBody
    @NoRepeatSubmit
    public AjaxResult updateMerchangood(@RequestBody Merchangood merchangood) {
        try {
            merchangoodService.updateMerchangood(merchangood);
            return AjaxResult.success("成功");
        }catch (Exception e){
            log.error("修改商品信息异常：{}",e);
            throw new RuntimeException("修改商品信息异常，请稍后再试");
        }
    }


    @PostMapping("/addMerchangood")
    @ResponseBody
    @NoRepeatSubmit
    public AjaxResult addMerchangood(@RequestBody Merchangood merchangood) {
        try {
            if(ObjectUtils.isEmpty(merchangood)){
                return AjaxResult.error("商品信息有误");
            }
            merchangood.setUserId(getAppUserId());
            merchangoodService.insertMerchangood(merchangood);
            return AjaxResult.success("成功");
        }catch (Exception e){
            log.error("添加商品信息异常：{}",e);
            throw new RuntimeException("添加商品信息异常，请稍后再试");
        }
    }



    @PostMapping("/addMerchangoodReservation")
    @ResponseBody
    @NoRepeatSubmit
    public AjaxResult addMerchangoodReservation(@RequestBody AddMerchangoodReservationDto dto, HttpServletRequest request) {
        try {
            if(ObjectUtils.isEmpty(dto) || ObjectUtils.isEmpty(dto.getGoodId())){
                return AjaxResult.error("预约产品不能为空");
            }
            Merchangood merchangood = merchangoodService.selectMerchangoodById(dto.getGoodId());
           if(ObjectUtils.isEmpty(merchangood)){
               return AjaxResult.error("预约产品不存在");
           }
           if(!merchangood.getGoodStatus().equals(1)){
               return AjaxResult.error("产品状态有误,请联系客服人员");
           }

            MerchangoodReservation merchan=new MerchangoodReservation();
            merchan.setUserId(getAppUserId());
            merchan.setGoodId(merchangood.getId());
            merchan.setWriteOffStatus(0);
            merchangoodReservationService.insertMerchangoodReservation(merchan);
            return AjaxResult.success("成功");
        }catch (Exception e){
            log.error("商品预约异常：{}",e);
            throw new RuntimeException("商品预约异常，请稍后再试");
        }
    }



    @PostMapping("/merchangoodReservationWriteOff")
    @ResponseBody
    @NoRepeatSubmit
    public AjaxResult merchangoodReservationWriteOff(@RequestBody MerchangoodReservationWriteOffDto dto, HttpServletRequest request) {
        try {
            if(ObjectUtils.isEmpty(dto) || ObjectUtils.isEmpty(dto.getMerchangoodReservationId())){
                return AjaxResult.error("核销预约不能为空");
            }
            Long appUserId = getAppUserId();
            UserApp userApp = userService.selectUserById(appUserId);
            if(!userApp.getUserIdentity().equals(1)){
                return AjaxResult.error("非商家,不能核销");
            }
            MerchangoodReservation merchangoodReservation = merchangoodReservationService.selectMerchangoodReservationById(dto.getMerchangoodReservationId());
            if(ObjectUtils.isEmpty(merchangoodReservation)){
                return AjaxResult.error("预约信息不存在");
            }
            if(!merchangoodReservation.getWriteOffStatus().equals(0)){
                return AjaxResult.error("预约信息状态有误,请联系客服人员");
            }
            //查询商品
            Merchangood merchangood1 = merchangoodService.selectMerchangoodById(merchangoodReservation.getGoodId());
            if(ObjectUtils.isEmpty(merchangood1)){
                return AjaxResult.error("商品信息有误,请联系客服人员");
            }
            if(!merchangood1.getUserId().equals(appUserId)){
                return AjaxResult.error("信息有误,请到指定门店核销");
            }

            merchangoodReservation.setWriteOffStatus(1);
            merchangoodReservation.setWriteOffUserId(getAppUserId());
            merchangoodReservation.setWriteOffTime(new Date());
            merchangoodReservationService.updateMerchangoodReservation(merchangoodReservation);

            //给商家激励
            UserExcitationTask task=new UserExcitationTask();
            task.setUserId(getAppUserId());
            task.setTaskStatus(0);
            task.setTaskAccount(0.66d);
            task.setTaskEndTime(DateUtils.addDayTime(new Date(),31));
            task.setUserIdentity(1);
            task.setTaskType(0);
            task.setTaskName("核销激励");
            userExcitationTaskService.insertUserExcitationTask(task);

            //获取商品数据
            Merchangood merchangood = merchangoodService.selectMerchangoodById(merchangoodReservation.getGoodId());
            return AjaxResult.success("成功",merchangood);
        }catch (Exception e){
            log.error("商品核销异常：{}",e);
            throw new RuntimeException("商品核销异常，请稍后再试");
        }
    }




    /**
     * 查询用户商品预约列表
     */
    @PostMapping("/findMerchangoodReservationlist")
    @ResponseBody
    public TableDataInfo findMerchangoodReservationlist()
    {
        MerchangoodReservation  merchangoodReservation= new MerchangoodReservation();
        merchangoodReservation.setUserId(getAppUserId());
        startPage();
        List<MerchangoodReservation> list = merchangoodReservationService.selectMerchangoodReservationList(merchangoodReservation);
        if(!ObjectUtils.isEmpty(list)){
            for (MerchangoodReservation reservation:list) {
                Merchangood merchangood = merchangoodService.selectMerchangoodById(reservation.getGoodId());
                reservation.setMerchangood(merchangood);
            }
        }
        return getDataTable(list);
    }

    /**
     * 查询商品核销列表
     */
    @PostMapping("/findMerchangoodReservationHXlist")
    @ResponseBody
    public TableDataInfo findMerchangoodReservationHXlist()
    {
        MerchangoodReservation  merchangoodReservation= new MerchangoodReservation();
        merchangoodReservation.setWriteOffUserId(getAppUserId());
        startPage();
        List<MerchangoodReservation> list = merchangoodReservationService.selectMerchangoodReservationList(merchangoodReservation);
        if(!ObjectUtils.isEmpty(list)){
            for (MerchangoodReservation reservation:list) {
                Merchangood merchangood = merchangoodService.selectMerchangoodById(reservation.getGoodId());
                reservation.setMerchangood(merchangood);
                UserApp userApp = userService.selectUserById(reservation.getUserId());
                reservation.setUserApp(userApp);
            }
        }
        return getDataTable(list);
    }


    /**
     * 查询用户推荐码
     */
    @PostMapping("/findUserPush")
    @ResponseBody
    public AjaxResult findUserPush() throws Exception {
        try {
        Map<String, String> map = new HashMap<>();
        UserApp userApp = userService.selectUserById(getAppUserId());
        if (ObjectUtils.isEmpty(userApp)) {
            return AjaxResult.error("用户不存在");
        }
        String pushCode = redisUtils.get("user:push:" + userApp.getPushCode());
        if (ObjectUtils.isEmpty(pushCode)) {
            String pushCodePath = posterMakerConfig.getErweimaUrl() + "?invite_code=" + userApp.getPushCode();
            String outpath = posterMakerConfig.getOutPath() + UUID.randomUUID() + ".jpg";
            posterMakerUtil.createRecommendationCode(posterMakerConfig.getBackgroundPath(), pushCodePath, outpath, userApp.getPushCode());
            //流文件转化
            MultipartFile multipartFile = ImagUtils.fileToMultipartFile(new File(outpath));
            //上传到空间
            String image = this.qiniuUtils.uploadImage(FileUtils.uploanFile(multipartFile));
            redisUtils.set("user:push:" + userApp.getPushCode(), "http://qiuniu.wlxp365.com/" + image);
        }
            //获取所有直推用户
            List<UserApp> userPushList = userService.selectUserPushListByUserId(userApp.getId());
            //获取所有直推人员信息、
            int maxPushUserCount=0;
            Long maxPushUserUserId=0L;
            if(!ObjectUtils.isEmpty(userPushList)){
                for (UserApp u:userPushList) {
                    int pushCount = userService.finduserPushCount(u.getId(), "1");
                    if(pushCount  > maxPushUserCount){
                        maxPushUserCount=pushCount;
                        maxPushUserUserId=u.getId();
                    }
                }
            }
        if(maxPushUserUserId > 0){
            UserApp userApp1 = userService.selectUserPushByUserId(maxPushUserUserId);
            log.info("查询推荐人：{}",JSONObject.toJSONString(userApp1));
            if (!ObjectUtils.isEmpty(userApp1)) {
                String pushChannelCode = redisUtils.get("user:push:ChannelCode:" + userApp1.getPushCode());
                log.info("查询推荐人pushChannelCode：{}",pushChannelCode);
                if (ObjectUtils.isEmpty(pushChannelCode)) {
                    String pushCodePath = posterMakerConfig.getErweimaUrl() + "?invite_code=" + userApp1.getPushCode()+"_channel_"+userApp1.getId();
                    String outpath = posterMakerConfig.getOutPath() + UUID.randomUUID() + ".jpg";
                    posterMakerUtil.createRecommendationCode(posterMakerConfig.getBackgroundPath(), pushCodePath, outpath, userApp1.getPushCode());
                    //流文件转化
                    MultipartFile multipartFile = ImagUtils.fileToMultipartFile(new File(outpath));
                    //上传到空间
                    String image = this.qiniuUtils.uploadImage(FileUtils.uploanFile(multipartFile));
                    redisUtils.set("user:push:ChannelCode:" + userApp1.getPushCode(), "http://qiuniu.wlxp365.com/" + image);
                }
                map.put("userPushChannelCode1", redisUtils.get("user:push:ChannelCode:" + userApp1.getPushCode()));
            }
        }
        map.put("userPushCode", redisUtils.get("user:push:" + userApp.getPushCode()));

        return AjaxResult.success(map);
        }catch (Exception e){
            log.error("查询用户推荐码异常：{}",e);
        }
        return AjaxResult.error("查询异常");
    }

    /**
     * 退出登陆
     */
    @PostMapping("/loginOut")
    @ResponseBody
    public AjaxResult loginOut()
    {
        String token = ServletUtils.getRequest().getHeader("token");
        if(ObjectUtils.isEmpty(token)){
            return AjaxResult.success();
        }
         this.redisUtils.del(SystemContstant.TOKEN + token);
        return AjaxResult.success();
    }


    /**
     * 切换用户
     */
    @PostMapping("/switchUserAuth")
    @ResponseBody
    @NoRepeatSubmit
    public AjaxResult switchUserAuth()
    {
        Long appUserId = getAppUserId();
        if(ObjectUtils.isEmpty(appUserId)){
            return AjaxResult.error("用户不存在或未登录");
        }
        UserApp userApp = userService.selectUserById(appUserId);
        if(ObjectUtils.isEmpty(userApp)){
            return AjaxResult.error("用户信息失败");
        }
        if(userApp.getUserIdentity().equals(0)){
            userApp.setUserIdentity(1);
        }else{
            userApp.setUserIdentity(0);
        }
        userService.updateUser(userApp);
        return AjaxResult.success();
    }

    /**
     * 忘记密码-验证用户手机号码
     */
    @PostMapping("/checkPhone/{phone}")
    @ResponseBody
    public AjaxResult checkPhone(@PathVariable("phone") String phone)
    {
        if(ObjectUtils.isEmpty(phone)){
           return AjaxResult.error("手机号码不能为空");
        }
        UserApp moble = userService.findUserByMoble(phone);
        if(ObjectUtils.isEmpty(moble)){
            return AjaxResult.error("用户不存在");
        }
        return AjaxResult.success();
    }
    /**
     * 忘记密码-修改用户密码
     */
    @PostMapping("/updateUserPassword")
    @ResponseBody
    @NoRepeatSubmit
    public AjaxResult updateUserPassword(@RequestBody UpdateUserPasswordDto dto)
    {
        if(ObjectUtils.isEmpty(dto)){
            return AjaxResult.error("修改信息有误");
        }
        if(ObjectUtils.isEmpty(dto.getUserMoble()) || ObjectUtils.isEmpty(dto.getUserPassword()) || ObjectUtils.isEmpty(dto.getIdCard())){
            return AjaxResult.error("修改信息有误");
        }

        UserApp moble = userService.findUserByMoble(dto.getUserMoble());
        if(ObjectUtils.isEmpty(moble)){
            return AjaxResult.error("用户不存在");
        }
        if(ObjectUtils.isEmpty(moble.getReanlIdnum())){
            return AjaxResult.error("修改失败，未实名认证");
        }
        if(!moble.getReanlIdnum().contains(dto.getIdCard())){
            return AjaxResult.error("修改失败，身份证后四位不匹配");
        }
        moble.setUserPassword(Md5Utils.hash(dto.getUserPassword()));
        userService.updateUser(moble);
        return AjaxResult.success();
    }

    /**
     * 我的团队
     */
    @PostMapping("/myTeam")
    @ResponseBody
    public AjaxResult myTeam()
    {
        Long userId = getAppUserId();
        if(ObjectUtils.isEmpty(userId)){
            return AjaxResult.error("用户未登录");
        }
        Object objKey = redisUtils.getObjKey("user:myTeam:" + userId);
        if(!ObjectUtils.isEmpty(objKey)){
            return AjaxResult.success(objKey);
        }
        AjaxResult ajaxResult = userService.myTeam(userId);
        Object data = ajaxResult.get("data");
        if(ObjectUtils.isEmpty(data)){
            Random random = new Random();
            // 生成随机分钟数，范围在0到59之间
            int minute = random.nextInt(60);
            if(minute < 1){
                minute = 6;
            }
            redisUtils.set("user:myTeam:" + userId,data,minute*60L);
        }
        return ajaxResult;
    }


    /**
     * 我的团队
     */
    @PostMapping("/myTeamPush")
    @ResponseBody
    public AjaxResult myTeamPush(@RequestBody MyTeamPushDto dto)
    {
        Long userId = getAppUserId();
        if(ObjectUtils.isEmpty(userId)){
            return AjaxResult.error("用户未登录");
        }
        Object objKey = redisUtils.getObjKey("user:myTeamPush:" + userId);
        if(!ObjectUtils.isEmpty(objKey)){
            return AjaxResult.success(objKey);
        }
        AjaxResult ajaxResult = userService.myTeamPush(userId, dto.getPhone());
        Object data = ajaxResult.get("data");
        if(ObjectUtils.isEmpty(data)){
            Random random = new Random();
            // 生成随机分钟数，范围在0到59之间
            int minute = random.nextInt(60);
            if(minute < 1){
                minute = 6;
            }
            redisUtils.set("user:myTeamPush:" + userId,data,minute*60L);
        }
        return ajaxResult;
    }



    @PostMapping("/setPassword/{passWord}")
    @ResponseBody
    @NoRepeatSubmit
    public AjaxResult setPassword(@PathVariable("passWord") String passWord) {

        try {
            if(ObjectUtils.isEmpty(passWord)){
                return AjaxResult.error("参数有误");
            }
            AjaxResult ajaxResult = userService.setPassword(getAppUserId(), passWord);
            return ajaxResult;
        }catch (Exception e){
            log.error("设置交易密码异常：{}",e);
            throw new RuntimeException("设置交易密码异常，请稍后再试");
        }
    }
    @PostMapping("/resetPassword")
    @ResponseBody
    @NoRepeatSubmit
    public AjaxResult resetPassword(@RequestBody ResetPasswordDto dto) {

        try {
            if(ObjectUtils.isEmpty(dto) || ObjectUtils.isEmpty(dto.getPassWord()) || ObjectUtils.isEmpty(dto.getIdCard())){
                return AjaxResult.error("参数有误");
            }
            AjaxResult ajaxResult = userService.resetPassword(getAppUserId(), dto.getPassWord(),dto.getIdCard());
            return ajaxResult;
        }catch (Exception e){
            log.error("重置交易密码异常：{}",e);
            throw new RuntimeException("重置交易密码异常，请稍后再试");
        }
    }

    /**
     * 生成不重复推荐码
     * @return
     */
    public static String generateRecommendationCode() {
        UUID uuid = UUID.randomUUID();
        long mostSigBits = uuid.getMostSignificantBits();
        long leastSigBits = uuid.getLeastSignificantBits();
        return Long.toHexString(mostSigBits ^ leastSigBits).substring(0, 8);
    }

}
