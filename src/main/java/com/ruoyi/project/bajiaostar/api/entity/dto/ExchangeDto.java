package com.ruoyi.project.bajiaostar.api.entity.dto;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class ExchangeDto implements Serializable {
    /**
     * 接收用户id
     */
    private Long sufferUserId;
    //交易总额
    private BigDecimal sufferAccount;
    //转入账户 0 用户  1 商户
    private Integer userType;

}
