package com.ruoyi.project.bajiaostar.api.entity.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class UpdateUserPushDto implements Serializable {
    //更改用户id
    private Long genggaiId;
    //上级id
    private Long xiajiId;
}
