package com.ruoyi.project.bajiaostar.api.entity.dto.user;

import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import lombok.Data;

import java.io.Serializable;

@Data
public class MerchantAuthDto extends UserAuthDto implements Serializable {

    /** 商家身份： 0 个人  1 个体店铺 2 企业店铺 */
    private Integer storeIdentity;

    /** 营业执照地址 */
    private String businessLicenseUrl;

    /** 门头照 */
    private String storefrontPhotoUrl;

}
