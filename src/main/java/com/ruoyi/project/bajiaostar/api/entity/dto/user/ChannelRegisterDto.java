package com.ruoyi.project.bajiaostar.api.entity.dto.user;

import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import lombok.Data;

import java.io.Serializable;

@Data
public class ChannelRegisterDto implements Serializable {

    /** 用户密码 */
    private String userPassword;

    /** 手机号码 */
    private String moble;

    /** 推荐人推荐码 */
    private String pushUserCode;

}
