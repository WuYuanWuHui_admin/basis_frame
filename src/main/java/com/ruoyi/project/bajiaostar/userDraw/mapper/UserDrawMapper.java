package com.ruoyi.project.bajiaostar.userDraw.mapper;

import com.ruoyi.project.bajiaostar.userDraw.domain.UserDraw;
import java.util.List;

/**
 * 用户提现申请Mapper接口
 * 
 * @author 谢少辉
 * @date 2024-04-08
 */
public interface UserDrawMapper 
{
    /**
     * 查询用户提现申请
     * 
     * @param id 用户提现申请ID
     * @return 用户提现申请
     */
    public UserDraw selectUserDrawById(Long id);

    /**
     * 查询用户提现申请列表
     * 
     * @param userDraw 用户提现申请
     * @return 用户提现申请集合
     */
    public List<UserDraw> selectUserDrawList(UserDraw userDraw);

    /**
     * 新增用户提现申请
     * 
     * @param userDraw 用户提现申请
     * @return 结果
     */
    public int insertUserDraw(UserDraw userDraw);

    /**
     * 修改用户提现申请
     * 
     * @param userDraw 用户提现申请
     * @return 结果
     */
    public int updateUserDraw(UserDraw userDraw);

    /**
     * 删除用户提现申请
     * 
     * @param id 用户提现申请ID
     * @return 结果
     */
    public int deleteUserDrawById(Long id);

    /**
     * 批量删除用户提现申请
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteUserDrawByIds(String[] ids);
}
