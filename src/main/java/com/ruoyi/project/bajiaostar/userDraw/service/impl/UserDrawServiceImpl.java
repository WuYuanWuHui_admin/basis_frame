package com.ruoyi.project.bajiaostar.userDraw.service.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.project.bajiaostar.api.entity.dto.user.ExecuteUserAccountDto;
import com.ruoyi.project.bajiaostar.userAccount.service.IUserAccountService;
import com.ruoyi.project.content.FlowTypeEnum;
import com.ruoyi.project.content.PayTypeEnum;
import com.ruoyi.project.payUtils.zfb.AliPayUtils;
import com.ruoyi.project.utils.JsoupUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.bajiaostar.userDraw.mapper.UserDrawMapper;
import com.ruoyi.project.bajiaostar.userDraw.domain.UserDraw;
import com.ruoyi.project.bajiaostar.userDraw.service.IUserDrawService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 用户提现申请Service业务层处理
 * 
 * @author 谢少辉
 * @date 2024-04-08
 */
@Service
@Slf4j
public class UserDrawServiceImpl implements IUserDrawService 
{
    @Autowired
    private UserDrawMapper userDrawMapper;
    @Autowired
    AliPayUtils aliPayUtils;
    @Autowired
    IUserAccountService userAccountService;

    /**
     * 查询用户提现申请
     * 
     * @param id 用户提现申请ID
     * @return 用户提现申请
     */
    @Override
    public UserDraw selectUserDrawById(Long id)
    {
        return userDrawMapper.selectUserDrawById(id);
    }

    /**
     * 查询用户提现申请列表
     * 
     * @param userDraw 用户提现申请
     * @return 用户提现申请
     */
    @Override
    public List<UserDraw> selectUserDrawList(UserDraw userDraw)
    {
        return userDrawMapper.selectUserDrawList(userDraw);
    }

    /**
     * 新增用户提现申请
     * 
     * @param userDraw 用户提现申请
     * @return 结果
     */
    @Override
    public int insertUserDraw(UserDraw userDraw)
    {
        userDraw.setCreateTime(DateUtils.getNowDate());
        return userDrawMapper.insertUserDraw(userDraw);
    }

    /**
     * 修改用户提现申请
     * 
     * @param userDraw 用户提现申请
     * @return 结果
     */
    @Override
    public int updateUserDraw(UserDraw userDraw)
    {
        userDraw.setUpdateTime(DateUtils.getNowDate());
        return userDrawMapper.updateUserDraw(userDraw);
    }

    /**
     * 删除用户提现申请对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteUserDrawByIds(String ids)
    {
        return userDrawMapper.deleteUserDrawByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除用户提现申请信息
     * 
     * @param id 用户提现申请ID
     * @return 结果
     */
    @Override
    public int deleteUserDrawById(Long id)
    {
        return userDrawMapper.deleteUserDrawById(id);
    }

    /**
     * 审核提现
     *
     * @param userDraw
     * @return
     */
    @Override
    public int updStatusByUserDraw(UserDraw userDraw) throws Exception {
        UserDraw tempDraw = this.selectUserDrawById(userDraw.getId());

        if (tempDraw == null) {
            throw new RuntimeException("提现申请不存在");
        }
        // 如果不是待审核状态，则不允许处理
        if (!tempDraw.getStatus().equals(0)) {
            throw new RuntimeException("当前申请已处理");
        }

        String remark = JsoupUtils.clearAll(userDraw.getRemark());
        tempDraw.setStatus(userDraw.getStatus());
        tempDraw.setRemark(remark);
        tempDraw.setUpdateBy(userDraw.getUpdateBy());
        int count = this.updateUserDraw(userDraw);
        if (count <= 0) {
            throw new RuntimeException("更新审核状态失败");
        }
        if (tempDraw.getStatus().equals(1)) {
            //通过 支付宝可自动提现
            if (PayTypeEnum.ALIPAY == PayTypeEnum.findByValue(tempDraw.getType())) {
                String result = this.aliPayUtils.cashWithdrawal(tempDraw.getAccount(), tempDraw.getUserName(),
                        tempDraw.getId() + "-" + DateUtils.dateTimeNow(),
                        (new BigDecimal(tempDraw.getAmount())).divide(new BigDecimal(100), 2, RoundingMode.HALF_UP)
                );
                log.info("支付宝提现返回" + result);
            } else if (PayTypeEnum.WXPAY == PayTypeEnum.findByValue(tempDraw.getType())) {
              /*  this.wxPayUtils.cashWithdrawal(tempDraw.getOpenId(), tempDraw.getUserName(), tempDraw.getId() + "D",
                        (new BigDecimal(tempDraw.getAmount())).setScale(0, RoundingMode.HALF_UP)
                );*/
            }
        } else if (tempDraw.getStatus().equals(2)) {
            //驳回 给用户增加余额
            ExecuteUserAccountDto dto=new ExecuteUserAccountDto();
            dto.setUserId(userDraw.getUserId());
            dto.setUserAccount(Double.valueOf(userDraw.getAccount()));
            dto.setUserType(userDraw.getDrawUserType());
            dto.setAccountSysType(1);
            dto.setAccountName("提现审核失败返还提现金额");
            dto.setAccountType(0);
            userAccountService.executeUserAccount(dto);
        }
        return count;
    }
}
