package com.ruoyi.project.bajiaostar.userDraw.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;
import java.util.Date;

/**
 * 用户提现申请对象 t_user_draw
 * 
 * @author 谢少辉
 * @date 2024-04-08
 */
public class UserDraw extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 提现手续费 */
    @Excel(name = "提现手续费")
    private Long amountys;

    /** 操作金额 */
    @Excel(name = "操作金额")
    private Long amount;

    /** 状态(0审核中 1 审核成功 2审核失败) */
    @Excel(name = "状态(0审核中 1 审核成功 2审核失败)")
    private Integer status;

    /** 提现类型(0 支付宝 1微信 2银行卡) */
    @Excel(name = "提现类型(0 支付宝 1微信 2银行卡)")
    private Integer type;

    /** 账号 */
    @Excel(name = "账号")
    private String account;

    /** 账号对应姓名 */
    @Excel(name = "账号对应姓名")
    private String userName;

    /** 身份证号 */
    @Excel(name = "身份证号")
    private String idCard;

    /** 银行 */
    @Excel(name = "银行")
    private String bank;

    /** 开户地址 */
    @Excel(name = "开户地址")
    private String openAddress;

    /** 微信提现密钥 */
    @Excel(name = "微信提现密钥")
    private String openId;

    /** 是否收取服务费 0 否  1 是 */
    @Excel(name = "是否收取服务费 0 否  1 是")
    private Integer usertype;

    /** 次数 */
    @Excel(name = "次数")
    private Long usernum;

    /** 审核时间 */
    @Excel(name = "审核时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date checkDate;

    @Excel(name = "用户身份 0:用户  1 商户")
    private Integer drawUserType;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setAmountys(Long amountys) 
    {
        this.amountys = amountys;
    }

    public Long getAmountys() 
    {
        return amountys;
    }
    public void setAmount(Long amount) 
    {
        this.amount = amount;
    }

    public Long getAmount() 
    {
        return amount;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }
    public void setType(Integer type) 
    {
        this.type = type;
    }

    public Integer getType() 
    {
        return type;
    }
    public void setAccount(String account) 
    {
        this.account = account;
    }

    public String getAccount() 
    {
        return account;
    }
    public void setUserName(String userName) 
    {
        this.userName = userName;
    }

    public String getUserName() 
    {
        return userName;
    }
    public void setIdCard(String idCard) 
    {
        this.idCard = idCard;
    }

    public String getIdCard() 
    {
        return idCard;
    }
    public void setBank(String bank) 
    {
        this.bank = bank;
    }

    public String getBank() 
    {
        return bank;
    }
    public void setOpenAddress(String openAddress) 
    {
        this.openAddress = openAddress;
    }

    public String getOpenAddress() 
    {
        return openAddress;
    }
    public void setOpenId(String openId) 
    {
        this.openId = openId;
    }

    public String getOpenId() 
    {
        return openId;
    }
    public void setUsertype(Integer usertype) 
    {
        this.usertype = usertype;
    }

    public Integer getUsertype() 
    {
        return usertype;
    }
    public void setUsernum(Long usernum) 
    {
        this.usernum = usernum;
    }

    public Long getUsernum() 
    {
        return usernum;
    }
    public void setCheckDate(Date checkDate) 
    {
        this.checkDate = checkDate;
    }

    public Date getCheckDate() 
    {
        return checkDate;
    }

    public Integer getDrawUserType() {
        return drawUserType;
    }

    public void setDrawUserType(Integer drawUserType) {
        this.drawUserType = drawUserType;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .append("userId", getUserId())
            .append("amountys", getAmountys())
            .append("amount", getAmount())
            .append("status", getStatus())
            .append("type", getType())
            .append("account", getAccount())
            .append("userName", getUserName())
            .append("idCard", getIdCard())
            .append("bank", getBank())
            .append("openAddress", getOpenAddress())
            .append("openId", getOpenId())
            .append("usertype", getUsertype())
            .append("usernum", getUsernum())
            .append("checkDate", getCheckDate())
            .toString();
    }
}
