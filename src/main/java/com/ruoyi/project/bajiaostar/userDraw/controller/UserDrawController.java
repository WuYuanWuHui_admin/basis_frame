package com.ruoyi.project.bajiaostar.userDraw.controller;

import java.util.List;

import cn.hutool.core.util.ObjectUtil;
import com.ruoyi.common.utils.security.ShiroUtils;
import com.ruoyi.framework.aop.NoRepeatSubmit;
import com.ruoyi.framework.interceptor.annotation.RepeatSubmit;
import com.ruoyi.project.system.user.domain.User;
import com.ruoyi.project.utils.RedisUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.bajiaostar.userDraw.domain.UserDraw;
import com.ruoyi.project.bajiaostar.userDraw.service.IUserDrawService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 用户提现申请Controller
 * 
 * @author 谢少辉
 * @date 2024-04-08
 */
@Controller
@Slf4j
@RequestMapping("/bajiaostar/userDraw")
public class UserDrawController extends BaseController
{
    private String prefix = "bajiaostar/userDraw";
    @Autowired
    RedisUtils redisUtils;

    @Autowired
    private IUserDrawService userDrawService;

    @RequiresPermissions("bajiaostar:userDraw:view")
    @GetMapping()
    public String userDraw()
    {
        return prefix + "/userDraw";
    }

    /**
     * 查询用户提现申请列表
     */
    @RequiresPermissions("bajiaostar:userDraw:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(UserDraw userDraw)
    {
        startPage();
        List<UserDraw> list = userDrawService.selectUserDrawList(userDraw);
        return getDataTable(list);
    }

    /**
     * 导出用户提现申请列表
     */
    @RequiresPermissions("bajiaostar:userDraw:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(UserDraw userDraw)
    {
        List<UserDraw> list = userDrawService.selectUserDrawList(userDraw);
        ExcelUtil<UserDraw> util = new ExcelUtil<UserDraw>(UserDraw.class);
        return util.exportExcel(list, "userDraw");
    }

    /**
     * 新增用户提现申请
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存用户提现申请
     */
    @RequiresPermissions("bajiaostar:userDraw:add")
    @Log(title = "用户提现申请", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(UserDraw userDraw)
    {
        return toAjax(userDrawService.insertUserDraw(userDraw));
    }

    /**
     * 修改用户提现申请
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        UserDraw userDraw = userDrawService.selectUserDrawById(id);
        mmap.put("userDraw", userDraw);
        return prefix + "/edit";
    }

    /**
     * 修改保存用户提现申请
     */
    @RequiresPermissions("bajiaostar:userDraw:edit")
    @Log(title = "用户提现申请", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(UserDraw userDraw)
    {
        return toAjax(userDrawService.updateUserDraw(userDraw));
    }

    /**
     * 删除用户提现申请
     */
    @RequiresPermissions("bajiaostar:userDraw:remove")
    @Log(title = "用户提现申请", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(userDrawService.deleteUserDrawByIds(ids));
    }



    /**
     * 审批用户提现申请
     */
    @RequiresPermissions ("bajiaostar:userDraw:edit")
    @Log (title = "用户提现申请", businessType = BusinessType.UPDATE)
    @RepeatSubmit
    @PostMapping ("/editDraw")
    @ResponseBody
    public AjaxResult editSaveDraw (UserDraw userDraw) {
        try {
            boolean lock = redisUtils.setLock("editDraw:editDraw_" + userDraw.getId(), 3);
            if(!lock){
                return error("操作失败，存在重复操作风险，请刷新后操作");
            }
            if (!this.redisUtils.hasKey("Virtual_password")) {
                //this.redisUtils.set("Virtual_password", getrandom(100000, 999999), 60 * 60);
                this.redisUtils.set("Virtual_password", "666888", 60 * 60);
            }
            User sysUser = ShiroUtils.getSysUser();
            String Virtual_password = this.redisUtils.get("Virtual_password").toString();//虚拟任务显示数量
            if (Virtual_password.equals(userDraw.getRemark())) {
                userDraw.setRemark("提现审核通过");
                userDraw.setUpdateBy(ObjectUtil.isNotEmpty(sysUser)?sysUser.getUserName():"admin");
                return AjaxResult.success(this.userDrawService.updStatusByUserDraw(userDraw));
            } else {
                return this.error("动态密码输入错误~");
            }
        } catch (Exception e) {
            log.info("用户提现申请异常：{}",e);
            return this.error(e.getMessage());
        }
    }


    /**
     * 审批用户提现申请
     */
    @RequiresPermissions ("bajiaostar:draw:edit")
    @Log (title = "用户提现申请", businessType = BusinessType.UPDATE)
    @PostMapping ("/editDraws")
    @ResponseBody
    @NoRepeatSubmit
    public AjaxResult editSaveDraws (UserDraw userDraw) {
        try {
            boolean lock = redisUtils.setLock("editDraws_" + userDraw.getId(), 3);
            if(!lock){
                return error("操作失败，存在重复操作风险，请刷新后操作");
            }
            User sysUser = ShiroUtils.getSysUser();
            userDraw.setUpdateBy(ObjectUtil.isNotEmpty(sysUser)?sysUser.getUserName():"admin");
            return AjaxResult.success(this.userDrawService.updStatusByUserDraw(userDraw));
        } catch (Exception e) {
            e.printStackTrace();
            return this.error("操作失败"+e.getMessage());
        }
    }
    //生成随机数
    public static int getrandom (int start, int end) {
        int num = (int) (Math.random() * (end - start + 1) + start);
        return num;
    }
}
