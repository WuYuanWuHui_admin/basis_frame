package com.ruoyi.project.bajiaostar.userAccoundetail.service;

import com.ruoyi.project.bajiaostar.userAccoundetail.domain.UserAccoundetail;
import java.util.List;

/**
 * 用户/商户操作数据（爱豆）Service接口
 * 
 * @author 谢少辉
 * @date 2024-03-31
 */
public interface IUserAccoundetailService 
{
    /**
     * 查询用户/商户操作数据（爱豆）
     * 
     * @param id 用户/商户操作数据（爱豆）ID
     * @return 用户/商户操作数据（爱豆）
     */
    public UserAccoundetail selectUserAccoundetailById(Long id);

    /**
     * 查询用户/商户操作数据（爱豆）列表
     * 
     * @param userAccoundetail 用户/商户操作数据（爱豆）
     * @return 用户/商户操作数据（爱豆）集合
     */
    public List<UserAccoundetail> selectUserAccoundetailList(UserAccoundetail userAccoundetail);

    /**
     * 新增用户/商户操作数据（爱豆）
     * 
     * @param userAccoundetail 用户/商户操作数据（爱豆）
     * @return 结果
     */
    public int insertUserAccoundetail(UserAccoundetail userAccoundetail);

    /**
     * 修改用户/商户操作数据（爱豆）
     * 
     * @param userAccoundetail 用户/商户操作数据（爱豆）
     * @return 结果
     */
    public int updateUserAccoundetail(UserAccoundetail userAccoundetail);

    /**
     * 批量删除用户/商户操作数据（爱豆）
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteUserAccoundetailByIds(String ids);

    /**
     * 删除用户/商户操作数据（爱豆）信息
     * 
     * @param id 用户/商户操作数据（爱豆）ID
     * @return 结果
     */
    public int deleteUserAccoundetailById(Long id);
}
