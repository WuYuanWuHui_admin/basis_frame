package com.ruoyi.project.bajiaostar.userAccoundetail.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 用户/商户操作数据（爱豆）对象 t_user_account_detail
 * 
 * @author 谢少辉
 * @date 2024-03-31
 */
public class UserAccoundetail extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 账户明细 */
    private Long id;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 用户身份 */
    @Excel(name = "用户身份")
    private Integer userIdentity;

    /** 操作数据 */
    @Excel(name = "操作数据")
    private Double operationAccount;
    /** 操作类型 */
    @Excel(name = "操作类型 0 增加  1 减少")
    private Integer operationType;

    /** 操作类型 */
    @Excel(name = "资金操作类型 0 爱豆  1 现金")
    private Integer accountType;

    /** 操作后数据 */
    @Excel(name = "操作后数据")
    private Double operationAfterAccount;

    /** 操作说明 */
    @Excel(name = "操作说明")
    private String operationExplain;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setUserIdentity(Integer userIdentity) 
    {
        this.userIdentity = userIdentity;
    }

    public Integer getUserIdentity() 
    {
        return userIdentity;
    }
    public void setOperationAccount(Double operationAccount) 
    {
        this.operationAccount = operationAccount;
    }

    public Double getOperationAccount() 
    {
        return operationAccount;
    }
    public void setOperationAfterAccount(Double operationAfterAccount) 
    {
        this.operationAfterAccount = operationAfterAccount;
    }

    public Double getOperationAfterAccount() 
    {
        return operationAfterAccount;
    }
    public void setOperationExplain(String operationExplain) 
    {
        this.operationExplain = operationExplain;
    }

    public String getOperationExplain() 
    {
        return operationExplain;
    }

    public Integer getOperationType() {
        return operationType;
    }

    public void setOperationType(Integer operationType) {
        this.operationType = operationType;
    }

    public Integer getAccountType() {
        return accountType;
    }

    public void setAccountType(Integer accountType) {
        this.accountType = accountType;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("userIdentity", getUserIdentity())
            .append("operationAccount", getOperationAccount())
            .append("operationAfterAccount", getOperationAfterAccount())
            .append("operationExplain", getOperationExplain())
            .append("createTime", getCreateTime())
            .toString();
    }
}
