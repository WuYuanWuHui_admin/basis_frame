package com.ruoyi.project.bajiaostar.userAccoundetail.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.bajiaostar.userAccoundetail.mapper.UserAccoundetailMapper;
import com.ruoyi.project.bajiaostar.userAccoundetail.domain.UserAccoundetail;
import com.ruoyi.project.bajiaostar.userAccoundetail.service.IUserAccoundetailService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 用户/商户操作数据（爱豆）Service业务层处理
 * 
 * @author 谢少辉
 * @date 2024-03-31
 */
@Service
public class UserAccoundetailServiceImpl implements IUserAccoundetailService 
{
    @Autowired
    private UserAccoundetailMapper userAccoundetailMapper;

    /**
     * 查询用户/商户操作数据（爱豆）
     * 
     * @param id 用户/商户操作数据（爱豆）ID
     * @return 用户/商户操作数据（爱豆）
     */
    @Override
    public UserAccoundetail selectUserAccoundetailById(Long id)
    {
        return userAccoundetailMapper.selectUserAccoundetailById(id);
    }

    /**
     * 查询用户/商户操作数据（爱豆）列表
     * 
     * @param userAccoundetail 用户/商户操作数据（爱豆）
     * @return 用户/商户操作数据（爱豆）
     */
    @Override
    public List<UserAccoundetail> selectUserAccoundetailList(UserAccoundetail userAccoundetail)
    {
        return userAccoundetailMapper.selectUserAccoundetailList(userAccoundetail);
    }

    /**
     * 新增用户/商户操作数据（爱豆）
     * 
     * @param userAccoundetail 用户/商户操作数据（爱豆）
     * @return 结果
     */
    @Override
    public int insertUserAccoundetail(UserAccoundetail userAccoundetail)
    {
        userAccoundetail.setCreateTime(DateUtils.getNowDate());
        return userAccoundetailMapper.insertUserAccoundetail(userAccoundetail);
    }

    /**
     * 修改用户/商户操作数据（爱豆）
     * 
     * @param userAccoundetail 用户/商户操作数据（爱豆）
     * @return 结果
     */
    @Override
    public int updateUserAccoundetail(UserAccoundetail userAccoundetail)
    {
        userAccoundetail.setUpdateTime(DateUtils.getNowDate());
        return userAccoundetailMapper.updateUserAccoundetail(userAccoundetail);
    }

    /**
     * 删除用户/商户操作数据（爱豆）对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteUserAccoundetailByIds(String ids)
    {
        return userAccoundetailMapper.deleteUserAccoundetailByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除用户/商户操作数据（爱豆）信息
     * 
     * @param id 用户/商户操作数据（爱豆）ID
     * @return 结果
     */
    @Override
    public int deleteUserAccoundetailById(Long id)
    {
        return userAccoundetailMapper.deleteUserAccoundetailById(id);
    }
}
