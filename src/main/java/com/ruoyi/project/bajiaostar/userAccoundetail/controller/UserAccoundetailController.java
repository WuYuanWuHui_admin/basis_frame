package com.ruoyi.project.bajiaostar.userAccoundetail.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.bajiaostar.userAccoundetail.domain.UserAccoundetail;
import com.ruoyi.project.bajiaostar.userAccoundetail.service.IUserAccoundetailService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 用户/商户操作数据（爱豆）Controller
 * 
 * @author 谢少辉
 * @date 2024-03-31
 */
@Controller
@RequestMapping("/bajiaostar/userAccoundetail")
public class UserAccoundetailController extends BaseController
{
    private String prefix = "bajiaostar/userAccoundetail";

    @Autowired
    private IUserAccoundetailService userAccoundetailService;

    @RequiresPermissions("bajiaostar:userAccoundetail:view")
    @GetMapping()
    public String userAccoundetail()
    {
        return prefix + "/userAccoundetail";
    }

    /**
     * 查询用户/商户操作数据（爱豆）列表
     */
    @RequiresPermissions("bajiaostar:userAccoundetail:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(UserAccoundetail userAccoundetail)
    {
        startPage();
        List<UserAccoundetail> list = userAccoundetailService.selectUserAccoundetailList(userAccoundetail);
        return getDataTable(list);
    }

    /**
     * 导出用户/商户操作数据（爱豆）列表
     */
    @RequiresPermissions("bajiaostar:userAccoundetail:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(UserAccoundetail userAccoundetail)
    {
        List<UserAccoundetail> list = userAccoundetailService.selectUserAccoundetailList(userAccoundetail);
        ExcelUtil<UserAccoundetail> util = new ExcelUtil<UserAccoundetail>(UserAccoundetail.class);
        return util.exportExcel(list, "userAccoundetail");
    }

    /**
     * 新增用户/商户操作数据（爱豆）
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存用户/商户操作数据（爱豆）
     */
    @RequiresPermissions("bajiaostar:userAccoundetail:add")
    @Log(title = "用户/商户操作数据（爱豆）", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(UserAccoundetail userAccoundetail)
    {
        return toAjax(userAccoundetailService.insertUserAccoundetail(userAccoundetail));
    }

    /**
     * 修改用户/商户操作数据（爱豆）
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        UserAccoundetail userAccoundetail = userAccoundetailService.selectUserAccoundetailById(id);
        mmap.put("userAccoundetail", userAccoundetail);
        return prefix + "/edit";
    }

    /**
     * 修改保存用户/商户操作数据（爱豆）
     */
    @RequiresPermissions("bajiaostar:userAccoundetail:edit")
    @Log(title = "用户/商户操作数据（爱豆）", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(UserAccoundetail userAccoundetail)
    {
        return toAjax(userAccoundetailService.updateUserAccoundetail(userAccoundetail));
    }

    /**
     * 删除用户/商户操作数据（爱豆）
     */
    @RequiresPermissions("bajiaostar:userAccoundetail:remove")
    @Log(title = "用户/商户操作数据（爱豆）", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(userAccoundetailService.deleteUserAccoundetailByIds(ids));
    }
}
