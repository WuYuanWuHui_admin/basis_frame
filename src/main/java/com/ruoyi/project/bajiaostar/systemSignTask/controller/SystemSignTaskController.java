package com.ruoyi.project.bajiaostar.systemSignTask.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.bajiaostar.systemSignTask.domain.SystemSignTask;
import com.ruoyi.project.bajiaostar.systemSignTask.service.ISystemSignTaskService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 系统任务Controller
 * 
 * @author 谢少辉
 * @date 2024-04-11
 */
@Controller
@RequestMapping("/bajiaostar/systemSignTask")
public class SystemSignTaskController extends BaseController
{
    private String prefix = "bajiaostar/systemSignTask";

    @Autowired
    private ISystemSignTaskService systemSignTaskService;

    @RequiresPermissions("bajiaostar:systemSignTask:view")
    @GetMapping()
    public String systemSignTask()
    {
        return prefix + "/systemSignTask";
    }

    /**
     * 查询系统任务列表
     */
    @RequiresPermissions("bajiaostar:systemSignTask:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(SystemSignTask systemSignTask)
    {
        startPage();
        List<SystemSignTask> list = systemSignTaskService.selectSystemSignTaskList(systemSignTask);
        return getDataTable(list);
    }

    /**
     * 导出系统任务列表
     */
    @RequiresPermissions("bajiaostar:systemSignTask:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(SystemSignTask systemSignTask)
    {
        List<SystemSignTask> list = systemSignTaskService.selectSystemSignTaskList(systemSignTask);
        ExcelUtil<SystemSignTask> util = new ExcelUtil<SystemSignTask>(SystemSignTask.class);
        return util.exportExcel(list, "systemSignTask");
    }

    /**
     * 新增系统任务
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存系统任务
     */
    @RequiresPermissions("bajiaostar:systemSignTask:add")
    @Log(title = "系统任务", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(SystemSignTask systemSignTask)
    {
        return toAjax(systemSignTaskService.insertSystemSignTask(systemSignTask));
    }

    /**
     * 修改系统任务
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        SystemSignTask systemSignTask = systemSignTaskService.selectSystemSignTaskById(id);
        mmap.put("systemSignTask", systemSignTask);
        return prefix + "/edit";
    }

    /**
     * 修改保存系统任务
     */
    @RequiresPermissions("bajiaostar:systemSignTask:edit")
    @Log(title = "系统任务", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(SystemSignTask systemSignTask)
    {
        return toAjax(systemSignTaskService.updateSystemSignTask(systemSignTask));
    }

    /**
     * 删除系统任务
     */
    @RequiresPermissions("bajiaostar:systemSignTask:remove")
    @Log(title = "系统任务", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(systemSignTaskService.deleteSystemSignTaskByIds(ids));
    }
}
