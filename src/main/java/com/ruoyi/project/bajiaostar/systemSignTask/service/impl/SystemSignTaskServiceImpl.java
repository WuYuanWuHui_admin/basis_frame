package com.ruoyi.project.bajiaostar.systemSignTask.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.bajiaostar.systemSignTask.mapper.SystemSignTaskMapper;
import com.ruoyi.project.bajiaostar.systemSignTask.domain.SystemSignTask;
import com.ruoyi.project.bajiaostar.systemSignTask.service.ISystemSignTaskService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 系统任务Service业务层处理
 * 
 * @author 谢少辉
 * @date 2024-04-11
 */
@Service
public class SystemSignTaskServiceImpl implements ISystemSignTaskService 
{
    @Autowired
    private SystemSignTaskMapper systemSignTaskMapper;

    /**
     * 查询系统任务
     * 
     * @param id 系统任务ID
     * @return 系统任务
     */
    @Override
    public SystemSignTask selectSystemSignTaskById(Long id)
    {
        return systemSignTaskMapper.selectSystemSignTaskById(id);
    }

    /**
     * 查询系统任务列表
     * 
     * @param systemSignTask 系统任务
     * @return 系统任务
     */
    @Override
    public List<SystemSignTask> selectSystemSignTaskList(SystemSignTask systemSignTask)
    {
        return systemSignTaskMapper.selectSystemSignTaskList(systemSignTask);
    }

    /**
     * 新增系统任务
     * 
     * @param systemSignTask 系统任务
     * @return 结果
     */
    @Override
    public int insertSystemSignTask(SystemSignTask systemSignTask)
    {
        return systemSignTaskMapper.insertSystemSignTask(systemSignTask);
    }

    /**
     * 修改系统任务
     * 
     * @param systemSignTask 系统任务
     * @return 结果
     */
    @Override
    public int updateSystemSignTask(SystemSignTask systemSignTask)
    {
        return systemSignTaskMapper.updateSystemSignTask(systemSignTask);
    }

    /**
     * 删除系统任务对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteSystemSignTaskByIds(String ids)
    {
        return systemSignTaskMapper.deleteSystemSignTaskByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除系统任务信息
     * 
     * @param id 系统任务ID
     * @return 结果
     */
    @Override
    public int deleteSystemSignTaskById(Long id)
    {
        return systemSignTaskMapper.deleteSystemSignTaskById(id);
    }
}
