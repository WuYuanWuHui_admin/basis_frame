package com.ruoyi.project.bajiaostar.systemSignTask.mapper;

import com.ruoyi.project.bajiaostar.systemSignTask.domain.SystemSignTask;
import java.util.List;

/**
 * 系统任务Mapper接口
 * 
 * @author 谢少辉
 * @date 2024-04-11
 */
public interface SystemSignTaskMapper 
{
    /**
     * 查询系统任务
     * 
     * @param id 系统任务ID
     * @return 系统任务
     */
    public SystemSignTask selectSystemSignTaskById(Long id);

    /**
     * 查询系统任务列表
     * 
     * @param systemSignTask 系统任务
     * @return 系统任务集合
     */
    public List<SystemSignTask> selectSystemSignTaskList(SystemSignTask systemSignTask);

    /**
     * 新增系统任务
     * 
     * @param systemSignTask 系统任务
     * @return 结果
     */
    public int insertSystemSignTask(SystemSignTask systemSignTask);

    /**
     * 修改系统任务
     * 
     * @param systemSignTask 系统任务
     * @return 结果
     */
    public int updateSystemSignTask(SystemSignTask systemSignTask);

    /**
     * 删除系统任务
     * 
     * @param id 系统任务ID
     * @return 结果
     */
    public int deleteSystemSignTaskById(Long id);

    /**
     * 批量删除系统任务
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSystemSignTaskByIds(String[] ids);
}
