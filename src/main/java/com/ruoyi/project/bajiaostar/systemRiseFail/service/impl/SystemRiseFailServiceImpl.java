package com.ruoyi.project.bajiaostar.systemRiseFail.service.impl;

import java.util.Date;
import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.framework.web.domain.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.bajiaostar.systemRiseFail.mapper.SystemRiseFailMapper;
import com.ruoyi.project.bajiaostar.systemRiseFail.domain.SystemRiseFail;
import com.ruoyi.project.bajiaostar.systemRiseFail.service.ISystemRiseFailService;
import com.ruoyi.common.utils.text.Convert;
import org.springframework.util.ObjectUtils;

/**
 * 涨幅信息Service业务层处理
 * 
 * @author 谢少辉
 * @date 2024-04-14
 */
@Service
public class SystemRiseFailServiceImpl implements ISystemRiseFailService 
{
    @Autowired
    private SystemRiseFailMapper systemRiseFailMapper;

    /**
     * 查询涨幅信息
     * 
     * @param id 涨幅信息ID
     * @return 涨幅信息
     */
    @Override
    public SystemRiseFail selectSystemRiseFailById(Long id)
    {
        return systemRiseFailMapper.selectSystemRiseFailById(id);
    }

    /**
     * 查询涨幅信息列表
     * 
     * @param systemRiseFail 涨幅信息
     * @return 涨幅信息
     */
    @Override
    public List<SystemRiseFail> selectSystemRiseFailList(SystemRiseFail systemRiseFail)
    {
        return systemRiseFailMapper.selectSystemRiseFailList(systemRiseFail);
    }

    /**
     * 新增涨幅信息
     * 
     * @param systemRiseFail 涨幅信息
     * @return 结果
     */
    @Override
    public int insertSystemRiseFail(SystemRiseFail systemRiseFail)
    {
        systemRiseFail.setCreateTime(DateUtils.getNowDate());
        return systemRiseFailMapper.insertSystemRiseFail(systemRiseFail);
    }

    /**
     * 修改涨幅信息
     * 
     * @param systemRiseFail 涨幅信息
     * @return 结果
     */
    @Override
    public int updateSystemRiseFail(SystemRiseFail systemRiseFail)
    {
        systemRiseFail.setUpdateTime(DateUtils.getNowDate());
        return systemRiseFailMapper.updateSystemRiseFail(systemRiseFail);
    }

    /**
     * 删除涨幅信息对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteSystemRiseFailByIds(String ids)
    {
        return systemRiseFailMapper.deleteSystemRiseFailByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除涨幅信息信息
     * 
     * @param id 涨幅信息ID
     * @return 结果
     */
    @Override
    public int deleteSystemRiseFailById(Long id)
    {
        return systemRiseFailMapper.deleteSystemRiseFailById(id);
    }

    /**
     * 涨幅版定时任务
     *
     * @return
     */
    @Override
    public AjaxResult executeSystemRiseFailTask() {

        Double riseFallPrice = 10d;
        //查询最大数据价格
        SystemRiseFail systemRiseFail = systemRiseFailMapper.selectSystemRiseFailMaxDate();
        if(!ObjectUtils.isEmpty(systemRiseFail)){
            riseFallPrice=systemRiseFail.getRiseFallPrice()+0.01d;
        }
        SystemRiseFail addRiseFail=new SystemRiseFail();
        addRiseFail.setRiseFallDate(new Date());
        addRiseFail.setRiseFallPrice(riseFallPrice);
        this.insertSystemRiseFail(addRiseFail);
        return AjaxResult.success();
    }

    /**
     * 查询涨幅信息列表(最近七天)
     *
     * @param systemRiseFail 涨幅信息
     * @return 涨幅信息集合
     */
    @Override
    public List<SystemRiseFail> finfSystemRiseFailList(SystemRiseFail systemRiseFail) {
        return systemRiseFailMapper.finfSystemRiseFailList(systemRiseFail);
    }
}
