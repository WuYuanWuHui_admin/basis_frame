package com.ruoyi.project.bajiaostar.systemRiseFail.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.bajiaostar.systemRiseFail.domain.SystemRiseFail;
import com.ruoyi.project.bajiaostar.systemRiseFail.service.ISystemRiseFailService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 涨幅信息Controller
 * 
 * @author 谢少辉
 * @date 2024-04-14
 */
@Controller
@RequestMapping("/bajiaostar/systemRiseFail")
public class SystemRiseFailController extends BaseController
{
    private String prefix = "bajiaostar/systemRiseFail";

    @Autowired
    private ISystemRiseFailService systemRiseFailService;

    @RequiresPermissions("bajiaostar:systemRiseFail:view")
    @GetMapping()
    public String systemRiseFail()
    {
        return prefix + "/systemRiseFail";
    }

    /**
     * 查询涨幅信息列表
     */
    @RequiresPermissions("bajiaostar:systemRiseFail:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(SystemRiseFail systemRiseFail)
    {
        startPage();
        List<SystemRiseFail> list = systemRiseFailService.selectSystemRiseFailList(systemRiseFail);
        return getDataTable(list);
    }

    /**
     * 导出涨幅信息列表
     */
    @RequiresPermissions("bajiaostar:systemRiseFail:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(SystemRiseFail systemRiseFail)
    {
        List<SystemRiseFail> list = systemRiseFailService.selectSystemRiseFailList(systemRiseFail);
        ExcelUtil<SystemRiseFail> util = new ExcelUtil<SystemRiseFail>(SystemRiseFail.class);
        return util.exportExcel(list, "systemRiseFail");
    }

    /**
     * 新增涨幅信息
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存涨幅信息
     */
    @RequiresPermissions("bajiaostar:systemRiseFail:add")
    @Log(title = "涨幅信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(SystemRiseFail systemRiseFail)
    {
        return toAjax(systemRiseFailService.insertSystemRiseFail(systemRiseFail));
    }

    /**
     * 修改涨幅信息
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        SystemRiseFail systemRiseFail = systemRiseFailService.selectSystemRiseFailById(id);
        mmap.put("systemRiseFail", systemRiseFail);
        return prefix + "/edit";
    }

    /**
     * 修改保存涨幅信息
     */
    @RequiresPermissions("bajiaostar:systemRiseFail:edit")
    @Log(title = "涨幅信息", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(SystemRiseFail systemRiseFail)
    {
        return toAjax(systemRiseFailService.updateSystemRiseFail(systemRiseFail));
    }

    /**
     * 删除涨幅信息
     */
    @RequiresPermissions("bajiaostar:systemRiseFail:remove")
    @Log(title = "涨幅信息", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(systemRiseFailService.deleteSystemRiseFailByIds(ids));
    }
}
