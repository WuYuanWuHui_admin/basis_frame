package com.ruoyi.project.bajiaostar.systemRiseFail.mapper;

import com.ruoyi.project.bajiaostar.systemRiseFail.domain.SystemRiseFail;
import java.util.List;

/**
 * 涨幅信息Mapper接口
 * 
 * @author 谢少辉
 * @date 2024-04-14
 */
public interface SystemRiseFailMapper 
{
    /**
     * 查询涨幅信息
     * 
     * @param id 涨幅信息ID
     * @return 涨幅信息
     */
    public SystemRiseFail selectSystemRiseFailById(Long id);

    /**
     * 查询涨幅信息列表
     * 
     * @param systemRiseFail 涨幅信息
     * @return 涨幅信息集合
     */
    public List<SystemRiseFail> selectSystemRiseFailList(SystemRiseFail systemRiseFail);

    /**
     * 新增涨幅信息
     * 
     * @param systemRiseFail 涨幅信息
     * @return 结果
     */
    public int insertSystemRiseFail(SystemRiseFail systemRiseFail);

    /**
     * 修改涨幅信息
     * 
     * @param systemRiseFail 涨幅信息
     * @return 结果
     */
    public int updateSystemRiseFail(SystemRiseFail systemRiseFail);

    /**
     * 删除涨幅信息
     * 
     * @param id 涨幅信息ID
     * @return 结果
     */
    public int deleteSystemRiseFailById(Long id);

    /**
     * 批量删除涨幅信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSystemRiseFailByIds(String[] ids);

    /**
     * 查询最大日期数据
     * @return
     */
    public SystemRiseFail selectSystemRiseFailMaxDate();

    /**
     * 获取最新七天数据
     * @param systemRiseFail
     * @return
     */
    List<SystemRiseFail> finfSystemRiseFailList(SystemRiseFail systemRiseFail);
}
