package com.ruoyi.project.bajiaostar.systemRiseFail.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;
import java.util.Date;

/**
 * 涨幅信息对象 t_system_rise_fail
 * 
 * @author 谢少辉
 * @date 2024-04-14
 */
public class SystemRiseFail extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 日期 */
    @Excel(name = "日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date riseFallDate;

    /** $column.columnComment */
    @Excel(name = "价格")
    private Double riseFallPrice;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setRiseFallDate(Date riseFallDate) 
    {
        this.riseFallDate = riseFallDate;
    }

    public Date getRiseFallDate() 
    {
        return riseFallDate;
    }
    public void setRiseFallPrice(Double riseFallPrice) 
    {
        this.riseFallPrice = riseFallPrice;
    }

    public Double getRiseFallPrice() 
    {
        return riseFallPrice;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("riseFallDate", getRiseFallDate())
            .append("riseFallPrice", getRiseFallPrice())
            .append("createTime", getCreateTime())
            .toString();
    }
}
