package com.ruoyi.project.bajiaostar.version.service.impl;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.text.Convert;
import com.ruoyi.project.bajiaostar.version.domain.AppVersion;
import com.ruoyi.project.bajiaostar.version.mapper.AppVersionMapper;
import com.ruoyi.project.bajiaostar.version.service.IAppVersionService;
import com.ruoyi.project.utils.RedisUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * app版本号管理Service业务层处理
 *
 * @author ${author}
 */
@Service
public class AppVersionServiceImpl implements IAppVersionService {

    @Autowired
    private RedisUtils redisUtils;

    @Autowired
    private AppVersionMapper appVersionMapper;

    /**
     * 查询app版本号管理
     *
     * @param id
     *         app版本号管理ID
     *
     * @return app版本号管理
     */
    @Override
    public AppVersion selectAppVersionById (Long id) {
        return this.appVersionMapper.selectAppVersionById(id);
    }

    /**
     * 查询app版本号管理列表
     *
     * @param appVersion
     *         app版本号管理
     *
     * @return app版本号管理
     */
    @Override
    public List <AppVersion> selectAppVersionList (AppVersion appVersion) {
        return this.appVersionMapper.selectAppVersionList(appVersion);
    }

    /**
     * 新增app版本号管理
     *
     * @param appVersion
     *         app版本号管理
     *
     * @return 结果
     */
    @Override
    public int insertAppVersion (AppVersion appVersion) {
        appVersion.setCreateTime(DateUtils.getNowDate());
        return this.appVersionMapper.insertAppVersion(appVersion);
    }

    /**
     * 修改app版本号管理
     *
     * @param appVersion
     *         app版本号管理
     *
     * @return 结果
     */
    @Override
    public int updateAppVersion (AppVersion appVersion) {
        appVersion.setUpdateTime(DateUtils.getNowDate());
        return this.appVersionMapper.updateAppVersion(appVersion);
    }

    /**
     * 删除app版本号管理对象
     *
     * @param ids
     *         需要删除的数据ID
     *
     * @return 结果
     */
    @Override
    public int deleteAppVersionByIds (String ids) {

        if (StringUtils.isEmpty(ids)) {
            return 0;
        }

        String[] idArray = Convert.toStrArray(ids);

        if (ArrayUtils.isEmpty(idArray)) {
            return 0;
        }

        int result = this.appVersionMapper.deleteAppVersionByIds(idArray);

        if (result > 0) {
            this.redisUtils.del("xiyi:app:app_version_0", "xiyi:app:app_version_1", "xiyi:app:app_version_2");
        }

        return result;
    }

    /**
     * 删除app版本号管理信息
     *
     * @param id
     *         app版本号管理ID
     *
     * @return 结果
     */
    @Override
    public int deleteAppVersionById (Long id) {
        return this.appVersionMapper.deleteAppVersionById(id);
    }

    @Override
    public AppVersion selectNewAppVersionByType (AppVersion appVersion) {
        return this.appVersionMapper.selectNewAppVersionByType(appVersion);
    }
}
