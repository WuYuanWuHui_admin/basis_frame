package com.ruoyi.project.bajiaostar.paymenrequest.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.bajiaostar.paymenrequest.domain.Paymenrequest;
import com.ruoyi.project.bajiaostar.paymenrequest.service.IPaymenrequestService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 请求第三方记录Controller
 * 
 * @author 谢少辉
 * @date 2024-03-22
 */
@Controller
@RequestMapping("/bajiaostar/paymenrequest")
public class PaymenrequestController extends BaseController
{
    private String prefix = "bajiaostar/paymenrequest";

    @Autowired
    private IPaymenrequestService paymenrequestService;

    @RequiresPermissions("bajiaostar:paymenrequest:view")
    @GetMapping()
    public String paymenrequest()
    {
        return prefix + "/paymenrequest";
    }

    /**
     * 查询请求第三方记录列表
     */
    @RequiresPermissions("bajiaostar:paymenrequest:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Paymenrequest paymenrequest)
    {
        startPage();
        List<Paymenrequest> list = paymenrequestService.selectPaymenrequestList(paymenrequest);
        return getDataTable(list);
    }

    /**
     * 导出请求第三方记录列表
     */
    @RequiresPermissions("bajiaostar:paymenrequest:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Paymenrequest paymenrequest)
    {
        List<Paymenrequest> list = paymenrequestService.selectPaymenrequestList(paymenrequest);
        ExcelUtil<Paymenrequest> util = new ExcelUtil<Paymenrequest>(Paymenrequest.class);
        return util.exportExcel(list, "paymenrequest");
    }

    /**
     * 新增请求第三方记录
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存请求第三方记录
     */
    @RequiresPermissions("bajiaostar:paymenrequest:add")
    @Log(title = "请求第三方记录", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(Paymenrequest paymenrequest)
    {
        return toAjax(paymenrequestService.insertPaymenrequest(paymenrequest));
    }

    /**
     * 修改请求第三方记录
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        Paymenrequest paymenrequest = paymenrequestService.selectPaymenrequestById(id);
        mmap.put("paymenrequest", paymenrequest);
        return prefix + "/edit";
    }

    /**
     * 修改保存请求第三方记录
     */
    @RequiresPermissions("bajiaostar:paymenrequest:edit")
    @Log(title = "请求第三方记录", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(Paymenrequest paymenrequest)
    {
        return toAjax(paymenrequestService.updatePaymenrequest(paymenrequest));
    }

    /**
     * 删除请求第三方记录
     */
    @RequiresPermissions("bajiaostar:paymenrequest:remove")
    @Log(title = "请求第三方记录", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(paymenrequestService.deletePaymenrequestByIds(ids));
    }
}
