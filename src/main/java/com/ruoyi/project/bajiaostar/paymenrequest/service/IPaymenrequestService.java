package com.ruoyi.project.bajiaostar.paymenrequest.service;

import com.ruoyi.project.bajiaostar.paymenrequest.domain.Paymenrequest;
import java.util.List;

/**
 * 请求第三方记录Service接口
 * 
 * @author 谢少辉
 * @date 2024-03-22
 */
public interface IPaymenrequestService 
{
    /**
     * 查询请求第三方记录
     * 
     * @param id 请求第三方记录ID
     * @return 请求第三方记录
     */
    public Paymenrequest selectPaymenrequestById(Long id);

    /**
     * 查询请求第三方记录列表
     * 
     * @param paymenrequest 请求第三方记录
     * @return 请求第三方记录集合
     */
    public List<Paymenrequest> selectPaymenrequestList(Paymenrequest paymenrequest);

    /**
     * 新增请求第三方记录
     * 
     * @param paymenrequest 请求第三方记录
     * @return 结果
     */
    public int insertPaymenrequest(Paymenrequest paymenrequest);

    /**
     * 修改请求第三方记录
     * 
     * @param paymenrequest 请求第三方记录
     * @return 结果
     */
    public int updatePaymenrequest(Paymenrequest paymenrequest);

    /**
     * 批量删除请求第三方记录
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deletePaymenrequestByIds(String ids);

    /**
     * 删除请求第三方记录信息
     * 
     * @param id 请求第三方记录ID
     * @return 结果
     */
    public int deletePaymenrequestById(Long id);

    /**
     * 根据订单号获取订单
     *
     * @param paymenrequest 请求第三方记录
     * @return 请求第三方记录集合
     */
    Paymenrequest selectPaymentRequestByServiceOrderNo(Paymenrequest paymenrequest);
}
