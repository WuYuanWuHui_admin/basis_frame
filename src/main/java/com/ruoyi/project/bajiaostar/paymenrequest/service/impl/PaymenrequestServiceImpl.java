package com.ruoyi.project.bajiaostar.paymenrequest.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.bajiaostar.paymenrequest.mapper.PaymenrequestMapper;
import com.ruoyi.project.bajiaostar.paymenrequest.domain.Paymenrequest;
import com.ruoyi.project.bajiaostar.paymenrequest.service.IPaymenrequestService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 请求第三方记录Service业务层处理
 * 
 * @author 谢少辉
 * @date 2024-03-22
 */
@Service
public class PaymenrequestServiceImpl implements IPaymenrequestService 
{
    @Autowired
    private PaymenrequestMapper paymenrequestMapper;

    /**
     * 查询请求第三方记录
     * 
     * @param id 请求第三方记录ID
     * @return 请求第三方记录
     */
    @Override
    public Paymenrequest selectPaymenrequestById(Long id)
    {
        return paymenrequestMapper.selectPaymenrequestById(id);
    }

    /**
     * 查询请求第三方记录列表
     * 
     * @param paymenrequest 请求第三方记录
     * @return 请求第三方记录
     */
    @Override
    public List<Paymenrequest> selectPaymenrequestList(Paymenrequest paymenrequest)
    {
        return paymenrequestMapper.selectPaymenrequestList(paymenrequest);
    }

    /**
     * 新增请求第三方记录
     * 
     * @param paymenrequest 请求第三方记录
     * @return 结果
     */
    @Override
    public int insertPaymenrequest(Paymenrequest paymenrequest)
    {
        paymenrequest.setCreateTime(DateUtils.getNowDate());
        return paymenrequestMapper.insertPaymenrequest(paymenrequest);
    }

    /**
     * 修改请求第三方记录
     * 
     * @param paymenrequest 请求第三方记录
     * @return 结果
     */
    @Override
    public int updatePaymenrequest(Paymenrequest paymenrequest)
    {
        paymenrequest.setUpdateTime(DateUtils.getNowDate());
        return paymenrequestMapper.updatePaymenrequest(paymenrequest);
    }

    /**
     * 删除请求第三方记录对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deletePaymenrequestByIds(String ids)
    {
        return paymenrequestMapper.deletePaymenrequestByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除请求第三方记录信息
     * 
     * @param id 请求第三方记录ID
     * @return 结果
     */
    @Override
    public int deletePaymenrequestById(Long id)
    {
        return paymenrequestMapper.deletePaymenrequestById(id);
    }

    /**
     * 根据订单号获取订单
     *
     * @param paymenrequest 请求第三方记录
     * @return 请求第三方记录集合
     */
    @Override
    public Paymenrequest selectPaymentRequestByServiceOrderNo(Paymenrequest paymenrequest) {
        return paymenrequestMapper.selectPaymentRequestByServiceOrderNo(paymenrequest);
    }
}
