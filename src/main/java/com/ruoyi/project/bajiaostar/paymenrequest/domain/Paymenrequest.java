package com.ruoyi.project.bajiaostar.paymenrequest.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;
import java.util.Date;

/**
 * 请求第三方记录对象 t_payment_request
 * 
 * @author 谢少辉
 * @date 2024-03-22
 */
public class Paymenrequest extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private Long id;

    /** 用户ID */
    @Excel(name = "用户ID")
    private Long userId;

    /** 业务订单号 */
    @Excel(name = "业务订单号")
    private String serviceOrderNo;

    /** 平台业务类型 */
    @Excel(name = "平台业务类型")
    private Integer serviceType;

    /** 交易订单号，用于第三方接口，有重复交易可能时多次请求订单号固定 */
    @Excel(name = "交易订单号，用于第三方接口，有重复交易可能时多次请求订单号固定")
    private String orderNo;

    /** 第三方接口业务类型 */
    @Excel(name = "第三方接口业务类型")
    private Integer payType;

    /** 状态：-1.失败、0.处理中、1.成功 */
    @Excel(name = "状态：-1.失败、0.处理中、1.成功")
    private Integer status;

    /** 交易完成时间 */
    @Excel(name = "交易完成时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date completedTime;

    /** 请求第三方提交的参数(包含备注参数) */
    @Excel(name = "请求第三方提交的参数(包含备注参数)")
    private String reqParams;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setServiceOrderNo(String serviceOrderNo) 
    {
        this.serviceOrderNo = serviceOrderNo;
    }

    public String getServiceOrderNo() 
    {
        return serviceOrderNo;
    }
    public void setServiceType(Integer serviceType) 
    {
        this.serviceType = serviceType;
    }

    public Integer getServiceType() 
    {
        return serviceType;
    }
    public void setOrderNo(String orderNo) 
    {
        this.orderNo = orderNo;
    }

    public String getOrderNo() 
    {
        return orderNo;
    }
    public void setPayType(Integer payType) 
    {
        this.payType = payType;
    }

    public Integer getPayType() 
    {
        return payType;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }
    public void setCompletedTime(Date completedTime) 
    {
        this.completedTime = completedTime;
    }

    public Date getCompletedTime() 
    {
        return completedTime;
    }
    public void setReqParams(String reqParams) 
    {
        this.reqParams = reqParams;
    }

    public String getReqParams() 
    {
        return reqParams;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("createTime", getCreateTime())
            .append("userId", getUserId())
            .append("serviceOrderNo", getServiceOrderNo())
            .append("serviceType", getServiceType())
            .append("orderNo", getOrderNo())
            .append("payType", getPayType())
            .append("status", getStatus())
            .append("completedTime", getCompletedTime())
            .append("reqParams", getReqParams())
            .append("remark", getRemark())
            .toString();
    }
}
