package com.ruoyi.project.bajiaostar.userBank.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.bajiaostar.userBank.domain.UserBank;
import com.ruoyi.project.bajiaostar.userBank.service.IUserBankService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 用户提现卡管理Controller
 * 
 * @author 谢少辉
 * @date 2024-04-10
 */
@Controller
@RequestMapping("/bajiaostar/userBank")
public class UserBankController extends BaseController
{
    private String prefix = "bajiaostar/userBank";

    @Autowired
    private IUserBankService userBankService;

    @RequiresPermissions("bajiaostar:userBank:view")
    @GetMapping()
    public String userBank()
    {
        return prefix + "/userBank";
    }

    /**
     * 查询用户提现卡管理列表
     */
    @RequiresPermissions("bajiaostar:userBank:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(UserBank userBank)
    {
        startPage();
        List<UserBank> list = userBankService.selectUserBankList(userBank);
        return getDataTable(list);
    }

    /**
     * 导出用户提现卡管理列表
     */
    @RequiresPermissions("bajiaostar:userBank:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(UserBank userBank)
    {
        List<UserBank> list = userBankService.selectUserBankList(userBank);
        ExcelUtil<UserBank> util = new ExcelUtil<UserBank>(UserBank.class);
        return util.exportExcel(list, "userBank");
    }

    /**
     * 新增用户提现卡管理
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存用户提现卡管理
     */
    @RequiresPermissions("bajiaostar:userBank:add")
    @Log(title = "用户提现卡管理", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(UserBank userBank)
    {
        return toAjax(userBankService.insertUserBank(userBank));
    }

    /**
     * 修改用户提现卡管理
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        UserBank userBank = userBankService.selectUserBankById(id);
        mmap.put("userBank", userBank);
        return prefix + "/edit";
    }

    /**
     * 修改保存用户提现卡管理
     */
    @RequiresPermissions("bajiaostar:userBank:edit")
    @Log(title = "用户提现卡管理", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(UserBank userBank)
    {
        return toAjax(userBankService.updateUserBank(userBank));
    }

    /**
     * 删除用户提现卡管理
     */
    @RequiresPermissions("bajiaostar:userBank:remove")
    @Log(title = "用户提现卡管理", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(userBankService.deleteUserBankByIds(ids));
    }
}
