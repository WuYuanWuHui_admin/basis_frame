package com.ruoyi.project.bajiaostar.userBank.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 用户提现卡管理对象 t_user_bank
 * 
 * @author 谢少辉
 * @date 2024-04-10
 */
public class UserBank extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 账号 */
    @Excel(name = "账号")
    private String account;

    /** 账号对应姓名 */
    @Excel(name = "账号对应姓名")
    private String userName;

    /** 身份证号 */
    @Excel(name = "身份证号")
    private String idCard;

    /** 银行 */
    @Excel(name = "银行")
    private String bank;

    /** 开户地址 */
    @Excel(name = "开户地址")
    private String openAddress;

    /** 账户类型 0:支付宝 1：微信 2：银行卡  */
    @Excel(name = "账户类型 0:支付宝 1：微信 2：银行卡 ")
    private Integer type;

    /** 微信提现openid */
    @Excel(name = "微信提现openid")
    private String openId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setAccount(String account) 
    {
        this.account = account;
    }

    public String getAccount() 
    {
        return account;
    }
    public void setUserName(String userName) 
    {
        this.userName = userName;
    }

    public String getUserName() 
    {
        return userName;
    }
    public void setIdCard(String idCard) 
    {
        this.idCard = idCard;
    }

    public String getIdCard() 
    {
        return idCard;
    }
    public void setBank(String bank) 
    {
        this.bank = bank;
    }

    public String getBank() 
    {
        return bank;
    }
    public void setOpenAddress(String openAddress) 
    {
        this.openAddress = openAddress;
    }

    public String getOpenAddress() 
    {
        return openAddress;
    }
    public void setType(Integer type) 
    {
        this.type = type;
    }

    public Integer getType() 
    {
        return type;
    }
    public void setOpenId(String openId) 
    {
        this.openId = openId;
    }

    public String getOpenId() 
    {
        return openId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .append("userId", getUserId())
            .append("account", getAccount())
            .append("userName", getUserName())
            .append("idCard", getIdCard())
            .append("bank", getBank())
            .append("openAddress", getOpenAddress())
            .append("type", getType())
            .append("openId", getOpenId())
            .toString();
    }
}
