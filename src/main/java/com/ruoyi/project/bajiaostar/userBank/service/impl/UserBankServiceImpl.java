package com.ruoyi.project.bajiaostar.userBank.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.bajiaostar.userBank.mapper.UserBankMapper;
import com.ruoyi.project.bajiaostar.userBank.domain.UserBank;
import com.ruoyi.project.bajiaostar.userBank.service.IUserBankService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 用户提现卡管理Service业务层处理
 * 
 * @author 谢少辉
 * @date 2024-04-10
 */
@Service
public class UserBankServiceImpl implements IUserBankService 
{
    @Autowired
    private UserBankMapper userBankMapper;

    /**
     * 查询用户提现卡管理
     * 
     * @param id 用户提现卡管理ID
     * @return 用户提现卡管理
     */
    @Override
    public UserBank selectUserBankById(Long id)
    {
        return userBankMapper.selectUserBankById(id);
    }

    /**
     * 查询用户提现卡管理列表
     * 
     * @param userBank 用户提现卡管理
     * @return 用户提现卡管理
     */
    @Override
    public List<UserBank> selectUserBankList(UserBank userBank)
    {
        return userBankMapper.selectUserBankList(userBank);
    }

    /**
     * 新增用户提现卡管理
     * 
     * @param userBank 用户提现卡管理
     * @return 结果
     */
    @Override
    public int insertUserBank(UserBank userBank)
    {
        userBank.setCreateTime(DateUtils.getNowDate());
        return userBankMapper.insertUserBank(userBank);
    }

    /**
     * 修改用户提现卡管理
     * 
     * @param userBank 用户提现卡管理
     * @return 结果
     */
    @Override
    public int updateUserBank(UserBank userBank)
    {
        userBank.setUpdateTime(DateUtils.getNowDate());
        return userBankMapper.updateUserBank(userBank);
    }

    /**
     * 删除用户提现卡管理对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteUserBankByIds(String ids)
    {
        return userBankMapper.deleteUserBankByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除用户提现卡管理信息
     * 
     * @param id 用户提现卡管理ID
     * @return 结果
     */
    @Override
    public int deleteUserBankById(Long id)
    {
        return userBankMapper.deleteUserBankById(id);
    }
}
