package com.ruoyi.project.bajiaostar.userBank.service;

import com.ruoyi.project.bajiaostar.userBank.domain.UserBank;
import java.util.List;

/**
 * 用户提现卡管理Service接口
 * 
 * @author 谢少辉
 * @date 2024-04-10
 */
public interface IUserBankService 
{
    /**
     * 查询用户提现卡管理
     * 
     * @param id 用户提现卡管理ID
     * @return 用户提现卡管理
     */
    public UserBank selectUserBankById(Long id);

    /**
     * 查询用户提现卡管理列表
     * 
     * @param userBank 用户提现卡管理
     * @return 用户提现卡管理集合
     */
    public List<UserBank> selectUserBankList(UserBank userBank);

    /**
     * 新增用户提现卡管理
     * 
     * @param userBank 用户提现卡管理
     * @return 结果
     */
    public int insertUserBank(UserBank userBank);

    /**
     * 修改用户提现卡管理
     * 
     * @param userBank 用户提现卡管理
     * @return 结果
     */
    public int updateUserBank(UserBank userBank);

    /**
     * 批量删除用户提现卡管理
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteUserBankByIds(String ids);

    /**
     * 删除用户提现卡管理信息
     * 
     * @param id 用户提现卡管理ID
     * @return 结果
     */
    public int deleteUserBankById(Long id);
}
