package com.ruoyi.project.bajiaostar.userSignIn.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.bajiaostar.userSignIn.domain.UserSignIn;
import com.ruoyi.project.bajiaostar.userSignIn.service.IUserSignInService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 用户签到信息Controller
 * 
 * @author 谢少辉
 * @date 2024-04-11
 */
@Controller
@RequestMapping("/bajiaostar/userSignIn")
public class UserSignInController extends BaseController
{
    private String prefix = "bajiaostar/userSignIn";

    @Autowired
    private IUserSignInService userSignInService;

    @RequiresPermissions("bajiaostar:userSignIn:view")
    @GetMapping()
    public String userSignIn()
    {
        return prefix + "/userSignIn";
    }

    /**
     * 查询用户签到信息列表
     */
    @RequiresPermissions("bajiaostar:userSignIn:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(UserSignIn userSignIn)
    {
        startPage();
        List<UserSignIn> list = userSignInService.selectUserSignInList(userSignIn);
        return getDataTable(list);
    }

    /**
     * 导出用户签到信息列表
     */
    @RequiresPermissions("bajiaostar:userSignIn:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(UserSignIn userSignIn)
    {
        List<UserSignIn> list = userSignInService.selectUserSignInList(userSignIn);
        ExcelUtil<UserSignIn> util = new ExcelUtil<UserSignIn>(UserSignIn.class);
        return util.exportExcel(list, "userSignIn");
    }

    /**
     * 新增用户签到信息
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存用户签到信息
     */
    @RequiresPermissions("bajiaostar:userSignIn:add")
    @Log(title = "用户签到信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(UserSignIn userSignIn)
    {
        return toAjax(userSignInService.insertUserSignIn(userSignIn));
    }

    /**
     * 修改用户签到信息
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        UserSignIn userSignIn = userSignInService.selectUserSignInById(id);
        mmap.put("userSignIn", userSignIn);
        return prefix + "/edit";
    }

    /**
     * 修改保存用户签到信息
     */
    @RequiresPermissions("bajiaostar:userSignIn:edit")
    @Log(title = "用户签到信息", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(UserSignIn userSignIn)
    {
        return toAjax(userSignInService.updateUserSignIn(userSignIn));
    }

    /**
     * 删除用户签到信息
     */
    @RequiresPermissions("bajiaostar:userSignIn:remove")
    @Log(title = "用户签到信息", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(userSignInService.deleteUserSignInByIds(ids));
    }
}
