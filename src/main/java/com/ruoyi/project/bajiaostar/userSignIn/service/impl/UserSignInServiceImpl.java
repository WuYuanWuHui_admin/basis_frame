package com.ruoyi.project.bajiaostar.userSignIn.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.bajiaostar.userSignIn.mapper.UserSignInMapper;
import com.ruoyi.project.bajiaostar.userSignIn.domain.UserSignIn;
import com.ruoyi.project.bajiaostar.userSignIn.service.IUserSignInService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 用户签到信息Service业务层处理
 * 
 * @author 谢少辉
 * @date 2024-04-11
 */
@Service
public class UserSignInServiceImpl implements IUserSignInService 
{
    @Autowired
    private UserSignInMapper userSignInMapper;

    /**
     * 查询用户签到信息
     * 
     * @param id 用户签到信息ID
     * @return 用户签到信息
     */
    @Override
    public UserSignIn selectUserSignInById(Long id)
    {
        return userSignInMapper.selectUserSignInById(id);
    }

    /**
     * 查询用户签到信息列表
     * 
     * @param userSignIn 用户签到信息
     * @return 用户签到信息
     */
    @Override
    public List<UserSignIn> selectUserSignInList(UserSignIn userSignIn)
    {
        return userSignInMapper.selectUserSignInList(userSignIn);
    }

    /**
     * 新增用户签到信息
     * 
     * @param userSignIn 用户签到信息
     * @return 结果
     */
    @Override
    public int insertUserSignIn(UserSignIn userSignIn)
    {
        return userSignInMapper.insertUserSignIn(userSignIn);
    }

    /**
     * 修改用户签到信息
     * 
     * @param userSignIn 用户签到信息
     * @return 结果
     */
    @Override
    public int updateUserSignIn(UserSignIn userSignIn)
    {
        return userSignInMapper.updateUserSignIn(userSignIn);
    }

    /**
     * 删除用户签到信息对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteUserSignInByIds(String ids)
    {
        return userSignInMapper.deleteUserSignInByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除用户签到信息信息
     * 
     * @param id 用户签到信息ID
     * @return 结果
     */
    @Override
    public int deleteUserSignInById(Long id)
    {
        return userSignInMapper.deleteUserSignInById(id);
    }

    /**
     * 查询当月签到数据
     *
     * @param id
     * @return
     */
    @Override
    public List<UserSignIn> selectUserSignInForData(Long id) {

        return userSignInMapper.selectUserSignInForData(id);
    }

    /**
     * 查询用户当日签到
     *
     * @param userId
     * @return
     */
    @Override
    public UserSignIn findUserSignInData(Long userId) {
        return userSignInMapper.findUserSignInData(userId);
    }

    /**
     * 获取用户当天团队活跃度
     *
     * @param userId
     * @return
     */
    @Override
    public Integer findUserSignInDataByUserId(Long userId) {
        return userSignInMapper.findUserSignInDataByUserId(userId);
    }

    /**
     * 查询当天是否签到
     *
     * @param userId
     * @param taskId
     * @return
     */
    @Override
    public UserSignIn findUserSignInDataByUserIdAndTaskId(Long userId, Long taskId) {
        return userSignInMapper.findUserSignInDataByUserIdAndTaskId(userId,taskId);
    }

    /**
     * 查询今日签到数据
     *
     * @return
     */
    @Override
    public List<UserSignIn> findDaySignIn() {
        return userSignInMapper.findDaySignIn();
    }
}
