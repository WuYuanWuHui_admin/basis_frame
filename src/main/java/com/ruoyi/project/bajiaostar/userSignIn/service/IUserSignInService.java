package com.ruoyi.project.bajiaostar.userSignIn.service;

import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.project.bajiaostar.userSignIn.domain.UserSignIn;
import com.ruoyi.project.bajiaostar.userSignInTask.domain.UserSignInTask;

import java.util.List;

/**
 * 用户签到信息Service接口
 * 
 * @author 谢少辉
 * @date 2024-04-11
 */
public interface IUserSignInService 
{
    /**
     * 查询用户签到信息
     * 
     * @param id 用户签到信息ID
     * @return 用户签到信息
     */
    public UserSignIn selectUserSignInById(Long id);

    /**
     * 查询用户签到信息列表
     * 
     * @param userSignIn 用户签到信息
     * @return 用户签到信息集合
     */
    public List<UserSignIn> selectUserSignInList(UserSignIn userSignIn);

    /**
     * 新增用户签到信息
     * 
     * @param userSignIn 用户签到信息
     * @return 结果
     */
    public int insertUserSignIn(UserSignIn userSignIn);

    /**
     * 修改用户签到信息
     * 
     * @param userSignIn 用户签到信息
     * @return 结果
     */
    public int updateUserSignIn(UserSignIn userSignIn);

    /**
     * 批量删除用户签到信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteUserSignInByIds(String ids);

    /**
     * 删除用户签到信息信息
     * 
     * @param id 用户签到信息ID
     * @return 结果
     */
    public int deleteUserSignInById(Long id);

    /**
     * 查询当月签到数据
     * @param id
     * @return
     */
    public List<UserSignIn> selectUserSignInForData(Long id);

    /**
     * 查询用户当日签到
     * @param userId
     * @return
     */
    UserSignIn findUserSignInData(Long userId);

    /**
     * 获取用户当天团队活跃度
     * @param userId
     * @return
     */
    Integer findUserSignInDataByUserId(Long userId);

    /**
     * 查询当天是否签到
     * @param userId
     * @param taskId
     * @return
     */
    UserSignIn findUserSignInDataByUserIdAndTaskId(Long userId,Long taskId);

    /**
     * 查询今日签到数据
     * @return
     */
    public List<UserSignIn> findDaySignIn();
}
