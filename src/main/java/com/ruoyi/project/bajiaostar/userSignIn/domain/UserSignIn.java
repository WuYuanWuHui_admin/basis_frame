package com.ruoyi.project.bajiaostar.userSignIn.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;
import java.util.Date;

/**
 * 用户签到信息对象 t_user_sign_in
 * 
 * @author 谢少辉
 * @date 2024-04-11
 */
public class UserSignIn extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 用户签到任务id */
    @Excel(name = "用户签到任务id")
    private Long userSigninId;

    /** 签到时间 */
    @Excel(name = "签到时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date signInDate;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setSignInDate(Date signInDate) 
    {
        this.signInDate = signInDate;
    }

    public Date getSignInDate() 
    {
        return signInDate;
    }

    public Long getUserSigninId() {
        return userSigninId;
    }

    public void setUserSigninId(Long userSigninId) {
        this.userSigninId = userSigninId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("signInDate", getSignInDate())
            .toString();
    }
}
