package com.ruoyi.project.bajiaostar.transHall.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;
import java.util.Date;

/**
 * 交易大厅对象 t_trans_hall
 * 
 * @author 谢少辉
 * @date 2024-04-15
 */
public class TransHall extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 用户名 */
    @Excel(name = "用户名")
    private String userName;

    /** 交易类型：0 出售 , 1 求购 */
    @Excel(name = "交易类型：0 出售 , 1 求购")
    private Integer transType;

    /** 截止日期 */
    @Excel(name = "截止日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date emdTime;

    /** 交易状态：0 交易中， 1 交易结束 */
    @Excel(name = "交易状态：0 交易中， 1 交易结束")
    private Integer transStatus;

    /** 亦豆 */
    @Excel(name = "亦豆")
    private Double yidouAccount;

    /** 金额 */
    @Excel(name = "金额")
    private Double account;

    /** 交易用户id */
    @Excel(name = "交易用户id")
    private Long transUserId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setUserName(String userName) 
    {
        this.userName = userName;
    }

    public String getUserName() 
    {
        return userName;
    }
    public void setTransType(Integer transType) 
    {
        this.transType = transType;
    }

    public Integer getTransType() 
    {
        return transType;
    }
    public void setEmdTime(Date emdTime) 
    {
        this.emdTime = emdTime;
    }

    public Date getEmdTime() 
    {
        return emdTime;
    }
    public void setTransStatus(Integer transStatus) 
    {
        this.transStatus = transStatus;
    }

    public Integer getTransStatus() 
    {
        return transStatus;
    }
    public void setYidouAccount(Double yidouAccount) 
    {
        this.yidouAccount = yidouAccount;
    }

    public Double getYidouAccount() 
    {
        return yidouAccount;
    }
    public void setAccount(Double account) 
    {
        this.account = account;
    }

    public Double getAccount() 
    {
        return account;
    }
    public void setTransUserId(Long transUserId) 
    {
        this.transUserId = transUserId;
    }

    public Long getTransUserId() 
    {
        return transUserId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("userName", getUserName())
            .append("transType", getTransType())
            .append("emdTime", getEmdTime())
            .append("transStatus", getTransStatus())
            .append("yidouAccount", getYidouAccount())
            .append("account", getAccount())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("transUserId", getTransUserId())
            .toString();
    }
}
