package com.ruoyi.project.bajiaostar.transHall.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.bajiaostar.transHall.domain.TransHall;
import com.ruoyi.project.bajiaostar.transHall.service.ITransHallService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 交易大厅Controller
 * 
 * @author 谢少辉
 * @date 2024-04-15
 */
@Controller
@RequestMapping("/bajiaostar/transHall")
public class TransHallController extends BaseController
{
    private String prefix = "bajiaostar/transHall";

    @Autowired
    private ITransHallService transHallService;

    @RequiresPermissions("bajiaostar:transHall:view")
    @GetMapping()
    public String transHall()
    {
        return prefix + "/transHall";
    }

    /**
     * 查询交易大厅列表
     */
    @RequiresPermissions("bajiaostar:transHall:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(TransHall transHall)
    {
        startPage();
        List<TransHall> list = transHallService.selectTransHallList(transHall);
        return getDataTable(list);
    }

    /**
     * 导出交易大厅列表
     */
    @RequiresPermissions("bajiaostar:transHall:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(TransHall transHall)
    {
        List<TransHall> list = transHallService.selectTransHallList(transHall);
        ExcelUtil<TransHall> util = new ExcelUtil<TransHall>(TransHall.class);
        return util.exportExcel(list, "transHall");
    }

    /**
     * 新增交易大厅
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存交易大厅
     */
    @RequiresPermissions("bajiaostar:transHall:add")
    @Log(title = "交易大厅", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(TransHall transHall)
    {
        return toAjax(transHallService.insertTransHall(transHall));
    }

    /**
     * 修改交易大厅
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        TransHall transHall = transHallService.selectTransHallById(id);
        mmap.put("transHall", transHall);
        return prefix + "/edit";
    }

    /**
     * 修改保存交易大厅
     */
    @RequiresPermissions("bajiaostar:transHall:edit")
    @Log(title = "交易大厅", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(TransHall transHall)
    {
        return toAjax(transHallService.updateTransHall(transHall));
    }

    /**
     * 删除交易大厅
     */
    @RequiresPermissions("bajiaostar:transHall:remove")
    @Log(title = "交易大厅", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(transHallService.deleteTransHallByIds(ids));
    }
}
