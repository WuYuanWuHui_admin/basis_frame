package com.ruoyi.project.bajiaostar.transHall.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.project.bajiaostar.api.entity.dto.transHall.AddUserTransHallDto;
import com.ruoyi.project.bajiaostar.api.entity.dto.user.ExecuteUserAccountDto;
import com.ruoyi.project.bajiaostar.user.domain.UserApp;
import com.ruoyi.project.bajiaostar.user.service.IUserAppService;
import com.ruoyi.project.bajiaostar.userAccount.domain.UserAccount;
import com.ruoyi.project.bajiaostar.userAccount.service.IUserAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.bajiaostar.transHall.mapper.TransHallMapper;
import com.ruoyi.project.bajiaostar.transHall.domain.TransHall;
import com.ruoyi.project.bajiaostar.transHall.service.ITransHallService;
import com.ruoyi.common.utils.text.Convert;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

/**
 * 交易大厅Service业务层处理
 * 
 * @author 谢少辉
 * @date 2024-04-15
 */
@Service
public class TransHallServiceImpl implements ITransHallService 
{
    @Autowired
    private TransHallMapper transHallMapper;

    @Autowired
    IUserAppService userAppService;
    @Autowired
    IUserAccountService userAccountService;

    /**
     * 查询交易大厅
     * 
     * @param id 交易大厅ID
     * @return 交易大厅
     */
    @Override
    public TransHall selectTransHallById(Long id)
    {
        return transHallMapper.selectTransHallById(id);
    }

    /**
     * 查询交易大厅列表
     * 
     * @param transHall 交易大厅
     * @return 交易大厅
     */
    @Override
    public List<TransHall> selectTransHallList(TransHall transHall)
    {
        return transHallMapper.selectTransHallList(transHall);
    }

    /**
     * 新增交易大厅
     * 
     * @param transHall 交易大厅
     * @return 结果
     */
    @Override
    public int insertTransHall(TransHall transHall)
    {
        transHall.setCreateTime(DateUtils.getNowDate());
        return transHallMapper.insertTransHall(transHall);
    }

    /**
     * 修改交易大厅
     * 
     * @param transHall 交易大厅
     * @return 结果
     */
    @Override
    public int updateTransHall(TransHall transHall)
    {
        transHall.setUpdateTime(DateUtils.getNowDate());
        return transHallMapper.updateTransHall(transHall);
    }

    /**
     * 删除交易大厅对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteTransHallByIds(String ids)
    {
        return transHallMapper.deleteTransHallByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除交易大厅信息
     * 
     * @param id 交易大厅ID
     * @return 结果
     */
    @Override
    public int deleteTransHallById(Long id)
    {
        return transHallMapper.deleteTransHallById(id);
    }

    /**
     * 添加交易信息
     *
     * @param dto
     * @return
     */
    @Override
    @Transactional
    public AjaxResult addUserTransHall(AddUserTransHallDto dto) {
        //查询用户信息
        UserApp userApp = userAppService.selectUserById(dto.getUserId());
        if(ObjectUtils.isEmpty(userApp)){
            return AjaxResult.error("用户不存在");
        }
        UserAccount userAccount = userAccountService.selectUserAccountByUserId(dto.getUserId());
        if(ObjectUtils.isEmpty(userAccount)){
            return AjaxResult.error("用户账户不存在");
        }
        if(dto.getTransType().equals(0)){
            //判断亦豆是否足够
            if(dto.getYidouAccount() > userAccount.getUserAccount()){
                return AjaxResult.error("亦豆余额不足");
            }
            if((userAccount.getUserAccount()-dto.getYidouAccount()) < 10){
                return AjaxResult.error("需要保留10亦豆");
            }
            //扣除用户
            ExecuteUserAccountDto dto1 = new ExecuteUserAccountDto();
            dto1.setUserId(dto.getUserId());
            dto1.setUserAccount(dto.getYidouAccount());
            dto1.setUserType(0);
            dto1.setAccountSysType(0);
            dto1.setAccountName("发布交易扣除亦豆");
            dto1.setAccountType(1);
            userAccountService.executeUserAccount(dto1);

        }else if(dto.getTransType().equals(1)){
            //判断亦豆是否足够
            if(dto.getAccount() > userAccount.getUserCashAccount()){
                return AjaxResult.error("账户余额不足");
            }
            if((userAccount.getUserCashAccount()-dto.getAccount()) < 10){
                return AjaxResult.error("需要保留10亦豆");
            }
            //扣除用户余额
            ExecuteUserAccountDto dto1 = new ExecuteUserAccountDto();
            dto1.setUserId(dto.getUserId());
            dto1.setUserAccount(dto.getAccount());
            dto1.setUserType(0);
            dto1.setAccountSysType(1);
            dto1.setAccountName("发布交易扣除金额");
            dto1.setAccountType(1);
            userAccountService.executeUserAccount(dto1);
        }else {
            return AjaxResult.error("交易类型有误");
        }

        TransHall transHall=new TransHall();
        transHall.setUserId(dto.getUserId());
        transHall.setUserName(userApp.getUserName());
        transHall.setTransType(dto.getTransType());
        transHall.setEmdTime(dto.getEmdTime());
        transHall.setTransStatus(0);
        transHall.setYidouAccount(dto.getYidouAccount());
        transHall.setAccount(dto.getAccount());
        insertTransHall(transHall);
        return AjaxResult.success();
    }

    /**
     * 撤回交易信息
     *
     * @param transHallId
     * @param userId
     * @return
     */
    @Override
    @Transactional
    public AjaxResult withdrawUserTransHall(Long transHallId, Long userId) {
        TransHall transHall = selectTransHallById(transHallId);
        if(ObjectUtils.isEmpty(transHall)){
            return AjaxResult.error("交易信息不存在");
        }
        if(!userId.equals(transHall.getUserId())){
            return AjaxResult.error("撤回只能发布用户撤回");
        }
        if(!transHall.getTransStatus().equals(0)){
            return AjaxResult.error("交易状态有误，请刷新再试");
        }
        if(transHall.getTransType().equals(0)){
            //扣除用户
            ExecuteUserAccountDto dto1 = new ExecuteUserAccountDto();
            dto1.setUserId(transHall.getUserId());
            dto1.setUserAccount(transHall.getYidouAccount());
            dto1.setUserType(0);
            dto1.setAccountSysType(0);
            dto1.setAccountName("撤回交易返还亦豆");
            dto1.setAccountType(0);
            userAccountService.executeUserAccount(dto1);

        }else if(transHall.getTransType().equals(1)){
            //扣除用户余额
            ExecuteUserAccountDto dto1 = new ExecuteUserAccountDto();
            dto1.setUserId(transHall.getUserId());
            dto1.setUserAccount(transHall.getAccount());
            dto1.setUserType(0);
            dto1.setAccountSysType(1);
            dto1.setAccountName("撤回交易返还金额");
            dto1.setAccountType(0);
            userAccountService.executeUserAccount(dto1);
        }
        transHall.setTransStatus(2);
        updateTransHall(transHall);
        return AjaxResult.success();
    }

    /**
     * 购买交易信息
     *
     * @param transHallId
     * @param userId
     * @return
     */
    @Override
    public AjaxResult buyUserTransHall(Long transHallId, Long userId) {
        TransHall transHall = selectTransHallById(transHallId);
        if(ObjectUtils.isEmpty(transHall)){
            return AjaxResult.error("交易信息不存在");
        }
        if(!transHall.getTransStatus().equals(0)){
            return AjaxResult.error("交易状态有误，请刷新再试");
        }
        //查询交易用户账户信息
        UserAccount userAccount = userAccountService.selectUserAccountByUserId(userId);
        if(ObjectUtils.isEmpty(userAccount)){
            return AjaxResult.error("账户信息不存在");
        }
        if(transHall.getTransType().equals(0)){
            //判断用户账户余额是否足够
            if(transHall.getAccount() >userAccount.getUserCashAccount()){
                return AjaxResult.error("账户余额不足");
            }

            if((userAccount.getUserCashAccount()-transHall.getAccount()) < 10){
                return AjaxResult.error("账户需要保留10元");
            }
            //扣除购买者账户余额
            ExecuteUserAccountDto dto1 = new ExecuteUserAccountDto();
            dto1.setUserId(userId);
            dto1.setUserAccount(transHall.getAccount());
            dto1.setUserType(0);
            dto1.setAccountSysType(1);
            dto1.setAccountName("购买交易扣除金额");
            dto1.setAccountType(1);
            userAccountService.executeUserAccount(dto1);
            //增加发布者余额
            ExecuteUserAccountDto dto2 = new ExecuteUserAccountDto();
            dto2.setUserId(transHall.getUserId());
            dto2.setUserAccount(transHall.getAccount());
            dto2.setUserType(0);
            dto2.setAccountSysType(1);
            dto2.setAccountName("交易达成增加账户金额");
            dto2.setAccountType(0);
            userAccountService.executeUserAccount(dto2);
            //增加购买者亦豆
            ExecuteUserAccountDto dto3 = new ExecuteUserAccountDto();
            dto3.setUserId(userId);
            dto3.setUserAccount(transHall.getYidouAccount());
            dto3.setUserType(0);
            dto3.setAccountSysType(0);
            dto3.setAccountName("购买交易增加亦豆");
            dto3.setAccountType(0);
            userAccountService.executeUserAccount(dto3);

        }else if(transHall.getTransType().equals(1)){
            //判断用户账户余额是否足够
            if(transHall.getYidouAccount()  < userAccount.getUserAccount()){
                return AjaxResult.error("账户余额不足");
            }
            if((userAccount.getUserAccount()-transHall.getYidouAccount()) < 10){
                return AjaxResult.error("账户需要保留10亦豆");
            }
            //增加发布者亦豆
            ExecuteUserAccountDto dto3 = new ExecuteUserAccountDto();
            dto3.setUserId(transHall.getUserId());
            dto3.setUserAccount(transHall.getYidouAccount());
            dto3.setUserType(0);
            dto3.setAccountSysType(0);
            dto3.setAccountName("求购交易增加亦豆");
            dto3.setAccountType(0);
            userAccountService.executeUserAccount(dto3);
            //扣除用户亦豆
            ExecuteUserAccountDto dto2 = new ExecuteUserAccountDto();
            dto2.setUserId(userId);
            dto2.setUserAccount(transHall.getYidouAccount());
            dto2.setUserType(0);
            dto2.setAccountSysType(0);
            dto2.setAccountName("求购交易扣除亦豆");
            dto2.setAccountType(1);
            userAccountService.executeUserAccount(dto2);
            //增加购买者余额
            ExecuteUserAccountDto dto1 = new ExecuteUserAccountDto();
            dto1.setUserId(userId);
            dto1.setUserAccount(transHall.getYidouAccount());
            dto1.setUserType(0);
            dto1.setAccountSysType(1);
            dto1.setAccountName("求购交易增加余额");
            dto1.setAccountType(0);
            userAccountService.executeUserAccount(dto1);
        }
        transHall.setTransStatus(2);
        transHall.setTransUserId(userId);
        updateTransHall(transHall);
        return AjaxResult.success();
    }
}
