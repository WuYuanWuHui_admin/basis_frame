package com.ruoyi.project.bajiaostar.transHall.mapper;

import com.ruoyi.project.bajiaostar.transHall.domain.TransHall;
import java.util.List;

/**
 * 交易大厅Mapper接口
 * 
 * @author 谢少辉
 * @date 2024-04-15
 */
public interface TransHallMapper 
{
    /**
     * 查询交易大厅
     * 
     * @param id 交易大厅ID
     * @return 交易大厅
     */
    public TransHall selectTransHallById(Long id);

    /**
     * 查询交易大厅列表
     * 
     * @param transHall 交易大厅
     * @return 交易大厅集合
     */
    public List<TransHall> selectTransHallList(TransHall transHall);

    /**
     * 新增交易大厅
     * 
     * @param transHall 交易大厅
     * @return 结果
     */
    public int insertTransHall(TransHall transHall);

    /**
     * 修改交易大厅
     * 
     * @param transHall 交易大厅
     * @return 结果
     */
    public int updateTransHall(TransHall transHall);

    /**
     * 删除交易大厅
     * 
     * @param id 交易大厅ID
     * @return 结果
     */
    public int deleteTransHallById(Long id);

    /**
     * 批量删除交易大厅
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteTransHallByIds(String[] ids);
}
