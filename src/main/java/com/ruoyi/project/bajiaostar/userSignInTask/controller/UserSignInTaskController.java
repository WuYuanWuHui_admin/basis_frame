package com.ruoyi.project.bajiaostar.userSignInTask.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.bajiaostar.userSignInTask.domain.UserSignInTask;
import com.ruoyi.project.bajiaostar.userSignInTask.service.IUserSignInTaskService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 用户任务Controller
 * 
 * @author 谢少辉
 * @date 2024-04-11
 */
@Controller
@RequestMapping("/bajiaostar/userSignInTask")
public class UserSignInTaskController extends BaseController
{
    private String prefix = "bajiaostar/userSignInTask";

    @Autowired
    private IUserSignInTaskService userSignInTaskService;

    @RequiresPermissions("bajiaostar:userSignInTask:view")
    @GetMapping()
    public String userSignInTask()
    {
        return prefix + "/userSignInTask";
    }

    /**
     * 查询用户任务列表
     */
    @RequiresPermissions("bajiaostar:userSignInTask:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(UserSignInTask userSignInTask)
    {
        startPage();
        List<UserSignInTask> list = userSignInTaskService.selectUserSignInTaskList(userSignInTask);
        return getDataTable(list);
    }

    /**
     * 导出用户任务列表
     */
    @RequiresPermissions("bajiaostar:userSignInTask:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(UserSignInTask userSignInTask)
    {
        List<UserSignInTask> list = userSignInTaskService.selectUserSignInTaskList(userSignInTask);
        ExcelUtil<UserSignInTask> util = new ExcelUtil<UserSignInTask>(UserSignInTask.class);
        return util.exportExcel(list, "userSignInTask");
    }

    /**
     * 新增用户任务
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存用户任务
     */
    @RequiresPermissions("bajiaostar:userSignInTask:add")
    @Log(title = "用户任务", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(UserSignInTask userSignInTask)
    {
        return toAjax(userSignInTaskService.insertUserSignInTask(userSignInTask));
    }

    /**
     * 修改用户任务
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        UserSignInTask userSignInTask = userSignInTaskService.selectUserSignInTaskById(id);
        mmap.put("userSignInTask", userSignInTask);
        return prefix + "/edit";
    }

    /**
     * 修改保存用户任务
     */
    @RequiresPermissions("bajiaostar:userSignInTask:edit")
    @Log(title = "用户任务", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(UserSignInTask userSignInTask)
    {
        return toAjax(userSignInTaskService.updateUserSignInTask(userSignInTask));
    }

    /**
     * 删除用户任务
     */
    @RequiresPermissions("bajiaostar:userSignInTask:remove")
    @Log(title = "用户任务", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(userSignInTaskService.deleteUserSignInTaskByIds(ids));
    }
}
