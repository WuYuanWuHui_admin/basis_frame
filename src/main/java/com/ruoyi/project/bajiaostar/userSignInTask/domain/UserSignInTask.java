package com.ruoyi.project.bajiaostar.userSignInTask.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;
import java.util.Date;

/**
 * 用户任务对象 t_user_sign_in_task
 * 
 * @author 谢少辉
 * @date 2024-04-11
 */
public class UserSignInTask extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 任务id */
    @Excel(name = "任务id")
    private Long taskId;

    /** 任务名称 */
    @Excel(name = "任务名称")
    private String taskName;

    /** 任务开始日期 */
    @Excel(name = "任务开始日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date taskStartTime;

    /** 任务结束日期 */
    @Excel(name = "任务结束日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date taskEndTime;

    /** 任务状态：0 未结束 1 已结束 */
    @Excel(name = "任务状态：0 未结束 1 已结束")
    private Long taskStatus;

    /** 总奖励 */
    @Excel(name = "总奖励")
    private Long totalReward;

    /** 任务周期 */
    @Excel(name = "任务周期")
    private Long taskCycleCount;

    /** 所需亦豆 */
    @Excel(name = "所需亦豆")
    private Double taskYidouCount;

    /** 活跃度 */
    @Excel(name = "活跃度")
    private Double activityLevel;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setTaskId(Long taskId) 
    {
        this.taskId = taskId;
    }

    public Long getTaskId() 
    {
        return taskId;
    }
    public void setTaskName(String taskName) 
    {
        this.taskName = taskName;
    }

    public String getTaskName() 
    {
        return taskName;
    }
    public void setTaskStartTime(Date taskStartTime) 
    {
        this.taskStartTime = taskStartTime;
    }

    public Date getTaskStartTime() 
    {
        return taskStartTime;
    }
    public void setTaskEndTime(Date taskEndTime) 
    {
        this.taskEndTime = taskEndTime;
    }

    public Date getTaskEndTime() 
    {
        return taskEndTime;
    }
    public void setTaskStatus(Long taskStatus) 
    {
        this.taskStatus = taskStatus;
    }

    public Long getTaskStatus() 
    {
        return taskStatus;
    }
    public void setTotalReward(Long totalReward) 
    {
        this.totalReward = totalReward;
    }

    public Long getTotalReward() 
    {
        return totalReward;
    }
    public void setTaskCycleCount(Long taskCycleCount) 
    {
        this.taskCycleCount = taskCycleCount;
    }

    public Long getTaskCycleCount() 
    {
        return taskCycleCount;
    }
    public void setTaskYidouCount(Double taskYidouCount) 
    {
        this.taskYidouCount = taskYidouCount;
    }

    public Double getTaskYidouCount() 
    {
        return taskYidouCount;
    }
    public void setActivityLevel(Double activityLevel) 
    {
        this.activityLevel = activityLevel;
    }

    public Double getActivityLevel() 
    {
        return activityLevel;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("taskId", getTaskId())
            .append("taskName", getTaskName())
            .append("taskStartTime", getTaskStartTime())
            .append("taskEndTime", getTaskEndTime())
            .append("taskStatus", getTaskStatus())
            .append("totalReward", getTotalReward())
            .append("taskCycleCount", getTaskCycleCount())
            .append("taskYidouCount", getTaskYidouCount())
            .append("activityLevel", getActivityLevel())
            .toString();
    }
}
