package com.ruoyi.project.bajiaostar.userSignInTask.service.impl;

import java.math.BigDecimal;
import java.util.List;

import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.project.bajiaostar.api.entity.dto.user.ExecuteUserAccountDto;
import com.ruoyi.project.bajiaostar.user.domain.UserApp;
import com.ruoyi.project.bajiaostar.user.service.IUserAppService;
import com.ruoyi.project.bajiaostar.userAccount.service.IUserAccountService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.bajiaostar.userSignInTask.mapper.UserSignInTaskMapper;
import com.ruoyi.project.bajiaostar.userSignInTask.domain.UserSignInTask;
import com.ruoyi.project.bajiaostar.userSignInTask.service.IUserSignInTaskService;
import com.ruoyi.common.utils.text.Convert;
import org.springframework.util.ObjectUtils;

/**
 * 用户任务Service业务层处理
 * 
 * @author 谢少辉
 * @date 2024-04-11
 */
@Service
@Slf4j
public class UserSignInTaskServiceImpl implements IUserSignInTaskService 
{
    @Autowired
    private UserSignInTaskMapper userSignInTaskMapper;

    @Autowired
    IUserAccountService userAccountService;

    @Autowired
    IUserAppService userAppService;

    /**
     * 查询用户任务
     * 
     * @param id 用户任务ID
     * @return 用户任务
     */
    @Override
    public UserSignInTask selectUserSignInTaskById(Long id)
    {
        return userSignInTaskMapper.selectUserSignInTaskById(id);
    }

    /**
     * 查询用户任务列表
     * 
     * @param userSignInTask 用户任务
     * @return 用户任务
     */
    @Override
    public List<UserSignInTask> selectUserSignInTaskList(UserSignInTask userSignInTask)
    {
        return userSignInTaskMapper.selectUserSignInTaskList(userSignInTask);
    }

    /**
     * 新增用户任务
     * 
     * @param userSignInTask 用户任务
     * @return 结果
     */
    @Override
    public int insertUserSignInTask(UserSignInTask userSignInTask)
    {
        return userSignInTaskMapper.insertUserSignInTask(userSignInTask);
    }

    /**
     * 修改用户任务
     * 
     * @param userSignInTask 用户任务
     * @return 结果
     */
    @Override
    public int updateUserSignInTask(UserSignInTask userSignInTask)
    {
        return userSignInTaskMapper.updateUserSignInTask(userSignInTask);
    }

    /**
     * 删除用户任务对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteUserSignInTaskByIds(String ids)
    {
        return userSignInTaskMapper.deleteUserSignInTaskByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除用户任务信息
     * 
     * @param id 用户任务ID
     * @return 结果
     */
    @Override
    public int deleteUserSignInTaskById(Long id)
    {
        return userSignInTaskMapper.deleteUserSignInTaskById(id);
    }

    /**
     * 更新当天过期任务
     *
     * @return
     */
    @Override
    public int updateUserSignTask() {
        return userSignInTaskMapper.updateUserSignTask();
    }

    /**
     * 根据用户查询礼包
     *
     * @param userId
     * @param taskId
     * @return
     */
    @Override
    public int findUserSignTaskByUserIdAndTaskId(Long userId, Integer taskId) {
        return userSignInTaskMapper.findUserSignTaskByUserIdAndTaskId(userId,taskId);
    }


    /**
     * 分销
     *
     * @param userId
     * @param totalAmount
     * @return
     */
    @Override
    public AjaxResult userPushDistribution(Long userId, BigDecimal totalAmount) {
        UserApp userApp = userAppService.selectUserById(userId);
        if(ObjectUtils.isEmpty(userApp)){
            return AjaxResult.error("账户信息消失啦");
        }
        distribution(userApp,totalAmount,userApp.getPushUserId(),true);
        return AjaxResult.success();
    }
    /**
     * 分润
     * @param userApp 当前用户
     * @param amount  分润总金额
     * @param directPushFlag 是否直推
     * @param lastTimePsuhUserId 上一次分润用户
     */
    private void distribution(UserApp userApp, BigDecimal amount,Long lastTimePsuhUserId,boolean directPushFlag){
        if(userApp.getUserIdentityLevel().equals(10)){
            log.error("用户级别达到10级，无法继续分润");
            return;
        }
        if(ObjectUtils.isEmpty(userApp.getPushUserId())){
            log.error("推荐人不存在，无法继续分润");
            return;
        }
        //获取推荐人信息
        UserApp pushuser = userAppService.selectUserById(lastTimePsuhUserId);
        if(ObjectUtils.isEmpty(pushuser)){
            log.error("推荐人不存在，无法继续分润");
            return;
        }
        if(userApp.getUserIdentityLevel() >= pushuser.getUserIdentityLevel() && !directPushFlag){
            log.error("推荐人级别小于当前用户，无法继续分润");
            //继续执行下一个分润
            distribution(pushuser,amount,pushuser.getPushUserId(),false);
            return;
        }
        BigDecimal pushAamount=BigDecimal.ZERO;
        //直推获取百分之十分润
        if(directPushFlag){
            pushAamount= amount.multiply(new BigDecimal(0.1));
        }else{
            //获取分润金额
           if(userApp.getUserIdentityLevel().equals(1)){
               pushAamount= amount.multiply(new BigDecimal(0.02));
            }else if(userApp.getUserIdentityLevel().equals(2)){
               pushAamount= amount.multiply(new BigDecimal(0.04));
            }else if(userApp.getUserIdentityLevel().equals(3)){
               pushAamount= amount.multiply(new BigDecimal(0.06));
            }else if(userApp.getUserIdentityLevel().equals(4)){
               pushAamount= amount.multiply(new BigDecimal(0.08));
            }else if(userApp.getUserIdentityLevel().equals(5)){
               pushAamount= amount.multiply(new BigDecimal(0.1));
            }else if(userApp.getUserIdentityLevel().equals(6)){
               pushAamount= amount.multiply(new BigDecimal(0.12));
            }else if(userApp.getUserIdentityLevel().equals(7)){
               pushAamount= amount.multiply(new BigDecimal(0.14));
            }else if(userApp.getUserIdentityLevel().equals(8)){
               pushAamount= amount.multiply(new BigDecimal(0.16));
            }else if(userApp.getUserIdentityLevel().equals(9)){
               pushAamount= amount.multiply(new BigDecimal(0.18));
            }else if(userApp.getUserIdentityLevel().equals(10)){
               pushAamount= amount.multiply(new BigDecimal(0.2));
            }
        }
        //添加账户余额明细
        ExecuteUserAccountDto dto=new ExecuteUserAccountDto();
        dto.setUserId(pushuser.getId());
        dto.setUserAccount(pushAamount.doubleValue());
        dto.setUserType(0);
        dto.setAccountSysType(1);
        dto.setAccountName("签到分润奖励");
        dto.setAccountType(0);
        userAccountService.executeUserAccount(dto);
        //继续执行下一个分润
        distribution(pushuser,amount,pushuser.getPushUserId(),false);
    }
}
