package com.ruoyi.project.bajiaostar.userSignInTask.service;

import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.project.bajiaostar.userSignInTask.domain.UserSignInTask;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

/**
 * 用户任务Service接口
 * 
 * @author 谢少辉
 * @date 2024-04-11
 */
public interface IUserSignInTaskService 
{
    /**
     * 查询用户任务
     * 
     * @param id 用户任务ID
     * @return 用户任务
     */
    public UserSignInTask selectUserSignInTaskById(Long id);

    /**
     * 查询用户任务列表
     * 
     * @param userSignInTask 用户任务
     * @return 用户任务集合
     */
    public List<UserSignInTask> selectUserSignInTaskList(UserSignInTask userSignInTask);

    /**
     * 新增用户任务
     * 
     * @param userSignInTask 用户任务
     * @return 结果
     */
    public int insertUserSignInTask(UserSignInTask userSignInTask);

    /**
     * 修改用户任务
     * 
     * @param userSignInTask 用户任务
     * @return 结果
     */
    public int updateUserSignInTask(UserSignInTask userSignInTask);

    /**
     * 批量删除用户任务
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteUserSignInTaskByIds(String ids);

    /**
     * 删除用户任务信息
     * 
     * @param id 用户任务ID
     * @return 结果
     */
    public int deleteUserSignInTaskById(Long id);

    /**
     * 更新当天过期任务
     * @return
     */
    int updateUserSignTask();

    /**
     * 分润奖励
     * @param userId
     * @param totalAmount
     * @return
     */
    AjaxResult userPushDistribution(Long userId, BigDecimal totalAmount);

    /**
     * 根据用户查询礼包
     * @return
     */
    int findUserSignTaskByUserIdAndTaskId(Long userId, Integer taskId);
}
