package com.ruoyi.project.bajiaostar.userShopFans.mapper;

import com.ruoyi.project.bajiaostar.userShopFans.domain.UserShopFans;
import java.util.List;

/**
 * 用户关注店铺Mapper接口
 * 
 * @author 谢少辉
 * @date 2024-03-31
 */
public interface UserShopFansMapper 
{
    /**
     * 查询用户关注店铺
     * 
     * @param id 用户关注店铺ID
     * @return 用户关注店铺
     */
    public UserShopFans selectUserShopFansById(Long id);

    /**
     * 查询用户关注店铺列表
     * 
     * @param userShopFans 用户关注店铺
     * @return 用户关注店铺集合
     */
    public List<UserShopFans> selectUserShopFansList(UserShopFans userShopFans);

    /**
     * 新增用户关注店铺
     * 
     * @param userShopFans 用户关注店铺
     * @return 结果
     */
    public int insertUserShopFans(UserShopFans userShopFans);

    /**
     * 修改用户关注店铺
     * 
     * @param userShopFans 用户关注店铺
     * @return 结果
     */
    public int updateUserShopFans(UserShopFans userShopFans);

    /**
     * 删除用户关注店铺
     * 
     * @param id 用户关注店铺ID
     * @return 结果
     */
    public int deleteUserShopFansById(Long id);

    /**
     * 批量删除用户关注店铺
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteUserShopFansByIds(String[] ids);

    /**
     * 查询关注店铺的粉丝数
     * @param merchantUserId
     * @return
     */
    int findUserByMerchantUserId(Long merchantUserId);
}
