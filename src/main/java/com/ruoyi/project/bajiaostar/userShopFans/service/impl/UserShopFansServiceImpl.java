package com.ruoyi.project.bajiaostar.userShopFans.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.bajiaostar.userShopFans.mapper.UserShopFansMapper;
import com.ruoyi.project.bajiaostar.userShopFans.domain.UserShopFans;
import com.ruoyi.project.bajiaostar.userShopFans.service.IUserShopFansService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 用户关注店铺Service业务层处理
 * 
 * @author 谢少辉
 * @date 2024-03-31
 */
@Service
public class UserShopFansServiceImpl implements IUserShopFansService 
{
    @Autowired
    private UserShopFansMapper userShopFansMapper;

    /**
     * 查询用户关注店铺
     * 
     * @param id 用户关注店铺ID
     * @return 用户关注店铺
     */
    @Override
    public UserShopFans selectUserShopFansById(Long id)
    {
        return userShopFansMapper.selectUserShopFansById(id);
    }

    /**
     * 查询用户关注店铺列表
     * 
     * @param userShopFans 用户关注店铺
     * @return 用户关注店铺
     */
    @Override
    public List<UserShopFans> selectUserShopFansList(UserShopFans userShopFans)
    {
        return userShopFansMapper.selectUserShopFansList(userShopFans);
    }

    /**
     * 新增用户关注店铺
     * 
     * @param userShopFans 用户关注店铺
     * @return 结果
     */
    @Override
    public int insertUserShopFans(UserShopFans userShopFans)
    {
        userShopFans.setCreateTime(DateUtils.getNowDate());
        return userShopFansMapper.insertUserShopFans(userShopFans);
    }

    /**
     * 修改用户关注店铺
     * 
     * @param userShopFans 用户关注店铺
     * @return 结果
     */
    @Override
    public int updateUserShopFans(UserShopFans userShopFans)
    {
        userShopFans.setUpdateTime(DateUtils.getNowDate());
        return userShopFansMapper.updateUserShopFans(userShopFans);
    }

    /**
     * 删除用户关注店铺对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteUserShopFansByIds(String ids)
    {
        return userShopFansMapper.deleteUserShopFansByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除用户关注店铺信息
     * 
     * @param id 用户关注店铺ID
     * @return 结果
     */
    @Override
    public int deleteUserShopFansById(Long id)
    {
        return userShopFansMapper.deleteUserShopFansById(id);
    }

    /**
     * 查询关注店铺的粉丝数
     *
     * @param merchantUserId
     * @return
     */
    @Override
    public int findUserByMerchantUserId(Long merchantUserId) {
        return userShopFansMapper.findUserByMerchantUserId(merchantUserId);
    }
}
