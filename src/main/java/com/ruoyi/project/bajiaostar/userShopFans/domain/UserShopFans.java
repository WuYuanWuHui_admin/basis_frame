package com.ruoyi.project.bajiaostar.userShopFans.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 用户关注店铺对象 t_user_shop_fans
 * 
 * @author 谢少辉
 * @date 2024-03-31
 */
public class UserShopFans extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 用户数据 */
    @Excel(name = "用户数据")
    private Long userId;

    /** 店铺用户id */
    @Excel(name = "店铺用户id")
    private Long shopUserId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setShopUserId(Long shopUserId) 
    {
        this.shopUserId = shopUserId;
    }

    public Long getShopUserId() 
    {
        return shopUserId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("shopUserId", getShopUserId())
            .append("createTime", getCreateTime())
            .toString();
    }
}
