package com.ruoyi.project.bajiaostar.userShopFans.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.bajiaostar.userShopFans.domain.UserShopFans;
import com.ruoyi.project.bajiaostar.userShopFans.service.IUserShopFansService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 用户关注店铺Controller
 * 
 * @author 谢少辉
 * @date 2024-03-31
 */
@Controller
@RequestMapping("/bajiaostar/userShopFans")
public class UserShopFansController extends BaseController
{
    private String prefix = "bajiaostar/userShopFans";

    @Autowired
    private IUserShopFansService userShopFansService;

    @RequiresPermissions("bajiaostar:userShopFans:view")
    @GetMapping()
    public String userShopFans()
    {
        return prefix + "/userShopFans";
    }

    /**
     * 查询用户关注店铺列表
     */
    @RequiresPermissions("bajiaostar:userShopFans:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(UserShopFans userShopFans)
    {
        startPage();
        List<UserShopFans> list = userShopFansService.selectUserShopFansList(userShopFans);
        return getDataTable(list);
    }

    /**
     * 导出用户关注店铺列表
     */
    @RequiresPermissions("bajiaostar:userShopFans:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(UserShopFans userShopFans)
    {
        List<UserShopFans> list = userShopFansService.selectUserShopFansList(userShopFans);
        ExcelUtil<UserShopFans> util = new ExcelUtil<UserShopFans>(UserShopFans.class);
        return util.exportExcel(list, "userShopFans");
    }

    /**
     * 新增用户关注店铺
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存用户关注店铺
     */
    @RequiresPermissions("bajiaostar:userShopFans:add")
    @Log(title = "用户关注店铺", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(UserShopFans userShopFans)
    {
        return toAjax(userShopFansService.insertUserShopFans(userShopFans));
    }

    /**
     * 修改用户关注店铺
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        UserShopFans userShopFans = userShopFansService.selectUserShopFansById(id);
        mmap.put("userShopFans", userShopFans);
        return prefix + "/edit";
    }

    /**
     * 修改保存用户关注店铺
     */
    @RequiresPermissions("bajiaostar:userShopFans:edit")
    @Log(title = "用户关注店铺", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(UserShopFans userShopFans)
    {
        return toAjax(userShopFansService.updateUserShopFans(userShopFans));
    }

    /**
     * 删除用户关注店铺
     */
    @RequiresPermissions("bajiaostar:userShopFans:remove")
    @Log(title = "用户关注店铺", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(userShopFansService.deleteUserShopFansByIds(ids));
    }
}
