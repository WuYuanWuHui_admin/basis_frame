package com.ruoyi.project.bajiaostar.merchangood.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 商户商品对象 t_merchant_good
 * 
 * @author 谢少辉
 * @date 2024-03-31
 */
public class Merchangood extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 商品信息id */
    private Long id;

    /** 商品图片 */
    @Excel(name = "商品图片")
    private String goodUrl;

    /** 商品名称 */
    @Excel(name = "商品名称")
    private String goodName;

    /** 商品状态 0 待上架  1 上架  2 下架 */
    @Excel(name = "商品状态 0 待上架  1 上架  2 下架")
    private Integer goodStatus;

    /** 商品详情 */
    @Excel(name = "商品详情")
    private String goodDetail;

    /** 商家用户id */
    @Excel(name = "商家用户id")
    private Long userId;

    /** 商品销量 */
    @Excel(name = "商品销量")
    private Long goodPinCount;
    //粉丝
    private Long fansCount;
    //佣金
    private Long commissionAmount;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setGoodUrl(String goodUrl) 
    {
        this.goodUrl = goodUrl;
    }

    public String getGoodUrl() 
    {
        return goodUrl;
    }
    public void setGoodName(String goodName) 
    {
        this.goodName = goodName;
    }

    public String getGoodName() 
    {
        return goodName;
    }
    public void setGoodStatus(Integer goodStatus) 
    {
        this.goodStatus = goodStatus;
    }

    public Integer getGoodStatus() 
    {
        return goodStatus;
    }
    public void setGoodDetail(String goodDetail) 
    {
        this.goodDetail = goodDetail;
    }

    public String getGoodDetail() 
    {
        return goodDetail;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setGoodPinCount(Long goodPinCount) 
    {
        this.goodPinCount = goodPinCount;
    }

    public Long getGoodPinCount() 
    {
        return goodPinCount;
    }

    public Long getFansCount() {
        return fansCount;
    }

    public void setFansCount(Long fansCount) {
        this.fansCount = fansCount;
    }

    public Long getCommissionAmount() {
        return commissionAmount;
    }

    public void setCommissionAmount(Long commissionAmount) {
        this.commissionAmount = commissionAmount;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("goodUrl", getGoodUrl())
            .append("goodName", getGoodName())
            .append("goodStatus", getGoodStatus())
            .append("goodDetail", getGoodDetail())
            .append("userId", getUserId())
            .append("goodPinCount", getGoodPinCount())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
