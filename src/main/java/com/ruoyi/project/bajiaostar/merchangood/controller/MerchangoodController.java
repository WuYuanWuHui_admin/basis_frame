package com.ruoyi.project.bajiaostar.merchangood.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.bajiaostar.merchangood.domain.Merchangood;
import com.ruoyi.project.bajiaostar.merchangood.service.IMerchangoodService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 商户商品Controller
 * 
 * @author 谢少辉
 * @date 2024-03-31
 */
@Controller
@RequestMapping("/bajiaostar/merchangood")
public class MerchangoodController extends BaseController
{
    private String prefix = "bajiaostar/merchangood";

    @Autowired
    private IMerchangoodService merchangoodService;

    @RequiresPermissions("bajiaostar:merchangood:view")
    @GetMapping()
    public String merchangood()
    {
        return prefix + "/merchangood";
    }

    /**
     * 查询商户商品列表
     */
    @RequiresPermissions("bajiaostar:merchangood:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Merchangood merchangood)
    {
        startPage();
        List<Merchangood> list = merchangoodService.selectMerchangoodList(merchangood);
        return getDataTable(list);
    }

    /**
     * 导出商户商品列表
     */
    @RequiresPermissions("bajiaostar:merchangood:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Merchangood merchangood)
    {
        List<Merchangood> list = merchangoodService.selectMerchangoodList(merchangood);
        ExcelUtil<Merchangood> util = new ExcelUtil<Merchangood>(Merchangood.class);
        return util.exportExcel(list, "merchangood");
    }

    /**
     * 新增商户商品
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存商户商品
     */
    @RequiresPermissions("bajiaostar:merchangood:add")
    @Log(title = "商户商品", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(Merchangood merchangood)
    {
        return toAjax(merchangoodService.insertMerchangood(merchangood));
    }

    /**
     * 修改商户商品
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        Merchangood merchangood = merchangoodService.selectMerchangoodById(id);
        mmap.put("merchangood", merchangood);
        return prefix + "/edit";
    }

    /**
     * 修改保存商户商品
     */
    @RequiresPermissions("bajiaostar:merchangood:edit")
    @Log(title = "商户商品", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(Merchangood merchangood)
    {
        return toAjax(merchangoodService.updateMerchangood(merchangood));
    }

    /**
     * 删除商户商品
     */
    @RequiresPermissions("bajiaostar:merchangood:remove")
    @Log(title = "商户商品", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(merchangoodService.deleteMerchangoodByIds(ids));
    }
}
