package com.ruoyi.project.bajiaostar.merchangood.mapper;

import com.ruoyi.project.bajiaostar.merchangood.domain.Merchangood;
import java.util.List;

/**
 * 商户商品Mapper接口
 * 
 * @author 谢少辉
 * @date 2024-03-31
 */
public interface MerchangoodMapper 
{
    /**
     * 查询商户商品
     * 
     * @param id 商户商品ID
     * @return 商户商品
     */
    public Merchangood selectMerchangoodById(Long id);

    /**
     * 查询商户商品列表
     * 
     * @param merchangood 商户商品
     * @return 商户商品集合
     */
    public List<Merchangood> selectMerchangoodList(Merchangood merchangood);

    /**
     * 新增商户商品
     * 
     * @param merchangood 商户商品
     * @return 结果
     */
    public int insertMerchangood(Merchangood merchangood);

    /**
     * 修改商户商品
     * 
     * @param merchangood 商户商品
     * @return 结果
     */
    public int updateMerchangood(Merchangood merchangood);

    /**
     * 删除商户商品
     * 
     * @param id 商户商品ID
     * @return 结果
     */
    public int deleteMerchangoodById(Long id);

    /**
     * 批量删除商户商品
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteMerchangoodByIds(String[] ids);
}
