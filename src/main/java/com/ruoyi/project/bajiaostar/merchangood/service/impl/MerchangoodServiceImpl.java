package com.ruoyi.project.bajiaostar.merchangood.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.bajiaostar.merchangood.mapper.MerchangoodMapper;
import com.ruoyi.project.bajiaostar.merchangood.domain.Merchangood;
import com.ruoyi.project.bajiaostar.merchangood.service.IMerchangoodService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 商户商品Service业务层处理
 * 
 * @author 谢少辉
 * @date 2024-03-31
 */
@Service
public class MerchangoodServiceImpl implements IMerchangoodService 
{
    @Autowired
    private MerchangoodMapper merchangoodMapper;

    /**
     * 查询商户商品
     * 
     * @param id 商户商品ID
     * @return 商户商品
     */
    @Override
    public Merchangood selectMerchangoodById(Long id)
    {
        return merchangoodMapper.selectMerchangoodById(id);
    }

    /**
     * 查询商户商品列表
     * 
     * @param merchangood 商户商品
     * @return 商户商品
     */
    @Override
    public List<Merchangood> selectMerchangoodList(Merchangood merchangood)
    {
        return merchangoodMapper.selectMerchangoodList(merchangood);
    }

    /**
     * 新增商户商品
     * 
     * @param merchangood 商户商品
     * @return 结果
     */
    @Override
    public int insertMerchangood(Merchangood merchangood)
    {
        merchangood.setCreateTime(DateUtils.getNowDate());
        return merchangoodMapper.insertMerchangood(merchangood);
    }

    /**
     * 修改商户商品
     * 
     * @param merchangood 商户商品
     * @return 结果
     */
    @Override
    public int updateMerchangood(Merchangood merchangood)
    {
        merchangood.setUpdateTime(DateUtils.getNowDate());
        return merchangoodMapper.updateMerchangood(merchangood);
    }

    /**
     * 删除商户商品对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteMerchangoodByIds(String ids)
    {
        return merchangoodMapper.deleteMerchangoodByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除商户商品信息
     * 
     * @param id 商户商品ID
     * @return 结果
     */
    @Override
    public int deleteMerchangoodById(Long id)
    {
        return merchangoodMapper.deleteMerchangoodById(id);
    }
}
