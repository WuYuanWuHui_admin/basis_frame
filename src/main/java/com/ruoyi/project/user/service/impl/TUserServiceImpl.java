package com.ruoyi.project.user.service.impl;

import com.ruoyi.common.constant.SystemContstant;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.Md5Utils;
import com.ruoyi.common.utils.text.Convert;
import com.ruoyi.project.user.domain.TUser;
import com.ruoyi.project.user.entity.LoginByMobileReq;
import com.ruoyi.project.user.mapper.TUserMapper;
import com.ruoyi.project.user.service.ITUserService;
import com.ruoyi.project.utils.RedisUtils;
import com.ruoyi.project.utils.TokenProccessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 会员用户Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-01-23
 */
@Service
public class TUserServiceImpl implements ITUserService 
{
    @Autowired
    private TUserMapper tUserMapper;
    @Autowired
    private RedisUtils redisUtils;

    /**
     * 查询会员用户
     * 
     * @param id 会员用户主键
     * @return 会员用户
     */
    @Override
    public TUser selectTUserById(Long id)
    {
        return tUserMapper.selectTUserById(id);
    }

    /**
     * 查询会员用户列表
     * 
     * @param tUser 会员用户
     * @return 会员用户
     */
    @Override
    public List<TUser> selectTUserList(TUser tUser)
    {
        return tUserMapper.selectTUserList(tUser);
    }

    /**
     * 新增会员用户
     * 
     * @param tUser 会员用户
     * @return 结果
     */
    @Override
    public int insertTUser(TUser tUser)
    {
        tUser.setCreateTime(DateUtils.getNowDate());
        return tUserMapper.insertTUser(tUser);
    }

    /**
     * 修改会员用户
     * 
     * @param tUser 会员用户
     * @return 结果
     */
    @Override
    public int updateTUser(TUser tUser)
    {
        return tUserMapper.updateTUser(tUser);
    }

    /**
     * 批量删除会员用户
     * 
     * @param ids 需要删除的会员用户主键
     * @return 结果
     */
    @Override
    public int deleteTUserByIds(String ids)
    {
        return tUserMapper.deleteTUserByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除会员用户信息
     * 
     * @param id 会员用户主键
     * @return 结果
     */
    @Override
    public int deleteTUserById(Long id)
    {
        return tUserMapper.deleteTUserById(id);
    }

    /**
     * 会员用户登陆
     *
     * @param req
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> loginByMobile(LoginByMobileReq req) throws Exception {
        if(ObjectUtils.isEmpty(req)){
            throw new RuntimeException("用户信息不正确");
        }
        //查询用户是否存在
        TUser user = tUserMapper.selectTUserByMobile(req.getMobile());
        //不存在就插入
        if(ObjectUtils.isEmpty(user)){
            user = new TUser();
            user.setUserName(req.getMobile());
            user.setPassword(null);
            user.setMobile(req.getMobile());
            user.setLastLoginIp(req.getLastLoginIp());
            user.setLastLoginTime(new Date());
            user.setUserStatus(0);
          int count=  tUserMapper.insertTUser(user);
          if(count < 1){
              throw new RuntimeException("登录失败,请稍后再试");
          }
            user = tUserMapper.selectTUserByMobile(req.getMobile());
        }else {
            user.setLastLoginIp(req.getLastLoginIp());
            user.setLastLoginTime(new Date());
        }
        if(ObjectUtils.isEmpty(user)){
            throw new RuntimeException("登录失败,请稍后重新登陆");
        }
        if(user.getUserStatus().equals(1)){
            throw new RuntimeException("登录失败,用户已被禁用");
        }else if(user.getUserStatus().equals(2)){
            throw new RuntimeException("登录失败,用户已被锁定");
        }
        String token = TokenProccessor.getInstance().makeToken();
        redisUtils.set(SystemContstant.TOKEN + Md5Utils.hash(token),user.getId());
        redisUtils.set(SystemContstant.TOKEN + "user",user);
        Map <String, Object> header = new HashMap<>();
        header.put("token", token);
        return header;
    }
}
