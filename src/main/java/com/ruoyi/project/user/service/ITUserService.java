package com.ruoyi.project.user.service;

import com.ruoyi.project.user.domain.TUser;
import com.ruoyi.project.user.entity.LoginByMobileReq;

import java.util.List;
import java.util.Map;

/**
 * 会员用户Service接口
 * 
 * @author ruoyi
 * @date 2024-01-23
 */
public interface ITUserService 
{
    /**
     * 查询会员用户
     * 
     * @param id 会员用户主键
     * @return 会员用户
     */
    public TUser selectTUserById(Long id);

    /**
     * 查询会员用户列表
     * 
     * @param tUser 会员用户
     * @return 会员用户集合
     */
    public List<TUser> selectTUserList(TUser tUser);

    /**
     * 新增会员用户
     * 
     * @param tUser 会员用户
     * @return 结果
     */
    public int insertTUser(TUser tUser);

    /**
     * 修改会员用户
     * 
     * @param tUser 会员用户
     * @return 结果
     */
    public int updateTUser(TUser tUser);

    /**
     * 批量删除会员用户
     * 
     * @param ids 需要删除的会员用户主键集合
     * @return 结果
     */
    public int deleteTUserByIds(String ids);

    /**
     * 删除会员用户信息
     * 
     * @param id 会员用户主键
     * @return 结果
     */
    public int deleteTUserById(Long id);

    /**
     * 会员用户登陆

     * @return
     * @throws Exception
     */
    Map<String, Object> loginByMobile (LoginByMobileReq req) throws Exception;
}
