package com.ruoyi.project.user.mapper;

import com.ruoyi.project.user.domain.TUser;

import java.util.List;

/**
 * 会员用户Mapper接口
 * 
 * @author ruoyi
 * @date 2024-01-23
 */
public interface TUserMapper 
{
    /**
     * 查询会员用户
     * 
     * @param id 会员用户主键
     * @return 会员用户
     */
    public TUser selectTUserById(Long id);

    /**
     * 查询会员用户列表
     * 
     * @param tUser 会员用户
     * @return 会员用户集合
     */
    public List<TUser> selectTUserList(TUser tUser);

    /**
     * 新增会员用户
     * 
     * @param tUser 会员用户
     * @return 结果
     */
    public int insertTUser(TUser tUser);

    /**
     * 修改会员用户
     * 
     * @param tUser 会员用户
     * @return 结果
     */
    public int updateTUser(TUser tUser);

    /**
     * 删除会员用户
     * 
     * @param id 会员用户主键
     * @return 结果
     */
    public int deleteTUserById(Long id);

    /**
     * 批量删除会员用户
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTUserByIds(String[] ids);

    /**
     * 根据用户手机号查询用户信息
     *
     * @param mobile 会员用户手机号码
     * @return 会员用户
     */
    public TUser selectTUserByMobile(String mobile);
}
