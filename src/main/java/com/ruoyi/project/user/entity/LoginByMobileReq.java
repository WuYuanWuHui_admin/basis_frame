package com.ruoyi.project.user.entity;

import java.io.Serializable;

/**
 * 会员登录信息
 */
public class LoginByMobileReq implements Serializable {
    /**
     * 手机号码
     */
    private String mobile;
    /**
     * 验证码
     */
    private String inviteCode;
    /**
     * 渠道
     */
    private String channel;
    /**
     * 最后登陆时间
     */
    private String lastLoginIp;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getInviteCode() {
        return inviteCode;
    }

    public void setInviteCode(String inviteCode) {
        this.inviteCode = inviteCode;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getLastLoginIp() {
        return lastLoginIp;
    }

    public void setLastLoginIp(String lastLoginIp) {
        this.lastLoginIp = lastLoginIp;
    }
}
