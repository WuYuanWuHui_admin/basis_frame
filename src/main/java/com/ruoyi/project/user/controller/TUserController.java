package com.ruoyi.project.user.controller;

import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.user.domain.TUser;
import com.ruoyi.project.user.service.ITUserService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 会员用户Controller
 * 
 * @author ruoyi
 * @date 2024-01-23
 */
@Controller
@RequestMapping("/user/user")
public class TUserController extends BaseController
{
    private String prefix = "user/user";

    @Autowired
    private ITUserService tUserService;

    @RequiresPermissions("user:user:view")
    @GetMapping()
    public String user()
    {
        return prefix + "/user";
    }

    /**
     * 查询会员用户列表
     */
    @RequiresPermissions("user:user:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(TUser tUser)
    {
        startPage();
        List<TUser> list = tUserService.selectTUserList(tUser);
        return getDataTable(list);
    }

    /**
     * 导出会员用户列表
     */
    @RequiresPermissions("user:user:export")
    @Log(title = "会员用户", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(TUser tUser)
    {
        List<TUser> list = tUserService.selectTUserList(tUser);
        ExcelUtil<TUser> util = new ExcelUtil<TUser>(TUser.class);
        return util.exportExcel(list, "会员用户数据");
    }

    /**
     * 新增会员用户
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存会员用户
     */
    @RequiresPermissions("user:user:add")
    @Log(title = "会员用户", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(TUser tUser)
    {
        return toAjax(tUserService.insertTUser(tUser));
    }

    /**
     * 修改会员用户
     */
    @RequiresPermissions("user:user:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        TUser tUser = tUserService.selectTUserById(id);
        mmap.put("tUser", tUser);
        return prefix + "/edit";
    }

    /**
     * 修改保存会员用户
     */
    @RequiresPermissions("user:user:edit")
    @Log(title = "会员用户", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(TUser tUser)
    {
        return toAjax(tUserService.updateTUser(tUser));
    }

    /**
     * 删除会员用户
     */
    @RequiresPermissions("user:user:remove")
    @Log(title = "会员用户", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(tUserService.deleteTUserByIds(ids));
    }
}
