package com.ruoyi.project.entity;

/**
 * 高德地图导航接口返回参数POJO
 */
public class MapNavResults {

    private String distance;//行驶距离

    private String duration;//行驶时间（单位：秒）

    private String tolls;//道路收费（单位：元）

    public String getDistance () {
        return this.distance;
    }

    public void setDistance (String distance) {
        this.distance = distance;
    }

    public String getDuration () {
        return this.duration;
    }

    public void setDuration (String duration) {
        this.duration = duration;
    }

    public String getTolls () {
        return this.tolls;
    }

    public void setTolls (String tolls) {
        this.tolls = tolls;
    }

    @Override
    public String toString () {
        return "MapNavResults [distance=" + this.distance + ", duration=" + this.duration + ", tolls=" + this.tolls + "]";
    }


}