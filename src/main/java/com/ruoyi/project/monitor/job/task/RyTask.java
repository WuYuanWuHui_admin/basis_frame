package com.ruoyi.project.monitor.job.task;

import com.ruoyi.project.bajiaostar.systemRiseFail.service.ISystemRiseFailService;
import com.ruoyi.project.bajiaostar.userExcitationTask.service.IUserExcitationTaskService;
import com.ruoyi.project.bajiaostar.userSignInTask.service.IUserSignInTaskService;
import com.ruoyi.project.push.AppPush;
import com.ruoyi.project.sms.ISMSService;
import com.ruoyi.project.system.dict.domain.DictData;
import com.ruoyi.project.system.dict.service.IDictDataService;
import com.ruoyi.project.system.user.domain.User;
import com.ruoyi.project.system.user.service.IUserService;
import com.ruoyi.project.utils.RedisUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.stream.Collectors;

/**
 * 定时任务调度测试
 *
 * @author ruoyi
 */
@Component ("ryTask")
public class RyTask {

    private static final Logger log = LoggerFactory.getLogger(RyTask.class);

    @Autowired
    private IUserExcitationTaskService userExcitationTaskService;
    @Autowired
    IUserSignInTaskService userSignInTaskService;

    @Autowired
    ISystemRiseFailService systemRiseFailService;

    //执行激励数据
    public void executeUserExcitationTask () {
        this.userExcitationTaskService.executeUserExcitationTask();
    }



    //用户任务过期
    public void updateUserSignTask () {
        this.userSignInTaskService.updateUserSignTask();
    }

    //涨幅信息任务
    public void executeSystemRiseFailTask () {
        this.systemRiseFailService.executeSystemRiseFailTask();
    }

}
