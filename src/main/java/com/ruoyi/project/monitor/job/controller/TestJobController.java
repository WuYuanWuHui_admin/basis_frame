package com.ruoyi.project.monitor.job.controller;

import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.project.utils.RedisUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 测试任务
 *
 * @author ruoyi
 */
@Controller
@Slf4j
@RequestMapping ("/monitor/job/test")
public class TestJobController extends BaseController {


    @Autowired
    private RedisUtils redisUtils;

    /**
     * 修改调度
     */
    @GetMapping ("initDateCheckUserStatistic")
    public String initDateCheckUserStatistic () {
        log.info("initDateCheckUserStatisticTask 两点统计前两天天客服绩效考核执行开始");
       if (!this.redisUtils.setLock("initDateCheckUserStatisticTask", 1800)) {
            log.info("initDateCheckUserStatisticTask——,已在执行，此服务不执行");
            return "initDateCheckUserStatisticTask——,已在执行，此服务不执行";
        }
        log.info("initDateCheckUserStatisticTask 两点统计前两天天客服绩效考核执行结束");
        return "initDateCheckUserStatisticTask——,成功执行";
    }

}
