package com.ruoyi.project.system.user.service;

import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.project.content.FlowTypeEnum;
import com.ruoyi.project.system.user.domain.User;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.List;
import java.util.Map;

/**
 * 用户 业务层
 *
 * @author ruoyi
 */
public interface IUserService {

    /**
     * 查找用户列表
     *
     * @param user
     *         用户
     *
     * @return
     */
    List <User> findList (User user);

    /**
     * 根据条件分页查询用户列表
     *
     * @param user
     *         用户信息
     *
     * @return 用户信息集合信息
     */
    public List <User> selectUserList (User user);

    /**
     * 根据条件分页查询已分配用户角色列表
     *
     * @param user
     *         用户信息
     *
     * @return 用户信息集合信息
     */
    public List <User> selectAllocatedList (User user);

    /**
     * 根据条件分页查询未分配用户角色列表
     *
     * @param user
     *         用户信息
     *
     * @return 用户信息集合信息
     */
    public List <User> selectUnallocatedList (User user);

    /**
     * 通过用户名查询用户
     *
     * @param userName
     *         用户名
     *
     * @return 用户对象信息
     */
    public User selectUserByLoginName (String userName);

    /**
     * 通过手机号码查询用户
     *
     * @param phoneNumber
     *         手机号码
     *
     * @return 用户对象信息
     */
    public User selectUserByPhoneNumber (String phoneNumber);

    public String selectUserIdsByPhoneNumbers (String phoneNumbers);

    /**
     * 通过邮箱查询用户
     *
     * @param email
     *         邮箱
     *
     * @return 用户对象信息
     */
    public User selectUserByEmail (String email);

    /**
     * 通过用户ID查询用户
     *
     * @param userId
     *         用户ID
     *
     * @return 用户对象信息
     */
    public User selectUserById (Long userId);

    /**
     * 通过用户ID删除用户
     *
     * @param userId
     *         用户ID
     *
     * @return 结果
     */
    public int deleteUserById (Long userId);

    /**
     * 批量删除用户信息
     *
     * @param ids
     *         需要删除的数据ID
     *
     * @return 结果
     *
     * @throws Exception
     *         异常
     */
    public int deleteUserByIds (String ids) throws Exception;

    /**
     * 保存用户信息
     *
     * @param user
     *         用户信息
     *
     * @return 结果
     */
    public int insertUser (User user);

    /**
     * 保存用户信息
     *
     * @param user
     *         用户信息
     *
     * @return 结果
     */
    public int updateUser (User user);
    /**
     * 更新用户信息
     *
     * @return 结果
     */
    public int updateUserByUserId (Integer age,Long userId);

    /**
     * 修改用户详细信息
     *
     * @param user
     *         用户信息
     *
     * @return 结果
     */
    public int updateUserInfo (User user);

    /**
     * 修改用户密码信息
     *
     * @param user
     *         用户信息
     *
     * @return 结果
     */
    public int resetUserPwd (User user);

    /**
     * 校验用户名称是否唯一
     *
     * @param loginName
     *         登录名称
     *
     * @return 结果
     */
    public String checkLoginNameUnique (String loginName);

    /**
     * 校验手机号码是否唯一
     *
     * @param user
     *         用户信息
     *
     * @return 结果
     */
    public String checkPhoneUnique (User user);

    /**
     * 校验email是否唯一
     *
     * @param user
     *         用户信息
     *
     * @return 结果
     */
    public String checkEmailUnique (User user);

    /**
     * 根据用户ID查询用户所属角色组
     *
     * @param userId
     *         用户ID
     *
     * @return 结果
     */
    public String selectUserRoleGroup (Long userId);

    /**
     * 根据用户ID查询用户所属岗位组
     *
     * @param userId
     *         用户ID
     *
     * @return 结果
     */
    public String selectUserPostGroup (Long userId);

    /**
     * 导入用户数据
     *
     * @param userList
     *         用户数据列表
     * @param isUpdateSupport
     *         是否更新支持，如果已存在，则进行更新数据
     *
     * @return 结果
     */
    public String importUser (List <User> userList, Boolean isUpdateSupport);

    /**
     * 用户状态修改
     *
     * @param user
     *         用户信息
     *
     * @return 结果
     */
    public int changeStatus (User user);

    /**
     * 更新支付密码
     *
     * @param userId
     * @param payPwd
     *
     * @return
     */
    int updPayPwd (Long userId, String payPwd) throws Exception;

    /**
     * 更新用户
     *
     * @param userUpd
     *
     * @return
     *
     * @throws Exception
     */
    int updUser (User userUpd) throws Exception;

    /**
     * 雇主端查询用户列表
     *
     * @param user
     *
     * @return
     */
    List <User> queryUserList (User user);

    /**
     * 获取普通用户
     *
     * @return
     */
    List <Map> selectCommonUser ();



    public List <User> queryUserListone (User user);


    public Integer selectNoAvatarto ();

    public List <User> selectUnalavaterList (User user);

    public int updateuseravater (User user);


    public int updateVipBygoldmedal (Long userId);

    public int updateVipBygoldmedalvips (Long userId);

    public int updateVipBygoldmedalleveltwe (Long userId);

    public int updateVipBygoldmedallevelone (Long userId);

    public int updateVipBygoldmedalst (long userId);

    /**
     * 检查并取消已过期的
     */
    void cancelVip ();




    /**
     * 根据loginName 查找当前用户是否注销
     *
     * @param loginName
     *         登录名
     *
     * @return
     */
    boolean isDestroy (String loginName);




    /**
     * 修改用户信息
     * @param userId
     * @param userName
     * @return
     */
    AjaxResult updateUserUserName (Long userId, String userName);
    /**
     * 查询用户所在部门即
     * @param user
     * @return
     */
    List<Long> selectDeptUserList(User user);
}
