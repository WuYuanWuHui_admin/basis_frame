package com.ruoyi.project.system.user.mapper;

import com.ruoyi.project.system.user.domain.User;

import java.util.List;

/**
 * 用户表 数据层
 *
 * @author ruoyi
 */
public interface AdminMapper {

    /**
     * 查找管理员列表
     *
     * @param user
     *         查询条件
     *
     * @return
     */
    List <User> findList (User user);
}
