/*
 * ...
 */

package com.ruoyi.project.system.user.service;

import com.ruoyi.project.system.user.domain.User;
import com.ruoyi.project.system.user.mapper.AdminMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 管理员service
 *
 * @author 挺好的 2023年02月22日 上午 10:40
 */
@Service ("adminServiceImpl")
public class AdminServiceImpl implements IAdminService {

    @Autowired
    private AdminMapper adminMapper;

    @Override
    public List <User> findList (User user) {
        if (user == null) {
            user = new User();
        }
        // 设置为管理员类型
        user.setUserType((short) 0);

        return this.adminMapper.findList(user);

    }
}
