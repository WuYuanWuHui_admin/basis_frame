package com.ruoyi.project.system.user.controller;

import com.ruoyi.framework.config.RuoYiConfig;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.project.system.menu.domain.Menu;
import com.ruoyi.project.system.menu.service.IMenuService;
import com.ruoyi.project.system.user.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

import javax.annotation.Resource;
import java.util.List;

/**
 * 首页 业务处理
 *
 * @author ruoyi
 */
@Controller
public class IndexController extends BaseController {

    @Autowired
    private IMenuService menuService;

    @Autowired
    private RuoYiConfig ruoYiConfig;



    // 系统首页
    @GetMapping ("/index")
    public String index (ModelMap mmap) {

        // 取身份信息
        User user = this.getSysUser();
        // 根据用户id取出菜单
        List <Menu> menus = this.menuService.selectMenusByUser(user);
        mmap.put("menus", menus);
        mmap.put("user", user);
        mmap.put("copyrightYear", this.ruoYiConfig.getCopyrightYear());
        mmap.put("demoEnabled", this.ruoYiConfig.isDemoEnabled());

        return "index";
    }

    // 系统介绍
    @GetMapping ("/system/main")
    public String main (ModelMap model) {
        model.put("version", this.ruoYiConfig.getVersion());
        return "main";
    }
}
