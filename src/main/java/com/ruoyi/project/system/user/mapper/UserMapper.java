package com.ruoyi.project.system.user.mapper;

import com.ruoyi.project.system.user.domain.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 用户表 数据层
 *
 * @author ruoyi
 */
public interface UserMapper {

    /**
     * 根据条件分页查询用户列表
     *
     * @param user
     *         用户信息
     *
     * @return 用户信息集合信息
     */
    public List <User> selectUserList (User user);

    /**
     * 查找用户列表，重构搜索语句， 2023-02-22
     *
     * @param user
     *         搜索条件
     *
     * @return
     */
    List <User> findList (User user);

    /**
     * 根据条件分页查询未已配用户角色列表
     *
     * @param user
     *         用户信息
     *
     * @return 用户信息集合信息
     */
    public List <User> selectAllocatedList (User user);

    /**
     * 根据条件分页查询未分配用户角色列表
     *
     * @param user
     *         用户信息
     *
     * @return 用户信息集合信息
     */
    public List <User> selectUnallocatedList (User user);

    /**
     * 通过用户名查询用户
     *
     * @param userName
     *         用户名
     *
     * @return 用户对象信息
     */
    public User selectUserByLoginName (String userName);

    /**
     * 通过手机号码查询用户
     *
     * @param phoneNumber
     *         手机号码
     *
     * @return 用户对象信息
     */
    public User selectUserByPhoneNumber (String phoneNumber);

    public String selectUserIdsByPhoneNumbers (@Param ("phoneNumbers") String phoneNumbers);

    /**
     * 通过邮箱查询用户
     *
     * @param email
     *         邮箱
     *
     * @return 用户对象信息
     */
    public User selectUserByEmail (String email);

    /**
     * 通过用户ID查询用户
     *
     * @param userId
     *         用户ID
     *
     * @return 用户对象信息
     */
    public User selectUserById (Long userId);

    public User selectUserDetailById (Long userId);

    /**
     * 通过用户ID删除用户
     *
     * @param userId
     *         用户ID
     *
     * @return 结果
     */
    public int deleteUserById (Long userId);

    /**
     * 批量删除用户信息
     *
     * @param ids
     *         需要删除的数据ID
     *
     * @return 结果
     */
    public int deleteUserByIds (Long[] ids);

    /**
     * 修改用户信息
     *
     * @param user
     *         用户信息
     *
     * @return 结果
     */
    public int updateUser (User user);

    /**
     * 新增用户信息
     *
     * @param user
     *         用户信息
     *
     * @return 结果
     */
    public int insertUser (User user);

    /**
     * 校验用户名称是否唯一
     *
     * @param loginName
     *         登录名称
     *
     * @return 结果
     */
    public int checkLoginNameUnique (String loginName);

    /**
     * 校验手机号码是否唯一
     *
     * @param phonenumber
     *         手机号码
     *
     * @return 结果
     */
    public User checkPhoneUnique (String phonenumber);

    /**
     * 校验email是否唯一
     *
     * @param email
     *         用户邮箱
     *
     * @return 结果
     */
    public User checkEmailUnique (String email);

    /**
     * 雇主端查询用户列表
     *
     * @param user
     *
     * @return
     */
    List <User> queryUserList (User user);

    /**
     * 获取普通用户
     *
     * @return
     */
    List <Map> selectCommonUser ();

    /**
     * 取消已过期vip用户的vip
     *
     * @return
     */
    int cancelVip ();

    /**
     * 根据用户id批量更新vip
     *
     * @return
     */
    int updateVipByUserIds (List <Long> userIds);

    public List <User> queryUserListone (User user);

    public Integer selectNoAvatarto ();

    public List <User> selectUnalavaterList (User user);

    public int updateuseravater (User user);

    public int updateVipBygoldmedal (long userId);

    public int updateVipBygoldmedalvips (Long userId);

    public int updateVipBygoldmedallevelone (Long userId);

    public int updateVipBygoldmedalleveltwe (Long userId);

    public int updateVipBygoldmedalst (long userId);

    public void updateVipByUserIdstow (List <Long> updUserIdts);

    /**
     * 获取注销状态
     *
     * @param loginName
     *         登录名称
     *
     * @return
     */
    Integer getBanStatus (@Param ("loginName") String loginName);

    /**
     * 根据用户id查找用户， 注意只返回部分数据
     *
     * @param userIds
     *         用户id列表
     *
     * @return
     */
    List <User> findListByIds (@Param ("userIds") List <Long> userIds);

    /**
     * 查找最近注册的用户
     *
     * @param queryMap
     *
     * @return
     */
    List <User> findNewRegister (Map <String, Object> queryMap);

    /**
     * 查询数据库中是否存在指定的userIds， 如果存在将会返回对应的数据
     *
     * @param userIds
     *         用户id
     *
     * @return
     */
    List <User> findUserIdIfExist (@Param ("userIds") List <Long> userIds);

    /**
     * 查询用户所在部门即
     * @param user
     * @return
     */
    List<Long> selectDeptUserList(User user);

    /**
     * 更新用户信息
     *
     * @return 结果
     */
    int updateUserByUserId (@Param ("age")Integer age,@Param ("userId")Long userId);

    /**
     * 更新用户信息
     *
     * @return 结果
     */
    int updateUserName (@Param ("userId")Long userId, @Param ("userName")String userName);
}
