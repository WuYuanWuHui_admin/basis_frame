package com.ruoyi.project.system.user.service;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.druid.support.json.JSONUtils;
import com.gexin.fastjson.JSON;
import com.gexin.fastjson.JSONObject;
import com.gexin.rp.sdk.base.IPushResult;
import com.ruoyi.common.constant.SystemContstant;
import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.exception.BusinessException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.security.ShiroUtils;
import com.ruoyi.common.utils.text.Convert;
import com.ruoyi.framework.aspectj.lang.annotation.DataScope;
import com.ruoyi.framework.shiro.service.PasswordService;
import com.ruoyi.framework.web.domain.AjaxResult;

import com.ruoyi.project.content.FlowTypeEnum;
import com.ruoyi.project.content.MsgPushTypeEnum;
import com.ruoyi.project.content.PropertyTypeEnum;
import com.ruoyi.project.content.TaskStatusEnum;
import com.ruoyi.project.push.AppPush;
import com.ruoyi.project.sms.ISMSService;
import com.ruoyi.project.system.config.service.IConfigService;
import com.ruoyi.project.system.post.domain.Post;
import com.ruoyi.project.system.post.mapper.PostMapper;
import com.ruoyi.project.system.role.domain.Role;
import com.ruoyi.project.system.role.mapper.RoleMapper;
import com.ruoyi.project.system.user.domain.User;
import com.ruoyi.project.system.user.domain.UserPost;
import com.ruoyi.project.system.user.domain.UserRole;
import com.ruoyi.project.system.user.mapper.UserMapper;
import com.ruoyi.project.system.user.mapper.UserPostMapper;
import com.ruoyi.project.system.user.mapper.UserRoleMapper;
import com.ruoyi.project.utils.NumberUtils;
import com.ruoyi.project.utils.RedisUtils;
import org.apache.commons.collections.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 用户 业务层处理
 *
 * @author ruoyi
 */
@Service
public class UserServiceImpl implements IUserService {

    private static final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);



    @Autowired
    private UserMapper userMapper;


    @Autowired
    private RoleMapper roleMapper;

    @Autowired
    private PostMapper postMapper;

    @Autowired
    private UserPostMapper userPostMapper;

    @Autowired
    private UserRoleMapper userRoleMapper;

    @Autowired
    private IConfigService configService;

    @Autowired
    private PasswordService passwordService;

    @Autowired
    private RedisUtils redisUtils;

    @Autowired
    private AppPush appPush;



    @Override
    public List <User> findList (User user) {
        Map <String, Object> params = user.getParams();

        if (MapUtils.isNotEmpty(params)) {
            String beginTime = (String) params.get("beginTime");
            String endTime = (String) params.get("endTime");

            // 如果开始时间或者结束时间为空，则重置当前时间
            if (StringUtils.isEmpty(beginTime) || StringUtils.isEmpty(endTime)) {
                beginTime = null;
                endTime   = null;
            } else {
                beginTime += " 00:00:00";
                endTime += " 23:59:59";
            }

            params.put("beginTime", beginTime);
            params.put("endTime", endTime);
        }

        return this.userMapper.findList(user);
    }

    /**
     * 根据条件分页查询用户列表
     *
     * @param user
     *         用户信息
     *
     * @return 用户信息集合信息
     */
    @Override
    @DataScope (deptAlias = "d", userAlias = "u")
    public List <User> selectUserList (User user) {
        // 生成数据权限过滤条件
        return this.userMapper.selectUserList(user);
    }

    /**
     * 根据条件分页查询已分配用户角色列表
     *
     * @param user
     *         用户信息
     *
     * @return 用户信息集合信息
     */
    @DataScope (deptAlias = "d", userAlias = "u")
    @Override
    public List <User> selectAllocatedList (User user) {
        return this.userMapper.selectAllocatedList(user);
    }

    /**
     * 根据条件分页查询未分配用户角色列表
     *
     * @param user
     *         用户信息
     *
     * @return 用户信息集合信息
     */
    @DataScope (deptAlias = "d", userAlias = "u")
    @Override
    public List <User> selectUnallocatedList (User user) {
        return this.userMapper.selectUnallocatedList(user);
    }

    /**
     * 通过用户名查询用户
     *
     * @param userName
     *         用户名
     *
     * @return 用户对象信息
     */
    @Override
    public User selectUserByLoginName (String userName) {
        return this.userMapper.selectUserByLoginName(userName);
    }

    /**
     * 通过手机号码查询用户
     *
     * @param phoneNumber
     *         手机号码
     *
     * @return 用户对象信息
     */
    @Override
    public User selectUserByPhoneNumber (String phoneNumber) {
        return this.userMapper.selectUserByPhoneNumber(phoneNumber);
    }


    @Override
    public String selectUserIdsByPhoneNumbers (String phoneNumbers) {
        return this.userMapper.selectUserIdsByPhoneNumbers(phoneNumbers);
    }

    /**
     * 通过邮箱查询用户
     *
     * @param email
     *         邮箱
     *
     * @return 用户对象信息
     */
    @Override
    public User selectUserByEmail (String email) {
        return this.userMapper.selectUserByEmail(email);
    }

    /**
     * 通过用户ID查询用户
     *
     * @param userId
     *         用户ID
     *
     * @return 用户对象信息
     */
    @Override
    public User selectUserById (Long userId) {
        return this.userMapper.selectUserById(userId);
    }

    /**
     * 通过用户ID删除用户
     *
     * @param userId
     *         用户ID
     *
     * @return 结果
     */
    @Override
    public int deleteUserById (Long userId) {
        // 删除用户与角色关联
        this.userRoleMapper.deleteUserRoleByUserId(userId);
        // 删除用户与岗位表
        this.userPostMapper.deleteUserPostByUserId(userId);
        return this.userMapper.deleteUserById(userId);
    }

    /**
     * 批量删除用户信息
     *
     * @param ids
     *         需要删除的数据ID
     *
     * @return 结果
     */
    @Override
    public int deleteUserByIds (String ids) throws BusinessException {
        Long[] userIds = Convert.toLongArray(ids);
        for (Long userId : userIds) {
            if (User.isAdmin(userId)) {
                throw new BusinessException("不允许删除超级管理员用户");
            }
        }
        return this.userMapper.deleteUserByIds(userIds);
    }

    /**
     * 新增保存用户信息
     *
     * @param user
     *         用户信息
     *
     * @return 结果
     */
    @Override
    @Transactional (rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public int insertUser (User user) {
        user.randomSalt();
        user.setPassword(this.passwordService.encryptPassword(user.getLoginName(), user.getPassword(), user.getSalt()));
        user.setCreateBy(user.getLoginName());
        // 新增用户信息
        int rows = this.userMapper.insertUser(user);
        // 新增用户岗位关联
        //   insertUserPost(user);
        // 新增用户与角色管理
        this.insertUserRole(user);
        return rows;
    }

    /**
     * 修改保存用户信息
     *
     * @param user
     *         用户信息
     *
     * @return 结果
     */
    @Override
    @Transactional
    public int updateUser (User user) {
        Long userId = user.getUserId();

        // 删除用户与角色关联
        this.userRoleMapper.deleteUserRoleByUserId(userId);
        // 新增用户与角色管理
        this.insertUserRole(user);
        // 删除用户与岗位关联
        this.userPostMapper.deleteUserPostByUserId(userId);
        // 新增用户与岗位管理
        this.insertUserPost(user);
        return this.userMapper.updateUser(user);
    }

    @Override
    @Transactional
    public int updateUserByUserId (Integer age,Long userId) {
        return this.userMapper.updateUserByUserId(age,userId);
}
    /**
     * 修改用户个人详细信息
     *
     * @param user
     *         用户信息
     *
     * @return 结果
     */
    @Transactional (propagation = Propagation.REQUIRED)
    @Override
    public int updateUserInfo (User user) {
        return this.userMapper.updateUser(user);
    }

    /**
     * 修改用户密码
     *
     * @param user
     *         用户信息
     *
     * @return 结果
     */
    @Override
    public int resetUserPwd (User user) {
        user.randomSalt();
        user.setPassword(this.passwordService.encryptPassword(user.getLoginName(), user.getPassword(), user.getSalt()));
        return this.updateUserInfo(user);
    }

    /**
     * 新增用户角色信息
     *
     * @param user
     *         用户对象
     */
    public void insertUserRole (User user) {
        Long[] roles = user.getRoleIds();
        if (StringUtils.isNotNull(roles)) {
            // 新增用户与角色管理
            List <UserRole> list = new ArrayList <UserRole>();
            for (Long roleId : user.getRoleIds()) {
                UserRole ur = new UserRole();
                ur.setUserId(user.getUserId());
                ur.setRoleId(roleId);
                list.add(ur);
            }
            if (list.size() > 0) {
                this.userRoleMapper.batchUserRole(list);
            }
        }
    }

    /**
     * 新增用户岗位信息
     *
     * @param user
     *         用户对象
     */
    public void insertUserPost (User user) {
        Long[] posts = user.getPostIds();
        if (StringUtils.isNotNull(posts)) {
            // 新增用户与岗位管理
            List <UserPost> list = new ArrayList <UserPost>();
            for (Long postId : user.getPostIds()) {
                UserPost up = new UserPost();
                up.setUserId(user.getUserId());
                up.setPostId(postId);
                list.add(up);
            }
            if (list.size() > 0) {
                this.userPostMapper.batchUserPost(list);
            }
        }
    }

    /**
     * 校验登录名称是否唯一
     *
     * @param loginName
     *         用户名
     *
     * @return
     */
    @Override
    public String checkLoginNameUnique (String loginName) {
        int count = this.userMapper.checkLoginNameUnique(loginName);
        if (count > 0) {
            return UserConstants.USER_NAME_NOT_UNIQUE;
        }
        return UserConstants.USER_NAME_UNIQUE;
    }

    /**
     * 校验用户名称是否唯一
     *
     * @param user
     *         用户信息
     *
     * @return
     */
    @Override
    public String checkPhoneUnique (User user) {
        Long userId = StringUtils.isNull(user.getUserId()) ? -1L : user.getUserId();
        User info = this.userMapper.checkPhoneUnique(user.getPhonenumber());
        if (StringUtils.isNotNull(info) && info.getUserId().longValue() != userId.longValue()) {
            return UserConstants.USER_PHONE_NOT_UNIQUE;
        }
        return UserConstants.USER_PHONE_UNIQUE;
    }

    /**
     * 校验email是否唯一
     *
     * @param user
     *         用户信息
     *
     * @return
     */
    @Override
    public String checkEmailUnique (User user) {
        Long userId = StringUtils.isNull(user.getUserId()) ? -1L : user.getUserId();
        User info = this.userMapper.checkEmailUnique(user.getEmail());
        if (StringUtils.isNotNull(info) && info.getUserId().longValue() != userId.longValue()) {
            return UserConstants.USER_EMAIL_NOT_UNIQUE;
        }
        return UserConstants.USER_EMAIL_UNIQUE;
    }

    /**
     * 查询用户所属角色组
     *
     * @param userId
     *         用户ID
     *
     * @return 结果
     */
    @Override
    public String selectUserRoleGroup (Long userId) {
        List <Role> list = this.roleMapper.selectRolesByUserId(userId);
        StringBuffer idsStr = new StringBuffer();
        for (Role role : list) {
            idsStr.append(role.getRoleName()).append(",");
        }
        if (StringUtils.isNotEmpty(idsStr.toString())) {
            return idsStr.substring(0, idsStr.length() - 1);
        }
        return idsStr.toString();
    }

    /**
     * 查询用户所属岗位组
     *
     * @param userId
     *         用户ID
     *
     * @return 结果
     */
    @Override
    public String selectUserPostGroup (Long userId) {
        List <Post> list = this.postMapper.selectPostsByUserId(userId);
        StringBuffer idsStr = new StringBuffer();
        for (Post post : list) {
            idsStr.append(post.getPostName()).append(",");
        }
        if (StringUtils.isNotEmpty(idsStr.toString())) {
            return idsStr.substring(0, idsStr.length() - 1);
        }
        return idsStr.toString();
    }

    /**
     * 导入用户数据
     *
     * @param userList
     *         用户数据列表
     * @param isUpdateSupport
     *         是否更新支持，如果已存在，则进行更新数据
     *
     * @return 结果
     */
    @Override
    public String importUser (List <User> userList, Boolean isUpdateSupport) {
        if (StringUtils.isNull(userList) || userList.size() == 0) {
            throw new BusinessException("导入用户数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        String operName = ShiroUtils.getLoginName();
        String password = this.configService.selectConfigByKey("sys.user.initPassword");
        for (User user : userList) {
            user.setUpdateBy(ShiroUtils.getLoginName());
            try {
                // 验证是否存在这个用户
                User u = this.userMapper.selectUserByLoginName(user.getLoginName());
                if (StringUtils.isNull(u)) {
                    user.setPassword(password);
                    user.setCreateBy(operName);
                    this.insertUser(user);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、账号 " + user.getLoginName() + " 导入成功");
                } else if (isUpdateSupport) {
                    user.setUpdateBy(operName);
                    this.updateUser(user);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、账号 " + user.getLoginName() + " 更新成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、账号 " + user.getLoginName() + " 已存在");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、账号 " + user.getLoginName() + " 导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new BusinessException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

    /**
     * 用户状态修改
     *
     * @param user
     *         用户信息
     *
     * @return 结果
     */
    @Override
    public int changeStatus (User user) {
        if (User.isAdmin(user.getUserId())) {
            throw new BusinessException("不允许修改超级管理员用户");
        }
        return this.userMapper.updateUser(user);
    }

    @Transactional (rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    @Override
    public int updPayPwd (Long userId, String payPwd) throws Exception {
        //查找当前用户
        User user = this.userMapper.selectUserById(userId);
        if (user == null) {
            throw new RuntimeException("获取不到用户信息");
        }
        //更新支付密码
        User userUpd = new User();
        userUpd.setUserId(userId);
        userUpd.setPayPwd(this.passwordService.encryptPassword(user.getLoginName(), payPwd, user.getSalt()));
        int count = this.updateUserInfo(userUpd);
        if (count <= 0) {
            throw new RuntimeException("密码更新失败");
        }
        //删除支付密码修改标识
        this.redisUtils.del(SystemContstant.PAY_CHECK + userId);

        return count;
    }

    @Transactional (rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    @Override
    public int updUser (User userUpd) throws Exception {
        int count = this.userMapper.updateUser(userUpd);
        if (count <= 0) {
            throw new RuntimeException("用户信息更新失败");
        }

        if (StringUtils.isNotBlank(userUpd.getLat()) && StringUtils.isNotBlank(userUpd.getLng())) {
            try {
                this.redisUtils.setGEOMember(Double.valueOf(userUpd.getLat()), Double.valueOf(userUpd.getLng()),
                        "user_" + userUpd.getUserId()
                );
            } catch (NumberFormatException e) {
                throw new RuntimeException("请获取正确的经纬度");
            }
        }
        return count;
    }

    @Override
    public List <User> queryUserList (User user) {
        return this.userMapper.queryUserList(user);
    }

    @Override
    public List <Map> selectCommonUser () {
        return this.userMapper.selectCommonUser();
    }



    @Override
    public List <User> queryUserListone (User user) {
        return this.userMapper.queryUserListone(user);
    }

    @Override
    public Integer selectNoAvatarto () {
        return this.userMapper.selectNoAvatarto();
    }

    @Override
    public List <User> selectUnalavaterList (User user) {
        return this.userMapper.selectUnalavaterList(user);
    }

    @Override
    public int updateuseravater (User user) {
        return this.userMapper.updateuseravater(user);
    }



    @Override
    public int updateVipBygoldmedal (Long userId) {
        return this.userMapper.updateVipBygoldmedal(userId);
    }

    @Override
    public int updateVipBygoldmedalvips (Long userId) {
        // TODO Auto-generated method stub
        return this.userMapper.updateVipBygoldmedalvips(userId);
    }

    @Override
    public int updateVipBygoldmedalleveltwe (Long userId) {
        // TODO Auto-generated method stub
        return this.userMapper.updateVipBygoldmedalleveltwe(userId);
    }

    @Override
    public int updateVipBygoldmedallevelone (Long userId) {
        // TODO Auto-generated method stub
        return this.userMapper.updateVipBygoldmedallevelone(userId);
    }

    @Override
    public int updateVipBygoldmedalst (long userId) {
        // TODO Auto-generated method stub
        return this.userMapper.updateVipBygoldmedalst(userId);
    }

    @Override
    public void cancelVip () {
        this.userMapper.cancelVip();
    }



    /**
     * 获取用户手机号更换限制
     *
     * @param userId
     *         用户ID
     *
     * @return 结果
     */
    private String getChangePhoneLimitKey (String userId) {
        return SystemContstant.PHONE_CHANGE_LIMIT + userId;
    }




    /**
     * 根据loginName 查找当前用户是否注销
     *
     * @param loginName
     *         登录名
     *
     * @return
     */
    @Override
    public boolean isDestroy (String loginName) {
        if (StringUtils.isEmpty(loginName)) {
            return true;
        }

        Integer banStatus = this.userMapper.getBanStatus(loginName);

        if (banStatus == null) {
            return false;
        }
        // 等于1，表示已注销
        return banStatus == 1;
    }



    /**
     * 根据用户获取最新的订单
     *
     *
     * @return
     */
    @Override
    public AjaxResult updateUserUserName (Long userId, String userName) {
        User user = this.selectUserById(userId);
        if(ObjectUtil.isEmpty(user)){
          return AjaxResult.error("用户不存在");
        }
        userMapper.updateUserName(userId,userName);
        return AjaxResult.success();
    }
    @Override
    public List <Long> selectDeptUserList (User user) {
        return userMapper.selectDeptUserList(user);
    }
}
