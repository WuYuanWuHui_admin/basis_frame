/*
 * ...
 */

package com.ruoyi.project.system.user.service;

import com.ruoyi.project.system.user.domain.User;

import java.util.List;

/**
 * 管理员service
 *
 * @author 挺好的 2023年02月22日 上午 10:39
 */
public interface IAdminService {

    /**
     * 查找管理员
     *
     * @param user
     *         查询条件
     *
     * @return
     */
    List <User> findList (User user);
}
