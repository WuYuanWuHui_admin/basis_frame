package com.ruoyi.project.system.user.domain;

import cn.hutool.core.util.ObjectUtil;
import com.ruoyi.common.sensitive.util.SensitiveInfoUtils;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.aspectj.lang.annotation.Excel.ColumnType;
import com.ruoyi.framework.aspectj.lang.annotation.Excel.Type;
import com.ruoyi.framework.web.domain.BaseEntity;
import com.ruoyi.project.system.dept.domain.Dept;
import com.ruoyi.project.system.role.domain.Role;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.shiro.crypto.SecureRandomNumberGenerator;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

/**
 * 用户对象 sys_user
 *
 * @author ruoyi
 */
public class User extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 用户ID
     */
    @Excel (name = "用户序号", cellType = ColumnType.NUMERIC, prompt = "用户编号")
    private Long userId;

    /**
     * 部门ID
     */
    @Excel (name = "部门编号", type = Type.IMPORT)
    private Long deptId;

    /**
     * 部门父ID
     */
    private Long parentId;

    /**
     * 角色ID
     */
    private Long roleId;

    /**
     * 登录名称
     */
    @Excel (name = "登录名称")
    private String loginName;

    /**
     * 用户名称
     */
    @Excel (name = "用户名称")
    private String userName;

    /**
     * 用户邮箱
     */

    private String email;

    /**
     * 余额
     */
    @Excel (name = "余额")
    private Long amount;

    /**
     * 手机号码
     */
    @Excel (name = "手机号码")
    private String phonenumber;

    private String avatarto;

    /**
     * 用户性别
     */

    @Excel (name = "用户性别", readConverterExp = "0=男,1=女,2=未知")
    private String sex;

    /**
     * 用户头像
     */
    private String avatar;

    /**
     * 密码
     */
    private String password;

    /**
     * 盐加密
     */
    private String salt;

    /**
     * 帐号状态（0正常 1停用）
     */
    @Excel (name = "帐号状态", readConverterExp = "0=正常,1=停用")
    private String status;

    /**
     * 删除标志（0代表存在 2代表删除）
     */
    private String delFlag;

    /**
     * 最后登陆IP
     */
    private String loginIp;

    /**
     * 最后登陆时间
     */
    @Excel (name = "最后登陆时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss", type = Type.EXPORT)
    private Date loginDate;

    /**
     * 年龄
     */
    @Excel (name = "年龄")
    private Integer age;

    /**
     * 用户标记  0 否 1 是
     */
    @Excel (name = "用户标记")
    private Integer userMark;

    /**
     * 支付密码
     */
    private Integer userphonetype;

    //头像状态
    private Integer idavaterStatus;

    //直通卡状态
    private Integer CardUserType;

    private Date Cardtime;

    private Integer CardNumber;

    private String CardJarid;

    private String payPwd;

    //是否收取服务费标识(0否 1是)
    @Excel (name = "服务费", readConverterExp = "0=否,1=是")
    private Integer serverFlag;

    //vip用户标识(0否 1是)
    @Excel (name = "vip", readConverterExp = "0=否,1=是")
    private Integer vipFlag;

    //身份验证开关(0否 1是)
    @Excel (name = "实名状态", readConverterExp = "0=未实名,1=实名")
    private Integer authSwitch;

    /**
     * 真实姓名
     */
    @Excel (name = "真实姓名")
    private String realName;

    /**
     * 身份证号码
     */
    @Excel (name = "身份证号码")
    private String idCardNum;

    @Excel (name = "用户等级")
    private Integer level;

    /**
     * 部门对象
     */

    private Dept dept;

    private List <Role> roles;

    /**
     * 角色组
     */
    private Long[] roleIds;

    /**
     * 岗位组
     */
    private Long[] postIds;

    /**
     * 学历
     */
    private String edu;

    /**
     * 期望类型
     */
    private String expectType;

    /**
     * 期望工种
     */
    private String expectWorkType;

    /**
     * 期望地址
     */
    private String expectAddress;

    /**
     * 期望工作时间
     */
    private String expectWorkTime;

    /**
     * 个性签名
     */
    private String gxSign;

    /**
     * 用户类型
     */
    @Excel (name = "用户类型", readConverterExp = "0=雇员,1=雇主,-1=未知")
    private Short userType;

    /**
     * 纬度
     */
    private String lat;

    /**
     * 经度
     */
    private String lng;

    /**
     * 区域
     */
    @Excel (name = "区域")
    private String region;

    /**
     * 身高
     */
    private Integer stature;

    private Double distance;

    @Excel (name = "诚信值")
    private Integer integrity;

    private Integer user_job_status;

    /**
     * 发单量
     */
    @Excel (name = "发单量")
    private Integer issuanceNum;

    /**
     * 发单量
     */
    @Excel (name = "接单量")
    private Integer receiveNum;

    @Excel (name = "实时发单量")
    private Integer ssfdl;

    @Excel (name = "实时接单量")
    private Integer ssjdl;

    /**
     * 临时字段用于查询
     */
    private Integer startAge;

    private Integer startStature;

    private Integer endAge;

    private Integer endStature;

    private Integer isInvite;

    private String[] userJobStatus;

    private String key;

    private Integer idCardStatus;

    private Long notUserId;

    private Integer userType1;

    private Integer spreadFlag;

    private Integer banStatus;

    @Excel (name = "上级手机号")
    private String upPhone;

    @Excel (name = "渠道")
    private String channel;

    private Integer CardType;

    @Excel (name = "登录次数")
    private Integer signInCount;

    /**
     * 区域编码
     */
    private String adCode;

    private String idCardDownPic;

    private String idCardUpPic;

    //vip到期时间
    private String vipEndTime;

    private Integer employeeRating;

    //用户身份：0 试用零工 1 正式零工 2 金牌
    private Integer userIdentityStatus;
    //用户身份明细
    private Integer userIdentity;
    //认证头像图片，后期与打卡使用校验
    private String authAvatar;
    //是否bd推广
    private String bdFlagName="否";

    /**
     * 密文手机号码
     */
    private String secretPhonenumber;

    /**
     * 密文身份证号码
     */
    private String secretIdCardNum;

    public Integer getUserIdentityStatus() {
        return userIdentityStatus;
    }

    public void setUserIdentityStatus(Integer userIdentityStatus) {
        this.userIdentityStatus = userIdentityStatus;
    }

    public Integer getEmployeeRating() {
        return employeeRating;
    }

    public void setEmployeeRating(Integer employeeRating) {
        this.employeeRating = employeeRating;
    }

    public Integer getSsfdl () {
        return this.ssfdl;
    }

    public void setSsfdl (Integer ssfdl) {
        this.ssfdl = ssfdl;
    }

    public Integer getSsjdl () {
        return this.ssjdl;
    }

    public void setSsjdl (Integer ssjdl) {
        this.ssjdl = ssjdl;
    }

    public Integer getSignInCount () {
        return this.signInCount;
    }

    public void setSignInCount (Integer signInCount) {
        this.signInCount = signInCount;
    }

    public String getIdCardDownPic () {
        return this.idCardDownPic;
    }

    public void setIdCardDownPic (String idCardDownPic) {
        this.idCardDownPic = idCardDownPic;
    }

    public String getIdCardUpPic () {
        return this.idCardUpPic;
    }

    public void setIdCardUpPic (String idCardUpPic) {
        this.idCardUpPic = idCardUpPic;
    }

    public String getAdCode () {
        return this.adCode;
    }

    public void setAdCode (String adCode) {
        this.adCode = adCode;
    }

    public Long getAmount () {
        return this.amount;
    }

    public void setAmount (Long amount) {
        this.amount = amount;
    }

    public Integer getCardType () {
        return this.CardType;
    }

    public void setCardType (Integer cardType) {
        this.CardType = cardType;
    }

    public Integer getIdavaterStatus () {
        return this.idavaterStatus;
    }

    public void setIdavaterStatus (Integer idavaterStatus) {
        this.idavaterStatus = idavaterStatus;
    }

    public Integer getLevel () {
        return this.level;
    }

    public String getAvatarto () {
        return this.avatarto;
    }

    public Integer getCardUserType () {
        return this.CardUserType;
    }

    public void setCardUserType (Integer cardUserType) {
        this.CardUserType = cardUserType;
    }

    public Date getCardtime () {
        return this.Cardtime;
    }

    public void setCardtime (Date cardtime) {
        this.Cardtime = cardtime;
    }

    public Integer getCardNumber () {
        return this.CardNumber;
    }

    public void setCardNumber (Integer cardNumber) {
        this.CardNumber = cardNumber;
    }

    public String getCardJarid () {
        return this.CardJarid;
    }

    public void setCardJarid (String cardJarid) {
        this.CardJarid = cardJarid;
    }

    public void setAvatarto (String avatarto) {
        this.avatarto = avatarto;
    }

    public Integer getUserphonetype () {
        return this.userphonetype;
    }

    public void setUserphonetype (Integer userphonetype) {
        this.userphonetype = userphonetype;
    }

    public void setLevel (Integer level) {
        this.level = level;
    }

    public Long getUserId () {
        return this.userId;
    }

    public void setUserId (Long userId) {
        this.userId = userId;
    }

    public boolean isAdmin () {
        return isAdmin(this.userId);
    }

    public static boolean isAdmin (Long userId) {
        return userId != null && 1L == userId;
    }

    public Long getDeptId () {
        return this.deptId;
    }

    public void setDeptId (Long deptId) {
        this.deptId = deptId;
    }

    public Long getParentId () {
        return this.parentId;
    }

    public void setParentId (Long parentId) {
        this.parentId = parentId;
    }

    public Long getRoleId () {
        return this.roleId;
    }

    public void setRoleId (Long roleId) {
        this.roleId = roleId;
    }

    public String getChannel () {
        return this.channel;
    }

    public void setChannel (String channel) {
        this.channel = channel;
    }

    @NotBlank (message = "登录账号不能为空")
    @Size (min = 0, max = 30, message = "登录账号长度不能超过30个字符")
    public String getLoginName () {
        return this.loginName;
    }

    public void setLoginName (String loginName) {
        this.loginName = loginName;
    }

    @Size (min = 0, max = 30, message = "用户昵称长度不能超过30个字符")
    public String getUserName () {
        return this.userName;
    }

    public void setUserName (String userName) {
        this.userName = userName;
    }

    @Email (message = "邮箱格式不正确")
    @Size (min = 0, max = 50, message = "邮箱长度不能超过50个字符")
    public String getEmail () {
        return this.email;
    }

    public void setEmail (String email) {
        this.email = email;
    }

    @Size (min = 0, max = 11, message = "手机号码长度不能超过11个字符")
    public String getPhonenumber () {
        return this.phonenumber;
    }

    public void setPhonenumber (String phonenumber) {
        this.phonenumber = phonenumber;
        setSecretPhonenumber(phonenumber);
    }

    public String getSex () {
        return this.sex;
    }

    public void setSex (String sex) {
        this.sex = sex;
    }

    public String getAvatar () {
        return this.avatar;
    }

    public void setAvatar (String avatar) {
        this.avatar = avatar;
    }

    public String getPassword () {
        return this.password;
    }

    public void setPassword (String password) {
        this.password = password;
    }

    public String getSalt () {
        return this.salt;
    }

    public void setSalt (String salt) {
        this.salt = salt;
    }

    /**
     * 生成随机盐
     */
    public void randomSalt () {
        // 一个Byte占两个字节，此处生成的3字节，字符串长度为6
        SecureRandomNumberGenerator secureRandom = new SecureRandomNumberGenerator();
        String hex = secureRandom.nextBytes(3).toHex();
        this.setSalt(hex);
    }

    public String getStatus () {
        return this.status;
    }

    public void setStatus (String status) {
        this.status = status;
    }

    public String getDelFlag () {
        return this.delFlag;
    }

    public void setDelFlag (String delFlag) {
        this.delFlag = delFlag;
    }

    public String getLoginIp () {
        return this.loginIp;
    }

    public void setLoginIp (String loginIp) {
        this.loginIp = loginIp;
    }

    public Date getLoginDate () {
        return this.loginDate;
    }

    public void setLoginDate (Date loginDate) {
        this.loginDate = loginDate;
    }

    public Dept getDept () {
        if (this.dept == null) {
            this.dept = new Dept();
        }
        return this.dept;
    }

    public void setDept (Dept dept) {
        this.dept = dept;
    }

    public List <Role> getRoles () {
        return this.roles;
    }

    public void setRoles (List <Role> roles) {
        this.roles = roles;
    }

    public Long[] getRoleIds () {
        return this.roleIds;
    }

    public void setRoleIds (Long[] roleIds) {
        this.roleIds = roleIds;
    }

    public Long[] getPostIds () {
        return this.postIds;
    }

    public void setPostIds (Long[] postIds) {
        this.postIds = postIds;
    }

    public Integer getAge () {
        return this.age;
    }

    public void setAge (Integer age) {
        this.age = age;
    }

    public String getPayPwd () {
        return this.payPwd;
    }

    public void setPayPwd (String payPwd) {
        this.payPwd = payPwd;
    }

    public String getExpectType () {
        return this.expectType;
    }

    public void setExpectType (String expectType) {
        this.expectType = expectType;
    }

    public String getExpectWorkType () {
        return this.expectWorkType;
    }

    public void setExpectWorkType (String expectWorkType) {
        this.expectWorkType = expectWorkType;
    }

    public String getExpectAddress () {
        return this.expectAddress;
    }

    public void setExpectAddress (String expectAddress) {
        this.expectAddress = expectAddress;
    }

    public String getExpectWorkTime () {
        return this.expectWorkTime;
    }

    public void setExpectWorkTime (String expectWorkTime) {
        this.expectWorkTime = expectWorkTime;
    }

    public String getGxSign () {
        return this.gxSign;
    }

    public void setGxSign (String gxSign) {
        this.gxSign = gxSign;
    }

    public String getEdu () {
        return this.edu;
    }

    public void setEdu (String edu) {
        this.edu = edu;
    }

    public Short getUserType () {
        return this.userType;
    }

    public void setUserType (Short userType) {
        this.userType = userType;
    }

    public String getLat () {
        return this.lat;
    }

    public void setLat (String lat) {
        this.lat = lat;
    }

    public String getLng () {
        return this.lng;
    }

    public void setLng (String lng) {
        this.lng = lng;
    }

    public void setRegion (String region) {
        if(ObjectUtil.isNotEmpty(region) && region.length() > 5){
            this.region = region.substring(0, 6);
        }
    }

    public String getRegion () {
        return this.region;
    }

    public Integer getStature () {
        return this.stature;
    }

    public void setStature (Integer stature) {
        this.stature = stature;
    }

    public Integer getStartAge () {
        return this.startAge;
    }

    public void setStartAge (Integer startAge) {
        this.startAge = startAge;
    }

    public Integer getStartStature () {
        return this.startStature;
    }

    public void setStartStature (Integer startStature) {
        this.startStature = startStature;
    }

    public Integer getEndAge () {
        return this.endAge;
    }

    public void setEndAge (Integer endAge) {
        this.endAge = endAge;
    }

    public Integer getEndStature () {
        return this.endStature;
    }

    public void setEndStature (Integer endStature) {
        this.endStature = endStature;
    }

    public String[] getUserJobStatus () {
        return this.userJobStatus;
    }

    public void setUserJobStatus (String[] userJobStatus) {
        this.userJobStatus = userJobStatus;
    }

    public Double getDistance () {
        return this.distance;
    }

    public void setDistance (Double distance) {
        this.distance = distance;
    }

    public Integer getIntegrity () {
        return this.integrity;
    }

    public void setIntegrity (Integer integrity) {
        this.integrity = integrity;
    }

    public Integer getUser_job_status () {
        return this.user_job_status;
    }

    public void setUser_job_status (Integer user_job_status) {
        this.user_job_status = user_job_status;
    }

    public Integer getIssuanceNum () {
        return this.issuanceNum;
    }

    public void setIssuanceNum (Integer issuanceNum) {
        this.issuanceNum = issuanceNum;
    }

    public Integer getReceiveNum () {
        return this.receiveNum;
    }

    public void setReceiveNum (Integer receiveNum) {
        this.receiveNum = receiveNum;
    }

    public Integer getIsInvite () {
        return this.isInvite;
    }

    public void setIsInvite (Integer isInvite) {
        this.isInvite = isInvite;
    }

    public String getKey () {
        return this.key;
    }

    public void setKey (String key) {
        this.key = key;
    }

    public Integer getIdCardStatus () {
        return this.idCardStatus;
    }

    public void setIdCardStatus (Integer idCardStatus) {
        this.idCardStatus = idCardStatus;
    }

    public Long getNotUserId () {
        return this.notUserId;
    }

    public void setNotUserId (Long notUserId) {
        this.notUserId = notUserId;
    }

    public Integer getUserType1 () {
        return this.userType1;
    }

    public void setUserType1 (Integer userType1) {
        this.userType1 = userType1;
    }

    public Integer getSpreadFlag () {
        return this.spreadFlag;
    }

    public void setSpreadFlag (Integer spreadFlag) {
        this.spreadFlag = spreadFlag;
    }

    public Integer getBanStatus () {
        return this.banStatus;
    }

    public void setBanStatus (Integer banStatus) {
        this.banStatus = banStatus;
    }

    public Integer getServerFlag () {
        return this.serverFlag;
    }

    public void setServerFlag (Integer serverFlag) {
        this.serverFlag = serverFlag;
    }

    public Integer getVipFlag () {
        return this.vipFlag;
    }

    public void setVipFlag (Integer vipFlag) {
        this.vipFlag = vipFlag;
    }

    public Integer getAuthSwitch () {
        return this.authSwitch;
    }

    public void setAuthSwitch (Integer authSwitch) {
        this.authSwitch = authSwitch;
    }

    public String getUpPhone () {
        return this.upPhone;
    }

    public void setUpPhone (String upPhone) {
        this.upPhone = upPhone;
    }

    public String getRealName () {
        return this.realName;
    }

    public void setRealName (String realName) {
        this.realName = realName;
    }

    public String getIdCardNum () {
        return this.idCardNum;
    }

    public void setIdCardNum (String idCardNum) {
        this.idCardNum = idCardNum;
        setSecretIdCardNum(idCardNum);
    }

    public Integer getUserMark () {
        return this.userMark;
    }

    public void setUserMark (Integer userMark) {
        this.userMark = userMark;
    }

    public String getVipEndTime () {
        return this.vipEndTime;
    }

    public void setVipEndTime (String vipEndTime) {
        this.vipEndTime = vipEndTime;
    }

    public Integer getUserIdentity () {
        return userIdentity;
    }

    public void setUserIdentity (Integer userIdentity) {
        this.userIdentity = userIdentity;
        if(this.userIdentity != null &&(this.userIdentity == 0 || this.userIdentity == 4)){
            this.userIdentityStatus=0;
        }else if(this.userIdentity != null &&(this.userIdentity == 1 || this.userIdentity == 5)){
            this.userIdentityStatus=1;
        }else if(this.userIdentity != null &&(this.userIdentity == 2 || this.userIdentity == 3)){
            this.userIdentityStatus=2;
        }
    }

    public String getBdFlagName () {
        return bdFlagName;
    }

    public void setBdFlagName (String bdFlagName) {
        this.bdFlagName = bdFlagName;
    }

    public String getAuthAvatar () {
        return authAvatar;
    }

    public void setAuthAvatar (String authAvatar) {
        this.authAvatar = authAvatar;
    }

    public String getSecretPhonenumber () {
        return secretPhonenumber;
    }

    public void setSecretPhonenumber (String secretPhonenumber) {
        this.secretPhonenumber =  SensitiveInfoUtils.mobilePhone(secretPhonenumber);
    }

    public String getSecretIdCardNum () {
        return secretIdCardNum;
    }

    public void setSecretIdCardNum (String secretIdCardNum) {
        this.secretIdCardNum = SensitiveInfoUtils.idCardNum(secretIdCardNum);
    }

    @Override
    public String toString () {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE).append("userId", this.getUserId())
                .append("deptId", this.getDeptId()).append("loginName", this.getLoginName())
                .append("userName", this.getUserName()).append("email", this.getEmail())
                .append("phonenumber", this.getPhonenumber()).append("sex", this.getSex())
                .append("avatar", this.getAvatar()).append("password", this.getPassword())
                .append("salt", this.getSalt()).append("status", this.getStatus()).append("delFlag", this.getDelFlag())
                .append("loginIp", this.getLoginIp()).append("loginDate", this.getLoginDate())
                .append("createBy", this.getCreateBy()).append("createTime", this.getCreateTime())
                .append("updateBy", this.getUpdateBy()).append("updateTime", this.getUpdateTime())
                .append("remark", this.getRemark()).append("idCardNum", this.getIdCardNum())
                .append("realName", this.getRealName()).append("dept", this.getDept()).toString();
    }
}
