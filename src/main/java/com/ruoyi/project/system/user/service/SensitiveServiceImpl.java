/*
 * ...
 */

package com.ruoyi.project.system.user.service;

import cn.hutool.core.util.ObjectUtil;
import com.google.common.base.Charsets;
import com.ruoyi.common.constant.RedisKeyConstants;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.framework.config.RuoYiConfig;
import com.ruoyi.project.utils.RedisUtils;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;

/**
 * 敏感词相关
 *
 * @author 挺好的 2023年02月16日 下午 21:12
 */
@Service ("sensitiveServiceImpl")
public class SensitiveServiceImpl implements ISensitiveService {

    private static final String TEXT="大法,国军,造反,民运,反恐委员会,游戏管理员,第五代领导,第5代领导,反攻大陆,帝国主义,分裂中国,大陆当局,弟大物勃,独裁政治,东北独立,优化官员,邓小平,邓晓平,大纪元,测绘局,反政府,党中央,颠覆,反华,党禁,党禁,布什,腐败,应召,多党,爱国者同盟,权力布局,北京政权,西藏独立,北京当局,安全局,保监会,届委员,届名单," +
            "18预测,常委,中央,天葬,爱滋,打压,藏西,暴政,暗杀,绑架,8大,18届,天安门录影带,爱国者同盟,天安门屠杀,天安门事件,天安门一代,人民报讯,两个中国,两岸关系,两岸关系,事实独立,秘书长,南联盟,两会,示威,六>四,唐捷,人弹,连战,西z,末世论,台湾青年独立联盟,台湾建国运动组织,人民内情真相,人民真实报道,台湾自由联盟,天府广场集会,讨伐中宣部,台湾政论区,台湾共合国,台湾独立," +
            "时事参考,人体炸弹,民主运动,异议人士,美国之音,党,美国佬,接班人,台办,湾台,统战,六四,人渣,绵恒,18大,胡晓炼,胡耀邦,胡玉敏,胡泽君,胡振民,上访,联合起诉最高人民法院,共青团背景,联合行动,政治,政治,社会主义,江泽民,江戏子,江责民,江则民,江泽慧,江泽林,江猪媳,江贼民,江折民,韩长赋,酱猪媳,教养院,江流氓,解放军,江蛤蟆,姜建清,韩东方,韩联潮,副市长,垮台,推翻社会主义制度," +
            "晚年周恩来,青天白日,蒙古独立,六四事件,学生运动,中央,令计划,外汇局,萨达姆,两会,蒙独,六四,迫害,省委,社会主义,昨日重现,四川独立,胡总书记,双十节,刘奇葆,膏药旗,书记,僵贼民,姜春云,江独裁,姜大明,姜伟新,姜异康,将则民,江澤>民,江八点,江核心,江ze民,江Core,江core,共军,锦涛,韩正,老江,联大,军事,主席,香港明报,西藏论坛,公开信胡书记空中民主墙,公开批评中央高层领导人,理想信念斗争,子女任职名单," +
            "检察官,检察院,警察,民警,周恩来,王中,老毛,鸡毛信文汇,持不同政见,周刊纪事,咨讯紫阳,军长发威,恐怖主义,恐怖份子,换届隐忧,复转军人,军转干部,军转安置,老毛子,公开信,积克馆,清华帮,腐败,公投,共党,暴动,回民,纪元,中共,中国,英雄纪念碑,信息产业部,18高官互调,信用危机,重新评定,中医药局,425事件,新生网,星相,版署,新党,燕南评论,民族问题,民族矛盾,民意论坛,宇宙真理,民主潮,民主墙,宇明网,民告官,东洲," +
            "北韩,民联,民运,民运,民阵,民猪,王兆国,共产党,常委,印尼伊斯兰祈祷团,40万名车车主名单,钟山风雨论坛,18大委员名单,大中华论坛,信用卡空卡,刘晓竹,胡平,刘宾雁,魏京生,坦克压大学生,89年春夏之交,8的平方事件,历史的伤口,四二六社论,贰拾周年,血洗京城,六河蟹四,六百度四,六和谐四,民主女神,北京风波,诺贝尔和平奖,央视内部晚会,20和谐年,四事件,北高联,高自联,八九年,天按门,门安天,贰拾年,64运动,64惨案,知道64,64时期,4事件,5月35,198964,陆肆," +
            "陆四,六四,2o年,四二六社论,吾尔开希,苏晓康,贺卫方,谭作人,焦国标,万润南,张志新,辛灝年,高勤荣,王炳章,高智晟,司马璐,柴玲,沈彤,中华,军区,小泽一郎,闹独立,fenlie,duli,告全国同胞书,台湾共和国,蒋公纪念歌,颜色革命,两岸战争,两岸战争,习近平,光复民国,一中一台,清zhen,江毒,鞑子,满狗,回回,真主,清真,回民,林文漪,三股势力,印尼屠华,台百度湾,印尼事件,中华联邦,青天白日,日本万岁,小泽一郎,规模冲突,规模冲突,劣等民族,基地组织,恐怖分子,吾尔开西," +
            "封从德,王超华,王维林,侯德健,阎明复,方励之,蒋捷连,丁子霖,辛灏年,蒋彦永,严家其v,王丹,江人马,藏春阁,东突厥斯坦解放组织,东突解放组织,蒙古分裂分子,雪山狮子旗,丹增嘉措,伊力哈木,阿旺晋美,藏青会,藏青会,藏暴乱,藏独立,藏妇会,藏字石,藏字石,打砸抢,藏春阁,热比娅,王千源,dl喇嘛,支持zd,zang人,西脏,藏獨,安拉,西臧,xizang,>藏西,希葬,希藏,西葬,西奘,硒藏,稀藏,汉人,啦撒,葬独,列确,藏人,臧人,藏民,赖达,哒赖,西独,回教,臧独,汉维,啦沙,藏毒,啦萨,zangdu,la萨,bjork,藏独立," +
            "藏妇会,塔利班,闹独立,入耳关,mayingjiu,李天羽,苏贞昌,陈水扁,陈随便,解放tw,陈s扁,x民党,台毒,fenlie,台军,台军台弯,taiwan,台军,台wan,入联,台完,湾台,台du,taidu,duli,a扁,twdl,dalai,xi藏,藏du,tibet,拉sa,藏m,x藏,lasa,西z,江丑闻,韩国狗,阿拉伯,qingzhen,安拉,藏西,拉sa,bjork,la萨,xi藏,tibet,lasa,满洲第三帝国,江派和胡派,圣战组织,江派人马,江胡内斗,高丽棒子,江泉集团,穆罕穆德,默罕默德,穆罕默德,伊斯兰,穆斯林,江绵恒,江家帮,江祸心,江黑心,江核心," +
            "江蛤蟆,江独裁,江嫡系,回族,满洲第三帝国,雪山狮子旗,江派和胡派,江泉集团,江派人马,穆罕穆德,穆罕默德,江胡内斗,默罕默德,默罕默德,圣战组织,高丽棒子,藏暴乱,穆斯林,伊斯兰,阿拉伯,韩国狗,江独裁,江丑闻,江嫡系,王千源,江蛤蟆,藏青会,x藏,台湾,西奘,回民,回族,西葬,回教,清真,真主,回回,西脏,稀藏,硒藏,希藏,希葬,啦沙,西臧,啦撒,鞑子,xizang,江毒,藏獨,啦萨,清zhen,qingzhen,藏字石,满狗,啦撒,免税接单,叫床,抽插,菊花洞,嫩逼,色情网站,幼交,奴,性爱,成人卡通,狠入,bao干,鸦片,阴阜,娇喘,扌" +
            "由插,菊门,嗑乐,色区,幼男,乳,porn,成人聊,狠日,成人电影,乖乖粉,外阴,血日,肏,抽一插,巨奶,嫩穴,色诱,幼女,玉穴,你麻痹,成人片,狠干,成人论坛,摇头丸,阴唇,血插,薬,春药,后人,捏弄,色欲,欲>火,援交,日n,成人视,猛干,成人小说,海洛因,阴蒂,血干,屄,发骚,菊穴,女优,色b,欲女,原味内衣,干n,成人图,猛日,成人论坛,冰毒,前庭,血弄,乳房,大力抽送,开苞,炮友,嗑炮,全套,援助交际,性伙伴,成人文,猛插,迷药,苯丙胺类,后庭,奶子,荡妇,口爆,砲友,看批,拳套,招鸡,性交,成人小,猛入,麻醉药," +
            "麦斯卡林,阴道,交配,荡女,口活,喷精,射爽,quan套,招妓,性感少,成人电,暴干,精神药品,罂粟,处女,射精,盗撮,口交,屁眼,射颜,全tao,被日,白痴,插逼,乱伦,吗啡,子宫,cao,多人轮,口射," +
            "品香堂,食精,骚逼,抓胸,麻痹的,狗娘,插进,乱伦类,大麻,阴毛,Cao,发浪,狂插,前凸后翘,释欲,zi慰,肏,妈了个逼,睾丸,插阴,乱伦小,Cannabis,男下女上,艹,放尿,浪逼,强暴,兽交,自慰,奸,马勒,diao,潮吹,伦理大,Heroin,男坐女站,做ai,肥逼,浪妇,情趣用品,兽欲,自wei,淫,狗娘养,Diao,潮喷,伦理电影,Morphine,后交,zuo爱,粉穴,浪叫,情色,熟妇,18禁,曰,贱比,兽性,爆干,杜冷丁,侧交,座爱,风月大陆,浪女,拳交,熟母,穴图,贱人,死全家,风骚,果体,伦理片,盐酸哌替啶,侧入,坐爱,干死你," +
            "狼友,全裸,熟女,大sb,全家死光,呻吟,裹体,裸聊,老汉推车,作爱,干穴,做ai,群交,爽片,颜射,傻逼,全家不得>好死,阉割,谜j药,裸聊网,可卡因,女上位,嘬奶,肛交,舔阴,人妻,阳具,傻b,全家死绝,高潮,骚妇,裸体写真,Cocaine,观音坐莲,嘬逼,肛门,凌辱,人兽,双臀,要射了,煞逼,sb,裸露,骚货,裸舞视,那可汀,男上位,嘬P,龟头,乱交,日逼,死逼,煞笔,杀b,舔下面,骚浪,裸照,可待因,氵朝,嘬屁,裹本,乱伦,日烂,套弄,一本道,刹笔,你吗b,神经病,骚女,美女裸体,盐酸二氢埃托啡,美眉写真,嘬B,包二奶," +
            "轮暴,肉棒,脱内裤,一夜欢,傻比,你妈的,精神病,骚嘴,美女写真,K粉,美眉上门,嘬bi,黑逼,裸陪,肉逼,文做,一夜情,沙比,婊子,看下面,骚b,美女上门,三唑仑,招ji,被操,后庭,买春,肉唇,骚货,一ye情,欠干,贱货,文爱,骚比,美艳少妇,海乐神,招j,操逼,后穴,美逼,肉洞,擦你妈,阴部,婊子养的,人渣,咪咪,骚逼,妹按摩,酣乐欣,zhao妓,操黑,车震,美少妇,肉缝,性感妖娆,阴精,我日你,混蛋,处男,色电影,妹上门,蒙汗药,嫖,操烂,换妻>俱乐部,肉棍,性感诱惑,阴唇,我草,浑蛋,少妇,色妹妹,迷幻药,安纳咖,女票,操死," +
            "黄片,美穴,肉茎,性虎,阴道,卧槽,屌,脱光,色情,迷幻藥,氟硝安定,女支,操我,几吧,美幼,肉具,性饥渴,阴阜,爆你菊,下体,干你,色视频,迷昏口,麦角乙二胺,bb,狂操,鸡吧,秘唇,肉穴,性技巧,>阴核,你他妈,a片,干死,色小说,迷昏药,安眠酮,干女,轮操,鸡巴,密穴,肉欲,性交,阴户,真他妈,性伴侣,你妈,性伴侣,迷昏藥,丁丙诺啡,生殖,我操,寂寞男,蜜穴,入肉,性福情,日你,别他吗,精子,欠日,性服务,迷魂香,地西泮,自刎,操你妈,寂寞女,做love,体位,性虐,干你,草你吗,射精,约爱,JB,迷魂药,有机溶剂,自杀,操你娘,妓女," +
            "我就色,磕炮,性息,娘西皮,草你丫,卵子,插你,勾巴,迷魂藥,鼻吸剂,繁殖,操他妈,三级片,无码,后妈,性欲,猛干,草泥,阴户,插我,勾B,迷情粉,阿片,nm,狗操,女干,舔diao,下贱,胸推,被插,曹你,阴茎,鸡8,几巴,迷情水,>氵章月,日一,操你祖宗,舔脚,舔毛,插暴,穴口,ji巴,草n,吟叫,插下,鸡鸡,迷情药,日下,操你全家,舔jio,插比,被干,鸡巴,操n,发情,干下,机巴,J8,勾8,床震,操你大爷,舔b,丝诱,后入,鸡ba,曹n,舔逼,嘬下,几八,机巴,几巴,骚叫,chu女,黑B,介少,魅纸,+q,+V,加V,加Q,妹z,卫>星,+微,加微,VX,WX,扣扣," +
            "抠抠,扣Q,抠Q,qq,爿,kp,双飞,粉碧,招陪,曹碧,看b,草b,佳v,+魏,策士,吃屎";
    /**
     * 敏感词文本
     */
    private static final String SENSITIVE_TEXT_PATH = "/sensitive.txt";

    private static final String REPLACE_TEXT = "*****";

    /**
     * redis工具类
     */
    @Autowired
    private RedisUtils redisUtils;


    /**
     * 替换内容
     *
     * @param content
     *         内容
     *
     * @return
     */
    @Override
    public String replace (String content) {

        if (StringUtils.isBlank(content)) {
            return content;
        }

        String[] valueArray = this.getSensitiveArray();
        if(ObjectUtil.isEmpty(valueArray)){
            return content;
        }
        for (String value : valueArray) {
            if (content.contains(value)) {
                content = content.replace(value, REPLACE_TEXT);
            }
        }

        return content;
    }

    @Override
    public boolean hasSensitive (String value) {
        if (StringUtils.isBlank(value)) {
            return false;
        }

        String[] valueArray = this.getSensitiveArray();
        if(ObjectUtil.isEmpty(valueArray)){
            return false;
        }
        for (int i = 0; i < valueArray.length; i++) {

            String sensitive = valueArray[i];

            if (StringUtils.isEmpty(sensitive)) {
                continue;
            }

            if (value.contains(sensitive)) {
                return true;
            }
        }

        return false;
    }

    /**
     * 获取服务端配置的敏感词
     *
     * @return
     */
    private String[] getSensitiveArray () {

        try {
            //String path = RuoYiConfig.getProfile() + SENSITIVE_TEXT_PATH;
            String values = this.redisUtils.get(RedisKeyConstants.SENSITIVE_KEY);

            if (StringUtils.isEmpty(values)) {
                values =TEXT;
            }

            if (StringUtils.isBlank(values)) {
                return null;
            }

            String[] valueArray = StringUtils.split(values, ",");

            // 设置为12小时过期
            this.redisUtils.set(RedisKeyConstants.SENSITIVE_KEY, values, 12 * 60 * 60);

            return valueArray;

        } catch (Exception e) {
            return null;
        }


    }
}
