/*
 * ...
 */

package com.ruoyi.project.system.user.service;

/**
 * 敏感词汇相关service
 *
 * @author 挺好的 2023年02月16日 下午 21:11
 */
public interface ISensitiveService {

    /**
     * 替换内容
     *
     * @param content
     *         内容
     *
     * @return
     */
    String replace (String content);

    /**
     * 是否包含敏感词
     *
     * @param value
     *
     * @return
     */
    boolean hasSensitive (String value);
}
