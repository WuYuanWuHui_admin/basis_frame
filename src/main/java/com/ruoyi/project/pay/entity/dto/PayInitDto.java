package com.ruoyi.project.pay.entity.dto;

import com.ruoyi.project.content.FlowTypeEnum;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Map;

/**
 * 支付统一请求入口
 */
@Data
public class PayInitDto implements Serializable {

    //参考 PayTypeEnum
    private Integer payType;

    //支付订单号
    private String tradeNo;

    //支付金额 (支付宝：元，微信：分)
    private BigDecimal account;

    //用户名称
    private String userName;

    //用户id
    private Long userId;

    //小程序微信支付需要
    private String openid;

    //业务id
    private Long projectId;

    //支付结算类型
    private FlowTypeEnum flowTypeEnum;

    //操作人
    private String operateName;

    //ip(微信支付需要)
    private String ip;

    //平台业务类型
    private Integer serviceType;
    //支付密码
    private String payPwd;

    //存储参数,回调使用
    private Map <String, Object> parmMap;

}
