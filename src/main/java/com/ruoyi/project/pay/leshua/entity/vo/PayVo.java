package com.ruoyi.project.pay.leshua.entity.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * 请求支付返回信息
 */
@Data
public class PayVo implements Serializable {
    /**
     * 商户号
     */
     private String mchId;
    /**
     * 商户订单号
     */
    private String outTradeNo;
    /**
     * 商品描述
     */
    private String 	body;
    /**
     * 用户子标识
     */
    private String	subOpenid;
    /**
     * 总金额
     */
    private String	totalFee;
    /**
     * 通知地址
     */
    private String notifyUrl;
}
