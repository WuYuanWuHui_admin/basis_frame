package com.ruoyi.project.pay.leshua.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.framework.web.domain.AjaxResult;

import com.ruoyi.project.pay.leshua.entity.dto.PayDto;
import com.ruoyi.project.pay.leshua.entity.vo.PayVo;
import com.ruoyi.project.pay.leshua.service.IPayService;
import com.ruoyi.project.utils.RedisUtils;
import com.ruoyi.project.utils.SnowFlakeUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;
import java.util.List;

/**
 *
 */
@Slf4j
@Service
public class PayServiceImpl implements IPayService {

    private String notifyUrl;
    private String mchId;
    private String key;


    @Autowired
    RedisUtils redisUtils;

    /**
     * 支付入口
     *
     * @param dto
     * @return
     */
    @Override
    public AjaxResult pay(PayDto dto) {
        log.info("支付入口请求参数：{}", JSONObject.toJSONString(dto));
        if(ObjectUtils.isEmpty(dto)){
            return AjaxResult.error("支付参数不能为空");
        }
        if(ObjectUtils.isEmpty(dto.getPayType()) ){
            return AjaxResult.error("支付类型参数不能为空");
        }
        if(dto.getPayType()< 0 || dto.getPayType() > 2 ){
            return AjaxResult.error("支付类型参数有误，请检查");
        }
        if(ObjectUtils.isEmpty(dto.getProjectId()) ){
            return AjaxResult.error("支付业务ID参数不能为空");
        }
        if(ObjectUtils.isEmpty(dto.getProjectType()) ){
            return AjaxResult.error("支付业务场景参数不能为空");
        }
        if(ObjectUtils.isEmpty(dto.getProjectAmount()) ){
            return AjaxResult.error("支付金额参数不能为空");
        }
        if(dto.getProjectAmount().compareTo(BigDecimal.ZERO) < 1){
            return AjaxResult.error("支付金额有误请检查");
        }

        String orderNo = "DLG"+dto.getProjectType()+"P"+SnowFlakeUtil.getDefaultSnowFlakeId();
        PayVo vo = new PayVo();
        vo.setMchId(mchId);
        vo.setBody("找零工");
        vo.setOutTradeNo(orderNo);
        vo.setTotalFee(dto.getProjectAmount().multiply(new BigDecimal(100))+"");
        vo.setNotifyUrl(notifyUrl);
        //生成订单
        return AjaxResult.success(vo);
    }

    /**
     * '
     * 乐刷支付回调
     *
     * @return
     */
    @Override
    public AjaxResult payCallback() {
        return null;
    }
}
