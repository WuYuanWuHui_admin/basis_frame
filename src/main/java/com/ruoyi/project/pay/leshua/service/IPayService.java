package com.ruoyi.project.pay.leshua.service;

import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.project.pay.leshua.entity.dto.PayDto;

/**
 * 支付统一入口
 */
public interface IPayService {
    /**
     * 支付入口
     * @return
     */
    AjaxResult pay(PayDto dto);

    /**'
     * 乐刷支付回调
     * @return
     */
    AjaxResult payCallback();
}
