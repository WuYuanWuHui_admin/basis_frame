package com.ruoyi.project.pay.leshua.entity.dto;

import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 支付请求实体
 */
@Data
public class PayDto implements Serializable {


    /** 支付类型 0：微信jsapi 1:微信小程序 2 支付宝H5 */
    private Integer payType;
    /** 业务id */
    private String projectId;

    /** 业务场景 1 购买直通卡  2 雇主结算 3 充值
     4 支付保证金 5 购买金牌  6 支付月卡  7 购买加量包  8 结算所有任务 9 充值 10 结算单个任务 11 解封*/
    private Integer projectType;
    //订单金额
    private BigDecimal projectAmount;
    /**
     * 用户id
     */
    private Long userId;
}
