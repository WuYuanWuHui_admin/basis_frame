package com.ruoyi.project.pay.init;

import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.project.pay.entity.dto.PayInitDto;

/**
 * @author 挺好的 2023年07月19日 下午 18:49
 */
public interface IPayInitService {

    /**
     * 支付同意入口
     *
     * @param dto
     *         入参
     *
     * @return
     */
    AjaxResult pay (PayInitDto dto) throws Exception;

}
