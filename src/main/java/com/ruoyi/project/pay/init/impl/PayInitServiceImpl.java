package com.ruoyi.project.pay.init.impl;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.framework.web.domain.AjaxResult;

import com.ruoyi.project.content.PayTypeEnum;
import com.ruoyi.project.content.PropertyTypeEnum;
import com.ruoyi.project.pay.entity.dto.PayInitDto;
import com.ruoyi.project.pay.init.IPayInitService;
import com.ruoyi.project.payUtils.leshua.LeshuaPayUtil;
import com.ruoyi.project.payUtils.wx.WxPayUtils;
import com.ruoyi.project.payUtils.zfb.AliPayUtils;
import com.ruoyi.project.system.user.domain.User;
import com.ruoyi.project.system.user.service.IUserService;
import com.ruoyi.project.utils.RedisUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Map;

/**
 * @author 挺好的 2023年07月19日 下午 18:51
 */
@Slf4j
@Service
public class PayInitServiceImpl implements IPayInitService {



    @Autowired
    private WxPayUtils wxPayUtils;

    @Autowired
    private AliPayUtils alipayUtils;

    @Autowired
    private LeshuaPayUtil leshuaPayUtil;



    @Autowired
    RedisUtils redisUtils;
    @Autowired
    private IUserService userService;



    @Override
    public AjaxResult pay (PayInitDto dto) throws Exception {
        if (ObjectUtil.isEmpty(dto)) {
            return AjaxResult.error("支付信息不能为空");
        }
        //是否开启
        String payChannelCloseList = this.redisUtils.hget(PropertyTypeEnum.SYS.getText(),
                "pay_channel_close_list"
        ) + "";
        if (ObjectUtil.isNotEmpty(payChannelCloseList)) {
            String[] split = payChannelCloseList.split(",");
            for (int i = 0; i < split.length; i++) {
                if (split[i].equals(dto.getPayType() + "")) {
                    throw new RuntimeException("渠道已关闭,请更换其它渠道");
                }
            }
        }
        boolean lockFlag = this.redisUtils.setLock("pay:" + dto.getTradeNo(), 3);
        if (!lockFlag) {
            return AjaxResult.error("请不要重复支付");
        }

        redisUtils.set("pay:payInit:"+dto.getUserId(),dto.getTradeNo(),600);
        switch (PayTypeEnum.findByValue(dto.getPayType())) {
            case AMOUNT:
                if(ObjectUtil.isEmpty(dto.getPayPwd())){
                    throw new RuntimeException("支付密码不能为空");
                }
                User user = this.userService.selectUserById(dto.getUserId());
                if(ObjectUtil.isEmpty(user.getPayPwd())){
                   throw new RuntimeException("获取不到当前用户支付密码,请先设置");
                }
                return AjaxResult.success();
            case WXPAY:
                Map <String, String> app = this.wxPayUtils.payPlaceOrder(dto.getTradeNo(), dto.getFlowTypeEnum(),
                        dto.getAccount().multiply(new BigDecimal(100)), dto.getIp(), DateUtils.getNowDate(), "APP",
                        dto.getOpenid(), dto.getUserId(), dto.getUserName()
                );
                return AjaxResult.success(app);
            case WXPAY_JSPI_PAY:
                Map <String, String> app1 = this.wxPayUtils.payPlaceOrder(dto.getTradeNo(), dto.getFlowTypeEnum(),
                        dto.getAccount().multiply(new BigDecimal(100)), dto.getIp(), DateUtils.getNowDate(), "JSAPI",
                        dto.getOpenid(), dto.getUserId(), dto.getUserName()
                );
                return AjaxResult.success(app1);
            case ALIPAY:
                String pay = this.alipayUtils.pay(dto.getTradeNo(), dto.getFlowTypeEnum().getValue(),
                        dto.getAccount() + "", DateUtils.getNowDate(), dto.getUserId(), dto.getUserName()
                );
                return AjaxResult.success("获取支付成功", pay);
            case LS_ALIPAY_H5_PAY:
                String alipay = this.leshuaPayUtil.alipay(dto.getTradeNo(), dto.getAccount().multiply(new BigDecimal(100)).intValue(),
                        DateUtils.getNowDate(), dto.getUserId(), dto.getUserName()
                );
                return AjaxResult.success("获取支付成功", alipay);
            case LS_WX_JSAPI_PAY:
                Map <String, Object> objectMap = this.leshuaPayUtil.wxJSPI(dto.getTradeNo(),
                        dto.getAccount().multiply(new BigDecimal(100)).intValue(), DateUtils.getNowDate(),
                        dto.getUserId(), dto.getUserName(), dto.getOpenid()
                );
                return AjaxResult.success(objectMap);
            case LS_WX_XCX_PAY:
                Map <String, Object> wxxcxpay = this.leshuaPayUtil.wxxcxpay(dto.getTradeNo(),
                        dto.getAccount().multiply(new BigDecimal(100)).intValue(), DateUtils.getNowDate(),
                        dto.getUserId(), dto.getUserName(), dto.getOpenid()
                );
                return AjaxResult.success(wxxcxpay);
            default:
                throw new RuntimeException("支付类型错误");
        }
    }
}
