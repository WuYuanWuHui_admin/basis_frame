/*
 * ...
 */

package com.ruoyi.project.common;

import lombok.Data;

import java.io.Serializable;

/**
 * @author 挺好的 2023年11月24日 上午 11:02
 */
@Data
public class SmsUpDto implements Serializable {

    /**
     *手机号码
     */
    private String phone_number;
    /**
     *发送时间
     */

    private String   send_time;
    /**
     *发送内容 。
     */
    private String  content;
    /**
     *。签名信息
     */
    private String  sign_name;
    /**
     *。上行短信扩展号码，系统后台自动生成，不支持自定义传入。
     */
    private String  dest_code;
    /**
     *。序列号。
     */
    private String    sequence_id;
}
