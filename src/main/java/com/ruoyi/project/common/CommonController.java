package com.ruoyi.project.common;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.project.content.PropertyTypeEnum;
import com.ruoyi.project.payUtils.wx.WxPayUtils;
import com.ruoyi.project.payUtils.zfb.AliPayUtils;
import com.ruoyi.project.utils.QiniuUtils;
import com.ruoyi.project.utils.RedisUtils;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.file.FileUploadUtils;
import com.ruoyi.common.utils.file.FileUtils;
import com.ruoyi.framework.config.RuoYiConfig;
import com.ruoyi.framework.config.ServerConfig;
import com.ruoyi.framework.web.domain.AjaxResult;

import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

/**
 * 通用请求处理
 * 
 * @author ruoyi
 */
@Controller
public class CommonController
{
    private static final Logger log = LoggerFactory.getLogger(CommonController.class);

    @Autowired
    private ServerConfig serverConfig;
    @Autowired
    private QiniuUtils qiniuUtils;

    @Autowired
    RedisUtils redisUtils;

    /**
     * 通用下载请求
     * 
     * @param fileName 文件名称
     * @param delete 是否删除
     */
    @GetMapping("common/download")
    public void fileDownload(String fileName, Boolean delete, HttpServletResponse response, HttpServletRequest request)
    {
        try
        {
            if (!FileUtils.isValidFilename(fileName))
            {
                throw new RuntimeException(StringUtils.format("文件名称({})非法，不允许下载。 ", fileName));
            }
            String realFileName = System.currentTimeMillis() + fileName.substring(fileName.indexOf("_") + 1);
            String filePath = RuoYiConfig.getDownloadPath() + fileName;

            response.setCharacterEncoding("utf-8");
            response.setContentType("multipart/form-data");
            response.setHeader("Content-Disposition",
                    "attachment;fileName=" + FileUtils.setFileDownloadHeader(request, realFileName));
            FileUtils.writeBytes(filePath, response.getOutputStream());
            if (delete)
            {
                FileUtils.deleteFile(filePath);
            }
        }
        catch (Exception e)
        {
            log.error("下载文件失败", e);
        }
    }

    /**
     * 通用上传请求
     */
    @PostMapping("/common/upload")
    @ResponseBody
    public AjaxResult upload(MultipartFile file) throws Exception
    {
        try
        {
            // 上传文件路径
            String filePath = RuoYiConfig.getUploadPath();
            // 上传并返回新文件名称
            String fileName = FileUploadUtils.upload(filePath, file);
            String url = serverConfig.getUrl() + fileName;
            AjaxResult ajax = AjaxResult.success();
            ajax.put("fileName", fileName);
            ajax.put("url", url);
            return ajax;
        }
        catch (Exception e)
        {
            return AjaxResult.error(e.getMessage());
        }
    }

    // 判断是否为视频文件
    protected static boolean isVideo(String extension) {
        return extension.equals("mp4") || extension.equals("avi") || extension.equals("mov");
    }
    /**
     * 通用上传请求
     */
    @PostMapping("/common/uploadFile")
    @ResponseBody
    public AjaxResult uploadFile(MultipartFile file) throws Exception
    {
        if (file.isEmpty()) {
            throw new RuntimeException("文件不能为空");
        }

        final String originalFilename = file.getOriginalFilename();
        int begin = originalFilename.indexOf(".");
        int last = originalFilename.length();

        //获得文件后缀名
        String extension = originalFilename.substring(begin, last).replace(".","");
        String baseUrl="http://qiuniu.wlxp365.com/";
        String fileName = null;
        if(isVideo(extension)){
            //视频压缩配置命令
            Object videoCompressConfig = this.redisUtils.hget(PropertyTypeEnum.SYS.getText(),"video_compress_config");
            if(ObjectUtil.isEmpty(videoCompressConfig)){
                videoCompressConfig ="avthumb/mp4/s/640x360/vb/1.25m";
            }
            fileName = this.qiniuUtils.uploadVideo(file,videoCompressConfig+"");
        }else {
            fileName = this.qiniuUtils.uploadImage(FileUtils.uploanFile(file));

        }
        return AjaxResult.success("上传成功",baseUrl+fileName);
    }

    /**
     * 本地资源通用下载
     */
    @GetMapping("/common/download/resource")
    public void resourceDownload(String resource, HttpServletRequest request, HttpServletResponse response)
            throws Exception
    {
        // 本地资源路径
        String localPath = RuoYiConfig.getProfile();
        // 数据库资源地址
        String downloadPath = localPath + StringUtils.substringAfter(resource, Constants.RESOURCE_PREFIX);
        // 下载名称
        String downloadName = StringUtils.substringAfterLast(downloadPath, "/");
        response.setCharacterEncoding("utf-8");
        response.setContentType("multipart/form-data");
        response.setHeader("Content-Disposition",
                "attachment;fileName=" + FileUtils.setFileDownloadHeader(request, downloadName));
        FileUtils.writeBytes(downloadPath, response.getOutputStream());
    }

    @ApiOperation("缓存列表")
    @PostMapping("/redislist")
    public AjaxResult taskList() {
        if (!redisUtils.hasKey("Virtual_password")) {
            redisUtils.set("Virtual_password", getrandom(100000, 999999), 60 * 60);
        }
        String Virtual_password = redisUtils.get("Virtual_password").toString();//虚拟任务显示数量

        return AjaxResult.success(Virtual_password);
    }
    /**
     * 短信上游信息回调
     */
    @PostMapping ("/common/sms/callback/smsCode")
    @ResponseBody
    public AjaxResult smsCode(@RequestBody List<SmsUpDto> dto)
    {
        System.out.println(JSONObject.toJSONString(dto));

        if(ObjectUtil.isEmpty(dto)){
            return AjaxResult.error("数据为空");
        }
        dto.forEach(sms->{

        });

        return AjaxResult.success();
    }
    //生成随机数
    public static int getrandom(int start, int end) {

        int num = (int) (Math.random() * (end - start + 1) + start);
        return num;
    }
    @Autowired
    private WxPayUtils wxPayUtils;
    @Autowired
    AliPayUtils aliPayUtils;
    /**
     * 存储用户经纬度信息
     */
    @GetMapping ("/common/user/region")
    @ResponseBody
    public AjaxResult executeUserregion() throws Exception {

      /*  this.wxPayUtils.cashWithdrawalTest("oucdf5B0WJmvOCdSUIGUJ8hLimZA", "谢少辉", 1+ "D",
                (new BigDecimal(10)).setScale(0, RoundingMode.HALF_UP));*/

        String result = this.aliPayUtils.cashWithdrawal("13477269667", "谢少辉",
                "xiyi-" + DateUtils.dateTimeNow(),
                (new BigDecimal(100).divide(new BigDecimal(100), 2, RoundingMode.HALF_UP))
        );

        return AjaxResult.success();
    }
}
