package com.ruoyi.project.common.listener;

import com.ruoyi.common.constant.SystemContstant;
import com.ruoyi.project.utils.RedisUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.listener.KeyExpirationEventMessageListener;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class RedisKeyExpirationListener extends KeyExpirationEventMessageListener {

    private static final Logger log = LoggerFactory.getLogger(RedisKeyExpirationListener.class);

    @Autowired
    private RedisUtils redisCache;



    //存放重试次数
    private static Map <String, Integer> retryMap = new HashMap <>();

    public RedisKeyExpirationListener (RedisMessageListenerContainer listenerContainer) {
        super(listenerContainer);
    }

    /**
     * 处理自动确认任务
     *
     * @param key
     */
    private void doAutoConfirmWork (String key) {
    }

    @Override
    public void onMessage (Message message, byte[] pattern) {
        String expiredKey = message.toString();

        this.doAutoConfirmWork(expiredKey);

        String[] s = expiredKey.split(":");
        if (s.length == 3 && SystemContstant.EXPIRE.equals(s[0])) {
            Object cacheMapValue = this.redisCache.hget(s[1], s[2]);
            if (cacheMapValue != null) {
                this.redisCache.hdel(s[1], s[2]);

        }
    }}
}
