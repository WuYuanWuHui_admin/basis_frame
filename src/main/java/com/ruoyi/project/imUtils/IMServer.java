package com.ruoyi.project.imUtils;

public interface IMServer {

    /**
     * 注册
     * @param userName
     * @param userId
     * @param avatar
     * @return
     * @throws Exception
     */
    String registerUsers(String userName,String userId,String avatar)  throws Exception ;

    void updateUsers(String userName, String nickName, String avatar) throws Exception;
}
