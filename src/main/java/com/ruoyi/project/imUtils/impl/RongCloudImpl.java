package com.ruoyi.project.imUtils.impl;

import com.ruoyi.project.imUtils.IMServer;
import io.rong.RongCloud;
import io.rong.methods.user.User;
import io.rong.models.response.TokenResult;
import io.rong.models.user.UserModel;
import org.springframework.stereotype.Service;


@Service ("rongCloud")
public class RongCloudImpl implements IMServer {

    String appKey = "pvxdm17jpezkr";

    String appSecret = "i4MiDbdR732BST";

    RongCloud rongCloud = RongCloud.getInstance(this.appKey, this.appSecret);

    User user = this.rongCloud.user;

    /**
     * 注册返回token
     *
     * @param userName
     * @param userId
     * @param avatar
     *
     * @return
     *
     * @throws Exception
     */
    @Override
    public String registerUsers (String userName, String userId, String avatar) throws Exception {
        UserModel userModel = new UserModel().setId(userId).setName(userName).setPortrait(avatar);
        TokenResult result = this.user.register(userModel);
        return result.toString();
    }

    @Override
    public void updateUsers (String userName, String nickName, String avatar) throws Exception {

    }


}
