package com.ruoyi.project.third.numericalPulse;
import lombok.extern.slf4j.Slf4j;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * 数脉科技
 */
@Component
@Slf4j
public class NumericalPulseUtil {

    @Value("${thord.shumai.appCode}")
    private String appCode;

    @Value("${thord.shumai.shuMaiUrl}")
    private String shuMaiUrl;

    public static void main(String[] args) throws IOException {
        String url = "https://idcardv2.shumaidata.com/new-idcard";
        String appCode = "5e02a7e731264e3bbaa1292bba4e6165";

        Map<String, String> params = new HashMap<>();
        params.put("idcard", "110101199001019618");
        params.put("name", "谢少辉");

        String result = get(appCode, url, params);
        System.out.println(result);
    }

    /**
     * 用到的HTTP工具包：okhttp 3.13.1
     * <dependency>
     * <groupId>com.squareup.okhttp3</groupId>
     * <artifactId>okhttp</artifactId>
     * <version>3.13.1</version>
     * </dependency>
     */
    public  String getTwoElementsAuth(Map<String, String> params) throws IOException {
        String url = shuMaiUrl + buildRequestUrl(params);
        OkHttpClient client = new OkHttpClient.Builder().build();
        Request request = new Request.Builder().url(url).addHeader("Authorization", "APPCODE " + appCode).build();
        Response response = client.newCall(request).execute();
        log.info("返回状态码" + response.code() + ",message:" + response.message());
        String result = response.body().string();
        return result;
    }

    /**
     * 用到的HTTP工具包：okhttp 3.13.1
     * <dependency>
     * <groupId>com.squareup.okhttp3</groupId>
     * <artifactId>okhttp</artifactId>
     * <version>3.13.1</version>
     * </dependency>
     */
    public static String get(String appCode, String url, Map<String, String> params) throws IOException {
        url = url + buildRequestUrl(params);
        OkHttpClient client = new OkHttpClient.Builder().build();
        Request request = new Request.Builder().url(url).addHeader("Authorization", "APPCODE " + appCode).build();
        Response response = client.newCall(request).execute();
        log.info("返回状态码" + response.code() + ",message:" + response.message());
        String result = response.body().string();
        return result;
    }

    public static String buildRequestUrl(Map<String, String> params) {
        StringBuilder url = new StringBuilder("?");
        Iterator<String> it = params.keySet().iterator();
        while (it.hasNext()) {
            String key = it.next();
            url.append(key).append("=").append(params.get(key)).append("&");
        }
        return url.toString().substring(0, url.length() - 1);
    }
}
