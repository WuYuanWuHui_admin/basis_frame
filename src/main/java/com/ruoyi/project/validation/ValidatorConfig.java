/*
 * ...
 */

package com.ruoyi.project.validation;

import org.hibernate.validator.HibernateValidator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

/**
 * 客户端参数校验配置
 *
 * @author 挺好的 2023年02月10日 15:00
 */
@Configuration
public class ValidatorConfig {

    /**
     * 校验模式，快速失败
     */
    private static final String VALIDATOR_FAIL_FAST_PROPERTY = "hibernate.validator.fail_fast";

    private static final String TRUE_ATTRIBUTE_VALUE = "true";

    /**
     * 配置校验器，使用快速失败模式
     *
     * @return
     */
    @Bean
    public Validator validator () {
        ValidatorFactory validatorFactory = Validation.byProvider(HibernateValidator.class).configure()
                .addProperty(VALIDATOR_FAIL_FAST_PROPERTY, TRUE_ATTRIBUTE_VALUE).buildValidatorFactory();
        return validatorFactory.getValidator();
    }
}