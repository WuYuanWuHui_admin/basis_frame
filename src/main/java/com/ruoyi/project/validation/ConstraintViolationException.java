package com.ruoyi.project.validation;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 约束违规异常
 *
 * @author 挺好的 2023年02月10日 15:00
 */
@Data
@EqualsAndHashCode (callSuper = false)
public class ConstraintViolationException extends ValidationException {

    private static final long serialVersionUID = 1041019591794368521L;

    /**
     * 构造表单校验错误信息
     *
     * @param message
     *         消息
     */
    public ConstraintViolationException (String message) {
        super(message);
    }

    /**
     * 构造表单校验错误消息
     *
     * @param message
     *         错误消息
     * @param cause
     *         异常
     */
    public ConstraintViolationException (String message, Throwable cause) {
        super(message, cause);
    }
}
