package com.ruoyi.project.validation;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 验证异常，主要用于表单校验抛出异常、逻辑校验抛出异常
 * <p>
 * 这个异常， 一个是基于表单校验的异常，
 * <p>
 * 一个是基于逻辑校验的异常
 *
 * @author 挺好的 2023年02月10日 15:00
 */
@Data
@EqualsAndHashCode (callSuper = false)
public class ValidationException extends RuntimeException {

    private static final long serialVersionUID = -989724002366917409L;

    /**
     * 国际化消息占位
     */
    private Object[] args;

    public ValidationException () {
        super();
    }

    public ValidationException (String message, Throwable cause) {
        super(message, cause);
    }

    public ValidationException (String message) {
        super(message);
    }
}
