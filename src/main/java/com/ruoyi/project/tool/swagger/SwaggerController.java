package com.ruoyi.project.tool.swagger;

import com.ruoyi.framework.web.controller.BaseController;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * swagger 接口
 *
 * @author ruoyi
 */
@Controller
@RequestMapping ("/tool/swagger")
public class SwaggerController extends BaseController {

    /**
     * 关闭swagger配置
     *
     * @return
     */
    @RequiresPermissions ("tool:swagger:view")
    @GetMapping ()
//    @ResponseBody
    public String index () {

//        return "swagger已关闭";
                return redirect("/swagger-ui.html");
    }
}
