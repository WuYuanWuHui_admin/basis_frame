package com.ruoyi.project.content;

public enum WebSocketMsgTypeEnum {
    /**
     * websocket消息类型
     */
    HEARTBEAT(0,"心跳"),
    // 消息类型: 聊天
    SEND_MSG(1,"发送消息"),
    RECEIPT_MSG(2,"离线消息回执"),
    // 消息类型: 聊天
    MAPPOINT_MSG(3, "地图点位消息"),
    // 消息类型: 聊天
    IMAGE_MSG(4, "图片消息"),
    OFFLIINE(5, "下线通知"),
    // 消息类型: 聊天
    TASK_CARD_MSG(6, "任务卡片消息"),
    INV_TASK_MSG(7, "邀请任务提醒"),
    TASK_STATUS_CHANGE(8, "订单状态变更, APP强刷状态")
    ;

    //类型
    private Integer value;
    //内容
    private String text;


    WebSocketMsgTypeEnum(Integer value, String text) {
        this.value = value;
        this.text = text;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }


    public static WebSocketMsgTypeEnum findByValue(Integer value){
        if (null==value){
            return null;
        }
        for (WebSocketMsgTypeEnum e : WebSocketMsgTypeEnum.values()){
            if (value.intValue()==e.getValue().intValue()){
                return e;
            }
        }
        return null;
    }



}
