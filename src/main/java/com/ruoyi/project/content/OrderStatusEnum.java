package com.ruoyi.project.content;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public enum OrderStatusEnum {
    /**
     * 订单状态
     */
    CANCEL(0,"取消","default"),
    NO_CHECK(1,"待审核","warning"),
    NO_START(2,"待开工","info"),
    NO_END(3,"待收工","primary"),
    NO_SETTLEMENT(4,"待结算","danger"),
    FINISH(5,"完成","default"),
    NO_CONFIRM(7,"待确认","warning"),
    NO_START_CONFIRM(8,"开工确认","info"),
    NO_END_CONFIRM(9,"收工确认","info"),
    ;


    private Integer value;
    private String text;
    private String listClass;

    OrderStatusEnum(Integer value, String text, String listClass) {
        this.value = value;
        this.text = text;
        this.listClass = listClass;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getListClass() {
        return listClass;
    }

    public void setListClass(String listClass) {
        this.listClass = listClass;
    }

    public static OrderStatusEnum findByValue(Integer value){
        if (null==value){
            return null;
        }
        for (OrderStatusEnum e : OrderStatusEnum.values()){
            if (value.equals(e.getValue())){
                return e;
            }
        }
        return null;
    }
    
    public static List<Map> getAllEnum(){
        List<Map> result=new LinkedList<>();
        for (OrderStatusEnum e : OrderStatusEnum.values()){
            Map<String,Object> map=new HashMap<>();
            map.put("dictValue",e.getValue());
            map.put("dictLabel",e.getText());
            map.put("listClass",e.getListClass());
            result.add(map);
        }
        return result;
    }
}
