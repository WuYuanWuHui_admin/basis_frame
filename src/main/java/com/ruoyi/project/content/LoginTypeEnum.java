package com.ruoyi.project.content;


public enum LoginTypeEnum {
    /**
     * 登录类型类型
     */
	WXXCXLOGIN(3,"微信小程序"),
    APPLELOGIN(2,"苹果"),
    WXLOGIN(1,"微信"),
    MOBILE(0,"手机号")
    ;

    //类型
    private Integer value;
    //内容
    private String text;

    LoginTypeEnum(Integer value, String text) {
        this.value = value;
        this.text = text;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public static LoginTypeEnum findByValue(Integer value){
        if (null==value){
            return null;
        }
        for (LoginTypeEnum e : LoginTypeEnum.values()){
            if (value.intValue()==e.getValue().intValue()){
                return e;
            }
        }
        return null;
    }



}
