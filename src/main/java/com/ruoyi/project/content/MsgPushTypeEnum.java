package com.ruoyi.project.content;


import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public enum MsgPushTypeEnum {
    /**
     * 消息推送类型
     */
    //雇员申请了您的任务! 发送给雇主
    APPLY_TASK(0, "雇员申请任务", ""),
    //雇主同意了您的接单 发送给雇员
    AGREE_APPLY_TASK(1, "雇主同意接单", ""),
    //有一位雇员申请开工了 发送给雇主
    APPLY_START_TASK(2, "雇员申请开工", ""),
    //雇主同意了您的开工 发送给雇员
    AGREE_START_TASK(3, "雇主同意开工", ""),
    //有一位雇员收工了 发送给雇主
    END_TASK(4, "雇员收工", ""),
    //雇主邀请您接单了 发送给雇员
    INVITE_TASK(5, "邀请接单", ""),
    //有一位雇员接受了您的任务 发送给雇主
    ACCEPT_TASK(6, "接受邀请接单", ""),
    //雇主结算了您的订单! 发送给雇员
    SETTMENT_TASK(7, "结算任务", ""),
    //恭喜,您发布的零工审核通过! 发送给雇主
    CHECK_PASS_TASK(8, "任务审核通过", ""),
    //附近有刚发布的零工,请及时查看! 发送给雇员
    NEARBY_TASK(9, "附近零工通知", ""),
    //抱歉,您发布的零工审核未通过! 发送给雇主
    CHECK_FAILED_TASK(10, "任务审核未通过", ""),
    //雇主拒绝了您的接单 发送给雇员
    FAILED_APPLY_TASK(11, "雇主拒绝接单", ""),
    //雇主公告 12 发送给雇主
    EMPLOYER_NOTICE(12, "雇主公告", ""),
    //雇员公告 13 发送给雇员
    EMPLOYEE_NOTICE(13, "雇员公告", ""),
    //发送给所有用户
    ALL_NOTICE(14, "所有用户公告", ""),
    //雇员被开除
    APPKC_START_ORDER(15, "任务被取消", ""),
    //雇主结算订单
    APPKC_JIESHUAN_ORDER(16, "雇主结算订单", ""),
    //申诉审核结果通知
    EXPLAIN_APPLY_RESULT(20, "申诉审核结果通知", ""),
    COMPLAINT_RESULT(21, "意见反馈结果通知", ""),
    ORDER_REPORT_RESULT(22, "订单举报回复结果通知", ""),
    ORDER_CANCEL_RESULT(23, "订单取消回复结果通知", ""),
    USER_ACTIVITY_REMINDER(100, "用户活动提醒", ""),
    ;

    //类型
    private Integer value;
    //内容
    private String text;
    //样式
    private String listClass;

    MsgPushTypeEnum(Integer value, String text, String listClass) {
        this.value = value;
        this.text = text;
        this.listClass = listClass;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getListClass() {
        return listClass;
    }

    public void setListClass(String listClass) {
        this.listClass = listClass;
    }

    public static MsgPushTypeEnum findByValue(Integer value) {
        if (null == value) {
            return null;
        }
        for (MsgPushTypeEnum e : MsgPushTypeEnum.values()) {
            if (value.intValue() == e.getValue().intValue()) {
                return e;
            }
        }
        return null;
    }

    public static List<Map> getAllEnum() {
        List<Map> result = new LinkedList<>();
        for (MsgPushTypeEnum e : MsgPushTypeEnum.values()) {
            Map<String, Object> map = new HashMap<>();
            map.put("dictValue", e.getValue());
            map.put("dictLabel", e.getText());
            map.put("listClass", e.getListClass());
            result.add(map);
        }
        return result;
    }


}
