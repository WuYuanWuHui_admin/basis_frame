package com.ruoyi.project.content;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * 流水类型
 */
public enum ScoreFlowTypeEnum {
    LATE(0, "迟到 ", "迟到", "info"), MISSED_APPOINTMENT(1, "爽约", "爽约", "info"),
    RESIGNED(2, "辞工", "辞工", "info"), FORCED_RESIGNATION(3, "强制辞工", "强制辞工", "info"),
    GAVE_NEGATIVE_FEEDBACK(4, "差评", "差评", "info"),
    CANCELED_AFTER_RECEIVING_THE_ORDER(5, "接单后取消", "接单后取消", "info"),
    CHECKED_IN_WITHOUT_TOOLING(6, "打卡无工装", "打卡无工装", "info"), LOW_RANGE(7, "每日恢复", "每日恢复", "info"),
    MEDIUM_RANGE(8, "每日恢复", "每日恢复", "info"), COMPLETE_BONUS_POINTS(9, "完成3单", "完成3单", "info"),
    FAVORABLE_COMMENT(10, "好评", "好评", "info"),
    IMMEDIATE_RECOVERY_AFTER_PURCHASE(11, "购买立即回暖", "购买立即回暖", "info"),
    USER_CLOCK_IN_NOT_FROCK(12, "打卡无工装", "打卡无工装", "info"),
    COMPLAINT(99, "被投诉", "被投诉", "info"),
    ;

    //类型
    private Integer value;

    //展示标签
    private String text;

    //备注
    private String remark;

    //样式
    private String listClass;

    ScoreFlowTypeEnum (Integer value, String text, String remark, String listClass) {
        this.value     = value;
        this.text      = text;
        this.remark    = remark;
        this.listClass = listClass;
    }

    public Integer getValue () {
        return this.value;
    }

    public void setValue (Integer value) {
        this.value = value;
    }

    public String getText () {
        return this.text;
    }

    public void setText (String text) {
        this.text = text;
    }

    public String getListClass () {
        return this.listClass;
    }

    public void setListClass (String listClass) {
        this.listClass = listClass;
    }

    public String getRemark () {
        return this.remark;
    }

    public void setRemark (String remark) {
        this.remark = remark;
    }

    public static ScoreFlowTypeEnum findByValue (Integer value) {
        if (null == value) {
            return null;
        }
        for (ScoreFlowTypeEnum e : ScoreFlowTypeEnum.values()) {
            if (value.equals(e.getValue())) {
                return e;
            }
        }
        return null;
    }

    public static List <Map> getAllEnum () {
        List <Map> result = new LinkedList <>();
        for (ScoreFlowTypeEnum e : ScoreFlowTypeEnum.values()) {
            Map <String, Object> map = new HashMap <>();
            map.put("dictValue", e.getValue());
            map.put("dictLabel", e.getText());
            map.put("remark", e.getRemark());
            map.put("listClass", e.getListClass());
            result.add(map);
        }
        return result;
    }

    public static List <Map> getAllEnumzj () {
        List <Map> result = new LinkedList <>();
        for (ScoreFlowTypeEnum e : ScoreFlowTypeEnum.values()) {
            if (e.getValue() < 50 || e.getValue() >= 100) {
                Map <String, Object> map = new HashMap <>();
                map.put("dictValue", e.getValue());
                map.put("dictLabel", e.getText());
                map.put("remark", e.getRemark());
                map.put("listClass", e.getListClass());
                result.add(map);
            }

        }
        return result;
    }
}
