package com.ruoyi.project.content;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * 配置类型
 */
public enum PropertyTypeEnum {
    /**
     * 系统配置
     */
    SYS(1,"dlg_sys","系统配置","info"),
    /**
     * 热门搜索
     */
    HOT_SEARCH(2,"dlg_hot_search","热门搜索","danger"),
    /**
     * 意见类型
     */
    OPINION_TYPE(3,"dlg_opinion_type","意见类型","warning"),
    /**
     * 街道类型
     */
    STREET(4,"dlg_street","街道","success"),
    /**
     * 期望类型
     */
    EXPECT_TYPE(6,"dlg_expect_type","期望类型","success"),
    /**
     * 期望工种
     */
    EXPECT_WORK_TYPE(7,"dlg_expect_work_type","期望工种","success"),
    /**
     * 期望接单时间
     */
    EXPECT_WORKTIME(8,"dlg_expect_workTime","期望接单时间","success"),
    /**
     * 热门工作名称
     */
    HOT_TASK_NAME(9,"dlg_hot_task_name","热门工作名称","warning"),
    /**
     * 虚拟电话
     */
    VIRTUAL_PHONE(10,"dlg_virtual_phone","虚拟电话","info"),
    /**
     * 惩罚等级天数押金
     */
    PENALTY_LEVEL(11,"dlg_penalty_level","惩罚等级配置","warning"),
    /**
     * 区域配置
     */
    REGION_CONFIG(12,"region_config","区域配置","warning");


    private Integer value;
    private String text;
    private String remark;
    private String listClass;

    PropertyTypeEnum(Integer value, String text, String remark,String listClass) {
        this.value = value;
        this.text = text;
        this.remark = remark;
        this.listClass = listClass;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getListClass() {
        return listClass;
    }

    public void setListClass(String listClass) {
        this.listClass = listClass;
    }

    public static PropertyTypeEnum findByValue(Integer value){
        if (null==value){
            return null;
        }
        for (PropertyTypeEnum e : PropertyTypeEnum.values()){
            if (value.intValue()==e.getValue().intValue()){
                return e;
            }
        }
        return null;
    }

    public static List<Map> getAllEnum(){
        List<Map> result=new LinkedList<>();
        for (PropertyTypeEnum e : PropertyTypeEnum.values()){
            Map<String,Object> map=new HashMap<>();
            map.put("dictValue",e.getValue());
            map.put("dictLabel",e.getRemark());
            map.put("listClass",e.getListClass());
            result.add(map);
        }
        return result;
    }

}
