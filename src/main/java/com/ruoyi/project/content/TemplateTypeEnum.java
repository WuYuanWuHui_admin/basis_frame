package com.ruoyi.project.content;

public enum TemplateTypeEnum {

    /**
     * 公众号消息管理
     */
    msg_2(2,"aH5jcybQX9bTCH9FAQOzsKRp4J1QLfXR44-_GjQ7x8k"),
    msg_3(3,"aH5jcybQX9bTCH9FAQOzsGYm6vwJof1VwtmyGLPedEY"),
    msg_4(4,"aH5jcybQX9bTCH9FAQOzsJfIU6T-kiNhp78OZgAxrPI")
    ;


    private Integer value;
    private String text;

    TemplateTypeEnum(Integer value, String text) {
        this.value = value;
        this.text = text;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public static TemplateTypeEnum findByValue(Integer value){
        if (null==value){
            return null;
        }
        for (TemplateTypeEnum e : TemplateTypeEnum.values()){
            if (value.intValue()==e.getValue().intValue()){
                return e;
            }
        }
        return null;
    }



}
