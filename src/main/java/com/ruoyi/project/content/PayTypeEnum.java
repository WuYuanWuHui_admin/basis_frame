package com.ruoyi.project.content;


public enum PayTypeEnum {
    /**
     * 支付类型
     */
    AMOUNT(2, "余额"), ALIPAY(0, "支付宝"), WXPAY(1, "微信"), LS_WX_XCX_PAY(3, "乐刷微信小程序"),
    LS_WX_JSAPI_PAY(4, "乐刷微信JSAPI"), LS_ALIPAY_H5_PAY(5, "乐刷支付宝H5"), WXPAY_JSPI_PAY(6, "微信JSAPI"),
    NO_AMOUNT(-1, "不需要保证金支付");

    //类型
    private Integer value;

    //内容
    private String text;

    PayTypeEnum (Integer value, String text) {
        this.value = value;
        this.text  = text;
    }

    public Integer getValue () {
        return this.value;
    }

    public void setValue (Integer value) {
        this.value = value;
    }

    public String getText () {
        return this.text;
    }

    public void setText (String text) {
        this.text = text;
    }

    public static PayTypeEnum findByValue (Integer value) {
        if (null == value) {
            return null;
        }
        for (PayTypeEnum e : PayTypeEnum.values()) {
            if (value.intValue() == e.getValue().intValue()) {
                return e;
            }
        }
        return null;
    }


}
