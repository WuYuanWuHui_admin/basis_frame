package com.ruoyi.project.content;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * 提现类型
 */
public enum WithdrawalTypeEnum {

    /** 0支付宝 1微信 2 银行卡 */
    IMMEDIATE_PAYMENT(0,"支付宝","warning"),
    WX_PAYMENT(1,"微信","info"),
    RECHARGE_PAYMENT(2,"银行卡","success"),
    ;

    //类型
    private Integer value;
    //内容
    private String text;
    //样式
    private String listClass;

    WithdrawalTypeEnum(Integer value, String text,String listClass) {
        this.value = value;
        this.text = text;
        this.listClass = listClass;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getListClass() {
        return listClass;
    }

    public void setListClass(String listClass) {
        this.listClass = listClass;
    }

    public static WithdrawalTypeEnum findByValue(Integer value){
        if (null==value){
            return null;
        }
        for (WithdrawalTypeEnum e : WithdrawalTypeEnum.values()){
            if (value.intValue()==e.getValue().intValue()){
                return e;
            }
        }
        return null;
    }

    public static List<Map> getAllEnum(){
        List<Map> result=new LinkedList<>();
        for (WithdrawalTypeEnum e : WithdrawalTypeEnum.values()){
            Map<String,Object> map=new HashMap<>();
            map.put("dictValue",e.getValue());
            map.put("dictLabel",e.getText());
            map.put("listClass",e.getListClass());
            result.add(map);
        }
        return result;
    }


}
