package com.ruoyi.project.content;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public enum UserJobStatusEnum {
    /**
     * 用户工作状态
     */
    RESTING(0,"休息中","warning"),
    WORKING(1,"工作中","info"),
    RECEIVING (2,"接单中","danger"),
    WAITWORK(3, "待开工", "primary")
    ;


    private Integer value;
    private String text;
    private String listClass;

    UserJobStatusEnum(Integer value, String text, String listClass) {
        this.value = value;
        this.text = text;
        this.listClass = listClass;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getListClass() {
        return listClass;
    }

    public void setListClass(String listClass) {
        this.listClass = listClass;
    }

    public static UserJobStatusEnum findByValue(Integer value){
        if (null==value){
            return null;
        }
        for (UserJobStatusEnum e : UserJobStatusEnum.values()){
            if (value.equals(e.getValue())){
                return e;
            }
        }
        return null;
    }
    
    public static List<Map> getAllEnum(){
        List<Map> result=new LinkedList<>();
        for (UserJobStatusEnum e : UserJobStatusEnum.values()){
            Map<String,Object> map=new HashMap<>();
            map.put("dictValue",e.getValue());
            map.put("dictLabel",e.getText());
            map.put("listClass",e.getListClass());
            result.add(map);
        }
        return result;
    }
}
