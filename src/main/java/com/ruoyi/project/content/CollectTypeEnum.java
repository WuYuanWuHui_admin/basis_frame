package com.ruoyi.project.content;


import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public enum CollectTypeEnum {
    /**
     * 收藏类型
     */
    COLLECT_EMPLOYER(0,"收藏雇主","info"),
    COLLECT_EMPLOYEE(1,"收藏雇员","warning"),
    COLLECT_TASK(2,"收藏零工","success"),
    ;

    //类型
    private Integer value;
    //内容
    private String text;
    //样式
    private  String listClass;

    CollectTypeEnum(Integer value, String text, String listClass) {
        this.value = value;
        this.text = text;
        this.listClass=listClass;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getListClass() {
        return listClass;
    }

    public void setListClass(String listClass) {
        this.listClass = listClass;
    }

    public static CollectTypeEnum findByValue(Integer value){
        if (null==value){
            return null;
        }
        for (CollectTypeEnum e : CollectTypeEnum.values()){
            if (value.intValue()==e.getValue().intValue()){
                return e;
            }
        }
        return null;
    }

    public static List<Map> getAllEnum(){
        List<Map> result=new LinkedList<>();
        for (FlowTypeEnum e : FlowTypeEnum.values()){
            Map<String,Object> map=new HashMap<>();
            map.put("dictValue",e.getValue());
            map.put("dictLabel",e.getText());
            map.put("listClass",e.getListClass());
            result.add(map);
        }
        return result;
    }



}
