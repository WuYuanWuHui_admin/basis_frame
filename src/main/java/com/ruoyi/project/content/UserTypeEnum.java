package com.ruoyi.project.content;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public enum UserTypeEnum {
    /**
     * 用户类型
     */
    SYS_USER(0,"系统用户","warning"),
    COMMON_USER(1,"普通用户","info"),
    ;


    private Integer value;
    private String text;
    private String listClass;

    UserTypeEnum(Integer value, String text,String listClass) {
        this.value = value;
        this.text = text;
        this.listClass = listClass;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getListClass() {
        return listClass;
    }

    public void setListClass(String listClass) {
        this.listClass = listClass;
    }

    public static UserTypeEnum findByValue(Integer value){
        if (null==value){
            return null;
        }
        for (UserTypeEnum e : UserTypeEnum.values()){
            if (value.equals(e.getValue())){
                return e;
            }
        }
        return null;
    }
    
    public static List<Map> getAllEnum(){
        List<Map> result=new LinkedList<>();
        for (UserTypeEnum e : UserTypeEnum.values()){
            Map<String,Object> map=new HashMap<>();
            map.put("dictValue",e.getValue());
            map.put("dictLabel",e.getText());
            map.put("listClass",e.getListClass());
            result.add(map);
        }
        return result;
    }
}
