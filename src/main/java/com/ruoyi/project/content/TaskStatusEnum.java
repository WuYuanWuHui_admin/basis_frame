package com.ruoyi.project.content;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public enum TaskStatusEnum {
    /**
     * 工作状态
     */
    NO_PAY(1,"待付款","warning"),
    NO_AUDIT(2,"审核中","info"),
    START_HIRING(3,"招募中","danger"),
    END_HIRING(4,"工作中","primary"),
    FINISH(5,"完成结算","success"),
    CANCEL(6,"取消任务","default"),
    TO_BE_SETTLED(7,"待结算","info")
    ;


    private Integer value;
    private String text;
    private String listClass;

    TaskStatusEnum(Integer value, String text, String listClass) {
        this.value = value;
        this.text = text;
        this.listClass = listClass;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getListClass() {
        return listClass;
    }

    public void setListClass(String listClass) {
        this.listClass = listClass;
    }

    public static TaskStatusEnum findByValue(Integer value){
        if (null==value){
            return null;
        }
        for (TaskStatusEnum e : TaskStatusEnum.values()){
            if (value.equals(e.getValue())){
                return e;
            }
        }
        return null;
    }
    
    public static List<Map> getAllEnum(){
        List<Map> result=new LinkedList<>();
        for (TaskStatusEnum e : TaskStatusEnum.values()){
            Map<String,Object> map=new HashMap<>();
            map.put("dictValue",e.getValue());
            map.put("dictLabel",e.getText());
            map.put("listClass",e.getListClass());
            result.add(map);
        }
        return result;
    }
}
