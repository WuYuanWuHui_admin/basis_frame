package com.ruoyi.project.content;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * 流水类型
 */
public enum FlowTypeEnum {
    /**
     * 流水类型以及样式
     */
    BALANCE_PAY_PENALTY(7, "余额支付罚金", "余额支付罚金", "warning",1),
    ALI_PAY_PENALTY(8, "支付罚金", "支付罚金", "success",1),
    WX_PAY_PENALTY(9, "支付罚金", "支付罚金", "danger",1),
    BALANCE_DEFAULT_ADD(10, "增加钱包余额", "管理员增加钱包金额", "info",0),
    BALANCE_DEFAULT_CUT(11, "减少钱包余额", "管理员减少钱包金额", "warning",1),
    BALANCE_DRAW(12, "提现", "提现转出", "warning",1),
    BALANCE_DRAW_REJECTED(13, "提现驳回", "回退提现金额", "info",0),
    BALANCE_PAY_BOND(14, "余额支付诚意金", "余额支付诚意金", "warning",1),
    WX_PAY_BOND(15, "支付诚意金", "支付诚意金", "danger",1),
    ALI_PAY_BOND(16, "支付诚意金", "支付诚意金", "success",1),
    BALANCE_REFUND_BOND(17, "诚意金退回到余额", "诚意金退回到余额", "info",0),
    WX_REFUND_BOND(18, "诚意金原返退回", "诚意金原返退回", "danger",0),
    ALI_REFUND_BOND(19, "诚意金原返退回", "诚意金原返退回", "warning",0),
    BALANCE_PAY_SETTLEMENT(20, "余额支付结算金", "余额支付结算金", "warning",1),
    WX_PAY_SETTLEMENT(21, "支付结算金", "支付结算金", "danger",1),
    ALI_PAY_SETTLEMENT(22, "支付结算金", "支付结算金", "success",1),
    FINISH_ORDER_COLLECTION(23, "完成订单收款", "完成订单收款", "danger",0),
    FINISH_ACTIVITY_REWARD(24, "完成活动奖励", "完成活动奖励", "success",0),
    WX_PAY_RECHARGE(25, "充值到余额", "充值到余额", "danger",0),
    ALI_PAY_RECHARGE(26, "充值到余额", "充值到余额", "success",0),
    BALANCE_PAY_SINGLE_SETTLEMENT(27, "余额支付单个任务结算金", "余额支付单个任务结算金", "warning",1),
    WX_PAY_SINGLE_SETTLEMENT(28, "支付单个任务结算金", "支付单个任务结算金", "danger",1),
    ALI_PAY_SINGLE_SETTLEMENT(29, "支付单个任务结算金", "支付单个任务结算金", "success",1),
    GROUP_DEDUCTION_BOND(30, "订单批量结算扣除诚意金", "订单批量结算扣除诚意金", "warning",1),
    SINGLE_DEDUCTION_BOND(31, "订单单个结算扣除诚意金", "订单单个结算扣除诚意金", "danger",1),
    FINISH_ORDER_SUBSIDY(32, "完成订单补贴", "完成订单补贴", "danger",0),
    BALANCE_REFUND_SERVER_BOND(33, "招聘费退回到余额", "招聘费退回到余额", "info",0),
    WX_REFUND_SERVER_BOND(34, "招聘费原返退回", "招聘费原返退回", "danger",0),
    ALI_REFUND_SERVER_BOND(35, "招聘费原返退回", "招聘费原返退回", "warning",0),
    BALANCE_PAY_SERVER_BOND(36, "余额支付招聘费", "余额支付招聘费", "warning",1),
    WX_PAY_SERVER_BOND(37, "支付招聘费", "支付招聘费", "danger",1),
    ALI_PAY_SERVER_BOND(38, "支付招聘费", "支付招聘费", "success",1),
    SHARE_ACTIVITY_Yaoqing(39, "完成邀请活动奖励", "完成邀请活动奖励", "success",0),
    BALANCE_SE_BOND(40, "被违约补贴", "被违约补贴", "warning",0),
    BALANCE_DE_BOND(41, "违约补贴", "违约补贴", "warning",1),
    WX_PAY_GOLD_MEDAL(42, "购买金牌", "购买金牌", "danger",1),
    ALI_PAY_GOLD_MEDAL(43, "购买金牌", "购买金牌", "success",1),
    BALANCE_PAY_GOLD_MEDAL(44, "余额购买金牌", "余额购买金牌", "success",1),
    ADMIN_ADD_INTEGRITY(51, "增加评分", "管理员增加评分", "info",0),
    ADMIN_CUT_INTEGRITY(52, "减少评分", "管理员减少评分", "warning",1),
    REGISTER_ADD_INTEGRITY(53, "注册增加评分", "注册增加评分", "info",0),
    CANCEL_ORDER_CUT_INTEGRITY(54, "取消订单减少评分", "取消订单减少评分", "warning",1),
    CANCEL_TASK_CUT_INTEGRITY(55, "取消任务减少评分", "取消任务减少评分", "warning",1),
    AUTH_ADD_INTEGRITY(56, "实名认证增加评分", "实名认证增加评分", "info",0),
    INVITE_REWARDS_INTEGRITY(57, "邀请奖励评分", "邀请奖励评分", "warning",0),
    EVALUATE_REWARDS_INTEGRITY(58, "评价奖励评分", "评价奖励评分", "warning",0),
    // FINISH_ACTIVITY_REWARD(59,"完成邀请活动奖励","完成活动奖励","success"),
    SHARE_ACTIVITY_REWARD(59, "分享活动奖励", "分享活动奖励", "success",0),
    ADMIN_CUT_INTEGRITY_CHIDAO(61, "迟到开工减少评分", "迟到开工减少评分", "warning",1),
    ADMIN_CUT_INTEGRITY_SHUANGYUE(62, "开工爽约减少评分", "开工爽约减少评分", "warning",1),
    ADMIN_CUT_INTEGRITY_KAICHU(63, "工作开除减少评分", "工作开除减少评分", "warning",1),
    ADMIN_CUT_INTEGRITY_BAOMING(64, "取消报名减少评分", "取消报名减少评分", "warning",1),
    ADMIN_CUT_INTEGRITY_PINJIA(65, "评价“很糟糕”减少评分", "评价“很糟糕”减少评分", "warning",1),
    ADMIN_CUT_INTEGRITY_JIESHUAN(66, "私下结算/交易减少评分", "私下结算/交易减少评分", "warning",1),
    ADMIN_CUT_INTEGRITY_DAIREN(67, "私下带人减少评分", "私下带人减少评分", "warning",1),
    ADMIN_CUT_INTEGRITY_XINGSHOU(68, "完成新手任务增加评分", "完成新手任务增加评分", "warning",0),
    ADMIN_CUT_INTEGRITY_XINXIRZ(69, "个人信息认证增加评分", "个人信息认证增加评分", "warning",0),
    WX_PAY_YUEKA_MEDAL(101, "购买-卡片", "购买-卡片", "danger",1),
    ALI_PAY_YUEKA_MEDAL(102, "购买-卡片", "购买-卡片", "success",1),
    BALANCE_PAY_YUEKA_MEDAL(103, "余额购买-卡片", "余额购买-卡片", "success",1),
    WX_PAY_RENSHU_MEDAL(104, "购买人数加量包", "购买人数加量包", "danger",1),
    ALI_PAY_RENSHU_MEDAL(105, "购买人数加量包", "购买人数加量包", "success",1),
    BALANCE_PAY_RENSHU_MEDAL(106, "余额购买人数加量包", "余额购买人数加量包", "success",1),
    BALANCE_PAY_ENVELOP_MEDAL(107, "新人专属红包", "新人专属红包", "success",0),
    BALANCE_PAY_BAN_MEDAL(108, "封禁后被二次封禁扣除保证金", "封禁后被二次封禁扣除保证金", "success",1),
    SHARE_ACTIVITY_MONTHPRICE(109, "阶梯邀请月度补发", "阶梯邀请月度补发", "success",0),
    LS_REFUND_SERVER_BOND(110, "原返退回", "原返退回", "success",0),
    LS_PAY_BOND(111, "乐刷支付诚意金", "乐刷支付诚意金", "success",1),
    RESIGNATION_COMPENSATION_REFUND(112, "辞工补偿退还", "辞工补偿退还", "info",0),
    ZHOU_CHONG_SINGLE_REWARD(113, "周冲单奖励", "周冲单奖励", "info",0),
    NEWCOMER_RED_ENVELOPE(114, "新人红包", "新人红包", "info",0),
    PAY_ENSURE_ACCOUNT(115, "支付保证金", "支付保证金", "info",1),
    REFUND_ENSURE_ACCOUNT(116, "退还保证金", "退还保证金", "info",0),
    PURCHASE_CLOTHING(117, "购买工装", "购买工装", "info",1),
    PAY_RECOVERY_AMOUNT(118, "购买立即回暖", "购买立即回暖", "info",1),
    PAY_RECOVERY_AMOUNT_ACCELERATE(119, "购买加速回暖", "购买加速回暖", "info",1),
    PAY_GOLD_MEDAL(120, "金牌之星", "金牌之星", "info",0),
    PURCHASE_CARD_DIVIDENDS(121,"购买直通卡分润","购买直通卡分润","info",0),
    PURCHASE_PACKAGE_PROFIT_SHARING(122,"购买加量包分润","购买加量包分润","info",0),
    RECRUITMENT_FEE_DISTRIBUTION(123,"招聘费分润","招聘费分润","info",0),
    WX_PURCHASE_CLOTHING(124, "购买工装", "购买工装", "danger",1),
    ALI_PURCHASE_CLOTHING(125, "购买工装", "购买工装", "success",1),
    WX_PAY_RECOVERY_AMOUNT(126, "购买立即回暖", "购买立即回暖", "danger",1),
    ALI_PAY_RECOVERY_AMOUNT(127, "购买立即回暖", "购买立即回暖", "success",1),
    WX_PAY_RECOVERY_AMOUNT_ACCELERATE(128, "购买加速回暖", "购买加速回暖", "danger",1),
    ALI_PAY_RECOVERY_AMOUNT_ACCELERATE(129, "购买加速回暖", "购买加速回暖", "success",1),
    COMPLETION_LOTTERY(130, "完单抽奖", "完单抽奖", "success",0),
    SYS_MEDAL(131, "系统赠送-卡片", "系统赠送-卡片", "success",1),
    REWARD_FOR_ODD_JOB_RECALL(132, "零工召回奖励", "零工召回奖励", "info",0),
    ;

    //类型
    private Integer value;

    //展示标签
    private String text;

    //备注
    private String remark;

    //样式
    private String listClass;
    //用户流水类型 0 收入 1 支出
    private Integer inOrOunt;

    FlowTypeEnum (Integer value, String text, String remark, String listClass,Integer inOrOunt) {
        this.value     = value;
        this.text      = text;
        this.remark    = remark;
        this.listClass = listClass;
        this.inOrOunt=inOrOunt;
    }

    public Integer getValue () {
        return this.value;
    }

    public void setValue (Integer value) {
        this.value = value;
    }

    public String getText () {
        return this.text;
    }

    public void setText (String text) {
        this.text = text;
    }

    public String getListClass () {
        return this.listClass;
    }

    public void setListClass (String listClass) {
        this.listClass = listClass;
    }

    public String getRemark () {
        return this.remark;
    }

    public void setRemark (String remark) {
        this.remark = remark;
    }

    public Integer getInOrOunt () {
        return inOrOunt;
    }

    public void setInOrOunt (Integer inOrOunt) {
        this.inOrOunt = inOrOunt;
    }

    public static FlowTypeEnum findByValue (Integer value) {
        if (null == value) {
            return null;
        }
        for (FlowTypeEnum e : FlowTypeEnum.values()) {
            if (value.equals(e.getValue())) {
                return e;
            }
        }
        return null;
    }


    public static List <Map> getAllEnum () {
        List <Map> result = new LinkedList <>();
        for (FlowTypeEnum e : FlowTypeEnum.values()) {
            Map <String, Object> map = new HashMap <>();
            map.put("dictValue", e.getValue());
            map.put("dictLabel", e.getText());
            map.put("remark", e.getRemark());
            map.put("listClass", e.getListClass());
            map.put("inOrOunt", e.getInOrOunt());
            result.add(map);
        }
        return result;
    }

    public static List <Map> getAllEnumzj () {
        List <Map> result = new LinkedList <>();
        for (FlowTypeEnum e : FlowTypeEnum.values()) {
            if (e.getValue() < 50 || e.getValue() >= 100) {
                Map <String, Object> map = new HashMap <>();
                map.put("dictValue", e.getValue());
                map.put("dictLabel", e.getText());
                map.put("remark", e.getRemark());
                map.put("listClass", e.getListClass());
                map.put("inOrOunt", e.getInOrOunt());
                result.add(map);
            }

        }
        return result;
    }
}
