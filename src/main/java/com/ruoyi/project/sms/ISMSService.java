package com.ruoyi.project.sms;

import java.util.List;

/**
 * 短信服务
 *
 * @author Administrator
 */
public interface ISMSService {

    /**
     * 发送验证码
     *
     * @param mobile
     * @param length
     *
     * @return
     *
     * @throws Exception
     */
    String sendCode (String mobile, Integer length) throws Exception;

    /**
     * 批量发送消息
     *
     * @param mobiles
     * @param msg
     *
     * @return
     *
     * @throws Exception
     */
    public String sendGroupMsg (String[] mobiles, String[] msg) throws Exception;

    /**
     * 校验手机验证码 验证失败返回true
     *
     * @param mobile
     * @param checkCode
     *
     * @return
     */
    boolean checkSmsCode (String mobile, String checkCode);

    /**
     * 获取随机验证码
     */
    String getRandomCode (int length);

    /**
     * 发送邀请信息
     */
    String sendYaoqing (String mobile, String username) throws Exception;

    String sendservice (String mobile, String TemplateCode) throws Exception;

    String getMobile (String mobile) throws Exception;

    /**
     * 解密数据
     *
     * @param str
     *         待解密的字符串
     *
     * @return
     */
    String decrypt (String str) throws Exception;

    /**
     * 发送营销短信
     *
     * @param phones
     *         手机号码
     * @param template
     *         短信模板
     */
    void sendMarketingMsg (List <String> phones, String template);

    void sendMsgByCode(String mobile,String code) throws Exception;

    Boolean sendMsgByCodeResult(String mobile,String code) throws Exception;

    //带参数
    void sendMsgByParameters(String mobile,String code,String templateParam) throws Exception;
}
