package com.ruoyi.project.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Data
public class PosterMakerConfig {

    @Value("${file.post.backgroundPath}")
    private String backgroundPath;
    @Value("${file.post.erweimaUrl}")
    private String erweimaUrl;
    @Value("${file.post.outPath}")
    private String outPath;


}
