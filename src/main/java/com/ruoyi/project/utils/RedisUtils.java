package com.ruoyi.project.utils;

import com.ruoyi.common.constant.SystemContstant;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.Point;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.data.redis.core.script.RedisScript;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@Component
@Slf4j
public class RedisUtils {

    private static final Logger log = LoggerFactory.getLogger(RedisUtils.class);

    /**
     * json序列化方式
     */
    private static GenericJackson2JsonRedisSerializer redisObjectSerializer = new GenericJackson2JsonRedisSerializer();


    @Autowired
    private RedisTemplate <String, Object> redisTemplate;


    // =============================common============================

    /**
     * 指定缓存失效时间
     *
     * @param key
     *         键
     * @param time
     *         时间(秒)
     *
     * @return
     */
    public boolean expire (String key, long time) {
        return this.expire(key, time, TimeUnit.SECONDS);
    }

    public boolean expire (String key, long time, TimeUnit unit) {
        try {
            if (time > 0) {
                this.redisTemplate.expire(key, time, unit);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 根据key 获取过期时间
     *
     * @param key
     *         键 不能为null
     *
     * @return 时间(秒) 返回0代表为永久有效
     */
    public long getExpire (String key) {

        return this.redisTemplate.getExpire(key, TimeUnit.SECONDS);
    }


    /**
     * 判断key是否存在
     *
     * @param key
     *         键
     *
     * @return true 存在 false不存在
     */
    public boolean hasKey (String key) {
        try {
            return this.redisTemplate.hasKey(key);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 删除缓存
     *
     * @param key
     *         可以传一个值 或多个
     */
    @SuppressWarnings ("unchecked")
    public void del (String... key) {
        if (key != null && key.length > 0) {
            if (key.length == 1) {
                this.redisTemplate.delete(key[0]);
            } else {
                this.redisTemplate.delete(CollectionUtils.arrayToList(key));
            }
        }
    }
    // ============================String=============================

    /**
     * 普通缓存获取
     *
     * @param key
     *         键
     *
     * @return 值
     */
    public String get (String key) {
        if (StringUtils.isEmpty(key)) {
            return null;
        }

        if (key.contains(SystemContstant.TOKEN)) {
            key = key.substring(SystemContstant.TOKEN.length());
        }

        Object value = this.redisTemplate.opsForValue().get(key);
        return value == null ? null : value.toString();
    }

    /**
     * 普通缓存获取
     *
     * @param key
     *         键
     *
     * @return 值
     */
    public Object getObjKey (String key) {
        if (StringUtils.isEmpty(key)) {
            return null;
        }

        if (key.contains(SystemContstant.TOKEN)) {
            key = key.substring(SystemContstant.TOKEN.length());
        }

        Object value = this.redisTemplate.opsForValue().get(key);
        return value;
    }

    /**
     * 老代码里面写了逻辑... 新开一个普通获取
     *
     * @param key
     *
     * @return
     */
    public String getval (String key) {
        if (StringUtils.isEmpty(key)) {
            return null;
        }
        Object value = this.redisTemplate.opsForValue().get(key);
        return value == null ? null : value.toString();
    }

    /**
     * 模糊查询key值
     *
     * @param key
     *
     * @return
     */
    public Set <String> getByVagueKey (String key) {
        if (StringUtils.isEmpty(key)) {
            return null;
        }
        Set <String> keys = this.redisTemplate.keys(key);
        return keys;
    }

    /**
     * 普通缓存放入
     *
     * @param key
     *         键
     * @param value
     *         值
     *
     * @return true成功 false失败
     */
    public boolean set (String key, Object value) {
        try {
            this.redisTemplate.opsForValue().set(key, value);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 普通缓存放入并设置时间
     *
     * @param key
     *         键
     * @param value
     *         值
     * @param time
     *         时间(秒) time要大于0 如果time小于等于0 将设置无限期
     *
     * @return true成功 false 失败
     */
    public boolean set (String key, Object value, long time) {
        try {
            if (time > 0) {
                this.redisTemplate.opsForValue().set(key, value, time, TimeUnit.SECONDS);
            } else {
                this.set(key, value);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    /**
     * 递增
     * @param key 键
     * @param delta 要增加几(大于0)
     * @return
     */
    //    public long incr(String key, long delta) {
    //        if (delta < 0) {
    //            throw new RuntimeException("递增因子必须大于0");
    //        }
    //        return redisTemplate.opsForValue().increment(key, delta);
    //    }

    /**
     * 递减
     *
     * @param key
     *         键
     * @param delta
     *         要减少几(小于0)
     *
     * @return
     */
    public long decr (String key, long delta) {
        if (delta < 0) {
            throw new RuntimeException("递减因子必须大于0");
        }
        return this.redisTemplate.opsForValue().decrement(key, delta);
    }

    public long incr (String key, Integer number) {
        synchronized (key) {
            long incr = this.redisTemplate.opsForValue().increment(key, 1);
            log.debug("原子性:" + key + "****" + incr);
            if (incr > number) {
                this.set(key, number, SystemContstant.TWO_DAY_SECONDS);
            }
            return incr;
        }
    }

    public long incr (String key) {
        synchronized (key) {
            long incr = this.redisTemplate.opsForValue().increment(key, 1);
            log.debug("原子性:" + key + "***" + incr);
            return incr;
        }
    }


    public long decr (String key) {
        synchronized (key) {
            long decr = this.decr(key, 1);
            log.debug("原子性:" + key + "***" + decr);
            if (decr < 0) {
                this.set(key, 0, SystemContstant.TWO_DAY_SECONDS);
            }
            return decr;
        }
    }
    // ================================Map=================================

    /**
     * HashGet
     *
     * @param key
     *         键 不能为null
     * @param item
     *         项 不能为null
     *
     * @return 值
     */
    public Object hget (String key, String item) {
        try {
            return this.redisTemplate.opsForHash().get(key, item);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 获取hashKey对应的所有键值
     *
     * @param key
     *         键
     *
     * @return 对应的多个键值
     */
    public Map <Object, Object> hmget (String key) {
        try {
            return this.redisTemplate.opsForHash().entries(key);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * HashSet
     *
     * @param key
     *         键
     * @param map
     *         对应多个键值
     *
     * @return true 成功 false 失败
     */
    public boolean hmset (String key, Map <String, Object> map) {
        try {
            this.redisTemplate.opsForHash().putAll(key, map);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * HashSet 并设置时间
     *
     * @param key
     *         键
     * @param map
     *         对应多个键值
     * @param time
     *         时间(秒)
     *
     * @return true成功 false失败
     */
    public boolean hmset (String key, Map <String, Object> map, long time) {
        try {
            this.redisTemplate.opsForHash().putAll(key, map);
            if (time > 0) {
                this.expire(key, time);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 向一张hash表中放入数据,如果不存在将创建
     *
     * @param key
     *         键
     * @param item
     *         项
     * @param value
     *         值
     *
     * @return true 成功 false失败
     */
    public boolean hset (String key, String item, Object value) {
        try {
            this.redisTemplate.opsForHash().put(key, item, value);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 向一张hash表中放入数据,如果不存在将创建
     *
     * @param key
     *         键
     * @param item
     *         项
     * @param value
     *         值
     * @param time
     *         时间(秒) 注意:如果已存在的hash表有时间,这里将会替换原有的时间
     *
     * @return true 成功 false失败
     */
    public boolean hset (String key, String item, Object value, long time) {
        try {
            this.redisTemplate.opsForHash().put(key, item, value);
            if (time > 0) {
                this.expire(key, time);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 删除hash表中的值
     *
     * @param key
     *         键 不能为null
     * @param item
     *         项 可以使多个 不能为null
     */
    public void hdel (String key, Object... item) {
        this.redisTemplate.opsForHash().delete(key, item);
    }

    /**
     * 删除hash表中全部的值
     *
     * @param key
     *         键 不能为null
     */
    public void hdelAll (String key) {
        Map<Object, Object> entries = redisTemplate.opsForHash().entries(key);
        redisTemplate.opsForHash().entries(key).clear();
    }

    /**
     * 判断hash表中是否有该项的值
     *
     * @param key
     *         键 不能为null
     * @param item
     *         项 不能为null
     *
     * @return true 存在 false不存在
     */
    public boolean hHasKey (String key, String item) {
        return this.redisTemplate.opsForHash().hasKey(key, item);
    }

    /**
     * hash递增 如果不存在,就会创建一个 并把新增后的值返回
     *
     * @param key
     *         键
     * @param item
     *         项
     * @param by
     *         要增加几(大于0)
     *
     * @return
     */
    public double hincr (String key, String item, double by) {
        return this.redisTemplate.opsForHash().increment(key, item, by);
    }

    /**
     * hash递减
     *
     * @param key
     *         键
     * @param item
     *         项
     * @param by
     *         要减少记(小于0)
     *
     * @return
     */
    public double hdecr (String key, String item, double by) {
        return this.redisTemplate.opsForHash().increment(key, item, -by);
    }
    // ============================set=============================

    /**
     * 根据key获取Set中的所有值
     *
     * @param key
     *         键
     *
     * @return
     */
    public Set <Object> sGet (String key) {
        try {
            return this.redisTemplate.opsForSet().members(key);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 根据value从一个set中查询,是否存在
     *
     * @param key
     *         键
     * @param value
     *         值
     *
     * @return true 存在 false不存在
     */
    public boolean sHasKey (String key, Object value) {
        try {
            return this.redisTemplate.opsForSet().isMember(key, value);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 将数据放入set缓存
     *
     * @param key
     *         键
     * @param values
     *         值 可以是多个
     *
     * @return 成功个数
     */
    public long sSet (String key, Object... values) {
        try {
            return this.redisTemplate.opsForSet().add(key, values);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * 将set数据放入缓存
     *
     * @param key
     *         键
     * @param time
     *         时间(秒)
     * @param values
     *         值 可以是多个
     *
     * @return 成功个数
     */
    public long sSetAndTime (String key, long time, Object... values) {
        try {
            Long count = this.redisTemplate.opsForSet().add(key, values);
            if (time > 0) {
                this.expire(key, time);
            }
            return count;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * 获取set缓存的长度
     *
     * @param key
     *         键
     *
     * @return
     */
    public long sGetSetSize (String key) {
        try {
            return this.redisTemplate.opsForSet().size(key);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * 移除值为value的
     *
     * @param key
     *         键
     * @param values
     *         值 可以是多个
     *
     * @return 移除的个数
     */
    public long setRemove (String key, Object... values) {
        try {
            Long count = this.redisTemplate.opsForSet().remove(key, values);
            return count;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }
    // ===============================list=================================

    /**
     * 获取list缓存的内容
     *
     * @param key
     *         键
     * @param start
     *         开始
     * @param end
     *         结束 0 到 -1代表所有值
     *
     * @return
     */
    public List <Object> lGet (String key, long start, long end) {
        try {
            return this.redisTemplate.opsForList().range(key, start, end);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 获取list缓存的长度
     *
     * @param key
     *         键
     *
     * @return
     */
    public long lGetListSize (String key) {
        try {
            return this.redisTemplate.opsForList().size(key);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * 通过索引 获取list中的值
     *
     * @param key
     *         键
     * @param index
     *         索引 index>=0时， 0 表头，1 第二个元素，依次类推；index<0时，-1，表尾，-2倒数第二个元素，依次类推
     *
     * @return
     */
    public Object lGetIndex (String key, long index) {
        try {
            return this.redisTemplate.opsForList().index(key, index);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 将list放入缓存
     *
     * @param key
     *         键
     * @param value
     *         值
     *
     * @return
     */
    public boolean lSet (String key, Object value) {
        try {
            this.redisTemplate.opsForList().rightPush(key, value);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 将list放入缓存
     *
     * @param key
     *         键
     * @param value
     *         值
     * @param time
     *         时间(秒)
     *
     * @return
     */
    public boolean lSet (String key, Object value, long time, TimeUnit unit) {
        try {
            this.redisTemplate.opsForList().rightPush(key, value);
            this.expire(key, time, unit);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 将list放入缓存
     *
     * @param key
     *         键
     * @param value
     *         值
     *
     * @return
     */
    public boolean lSet (String key, List <Object> value) {
        try {
            this.redisTemplate.opsForList().rightPushAll(key, value);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 将list放入缓存
     *
     * @param key
     *         键
     * @param value
     *         值
     * @param time
     *         时间(秒)
     *
     * @return
     */
    public boolean lSet (String key, List <Object> value, long time) {
        try {
            this.redisTemplate.opsForList().rightPushAll(key, value);
            if (time > 0) {
                this.expire(key, time);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 根据索引修改list中的某条数据
     *
     * @param key
     *         键
     * @param index
     *         索引
     * @param value
     *         值
     *
     * @return
     */
    public boolean lUpdateIndex (String key, long index, Object value) {
        try {
            this.redisTemplate.opsForList().set(key, index, value);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 移除N个值为value
     *
     * @param key
     *         键
     * @param count
     *         移除多少个
     * @param value
     *         值
     *
     * @return 移除的个数
     */
    public long lRemove (String key, long count, Object value) {
        try {
            Long remove = this.redisTemplate.opsForList().remove(key, count, value);
            return remove;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * 发送订阅消息
     *
     * @param channel
     * @param msg
     */
    public void sendMsg (String channel, String msg) {
        this.redisTemplate.convertAndSend(channel, msg);
    }

    /**
     * 将对象经纬度传入到redis中
     *
     * @param lat
     *         纬度
     * @param lng
     *         经度
     * @param member
     *         二级key
     */
    public void setGEOMember (Double lat, Double lng, String member) {
        Point point = new Point(lng, lat);
        Long add = this.redisTemplate.opsForGeo().add(SystemContstant.DLG_GEO_KEY, point, member);
    }


    public List <Point> getGEOMember (String member) {
        List <Point> position = this.redisTemplate.opsForGeo().position(SystemContstant.DLG_GEO_KEY, member);
        return position;
    }

    /**
     * 计算对象之间的距离
     *
     * @param member1
     *         对象1
     * @param member2
     *         对象2
     */
    public Double getDistance (String member1, String member2) {
        try {
            Distance distance = this.redisTemplate.opsForGeo().distance(SystemContstant.DLG_GEO_KEY, member1, member2);
            /*  System.out.println(distance.getValue()+""+distance.getUnit());*/
            return distance == null ? 0.1 : distance.getValue();
        } catch (Exception e) {
            log.error("获取距离失败", e);
            return 0.1;
        }
    }

    /**
     * 删除当前距离对象
     *
     * @param member
     */
    public void delGEOMember (String member) {
        this.redisTemplate.opsForGeo().remove(SystemContstant.DLG_GEO_KEY, member);
    }

    //    /**
    //     * 删除当前距离对象
    //     *
    //     * @param member
    //     * @return
    //     */
    //    public List<Point> getGEOMember(String member) {
    //        return redisTemplate.opsForGeo().geoPos(DLG_GEO_KEY, member.substring(member.length() - 1, member.length()));
    //    }

    public void hset (String key, String hKey, Object value, long timeout, TimeUnit unit) {
        this.redisTemplate.opsForHash().put(key, hKey, value);
        this.setExpireListener(key, hKey, timeout, unit, 1);
    }

    public void setExpireListener (String key1, String key2, Long timeout, TimeUnit unit, int count) {
        String key = new StringBuilder().append(SystemContstant.EXPIRE).append(SystemContstant.COLON).append(key1)
                .append(SystemContstant.COLON).append(key2).toString();
        this.redisTemplate.opsForValue().set(key, 1, timeout, unit);
        if (this.redisTemplate.getExpire(key) <= 0) {
            if (count > 5) {
                return;
            }
            count++;
            this.setExpireListener(key1, key2, timeout, unit, count);
        }
    }

    // 锁名称
    public static final String LOCK_PREFIX = "redis_lock";

    /**
     * 最终加强分布式锁
     *
     * @param key
     *         key值
     *         <p>
     * @param lockTime
     *         锁时间  毫秒
     *
     * @return 是否获取到
     */
    /*public boolean lock (String key, int lockTime) {
        String lock = LOCK_PREFIX + key;
        // 利用lambda表达式
        return (Boolean) this.redisTemplate.execute((RedisCallback) connection -> {
            long expireAt = System.currentTimeMillis() + lockTime + 1;
            log.info("失效的时间是【{}】", String.valueOf(expireAt));
            Boolean acquire = connection.setNX(lock.getBytes(), String.valueOf(expireAt).getBytes());
            log.info("查看acquire========" + acquire);
            if (acquire) {
                return true;
            } else {
                byte[] value = connection.get(lock.getBytes());
                log.info("查看value========" + value);
                if (Objects.nonNull(value) && value.length > 0) {
                    long expireTime = Long.parseLong(new String(value));
                    log.info("查看expireTime========" + expireTime);
                    // 如果锁已经过期
                    if (expireTime < System.currentTimeMillis()) {
                        log.info("如果锁已经过期,重新加锁");
                        // 重新加锁，防止死锁
                        byte[] oldValue = connection.getSet(lock.getBytes(),
                                String.valueOf(System.currentTimeMillis() + lockTime + 1).getBytes()
                        );
                        log.info("查看oldValue========" + oldValue);
                        Boolean newLoing = Long.parseLong(new String(oldValue)) < System.currentTimeMillis();
                        log.info("查看Long.parseLong========" + newLoing);
                        return newLoing;
                    }
                }
            }
            return false;
        });
    }*/

    /**
     * 删除锁
     *
     * @param key
     */
    public void unLock (String key) {
        this.redisTemplate.delete(key);
    }

    /**
     * 普通缓存放入并设置时间 new
     *
     * @param key
     *         键
     * @param value
     *         值
     * @param time
     *         时间(秒) time要大于0 如果time小于等于0 将设置无限期
     *
     * @return true成功 false 失败
     */
    public boolean setPivotal (String key, Object value, long time) {
        try {
            if (time > 0) {

                this.redisTemplate.execute((RedisCallback <Long>) connection -> {
                    // redis info
                    byte[] values = redisObjectSerializer.serialize(value);
                    connection.set(key.getBytes(), values);
                    connection.expire(key.getBytes(), time);
                    connection.close();
                    return 1L;
                });

            } else {
                this.set(key, value);
            }
            return true;
        } catch (Exception e) {
            StackTraceElement stackTraceElement = e.getStackTrace()[0];
            log.error("set-" + stackTraceElement.getMethodName() + "--" + stackTraceElement.getLineNumber());
            return false;
        }
    }


    /**
     * 普通缓存放入
     *
     * @param key
     *         键
     * @param value
     *         值
     *
     * @return true成功 false失败
     */
    public boolean setKey (String key, Object value) {
        try {

            this.redisTemplate.execute((RedisCallback <Long>) connection -> {
                // redis info
                byte[] values = redisObjectSerializer.serialize(value);
                connection.set(key.getBytes(), values);
                connection.close();
                return 1L;
            });
            return true;
        } catch (Exception e) {
            StackTraceElement stackTraceElement = e.getStackTrace()[0];
            log.error("set-" + stackTraceElement.getMethodName() + "--" + stackTraceElement.getLineNumber());
            return false;
        }
    }

    /**
     * 普通缓存获取
     *
     * @param key
     *         键
     *
     * @return 值
     */
    public Object getKey (String key) {
        if (StringUtils.isEmpty(key)) {
            return null;
        }
        if (key.contains(SystemContstant.TOKEN)) {
            key = key.substring(SystemContstant.TOKEN.length());
        }
        Object value = this.redisTemplate.opsForValue().get(key);
        return value;
    }

    /**
     * 普通缓存放入并设置时间
     *
     * @param key
     *         键
     * @param value
     *         值
     * @param time
     *         时间(秒) time要大于0 如果time小于等于0 将设置无限期
     *
     * @return true成功 false 失败
     */
    public boolean setKey (String key, Object value, long time) {
        try {
            if (time > 0) {
                this.redisTemplate.execute((RedisCallback <Long>) connection -> {
                    // redis info
                    byte[] values = redisObjectSerializer.serialize(value);
                    connection.set(key.getBytes(), values);
                    connection.expire(key.getBytes(), 60 * time);
                    connection.close();
                    return 1L;
                });

            } else {
                this.set(key, value);
            }
            return true;
        } catch (Exception e) {
            StackTraceElement stackTraceElement = e.getStackTrace()[0];
            log.error("set-" + stackTraceElement.getMethodName() + "--" + stackTraceElement.getLineNumber());
            return false;
        }
    }

    /*
        普通缓存放入并设置时间
        @param key 键
        @param value 值
        @param time 时间（秒） time要大于0 如果time小于0，将设置成无限制
        @return turn 成功 false 失败
         */
    public boolean set0bj (String key, Object value, long time) {
        try {
            if (time > 0) {
                this.redisTemplate.opsForValue().set(key, value, time, TimeUnit.SECONDS);
            } else {
                this.redisTemplate.opsForValue().set(key, value);
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * 枷锁
     *
     * @param key
     * @param expireTime
     *         时间
     *
     * @return
     */
    public boolean setLock (String key, int expireTime) {
        return this.lock(key, "1", expireTime, TimeUnit.SECONDS);
    }

    public boolean lock (String key, String value, int expireTime, TimeUnit timeUnit) {
        Boolean flag = this.redisTemplate.opsForValue().setIfAbsent(key, value, expireTime, timeUnit);
        if (flag == null || !flag) {
            log.info("申请锁(" + key + "," + value + ")失败");
            return false;
        }
        log.error("申请锁(" + key + "," + value + ")成功");
        return true;
    }

    public void unLock (String key, String value) {
        String script = "if redis.call('get', KEYS[1]) == KEYS[2] then return redis.call('del', KEYS[1]) else return 0 end";
        RedisScript <Long> redisScript = new DefaultRedisScript <>(script, Long.class);
        Long result = this.redisTemplate.execute(redisScript, Arrays.asList(key, value));
        if (result == null || result == 0) {
            log.info("释放锁(" + key + "," + value + ")失败,该锁不存在或锁已经过期");
        } else {
            log.info("释放锁(" + key + "," + value + ")成功");
        }
    }

    /**
     * 匹配key
     * @param pattern
     * @return
     */
    public Set<String> keys(String pattern) {
        return this.redisTemplate.keys(pattern);
    }
}
