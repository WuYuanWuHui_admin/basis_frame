package com.ruoyi.project.utils;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.geom.Ellipse2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import javax.imageio.ImageIO;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageConfig;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class PosterMakerUtil {



    private static BufferedImage generateQRCodeImage(String text, int width, int height, String filePath) throws WriterException, IOException {
        QRCodeWriter qrCodeWriter = new QRCodeWriter();

        BitMatrix bitMatrix = qrCodeWriter.encode(text, BarcodeFormat.QR_CODE, width, height);

        // 生成流
        MatrixToImageConfig matrixToImageConfig = new MatrixToImageConfig();
        BufferedImage image = MatrixToImageWriter.toBufferedImage(bitMatrix, matrixToImageConfig);

        return image;

    }

    public static void main(String[] args) {
        try {
            //图片的本地地址
            Image src = ImageIO.read(new File("F:/util-project/11.jpg"));
            BufferedImage url = (BufferedImage) src;
            //处理图片将其压缩成正方形的小图
//	        BufferedImage  convertImage= scaleByPercentage(url, 100,100);
//	        //裁剪成圆形 （传入的图像必须是正方形的 才会 圆形 如果是长方形的比例则会变成椭圆的）
//	        convertImage = convertCircular(url);
//	        //生成的图片位置
//	    	String imagePath= "C:/Users/admin/Desktop/Imag.png";
//	        ImageIO.write(convertImage, imagePath.substring(imagePath.lastIndexOf(".") + 1), new File(imagePath));
//	        System.out.println("ok");

            Graphics2D gd = url.createGraphics();
            //int height= url.getHeight();
            //int fontHeight= 30;
            //String drawStr = "test";

            //设置透明  start
//            url = gd.getDeviceConfiguration().createCompatibleImage(width, height, Transparency.TRANSLUCENT);
//            gd=url.createGraphics();
            //设置透明  end

            // 合成文字
//            gd.setFont(new Font("方正舒体", Font.PLAIN, fontHeight)); //设置字体
//            gd.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_HRGB); //消除锯齿状
//            gd.setColor(Color.white); //设置颜色
//			//gd.drawRect(0, 0, width - 1, height - 1); //画边框
//            gd.drawString(drawStr, width/2-fontHeight*drawStr.length()/2,fontHeight); //输出文字（中文横向居中）


            // 写入二维码
            BufferedImage qrcode = generateQRCodeImage("test.com", 207, 209, "");
            // 左右上下
            //gd.drawImage(qrcode, width/2, height1, 80, 80, null);
            gd.drawImage(qrcode, 218, 180, 400, 390, null);

            gd.setFont(new Font("Microsoft YaHei", Font.PLAIN, 100));
            gd.setColor(Color.gray);
            gd.drawString("000000" , 230, 1000);
            //生成的图片位置
            String imagePath= "F:/util-project/22.jpg";
            ImageIO.write(url, imagePath.substring(imagePath.lastIndexOf(".") + 1), new File(imagePath));
            System.out.println("ok");
        } catch (Exception e) {
            log.error("二维码生成异常：{}",e);
        }
    }


    /**
     * 生成海报图片
     * @param bjPath
     * @param erweimaUrl
     * @param outPath
     * @return
     */
    public void createRecommendationCode(String bjPath,String erweimaUrl,String outPath,String pushCode){
        try {
            //图片的本地地址
            //Image src = ImageIO.read(new File("F:/util-project/11.jpg"));
            Image src = ImageIO.read(new File(bjPath));
            BufferedImage url = (BufferedImage) src;
            Graphics2D gd = url.createGraphics();
            // 写入二维码
            //BufferedImage qrcode = generateQRCodeImage("test.com", 207, 209, "");
            BufferedImage qrcode = generateQRCodeImage(erweimaUrl, 207, 209, "");
            // 左右上下
            //gd.drawImage(qrcode, width/2, height1, 80, 80, null);
            gd.drawImage(qrcode, 218, 180, 400, 390, null);

            gd.setFont(new Font("Microsoft YaHei", Font.PLAIN, 100));
            gd.setColor(Color.gray);
            gd.drawString(pushCode , 230, 1000);
            //生成的图片位置
            //String imagePath= "F:/util-project/22.jpg";
            String imagePath= outPath;
            ImageIO.write(url, imagePath.substring(imagePath.lastIndexOf(".") + 1), new File(imagePath));
            System.out.println("ok");
        } catch (Exception e) {
            log.error("二维码生成异常：{}",e);
            throw new RuntimeException("系统异常,请稍后再试");
        }
    }


    /**
     * 缩小Image，此方法返回源图像按给定宽度、高度限制下缩放后的图像
     *
     * @param inputImage
     *            ：压缩后宽度
     *            ：压缩后高度
     * @throws java.io.IOException
     *             return
     */
    public static BufferedImage scaleByPercentage(BufferedImage inputImage, int newWidth, int newHeight) throws Exception {
        // 获取原始图像透明度类型
        int type = inputImage.getColorModel().getTransparency();
        int width = inputImage.getWidth();
        int height = inputImage.getHeight();
        // 开启抗锯齿
        RenderingHints renderingHints = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        // 使用高质量压缩
        renderingHints.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        BufferedImage img = new BufferedImage(newWidth, newHeight, type);
        Graphics2D graphics2d = img.createGraphics();
        graphics2d.setRenderingHints(renderingHints);
        graphics2d.drawImage(inputImage, 0, 0, newWidth, newHeight, 0, 0, width, height, null);
        graphics2d.dispose();
        return img;
    }

    /**
     * 通过网络获取图片
     *
     * @param url
     * @return
     */
    public static BufferedImage getUrlByBufferedImage(String url) {
        try {
            URL urlObj = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();
            // 连接超时
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setConnectTimeout(25000);
            // 读取超时 --服务器响应比较慢,增大时间
            conn.setReadTimeout(25000);
            conn.setRequestMethod("GET");
            conn.addRequestProperty("Accept-Language", "zh-cn");
            conn.addRequestProperty("Content-type", "image/jpeg");
            conn.addRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; .NET CLR 2.0.50727)");
            conn.connect();
            BufferedImage bufImg = ImageIO.read(conn.getInputStream());
            conn.disconnect();
            return bufImg;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 传入的图像必须是正方形的 才会 圆形 如果是长方形的比例则会变成椭圆的
     *
     * @return
     * @throws IOException
     */
    public static BufferedImage convertCircular(BufferedImage bi1) throws IOException {

//		BufferedImage bi1 = ImageIO.read(new File(url));

        // 这种是黑色底的
//		BufferedImage bi2 = new BufferedImage(bi1.getWidth(), bi1.getHeight(), BufferedImage.TYPE_INT_RGB);

        // 透明底的图片
        BufferedImage bi2 = new BufferedImage(bi1.getWidth(), bi1.getHeight(), BufferedImage.TYPE_4BYTE_ABGR);
        Ellipse2D.Double shape = new Ellipse2D.Double(0, 0, bi1.getWidth(), bi1.getHeight());
        Graphics2D g2 = bi2.createGraphics();
        g2.setClip(shape);
        // 使用 setRenderingHint 设置抗锯齿
        g2.drawImage(bi1, 0, 0, null);
        // 设置颜色
        g2.setBackground(Color.green);
        g2.dispose();
        return bi2;
    }
}