package com.ruoyi.project.utils;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.utils.http.HttpUtils;
import com.ruoyi.project.content.TemplateTypeEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class WxAppClientUtil {

    private static final Logger log = LoggerFactory.getLogger(WxAppClientUtil.class);

    @Value ("${wx.xcx.appid}")
    private String appid;

    @Value ("${wx.xcx.secretid}")
    private String secret;


    @Value ("${wx.url.getAccessToken}")
    private String APP_TOKEN_URL;

    @Value ("${wx.url.code2Session}")
    private String CODE_2_SESSION_URL;

    @Value ("${wx.url.sendMsg}")
    private String SEND_MSG_URL;

    @Value ("${wx.url.createQrCode}")
    private String CREATE_QR_CODE;


    @Value ("${wx.gzh.appid}")
    private String gzhAppid;

    @Value ("${wx.gzh.secretid}")
    private String gzhSecret;

    @Value ("${wx.gzh.url.gzhAccessToken}")
    private String GZH_ACCESS_TOKEN_URL;

    @Value ("${wx.gzh.url.gzhUser}")
    private String GZH_USER;



    /**
     * 小程序获取token
     *
     * @return
     */
    public Map <String, Object> getWXAPPAccessToken () {
        log.debug("appid:" + this.appid + "  secret" + this.secret + "    app_token_url:" + this.APP_TOKEN_URL);
        //没有则请求获取token
        Map <String, String> param = new HashMap <String, String>();
        param.put("grant_type", "client_credential");
        param.put("appid", this.appid);
        param.put("secret", this.secret);
        //发送get请求
        String result = HttpUtils.sendGet(this.APP_TOKEN_URL, HttpUtils.getParam(param));
        log.debug("result:" + result);
        return JsonUtils.jsonToObject(result, Map.class);
    }

    /**
     * 小程序获取openId
     *
     * @return
     */
    public Map <String, Object> getWXAPPCode2Session (String code) {
        //没有则请求获取token
        Map <String, String> param = new HashMap <String, String>();
       /* param.put("appid", this.appid);
        param.put("secret", this.secret);*/
        param.put("appid", "wx58cd94250038dbdb");
        param.put("secret", this.secret);
        param.put("js_code", code);
        param.put("grant_type", "authorization_code");
        //发送get请求
        String result = HttpUtils.sendGet(this.CODE_2_SESSION_URL, HttpUtils.getParam(param));
        log.debug("测试返回数据" + result);
        return JsonUtils.jsonToObject(result, Map.class);
    }


    public Map <String, Object> sendMsg (String accessToken, String openId, TemplateTypeEnum type,
            Map <String, Object> data, String page) {
        Map <String, Object> param = new HashMap <String, Object>();
        param.put("touser", openId);
        param.put("template_id", type.getText());
        param.put("page", page);
        param.put("data", data);
        log.debug("-------" + JsonUtils.objectToJson(param));
        JSONObject result = RestTemplateUtils.post(param, this.SEND_MSG_URL + accessToken);
        return JsonUtils.jsonToObject(result.toJSONString(), Map.class);
    }


    public Map <String, Object> getAppQrCode (String accessToken) {

        Map <String, Object> param = new HashMap <String, Object>();
        param.put("path", "pages/device-detail/device-detail");
        log.debug("-------" + JsonUtils.objectToJson(param));
        JSONObject result = RestTemplateUtils.post(param, this.SEND_MSG_URL + accessToken);
        return JsonUtils.jsonToObject(result.toJSONString(), Map.class);
    }


    public String getWXGZHAccessToken () {
        log.debug("appid:" + this.gzhAppid + "  secret" + this.gzhSecret + "    app_token_url:" + this.APP_TOKEN_URL);
        //没有则请求获取token
        Map <String, String> param = new HashMap <String, String>();
        param.put("grant_type", "client_credential");
        param.put("appid", this.gzhAppid);
        param.put("secret", this.gzhSecret);
        //发送get请求
        String result = HttpUtils.sendGet(this.APP_TOKEN_URL, HttpUtils.getParam(param));
        log.debug("result:" + result);
        Map map = JsonUtils.jsonToObject(result, Map.class);
        if (map.get("access_token") != null) {
            return String.valueOf(map.get("access_token"));
        } else {
            return "";
        }
    }

    /**
     * 公众号获取openId
     *
     * @return
     */
    public Map <String, Object> getWXGZHCode2Session (String code) {
        //没有则请求获取token
        Map <String, String> param = new HashMap <String, String>();

        param.put("appid", this.gzhAppid);
        param.put("secret", this.gzhSecret);
        param.put("code", code);
        param.put("grant_type", "authorization_code");
        //发送get请求
        String result = HttpUtils.sendGet(this.GZH_ACCESS_TOKEN_URL, HttpUtils.getParam(param));
        log.debug("测试返回数据" + result);
        return JsonUtils.jsonToObject(result, Map.class);
    }

    /**
     * 公众号根据openId 换取用户数据
     *
     * @param openid
     *
     * @return
     */
    public Map <String, Object> getWXGZHUserByOpenIdAndAccessToken (String openid) {
        Map <String, String> param = new HashMap <String, String>();
        param.put("access_token", this.getWXGZHAccessToken());
        param.put("openid", openid);
        //发送get请求
        String result = HttpUtils.sendGet(this.GZH_USER, HttpUtils.getParam(param));
        log.debug("测试返回数据" + result);
        return JsonUtils.jsonToObject(result, Map.class);
    }
}
