package com.ruoyi.project.utils;

import com.alibaba.fastjson.JSON;
import com.baidubce.http.ApiExplorerClient;
import com.baidubce.http.HttpMethodName;
import com.baidubce.model.ApiExplorerRequest;
import com.baidubce.model.ApiExplorerResponse;
import com.ruoyi.common.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

@Component
public class HHCompUtils {

    // 百度人脸识别
    @Autowired
    private RedisUtils redisUtils;

    private String AKI = "YaMXWjllyfOxfUzGGMdrN83h";

    private String AKS = "ZBSnWKktNGSNwaex7IQLVEZFpdSvMNNU";

    public String getAccessToken () throws Exception {
        String token = this.redisUtils.get("DLG_CMP_TOKEN");
        if (token == null) {
            String link = "https://aip.baidubce.com/oauth/2.0/token?grant_type=client_credentials&client_id=" + this.AKI + "&client_secret=" + this.AKS + "&";
            URL url = new URL(link);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestProperty("accept", "*/*");
            connection.setRequestProperty("content-type", "application/x-www-form-urlencoded");
            connection.setRequestProperty("connection", "keep-alive");
            connection.setRequestMethod("POST");
            connection.connect();
            BufferedReader read = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
            String line;
            String result = "";
            while ((line = read.readLine()) != null) {
                result += line;
            }
            token = JSON.parseObject(result).get("access_token").toString();
            this.redisUtils.set("DLG_CMP_TOKEN", token, 43200);
        }
        return token;
    }

    public String doCmp (String imgBaseAvatar, String imgBasePhoto) throws Exception {
        String token = this.getAccessToken();

        String path = "https://aip.baidubce.com/rest/2.0/face/v3/match";
        ApiExplorerRequest request = new ApiExplorerRequest(HttpMethodName.POST, path);
        request.addHeaderParameter("Content-Type", "application/json;charset=UTF-8");
        request.addQueryParameter("access_token", token);
        String jsonBody = "[{\"image\":\"" + imgBaseAvatar + "\",\"image_type\":\"BASE64\"},\n{\"image\":\"" + imgBasePhoto + "\",\"image_type\":\"BASE64\"}]";
        request.setJsonBody(jsonBody);
        try {
            ApiExplorerClient client = new ApiExplorerClient();
            ApiExplorerResponse response = client.sendRequest(request);
            String result = JSON.parseObject(response.getResult()).getJSONObject("result").getString("score");
            if (StringUtils.isBlank(result)) {
                return null;
            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
