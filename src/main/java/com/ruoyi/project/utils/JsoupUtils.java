/*
 * ...
 */

package com.ruoyi.project.utils;

import com.ruoyi.common.utils.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

/**
 * jsoup 工具类
 *
 * @author 挺好的 2023年02月17日 上午 9:01
 */
public final class JsoupUtils {

    /**
     * 清除全部html标记
     *
     * @param content
     *         内容
     *
     * @return
     */
    public static String clearAll (String content) {
        if (StringUtils.isBlank(content)) {
            return StringUtils.EMPTY;
        }

        return Jsoup.clean(content, Whitelist.none());
    }

    private JsoupUtils () {
    }
}
