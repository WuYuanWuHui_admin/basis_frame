package com.ruoyi.project.utils;

import java.util.Calendar;

public class IdCard {

    /**
     * 中国公民身份证号码最小长度。
     */
    public final int CHINA_ID_MIN_LENGTH = 15;

    /**
     * 中国公民身份证号码最大长度。
     */
    public final int CHINA_ID_MAX_LENGTH = 18;

    /**
     * 校验身份证
     *
     * @param idCard
     *
     * @return
     */
    public static boolean checkIdCard (String idCard) {
        // 定义判别用户身份证号的正则表达式（15位或者18位，最后一位可以为字母）
        String regularExpression = "(^[1-9]\\d{5}(18|19|20)\\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\\d{3}[0-9Xx]$)|" + "(^[1-9]\\d{5}\\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\\d{3}$)";
        boolean matches = idCard.matches(regularExpression);

        //判断第18位校验值
        if (matches) {

            if (idCard.length() == 18) {
                try {
                    char[] charArray = idCard.toCharArray();
                    //前十七位加权因子
                    int[] idCardWi = {7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2};
                    //这是除以11后，可能产生的11位余数对应的验证码
                    String[] idCardY = {"1", "0", "X", "9", "8", "7", "6", "5", "4", "3", "2"};
                    int sum = 0;
                    for (int i = 0; i < idCardWi.length; i++) {
                        int current = Integer.parseInt(String.valueOf(charArray[i]));
                        int count = current * idCardWi[i];
                        sum += count;
                    }
                    char idCardLast = charArray[17];
                    int idCardMod = sum % 11;
                    if (idCardY[idCardMod].toUpperCase().equals(String.valueOf(idCardLast).toUpperCase())) {
                        return true;
                    } else {
                        return false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    return false;
                }
            }

        }
        return matches;

    }

    /**
     * 根据身份编号获取年龄
     *
     * @param idCard
     *         身份编号
     *
     * @return 年龄
     */
    public static int getAgeByIdCard (String idCard) {
        int iAge = 0;
        Calendar cal = Calendar.getInstance();
        String year = idCard.substring(6, 10);
        int iCurrYear = cal.get(Calendar.YEAR);
        iAge = iCurrYear - Integer.valueOf(year);
        return iAge;
    }

    /**
     * 根据身份编号获取生日
     *
     * @param idCard
     *         身份编号
     *
     * @return 生日(yyyyMMdd)
     */
    public static String getBirthByIdCard (String idCard) {
        return idCard.substring(6, 14);
    }

    /**
     * 根据身份编号获取生日年
     *
     * @param idCard
     *         身份编号
     *
     * @return 生日(yyyy)
     */
    public static Short getYearByIdCard (String idCard) {
        return Short.valueOf(idCard.substring(6, 10));
    }

    /**
     * 根据身份编号获取生日月
     *
     * @param idCard
     *         身份编号
     *
     * @return 生日(MM)
     */
    public static Short getMonthByIdCard (String idCard) {
        return Short.valueOf(idCard.substring(10, 12));
    }

    /**
     * 根据身份编号获取生日天
     *
     * @param idCard
     *         身份编号
     *
     * @return 生日(dd)
     */
    public static Short getDateByIdCard (String idCard) {
        return Short.valueOf(idCard.substring(12, 14));
    }

    /**
     * 根据身份编号获取性别
     *
     * @param idCard
     *         身份编号
     *
     * @return 性别(M - 男 ， F - 女 ， N - 未知)
     */
    public static String getGenderByIdCard (String idCard) {
        String sGender = "未知";

        String sCardNum = idCard.substring(16, 17);
        if (Integer.parseInt(sCardNum) % 2 != 0) {
            //设置为男
            sGender = "0";
        } else {
            //设置为女
            sGender = "1";
        }
        return sGender;
    }

    //修改年龄的方法
 /*   @RequestMapping(value = "shebaoshuju",method = RequestMethod.GET)
    public String shebaoshuju(){
        List<Wcbxx> list= wcbxxservice.all();
        int  cou=0;
        for(Wcbxx w:list){
            if(w.getIdcard()!=null && w.getIdcard().length()>17) {
                String sex = IdCard.getGenderByIdCard(w.getIdcard());
                int age = IdCard.getAgeByIdCard(w.getIdcard());
                w.setAge(Long.valueOf(age));
                w.setSex(sex);
                int a = wcbxxservice.updatess(w);
                if (a>0) {
                }
            }
            cou++;
            System.out.println(cou);
        }
        return "";
    }*/
}
