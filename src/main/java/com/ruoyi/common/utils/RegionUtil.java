package com.ruoyi.common.utils;

import cn.hutool.core.util.ObjectUtil;

/**
 * @author 挺好的 2023年09月23日 上午 11:27
 */
public class RegionUtil {

    /**
     * 区域编码转城市
     * @param region
     * @return
     */
    public static String  regionToCity(String region){
        if(ObjectUtil.isEmpty(region)){
            return null;
        }
        if(region.length() < 6){
            return null;
        }
        if(region.length() == 6){
            return region;
        }
        return region.substring(0,6);
    }

}
