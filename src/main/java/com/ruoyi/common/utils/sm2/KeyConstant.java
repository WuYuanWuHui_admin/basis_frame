package com.ruoyi.common.utils.sm2;

/**
 * 国密加解密常量
 */
public class KeyConstant {

    public static final String PRIVATE_KEY = "7763492F5B3486717D3A1169D62A8B63B450CF0FEFDFD4220A8A6A422E35336F"; // 私钥
    public static final String PUBLIC_KEY = "041039DEF54C44A3469ECA06B336E63F6FA4C9A4364E7B2AF0B02FF0CA4D787AD4DB30FBA4038E9FF93F86A287BD1E4751B01A8BD4672EC6F667833E431163EFEC"; // 公钥

    public static final String GM_NAME_CURVE = "sm2p256v1";
    public static final String ALGORITHM = "SHA1PRNG";

}

