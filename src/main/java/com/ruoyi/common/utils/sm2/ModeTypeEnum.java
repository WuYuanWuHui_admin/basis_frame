package com.ruoyi.common.utils.sm2;
import org.bouncycastle.crypto.engines.SM2Engine;

public enum ModeTypeEnum {

    BASE_MODE(ModeTypeConstant.BASE, SM2Engine.Mode.C1C3C2),
    BC_MODE(ModeTypeConstant.BC, SM2Engine.Mode.C1C2C3);

    private String type;
    private SM2Engine.Mode mode;

    ModeTypeEnum(String type, SM2Engine.Mode mode) {
        this.type = type;
        this.mode = mode;
    }

    public String getType(){
        return type;
    }

    public SM2Engine.Mode getMode(){
        return mode;
    }

}

