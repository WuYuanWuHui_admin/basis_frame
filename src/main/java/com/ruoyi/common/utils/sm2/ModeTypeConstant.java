package com.ruoyi.common.utils.sm2;

import org.bouncycastle.crypto.engines.SM2Engine;

public class ModeTypeConstant {

    public static final String BASE = "base";
    public static final String BC = "bc";

    @Deprecated
    public static final SM2Engine.Mode BASE_MODE = SM2Engine.Mode.C1C3C2;
    @Deprecated
    public static final SM2Engine.Mode BC_MODE = SM2Engine.Mode.C1C2C3;

    public static ModeTypeEnum getMode(String modeType){
        if (ModeTypeEnum.BASE_MODE.getType().equals(modeType)) return ModeTypeEnum.BASE_MODE;
        return ModeTypeEnum.BC_MODE;
    }

}
