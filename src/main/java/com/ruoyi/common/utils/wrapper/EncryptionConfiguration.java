package com.ruoyi.common.utils.wrapper;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author 猴哥
 */
//@Configuration
public class EncryptionConfiguration {
    /**
     * 过滤器配置
     */
    //@Bean
    public FilterRegistrationBean<RequestHandler> filterRegistration(RequestHandler requestHandler) {
        FilterRegistrationBean<RequestHandler> registration = new FilterRegistrationBean<>();
        registration.setFilter(requestHandler);
        registration.addUrlPatterns("/plugin/*");
        registration.setName("encryptionFilter");
        //设置优先级别
        registration.setOrder(1);
        return registration;
    }
}