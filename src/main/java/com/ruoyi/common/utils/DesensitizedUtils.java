package com.ruoyi.common.utils;


public class DesensitizedUtils {

    public static String chineseName(String fullName) {
        if (StringUtils.isBlank(fullName) || fullName.length() < 2) {
            return fullName;
        }
        return fullName.replaceFirst(fullName.substring(1, 2), "*");
    }
}
