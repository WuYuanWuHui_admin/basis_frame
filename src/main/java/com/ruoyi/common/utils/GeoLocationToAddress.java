package com.ruoyi.common.utils;
import com.alibaba.fastjson.JSONObject;
import com.gexin.fastjson.JSONArray;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

/**
 * 根据经纬度获取地址信息
 */
@Slf4j
public class GeoLocationToAddress {

    private static final String API_URL = "https://restapi.amap.com/v3/geocode/regeo";
    private static final String KEY = "d73cc951a96637a251a9a2446b48ce9c";

    public static String getProvinceCityDistrict(double longitude, double latitude) {
        String parameters = "?key=" + KEY;
        parameters += "&location=" + longitude + "," + latitude;
        parameters += "&extensions=all";
        parameters += "&output=JSON";

        String urlString = API_URL + parameters;
        StringBuilder res = new StringBuilder();

        try {
            log.info("Amap API Request URL: " + urlString);
            URL url = new URL(urlString);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("GET");
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), StandardCharsets.UTF_8));
            String line;
            while ((line = in.readLine()) != null) {
                res.append(line);
            }
            in.close();

            log.info("Map API Response: " + res);

            JSONObject json = JSONObject.parseObject(res.toString());
            if ("1".equals(json.getString("status"))) {
                JSONObject regeocode = json.getJSONObject("regeocode");
                JSONObject addressComponent = regeocode.getJSONObject("addressComponent");
                String province = addressComponent.getString("province");
                String city = addressComponent.getString("city");
                String district = addressComponent.getString("district");

                return "City:" + city + ", District:" + district;
            } else {
                log.info("Map API Response Error: " + json.getString("info"));
            }
        } catch (Exception e) {
            log.info("Error getting address information");
            e.printStackTrace();
        }
        return null;
    }


    public static void main(String[] args) {

        double longitude = 120.036845;
        double latitude = 29.278930;
        String addressInfo = getProvinceCityDistrict(longitude, latitude);
        String[] parts = addressInfo.split(", "); // 拆分成 "City:大连市" 和 "District:甘井子区"
        String city = "";
        String district = "";
        for (String part : parts) {
            if (part.startsWith("City:")) {
                city = part.substring("City:".length()); // 获取 "市"
            } else if (part.startsWith("District:")) {
                district = part.substring("District:".length()); // 获取 "区"
            }
        }
        System.out.println(longitude+","+latitude+"的城市区县为:"+addressInfo);
        System.out.println("City: " + city);
        System.out.println("District: " + district);
    }
}
