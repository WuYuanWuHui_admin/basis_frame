package com.ruoyi.common.enums;


import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 *
 */
@AllArgsConstructor
@Getter
public enum GaoDeEnum {

    // 高德地图固定字段
    STATUS("status"), INT_ONE("1"), RE_GEO_CODE("regeocode"), GEO_CODES("geocodes"), LOCATION("location"),
    FORMATTED_ADDRESS("formatted_address"), RESULTS("results"), DISTANCE("distance");

    private String code;
}
