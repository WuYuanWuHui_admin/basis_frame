package com.ruoyi.common.enums;

/**
 * @author lian-chen
 * @date 2022/06/29 14:54
 */
public enum ExplainStatus {
    ING(0, "申诉中"),
    REJECT(1, "驳回"),
    AGREE(2, "同意"),
    ;
    private Integer code;
    private String value;

    ExplainStatus(Integer code, String value) {
        this.code = code;
        this.value = value;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
