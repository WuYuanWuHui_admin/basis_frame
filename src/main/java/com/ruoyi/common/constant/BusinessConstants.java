package com.ruoyi.common.constant;

/**
 * 业务常量
 *
 * @author 挺好的 2023年02月16日 下午 19:30
 */
public final class BusinessConstants {


    /**
     * 用户相关常量
     */
    public static class UserConstants {

        /**
         * 支付密码最大输入次数
         */
        public static int PAY_PASSWORD_RETRY_COUNT = 5;


        private UserConstants () {
        }
    }

    private BusinessConstants () {
    }
}
