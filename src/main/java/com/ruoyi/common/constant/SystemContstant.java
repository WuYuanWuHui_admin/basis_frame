package com.ruoyi.common.constant;

/**
 * 系统常量
 */
public class SystemContstant {

    //设置常量 两天的计量单位 秒
    public static final long TWO_DAY_SECONDS = 172800;

    public static final String LOGIN_TYPE = "LOGIN_TYPE_";

    public static final String LOGIN_MOBILE = "LOGIN_MOBILE_";

    public static final String CHECK_CODE = "DLG_CHECK_CODE_";

    public static final String CHECK_CODES = "DLG_CHECK_CODES_";

    public static final String PAY_CHECK = "DLG_PAY_CHECK_";

    public static final String ROTATION_CHART_PATH = "NM_ROTATION_CHART_PATH";

    public static final String PROPERTY = "PROPERTY";

    public static final String PROPERTY_NAME = "PROPERTY_NAME";

    public static final String TOKEN = "MY_TOKEN_";
    // public static final String TOKEN = "";

    public static final String USERID = "DLG_USERID_";

    public static final String REQUEST_TOKEN = "DLG_REQUEST_TOKEN";

    //用于存放用户类型标识
    public static final String USER_TYPE = "DLG_USER_TYPE_";

    //存放同时报名订单数
    public static final String USER_ORDER = "DLG_USER_ORDER_";

    //用户语音播报标识
    public static final String USER_ORDER_VOICE = "DLG_USER_ORDER_VOICE_";

    //存放不可接单用户
    public static final String NO_ORDER_USER = "DLG_NO_ORDER_USER_";

    //存放取消接单次数
    public static final String ORDER_USER_NUMBER = "DLG_ORDER_USER_NUMBER_";

    public static final String PICURL = "pic_url";

    public static final String DLG_GEO_KEY = "DLG_GEO";

    public static final String DLG_BIND_PHONE_TIME = "dlg_bind_phone_time_";

    public static final String DLG_BIND_VIRTUAL_PHONE = "dlg_bind_virtual_phone_";

    public static final String DLG_BIND_PHONE = "dlg_bind_phone_";

    //删除前缀
    public static final String EXPIRE = "dlg_expire";

    public static final Character COLON = ':';

    //保证金支付请求缓存
    public static final String PAY_BOND = "dlg_pay_bond";

    public static final String PAY_GOLD_MEDAL = "dlg_pay_gold_medal";

    public static final String PAY_YUEKA_MEDAL = "dlg_pay_yueka_medal";

    public static final String PAY_RENSHU_MEDAL = "dlg_pay_renshu_medal";

    public static final String PAY_SETTLEMENT = "dlg_pay_settlement";

    public static final String PAY_RECHARGE = "dlg_pay_recharge";

    public static final String PAY_SINGLE_SETTLEMENT = "dlg_pay_single_settlement";

    public static final String PAY_PENALTY = "dlg_pay_penalty";

    //任务申请数累计
    public static final String TASK_APPLY_NUM = "dlg_task_apply_";

    // 头像修改限制
    public static final String USER_AVATAR_LIMIT = "dlg_user_avatar_limit:";

    public static final String UPDATE_AVATAR_LIMIT = "update_avatar_limit";

    public static final String UPDATE_AVATAR_TIP = "update_avatar_tip";

    // 更换手机号限制
    public static final String PHONE_CHANGE_LIMIT = "dlg_phone_change_limit:";

    public static final String PHONE_CHANGE_TIP = "phone_change_tip";

    public static final String PHONE_CHANGE_LIMIT_DAY = "phone_change_limit";

    //雇主订单详情文案
    public static final String EMPLOYER_DETAIL_TITLE = "employer_detail_title";

    //提现文案
    public static final String WITHDRAW_TIP = "withdraw_tip";

    // 实名认证文案
    public static final String REAL_AUTH_TIP = "real_auth_tip";

    //工种 检索关键字
    public static final String TASK_CATE_MAP = "dlg_task_cate_map";

    public static final String OFFLINE_MSG = "offline_msg::";

    /**
     * 分享限制
     */
    public static final String SHARE_LIMIT = "dlg_share_limit:";

    public static final String SHARE_LIMIT_KEY = "share_limit";

    /**
     * 微信小程序支付 暂存 订单号
     */
    public static final String PAY_WX_TRADE = "pay_wx_trade:";

    //获取openId appid
    public static final String APPID = "wx7dedaad636ca78fe";

    //获取openId SECRET
    public static final String SECRET = "0798c8c83853a2fa1948f2862a749a01";

    //阿里云外呼accessKeyId
    public static final String accessKeyId = "LTAI5tDBHs8xzGFXFCNpibro";

    //阿里云外呼 accessSecret
    public static final String accessSecret = "3JGPo7uGPYLQA8bBZ3LwQJELWMsAWo";

    //阿里云外呼 SCRIPTID 场景id
    public static final String SCRIPTID = "9eb7cc56-f240-4f55-9e97-c94dc2822d4a";

    //阿里云外呼 InstanceId 业务id
    public static final String InstanceId = "654a40ba-ba3d-4fec-9f9f-246b22eead61";

    //渠道list
    public static final String[] cheannelList = {"qita", "douying", "baidu", "xiaomi", "pingguo", "yingyongbao", "360", "huawei", "oppo", "vivo", "anzhuo", "91", "ali", "wandou", "pp", "lianxiang"};

    //输入密码错误限制次数
    public static final String INCORRECT_PASSWORD = "incorrect_password:";

}
