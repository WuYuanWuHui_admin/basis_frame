/*
 * ...
 */

package com.ruoyi.common.constant;

/**
 * Redis key 常量
 *
 * @author 挺好的 2023年02月16日 下午 19:24
 */
public final class RedisKeyConstants {

    /**
     * 支付密码错误key
     */
    public static final String PAY_PASSWORD_ERROR_COUNT_KEY = "payPasswordErrorCount";

    /**
     * 敏感词列表key
     */
    public static final String SENSITIVE_KEY = "sensitive";

    /**
     * 自动确认开工申请
     */
    public static final String AUTO_CONFIRM_WORK = "AUTO_CONFIRM_WORK-";

    private RedisKeyConstants () {
    }
}
