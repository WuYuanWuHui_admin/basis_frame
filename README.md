g# 找零工-后台仓库

#### 介绍

找零工

#### 技术选型

````
该项目前期主要基于RuoYi脚手架开发，详情 https://gitee.com/y_project/RuoYi?_from=gitee_search
````

1. SpringBoot
2. Apache Shiro
3. Mybatis-plus
4. Redis
4. Swagger(2023-2月份已废弃)
5. Druid
6. layui
7. Hibernate Validator(2023-2月份加入)
8. MapStruct(2023-2月份加入)
9. EhCache（主要用于Shiro缓存）
10. thymeleaf

##### 第三方库

1. APP推送： 个推（http://sdk.open.api.igexin.com/apiex.htm）
2. 短信：阿里云（http://www.aliyun.com）
3. 云存储：七牛云

##### 消息推送

1. WebSocket

##### 证书说明

1. 远程服务器上项目 file 目录下存放的是支付相关的一些证书，其中 以 alipay开始的为 支付宝相关证书
2. 微信支付（V2）：
    1. 证书存储目录： resources/cert目录
    2. 目前只有退款使用了证书
3. 微信支付(V3-该接口未正式启用，相关代码已完成->除回调接口业务处理)：
    1. 证书存储目录： file
    2. 分为两个证书： API证书以及平台证书

#### 安装教程

```
项目为单体应用架构，只区分测试环境（开发环境也是测试环境）以及正式环境
```

##### 一、打包测试环境

    1. application.yml -> profiles -> active -> 选择dev
    2. 执行maven package
    3. 上传发布，远程目录： /home/dlg_2

##### 二、打包正式环境

    1. application.yml -> profiles -> active -> 选择druid
    2. 执行maven package
    3. 上传发布，远程目录： /home/zlg