/*
 Navicat Premium Data Transfer

 Source Server         : 本地
 Source Server Type    : MySQL
 Source Server Version : 50703
 Source Host           : 127.0.0.1:3306
 Source Schema         : ry-yxmy

 Target Server Type    : MySQL
 Target Server Version : 50703
 File Encoding         : 65001

 Date: 04/03/2024 21:54:32
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for b_property_info
-- ----------------------------
DROP TABLE IF EXISTS `b_property_info`;
CREATE TABLE `b_property_info`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `create_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '更新人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  `type` tinyint(3) NOT NULL COMMENT '配置类型',
  `property_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '配置名称',
  `property_value` varchar(5096) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '配置值',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `type_name_uni`(`type`, `property_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 671 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '配置表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of b_property_info
-- ----------------------------
INSERT INTO `b_property_info` VALUES (12, '', '2020-05-19 15:59:23', '', '2023-09-07 18:03:06', '平台提示', 1, 'platform_prompt', '平台提示');
INSERT INTO `b_property_info` VALUES (13, '', '2020-04-08 17:40:20', '', '2023-11-14 14:11:42', '图片访问域名', 1, 'pic_url', 'https://images.linglinggong.net/');
INSERT INTO `b_property_info` VALUES (22, '', '2020-05-13 14:47:16', '', NULL, '钟点工', 2, '1', '钟点工');
INSERT INTO `b_property_info` VALUES (23, '', '2020-05-13 14:47:27', '', NULL, '文员', 2, '2', '文员');
INSERT INTO `b_property_info` VALUES (24, '', '2020-05-13 14:47:41', '', NULL, '不坐班', 2, '3', '不坐班');
INSERT INTO `b_property_info` VALUES (25, '', '2020-05-13 14:47:55', '', NULL, '服务生', 2, '4', '服务生');
INSERT INTO `b_property_info` VALUES (26, '', '2020-05-13 14:48:07', '', NULL, '货物搬运', 2, '5', '货物搬运');
INSERT INTO `b_property_info` VALUES (27, '', '2020-05-13 14:48:30', '', NULL, '兼职', 2, '6', '兼职');
INSERT INTO `b_property_info` VALUES (28, '', '2020-05-13 14:49:26', '', NULL, '功能问题', 3, '1', '功能问题');
INSERT INTO `b_property_info` VALUES (29, '', '2020-05-13 14:49:52', '', NULL, '体验问题', 3, '2', '体验问题');
INSERT INTO `b_property_info` VALUES (30, '', '2020-05-13 14:50:08', '', NULL, '优化问题', 3, '3', '优化问题');
INSERT INTO `b_property_info` VALUES (31, '', '2020-05-13 14:50:25', '', NULL, '其他', 3, '0', '其他');
INSERT INTO `b_property_info` VALUES (32, '', '2020-05-15 11:13:23', '', '2023-09-07 18:00:29', '发布任务时缴纳保证金配置(元/人)', 1, 'bond', '5');
INSERT INTO `b_property_info` VALUES (33, '', '2020-05-19 15:59:23', '', NULL, '义乌市', 4, '330782001', '稠城街道');
INSERT INTO `b_property_info` VALUES (34, '', '2020-05-19 15:59:23', '', NULL, '义乌市', 4, '330782002', '江东街道');
INSERT INTO `b_property_info` VALUES (35, '', '2020-05-19 15:59:23', '', NULL, '义乌市', 4, '330782003', '稠江街道');
INSERT INTO `b_property_info` VALUES (36, '', '2020-05-19 15:59:23', '', NULL, '义乌市', 4, '330782004', '北苑街道');
INSERT INTO `b_property_info` VALUES (37, '', '2020-05-19 15:59:23', '', NULL, '义乌市', 4, '330782005', '后宅街道');
INSERT INTO `b_property_info` VALUES (38, '', '2020-05-19 15:59:23', '', NULL, '义乌市', 4, '330782006', '城西街道');
INSERT INTO `b_property_info` VALUES (39, '', '2020-05-19 15:59:23', '', NULL, '义乌市', 4, '330782007', '廿三里街道');
INSERT INTO `b_property_info` VALUES (40, '', '2020-05-19 15:59:23', '', NULL, '义乌市', 4, '330782100', '佛堂镇');
INSERT INTO `b_property_info` VALUES (41, '', '2020-05-19 15:59:23', '', NULL, '义乌市', 4, '330782101', '赤岸镇');
INSERT INTO `b_property_info` VALUES (42, '', '2020-05-19 15:59:23', '', NULL, '义乌市', 4, '330782102', '义亭镇');
INSERT INTO `b_property_info` VALUES (43, '', '2020-05-19 15:59:23', '', NULL, '义乌市', 4, '330782104', '上溪镇');
INSERT INTO `b_property_info` VALUES (44, '', '2020-05-19 15:59:23', '', NULL, '义乌市', 4, '330782105', '苏溪镇');
INSERT INTO `b_property_info` VALUES (45, '', '2020-05-19 15:59:23', '', NULL, '义乌市', 4, '330782106', '大陈镇');
INSERT INTO `b_property_info` VALUES (55, '', '2020-05-19 16:04:05', '', NULL, '类型不限', 6, '0', '类型不限');
INSERT INTO `b_property_info` VALUES (56, '', '2020-05-19 16:04:21', '', NULL, '长期兼职', 6, '1', '长期兼职');
INSERT INTO `b_property_info` VALUES (57, '', '2020-05-19 16:04:40', '', NULL, '短期兼职', 6, '2', '短期兼职');
INSERT INTO `b_property_info` VALUES (58, '', '2020-05-19 16:04:59', '', NULL, '在家兼职', 6, '3', '在家兼职');
INSERT INTO `b_property_info` VALUES (62, '', '2020-05-19 16:07:59', '', NULL, '时间不限', 8, '0', '时间不限');
INSERT INTO `b_property_info` VALUES (63, '', '2020-05-19 16:08:12', '', NULL, '周末', 8, '1', '周末');
INSERT INTO `b_property_info` VALUES (64, '', '2020-05-19 16:08:23', '', NULL, '工作日', 8, '2', '工作日');
INSERT INTO `b_property_info` VALUES (65, '', '2020-05-19 16:08:34', '', NULL, '寒暑假', 8, '3', '寒暑假');
INSERT INTO `b_property_info` VALUES (66, '', '2020-05-19 16:08:49', '', NULL, '节假日', 8, '4', '节假日');
INSERT INTO `b_property_info` VALUES (67, '', '2020-05-21 16:43:29', '', '2023-09-07 18:01:27', '同时可报名数限制', 1, 'order_num_limit', '30');
INSERT INTO `b_property_info` VALUES (68, '', '2020-05-22 10:02:50', '', '2020-10-31 13:47:25', '雇员取消x小时内不可接单', 1, 'x_hours_no_order', '72');
INSERT INTO `b_property_info` VALUES (69, '', '2020-05-22 10:04:26', '', '2023-09-07 18:01:25', '雇员取消订单扣除诚信值', 1, 'order_user_cancel_cut_integrity', '5');
INSERT INTO `b_property_info` VALUES (70, '', '2020-05-22 10:06:01', '', '2023-09-07 18:04:41', '雇主取消任务扣除诚信值', 1, 'task_user_cancel_cut_integrity', '0');
INSERT INTO `b_property_info` VALUES (71, '', '2020-05-22 14:54:01', '', '2020-07-21 14:49:36', 'x分钟自动确认开工配置', 1, 'x_minute_auto_confirm', '3');
INSERT INTO `b_property_info` VALUES (72, '', '2020-05-23 11:47:06', '', '2023-09-07 18:03:19', '注册时增加诚信值', 1, 'register_integrity', '36.5');
INSERT INTO `b_property_info` VALUES (73, '', '2020-05-23 13:58:30', '', '2023-09-07 18:03:13', '认证诚信值配置', 1, 'real_auth_integrity', '2');
INSERT INTO `b_property_info` VALUES (74, '', '2020-05-25 13:59:02', '', '2024-01-16 13:48:43', '计件每日低保工资配置', 1, 'daily_minimum_wage', '0.5');
INSERT INTO `b_property_info` VALUES (75, '', '2020-06-02 14:27:19', '', '2023-09-07 18:04:45', '客服电话', 1, 'telephone', '0579-85581998');
INSERT INTO `b_property_info` VALUES (79, '', '2020-06-04 10:36:59', '', '2023-09-07 18:00:05', '金牌月活动奖励', 1, 'activity_reward', '300');
INSERT INTO `b_property_info` VALUES (82, '', '2020-06-11 16:22:44', '', '2021-05-06 16:47:37', '收取雇员百分之3手续费', 1, 'user_amount_ratio', '0');
INSERT INTO `b_property_info` VALUES (83, '1', '2020-07-03 14:23:51', '', NULL, '乐清市\r\n', 4, '330382100', '乐成镇');
INSERT INTO `b_property_info` VALUES (84, '1', '2020-07-03 14:23:51', '', NULL, '乐清市\r\n', 4, '330382101', '大荆镇');
INSERT INTO `b_property_info` VALUES (85, '1', '2020-07-03 14:23:51', '', NULL, '乐清市\r\n', 4, '330382102', '仙溪镇');
INSERT INTO `b_property_info` VALUES (86, '1', '2020-07-03 14:23:51', '', NULL, '乐清市\r\n', 4, '330382103', '湖雾镇');
INSERT INTO `b_property_info` VALUES (87, '1', '2020-07-03 14:23:51', '', NULL, '乐清市\r\n', 4, '330382104', '雁荡镇');
INSERT INTO `b_property_info` VALUES (88, '1', '2020-07-03 14:23:51', '', NULL, '乐清市\r\n', 4, '330382105', '芙蓉镇');
INSERT INTO `b_property_info` VALUES (89, '1', '2020-07-03 14:23:51', '', NULL, '乐清市\r\n', 4, '330382106', '清江镇');
INSERT INTO `b_property_info` VALUES (90, '1', '2020-07-03 14:23:51', '', NULL, '乐清市\r\n', 4, '330382107', '南塘镇');
INSERT INTO `b_property_info` VALUES (91, '1', '2020-07-03 14:23:51', '', NULL, '乐清市\r\n', 4, '330382108', '虹桥镇');
INSERT INTO `b_property_info` VALUES (92, '1', '2020-07-03 14:23:51', '', NULL, '乐清市\r\n', 4, '330382109', '南岳镇');
INSERT INTO `b_property_info` VALUES (93, '1', '2020-07-03 14:23:51', '', NULL, '乐清市\r\n', 4, '330382110', '蒲岐镇');
INSERT INTO `b_property_info` VALUES (94, '1', '2020-07-03 14:23:51', '', NULL, '乐清市\r\n', 4, '330382111', '淡溪镇');
INSERT INTO `b_property_info` VALUES (95, '1', '2020-07-03 14:23:51', '', NULL, '乐清市\r\n', 4, '330382112', '石帆镇');
INSERT INTO `b_property_info` VALUES (96, '1', '2020-07-03 14:23:51', '', NULL, '乐清市\r\n', 4, '330382113', '白石镇');
INSERT INTO `b_property_info` VALUES (97, '1', '2020-07-03 14:23:51', '', NULL, '乐清市\r\n', 4, '330382114', '柳市镇');
INSERT INTO `b_property_info` VALUES (98, '1', '2020-07-03 14:23:51', '', NULL, '乐清市\r\n', 4, '330382115', '北白象镇');
INSERT INTO `b_property_info` VALUES (99, '1', '2020-07-03 14:23:51', '', NULL, '乐清市\r\n', 4, '330382116', '象阳镇');
INSERT INTO `b_property_info` VALUES (100, '1', '2020-07-03 14:23:51', '', NULL, '乐清市\r\n', 4, '330382117', '翁垟镇');
INSERT INTO `b_property_info` VALUES (101, '1', '2020-07-03 14:23:51', '', NULL, '乐清市\r\n', 4, '330382118', '磐石镇');
INSERT INTO `b_property_info` VALUES (102, '1', '2020-07-03 14:23:51', '', NULL, '乐清市\r\n', 4, '330382119', '七里港镇');
INSERT INTO `b_property_info` VALUES (103, '1', '2020-07-03 14:23:51', '', NULL, '乐清市\r\n', 4, '330382120', '黄华镇');
INSERT INTO `b_property_info` VALUES (104, '1', '2020-07-03 14:23:51', '', NULL, '乐清市\r\n', 4, '330382200', '智仁乡');
INSERT INTO `b_property_info` VALUES (105, '1', '2020-07-03 14:23:51', '', NULL, '乐清市\r\n', 4, '330382201', '镇安乡');
INSERT INTO `b_property_info` VALUES (106, '1', '2020-07-03 14:23:51', '', NULL, '乐清市\r\n', 4, '330382202', '福溪乡');
INSERT INTO `b_property_info` VALUES (107, '1', '2020-07-03 14:23:51', '', NULL, '乐清市\r\n', 4, '330382203', '双峰乡');
INSERT INTO `b_property_info` VALUES (108, '1', '2020-07-03 14:23:51', '', NULL, '乐清市\r\n', 4, '330382204', '龙西乡');
INSERT INTO `b_property_info` VALUES (109, '1', '2020-07-03 14:23:51', '', NULL, '乐清市\r\n', 4, '330382205', '雁湖乡');
INSERT INTO `b_property_info` VALUES (110, '1', '2020-07-03 14:23:51', '', NULL, '乐清市\r\n', 4, '330382206', '岭底乡');
INSERT INTO `b_property_info` VALUES (111, '1', '2020-07-03 14:23:51', '', NULL, '乐清市\r\n', 4, '330382207', '四都乡');
INSERT INTO `b_property_info` VALUES (112, '1', '2020-07-03 14:23:51', '', NULL, '乐清市\r\n', 4, '330382208', '天成乡');
INSERT INTO `b_property_info` VALUES (113, '1', '2020-07-03 14:23:51', '', NULL, '乐清市\r\n', 4, '330382209', '城北乡');
INSERT INTO `b_property_info` VALUES (114, '', '2020-07-18 11:45:49', '', '2021-01-08 15:36:06', '搬运', 9, '1', '搬运');
INSERT INTO `b_property_info` VALUES (115, '', '2020-07-18 11:46:43', '', '2021-01-08 15:33:20', '淘宝打包', 9, '2', '淘宝打包');
INSERT INTO `b_property_info` VALUES (116, '', '2020-07-18 11:47:15', '', '2021-01-08 15:33:53', '杂工', 9, '3', '杂工');
INSERT INTO `b_property_info` VALUES (117, '', '2020-07-20 15:30:42', '', '2023-09-07 18:03:37', '将任务发布信息通知给5公里以内用户', 1, 'scanning_distance', '5');
INSERT INTO `b_property_info` VALUES (118, '', '2020-07-20 15:41:10', '', NULL, '义乌市', 4, '330782008', '福田街道');
INSERT INTO `b_property_info` VALUES (121, '', '2020-08-05 15:49:51', '', '2024-03-04 11:17:57', '注册支持区号配置', 1, 'adcode', '330701,330702,330703,330783,330726,330784,330782');
INSERT INTO `b_property_info` VALUES (122, '1', '2020-09-10 17:17:04', '', '2023-09-07 18:02:22', '雇主评论增加诚信值', 1, 'employer_evaluate_add_credit', '0.2');
INSERT INTO `b_property_info` VALUES (123, '1', '2020-09-17 10:38:11', '', '2023-09-07 18:03:04', '计件文案', 1, 'piece_tip', '在计件工作中，比较（计件的数量x单价 和 保底小时x单价），取价格高者作为结算金额。');
INSERT INTO `b_property_info` VALUES (125, '1', '2020-09-18 13:46:12', '', '2023-09-07 18:00:18', '可通知或可开工距离单位米', 1, 'allow_notice_distance', '500');
INSERT INTO `b_property_info` VALUES (126, '1', '2020-09-18 17:01:15', '', '2023-09-07 18:01:21', '接单超时取消 单位 分钟 整数 不要输入小数点', 1, 'overtime_cancel', '8');
INSERT INTO `b_property_info` VALUES (127, '1', '2020-09-19 14:48:35', '', '2023-09-20 17:22:08', '计时工作最低小时工资 单位 元', 1, 'time_lowest_amount', '17');
INSERT INTO `b_property_info` VALUES (128, '', '2020-09-21 10:24:31', '', '2023-09-07 18:02:37', '下架超过结束时间5小时未下架的任务', 1, 'hiring_overtime_task_hours', '5');
INSERT INTO `b_property_info` VALUES (129, '', '2020-09-25 17:50:06', '', '2023-12-08 15:25:48', '计件最低保底时薪,单位元', 1, 'piece_time_lowest_amount', '16,18,20');
INSERT INTO `b_property_info` VALUES (130, '', '2020-09-29 14:23:18', '', '2023-09-07 18:00:42', '每天有3次取消机会', 1, 'can_cancel_every_day', '3');
INSERT INTO `b_property_info` VALUES (135, '', '2020-10-17 10:02:49', '', '2023-09-07 18:03:46', '雇主免费发单次数，超过后，发单缴纳服务费（功能暂时无用）', 1, 'service_task_num', '2');
INSERT INTO `b_property_info` VALUES (136, '', '2020-10-17 10:04:22', '', '2021-11-25 15:11:40', '当月vip达标所需单数配置', 1, 'vip_standard_order_num', '12');
INSERT INTO `b_property_info` VALUES (137, '', '2020-10-17 10:04:40', '', '2021-11-25 15:11:16', '当月vip达标所需雇主数', 1, 'vip_standard_employer_num', '6');
INSERT INTO `b_property_info` VALUES (138, '', '2020-10-17 10:04:56', '', '2023-09-07 18:00:25', '差评标准配置（低于当前数为差评）', 1, 'bad_review_score', '3');
INSERT INTO `b_property_info` VALUES (140, '', '2020-10-17 17:50:40', '', '2023-09-07 18:00:20', '打开app时设置手机的音量，数值范围：0-1，以0.125为倍数', 1, 'app_volume', '0');
INSERT INTO `b_property_info` VALUES (178, '', '2020-12-25 12:53:49', '', '2023-09-07 18:02:32', '红单虚拟订单数', 1, 'fictitiousRed', '20');
INSERT INTO `b_property_info` VALUES (179, '', '2020-12-25 12:54:37', '', '2023-09-07 18:02:30', '绿单虚拟订单数', 1, 'fictitiousGreen', '20');
INSERT INTO `b_property_info` VALUES (182, '', '2020-12-25 15:04:09', '', '2023-09-07 17:59:38', '雇员banner现在用的', 1, 'Systemeject', 'http://images.linglinggong.net/FlzFPJxVqNTDUi0i-ObmMWfYB55j,/pages/invite-friend/invite-friend,http://images.linglinggong.net/FqqbsItLmUuW8H56zRFPACa3vXCZ,/pages/beginner-guidance/beginnerGuidanceByStaff,http://images.linglinggong.net/Fnpmi-NeCkzZE0KCkxZV9ynqJm4q,/pages/gold-medal/gold-medal-info');
INSERT INTO `b_property_info` VALUES (183, '', '2020-12-25 15:43:57', '', '2023-09-07 17:59:40', '雇员banner', 1, 'Systememployee', 'http://images.linglinggong.net/Fg0o6YxFxg7Ch-A6Yat_0Vuw6aKj,/pages/invite-friend/invite-friend,http://images.linglinggong.net/Fq6FVG0nNZX3m5ImUjEUZxaEZX0T,/pages/help-detail/help-detail?id=13');
INSERT INTO `b_property_info` VALUES (184, '', '2021-01-12 13:33:29', '', '2023-09-07 17:59:31', '发布页面说明走马灯', 1, 'Release_notes', '温馨提示：雇员若不符岗位要求，您有权在10分钟内免责开除');
INSERT INTO `b_property_info` VALUES (186, '', '2021-01-12 17:56:38', '', '2021-01-14 15:12:39', '东阳市', 4, '330783001', '吴宁街道');
INSERT INTO `b_property_info` VALUES (187, '', '2021-01-12 17:57:08', '', '2021-01-14 15:12:34', '东阳市', 4, '330783003', '南市街道');
INSERT INTO `b_property_info` VALUES (188, '', '2021-01-12 17:57:27', '', '2021-01-14 15:12:27', '东阳市', 4, '330783004', '白云街道');
INSERT INTO `b_property_info` VALUES (189, '', '2021-01-12 17:58:50', '', '2021-01-14 15:12:47', '东阳市', 4, '330783005', '江北街道');
INSERT INTO `b_property_info` VALUES (192, '', '2021-01-12 18:02:45', '', '2021-01-14 15:12:53', '东阳市', 4, '330783006', '城东街道');
INSERT INTO `b_property_info` VALUES (193, '', '2021-01-12 18:03:06', '', '2021-01-14 15:13:02', '东阳市', 4, '330783007', '六石街道');
INSERT INTO `b_property_info` VALUES (194, '', '2021-01-12 18:03:34', '', '2021-01-14 15:13:08', '东阳市', 4, '330783106', '巍山镇');
INSERT INTO `b_property_info` VALUES (195, '', '2021-01-12 18:03:58', '', '2021-01-14 15:13:13', '东阳市', 4, '330783107', '虎鹿镇');
INSERT INTO `b_property_info` VALUES (197, '', '2021-01-12 18:04:46', '', '2021-01-14 15:13:19', '东阳市', 4, '330783108', '歌山镇');
INSERT INTO `b_property_info` VALUES (198, '', '2021-01-12 18:05:05', '', '2021-01-14 15:13:24', '东阳市', 4, '330783109', '佐村镇');
INSERT INTO `b_property_info` VALUES (200, '', '2021-01-12 18:05:52', '', '2021-01-14 15:13:37', '东阳市', 4, '330783112', '湖溪镇');
INSERT INTO `b_property_info` VALUES (201, '', '2021-01-12 18:06:18', '', '2021-01-14 15:13:31', '东阳市', 4, '330783110', '东阳江镇');
INSERT INTO `b_property_info` VALUES (202, '', '2021-01-12 18:06:40', '', '2021-01-14 15:13:43', '东阳市', 4, '330783114', '马宅镇');
INSERT INTO `b_property_info` VALUES (203, '', '2021-01-12 18:06:59', '', '2021-01-14 15:13:49', '东阳市', 4, '330783116', '千祥镇');
INSERT INTO `b_property_info` VALUES (204, '', '2021-01-12 18:07:19', '', '2021-01-14 15:13:53', '东阳市', 4, '330783118', '南马镇');
INSERT INTO `b_property_info` VALUES (205, '', '2021-01-12 18:07:38', '', '2021-01-14 15:14:02', '东阳市', 4, '330783122', '画水镇');
INSERT INTO `b_property_info` VALUES (206, '', '2021-01-12 18:07:57', '', '2021-01-14 15:14:07', '东阳市', 4, '330783123', '横店镇');
INSERT INTO `b_property_info` VALUES (207, '', '2021-01-12 18:08:16', '', '2021-01-14 15:14:12', '东阳市', 4, '330783201', '三单乡');
INSERT INTO `b_property_info` VALUES (209, '', '2021-01-13 16:47:00', '', '2023-09-07 18:00:34', '根据等级配置支付金额', 1, 'bondS', '5,10');
INSERT INTO `b_property_info` VALUES (210, '', '2021-01-27 09:37:03', '', '2023-09-07 17:59:44', '虚拟任务显示数量', 1, 'Virtual_task_quantity', '30');
INSERT INTO `b_property_info` VALUES (211, '', '2021-01-30 10:15:13', '', '2023-09-07 17:59:24', '发布任务控制器1关闭2开启', 1, 'Release_control', '2');
INSERT INTO `b_property_info` VALUES (212, '', '2021-02-25 16:02:51', '', '2023-09-07 17:59:33', '月任务雇主数', 1, 'Release_num', '5');
INSERT INTO `b_property_info` VALUES (213, '', '2021-02-28 11:15:19', '', '2023-09-07 17:59:00', '支付宝支付开关2开启/1关闭', 1, 'ALIPAY', '2');
INSERT INTO `b_property_info` VALUES (214, '', '2021-03-02 19:56:04', '', '2023-09-07 17:59:36', '封禁次数连续取消报名', 1, 'Release_style', '5');
INSERT INTO `b_property_info` VALUES (215, '', '2021-03-05 16:57:07', '', '2023-09-07 17:59:28', '不可查看新雇主发布任务的雇员订单要求数以下', 1, 'Release_examine', '0');
INSERT INTO `b_property_info` VALUES (216, '', '2021-03-09 14:58:56', '', NULL, '金东区', 4, '330703001', '多湖街道');
INSERT INTO `b_property_info` VALUES (217, '', '2021-03-09 14:59:33', '', NULL, '金东区', 4, '330703002', '东孝街道');
INSERT INTO `b_property_info` VALUES (218, '', '2021-03-09 14:59:54', '', NULL, '金东区', 4, '330703101', '孝顺镇');
INSERT INTO `b_property_info` VALUES (219, '', '2021-03-09 15:00:11', '', NULL, '金东区', 4, '330703102', '傅村镇');
INSERT INTO `b_property_info` VALUES (220, '', '2021-03-09 15:00:28', '', NULL, '金东区', 4, '330703103', '曹宅镇');
INSERT INTO `b_property_info` VALUES (221, '', '2021-03-09 15:00:43', '', NULL, '金东区', 4, '330703104', '澧浦镇');
INSERT INTO `b_property_info` VALUES (222, '', '2021-03-09 15:01:03', '', NULL, '金东区', 4, '330703105', '岭下镇');
INSERT INTO `b_property_info` VALUES (223, '', '2021-03-09 15:01:31', '', NULL, '金东区', 4, '330703106', '江东镇');
INSERT INTO `b_property_info` VALUES (224, '', '2021-03-09 15:01:50', '', NULL, '金东区', 4, '330703107', '塘雅镇');
INSERT INTO `b_property_info` VALUES (225, '', '2021-03-09 15:02:07', '', NULL, '金东区', 4, '330703108', '赤松镇');
INSERT INTO `b_property_info` VALUES (226, '', '2021-03-09 15:02:23', '', NULL, '金东区', 4, '330703200', '源东乡');
INSERT INTO `b_property_info` VALUES (233, '', '2021-03-10 16:46:03', '', '2023-09-07 18:01:23', '邀请超时取消任务', 1, 'orderover', '5');
INSERT INTO `b_property_info` VALUES (235, '', '2021-03-10 17:45:34', '', '2023-09-20 11:56:20', '雇员红包弹窗 图片链接。跳转地址，宽 高', 1, 'staff_popup_watch', 'https://images.linglinggong.net/yaoqinghongbao.png,pages/invite-friend/invite-friend,660rpx,740rpx');
INSERT INTO `b_property_info` VALUES (236, '', '2021-03-11 09:21:55', '', '2023-09-07 18:00:40', '雇主弹窗先分享再发单 图片链接。跳转地址，宽 高', 1, 'boss_popup_watch', 'http://images.linglinggong.net/FqUokNE3WWHFFw3UMtLm_Rp67qJl,/pages/invite-friend/new-boss-invite-friend,630rpx,810rpx');
INSERT INTO `b_property_info` VALUES (237, '', '2021-03-17 11:20:53', '', '2022-01-11 17:55:11', '实时雇员数据展示比例', 1, 'user_list_num', '1.2');
INSERT INTO `b_property_info` VALUES (238, '', '2021-03-17 15:28:05', '', '2023-09-07 18:00:38', '雇主页banner', 1, 'boss_banner', 'http://images.linglinggong.net/FpDfC3uG-bOSo_OZIhZ8dNgz0nXe,/pages/big-customer/bigCustomer/bigCustomer,http://images.linglinggong.net/Fvu-Um8xEVgLr9G19Wx-1ixcX3G3,/pages/invite-friend/new-boss-invite-friend,http://images.linglinggong.net/FkdV5xt-yLpWdxUCKxAOH7qizS_X,null');
INSERT INTO `b_property_info` VALUES (239, '', '2021-03-20 10:54:11', '', '2023-11-29 16:26:45', '违约补偿开关/1开启/2关闭', 1, 'Compensation', '1');
INSERT INTO `b_property_info` VALUES (380, '', '2021-03-31 15:16:45', '', '2023-09-07 18:03:11', '中评分值比例', 1, 'rate', '0.5');
INSERT INTO `b_property_info` VALUES (382, '', '2021-04-08 11:09:37', '', '2021-04-28 09:07:40', '其他类', 7, '1', '杂工');
INSERT INTO `b_property_info` VALUES (383, '', '2021-04-08 11:10:36', '', '2021-04-28 09:12:05', '快递类', 7, '2', '卸货');
INSERT INTO `b_property_info` VALUES (384, '', '2021-04-08 11:10:51', '', '2021-04-28 09:14:08', '技工类', 7, '3', '平车');
INSERT INTO `b_property_info` VALUES (385, '', '2021-04-08 11:11:09', '', '2021-04-28 09:10:06', '跑腿类', 7, '4', '跟车');
INSERT INTO `b_property_info` VALUES (387, '', '2021-04-08 11:11:57', '', '2021-04-28 09:21:33', '技工类', 7, '5', '拷边');
INSERT INTO `b_property_info` VALUES (388, '', '2021-04-08 11:12:18', '', '2021-04-28 09:21:42', '技工类', 7, '6', '点数');
INSERT INTO `b_property_info` VALUES (389, '', '2021-04-08 11:12:34', '', '2021-04-28 09:21:49', '技工类', 7, '7', '点钻');
INSERT INTO `b_property_info` VALUES (390, '', '2021-04-08 11:13:16', '', '2021-04-28 09:22:01', '其他类', 7, '8', '清理');
INSERT INTO `b_property_info` VALUES (391, '', '2021-04-08 11:13:31', '', '2021-04-28 09:22:16', '技工类', 7, '9', '客服');
INSERT INTO `b_property_info` VALUES (392, '', '2021-04-08 11:14:40', '', '2021-04-28 09:07:56', '快递类', 7, '10', '搬运');
INSERT INTO `b_property_info` VALUES (393, '', '2021-04-08 11:14:57', '', '2021-04-28 09:08:06', '快递类', 7, '11', '封箱');
INSERT INTO `b_property_info` VALUES (394, '', '2021-04-08 11:15:26', '', '2021-04-28 09:08:32', '快递类', 7, '12', '理货');
INSERT INTO `b_property_info` VALUES (395, '', '2021-04-08 11:15:51', '', '2021-04-28 09:08:50', '快递类', 7, '13', '打包');
INSERT INTO `b_property_info` VALUES (396, '', '2021-04-08 11:16:33', '', '2021-04-28 09:09:05', '快递类', 7, '14', '拆包');
INSERT INTO `b_property_info` VALUES (397, '', '2021-04-08 11:17:06', '', '2021-04-28 09:09:15', '快递类', 7, '15', '派件');
INSERT INTO `b_property_info` VALUES (399, '', '2021-04-08 11:17:55', '', '2021-04-28 09:09:26', '快递类', 7, '16', '集包');
INSERT INTO `b_property_info` VALUES (400, '', '2021-04-08 11:18:52', '', '2021-04-28 09:09:37', '快递类', 7, '17', '物流');
INSERT INTO `b_property_info` VALUES (401, '', '2021-04-08 11:19:11', '', NULL, '快递类', 7, '18', '快递类');
INSERT INTO `b_property_info` VALUES (403, '', '2021-04-08 11:20:17', '', NULL, '电脑类', 7, '20', '电脑类');
INSERT INTO `b_property_info` VALUES (404, '', '2021-04-08 11:20:51', '', '2021-04-28 09:11:34', '跑腿类', 7, '19', '送货员');
INSERT INTO `b_property_info` VALUES (405, '', '2021-04-08 11:21:15', '', '2021-04-28 09:13:26', '技术类', 7, '21', '手工活');
INSERT INTO `b_property_info` VALUES (407, '', '2021-04-08 11:22:00', '', '2021-04-28 09:13:38', '技工类', 7, '22', '贴纸类');
INSERT INTO `b_property_info` VALUES (408, '', '2021-04-08 11:22:17', '', '2021-04-28 09:13:46', '技工类', 7, '23', '饰品类');
INSERT INTO `b_property_info` VALUES (409, '', '2021-04-08 11:22:43', '', NULL, '餐厅类', 7, '24', '餐厅类');
INSERT INTO `b_property_info` VALUES (410, '', '2021-04-08 11:23:02', '', '2021-04-28 09:13:57', '餐厅类', 7, '25', '洗碗类');
INSERT INTO `b_property_info` VALUES (412, '', '2021-04-16 08:53:28', '', '2023-09-07 18:03:08', '0是显示好评度1是显示诚信值9是两个都显示', 1, 'praise_or_goodFaith', '0');
INSERT INTO `b_property_info` VALUES (413, '', '2021-04-17 12:38:36', '', NULL, '优惠券使用规则', 1, 'youhuiquan_regulation', '123');
INSERT INTO `b_property_info` VALUES (414, '', '2021-04-21 13:11:33', '', '2023-09-07 17:59:42', '任务被点击次数*当前倍数 自然数', 1, 'TASK_USER_NUM_BEISHU', '10');
INSERT INTO `b_property_info` VALUES (415, '', '2021-05-06 16:49:02', '', '2023-10-17 15:36:34', '雇员提现费率', 1, 'user_amount_redio', '3');
INSERT INTO `b_property_info` VALUES (446, '', '2021-05-19 10:45:48', '', '2023-09-07 17:59:05', '金牌金额 *100', 1, 'Gold_medal_amount', '68');
INSERT INTO `b_property_info` VALUES (448, '', '2021-05-28 14:12:49', '', '2023-09-07 18:01:12', '封禁账户--提现封禁开关 1开启 2关闭', 1, 'draw_apply_status', '1');
INSERT INTO `b_property_info` VALUES (449, '', '2021-06-01 08:31:53', '', '2024-01-31 17:38:14', '收费城市', 1, 'regions', '330782,330701,330702,330703,330783,330726');
INSERT INTO `b_property_info` VALUES (450, '', '2021-06-01 12:24:23', '', '2023-09-07 18:00:27', 'vip差评数', 1, 'bad_review_scorecp', '3');
INSERT INTO `b_property_info` VALUES (451, '', '2021-06-01 12:24:53', '', '2021-10-31 23:35:26', 'vip达标时长', 1, 'vip_standard_time', '198');
INSERT INTO `b_property_info` VALUES (453, '', '2021-06-09 14:29:19', '', '2023-09-07 18:03:56', '每天免费发布任务名额', 1, 'service_task_num_tus', '1');
INSERT INTO `b_property_info` VALUES (454, '', '2021-06-11 13:47:06', '', '2023-09-07 18:04:47', '真实号/虚拟号全部开启 1开启/2关闭', 1, 'telephonestatus', '2');
INSERT INTO `b_property_info` VALUES (476, '', '2021-06-14 13:55:11', '', '2023-09-07 18:01:37', '新人雇主弹窗', 1, 'new_boss_banner', 'http://images.linglinggong.net/Fo3H649MJJw_jDVH2MQ5uO_IvDmQ,/pages/app-employer/send-job/send-job,630rpx,737rpx');
INSERT INTO `b_property_info` VALUES (477, '', '2021-06-14 13:58:19', '', '2023-09-07 18:01:49', '客服微信', 1, 'kefu_wechat', 'zhaolinggong003');
INSERT INTO `b_property_info` VALUES (478, '', '2021-06-18 14:15:02', '', '2023-09-07 17:59:07', '完单邀请免审核开关1免审核2.需要审核', 1, 'Invite_tasks_status', '1');
INSERT INTO `b_property_info` VALUES (479, '', '2021-06-18 19:50:09', '', '2023-09-07 18:01:33', '新雇员活动弹窗', 1, 'new_staff_popup_watch', 'https://images.linglinggong.net/%E5%8F%91%E7%BA%A2%E5%8C%85%E5%95%A6.png,/pages/newStaff/newStaffRedEnvelope,750rpx,778rpx');
INSERT INTO `b_property_info` VALUES (480, '', '2021-07-08 13:58:56', '', '2023-09-07 17:59:11', '新人红包工时规则', 1, 'New_people_share_rewards_time', '26');
INSERT INTO `b_property_info` VALUES (481, '', '2021-07-09 15:47:49', '', '2023-09-07 18:03:43', '待开工开除标签', 1, 'send_not_down', '评分太低,已招到人,联系不上,沟通态度很差,年龄不符,其他原因');
INSERT INTO `b_property_info` VALUES (482, '', '2021-07-09 15:48:13', '', '2023-09-07 18:03:40', '开工后开除标签', 1, 'send_down', '干活不行,态度很差,年龄不符,其他原因');
INSERT INTO `b_property_info` VALUES (483, '', '2021-07-09 15:52:37', '', '2024-03-04 11:05:13', '岗位要求标签', 1, 'task_label', '工作熟悉,不磨洋工,平台接单,私下不要,禁止吸烟,手速要快,中途离开不结算工资中途离开不结算工资,请不要离开请不要离开请不要离开');
INSERT INTO `b_property_info` VALUES (484, '', '2021-07-19 10:08:20', '', '2023-09-07 18:00:16', '新人礼开关 0默认,新用户开放,1已参加活动的开放,2全部关闭,', 1, 'adrecsivstatus', '0');
INSERT INTO `b_property_info` VALUES (485, '', '2021-07-24 09:24:46', '', '2023-09-07 18:00:07', '钻石月活动奖励', 1, 'activity_reward_zuanshi', '500');
INSERT INTO `b_property_info` VALUES (486, '', '2021-08-01 09:59:15', '', '2023-09-07 18:03:54', '雇员辞工时间', 1, 'staff_expel_time', '2');
INSERT INTO `b_property_info` VALUES (487, '', '2021-10-09 16:38:24', '', '2023-06-14 10:51:37', '1级惩罚 封3天 解封需29.9元', 11, '1', '1-29.9');
INSERT INTO `b_property_info` VALUES (488, '', '2021-10-09 16:39:20', '', '2021-10-22 12:42:44', '2级惩罚 封6天 解封需39.9元', 11, '2', '6-39.9');
INSERT INTO `b_property_info` VALUES (489, '', '2021-10-09 16:43:10', '', '2021-10-22 12:41:55', '3级惩罚 封15天 解封需69.9元', 11, '3', '15-69.9');
INSERT INTO `b_property_info` VALUES (490, '', '2021-10-09 16:44:09', '', '2021-10-09 17:58:30', '4级惩罚 封30天 解封需99.9元', 11, '4', '30-99.9');
INSERT INTO `b_property_info` VALUES (491, '', '2021-10-20 09:30:30', '', NULL, 'vip每天能取消订单数量', 1, 'vip_can_cancel_every_day', '6');
INSERT INTO `b_property_info` VALUES (492, '', '2021-10-20 09:33:09', '', '2021-11-09 19:34:28', '提现日配置开关', 1, 'withdraw_peoperty_switch', '0');
INSERT INTO `b_property_info` VALUES (493, '', '2021-10-20 09:34:55', '', '2024-02-22 19:09:03', '免费提现日(范围1-31),无效数字将自动忽略,多个日用（,）隔开', 1, 'free_withdraw_day', '10,25');
INSERT INTO `b_property_info` VALUES (494, '', '2022-06-28 16:59:05', '', '2023-12-23 15:19:12', '用户头像上传间隔天数', 1, 'update_avatar_limit', '30');
INSERT INTO `b_property_info` VALUES (495, '', '2022-06-28 17:04:34', '', NULL, '头像更新文案', 1, 'update_avatar_tip', '天更换一次');
INSERT INTO `b_property_info` VALUES (496, '', '2022-06-30 14:41:28', '', '2023-09-07 18:02:52', '更换手机文案', 1, 'phone_change_tip', '用户可以在app内自行修改手机号，一个月只能一次');
INSERT INTO `b_property_info` VALUES (497, '', '2022-06-30 14:41:56', '', '2023-09-07 18:02:50', '更换手机号限制天数', 1, 'phone_change_limit', '60');
INSERT INTO `b_property_info` VALUES (498, '', '2022-07-04 14:11:47', '', '2023-09-07 18:01:14', '雇主订单详情员工列表文案', 1, 'employer_detail_title', '如对雇员不满意，找零工客服随时可介入处理噢。');
INSERT INTO `b_property_info` VALUES (499, '', '2022-07-04 14:31:59', '', '2023-09-26 19:54:30', '提现文案', 1, 'withdraw_tip', '9:00~18:00');
INSERT INTO `b_property_info` VALUES (502, '', '2022-07-14 17:20:58', '', '2023-09-07 18:03:15', '实名认证文案', 1, 'real_auth_tip', '<h3>实名认证文案实名认</h3>证文案实名认证 \n<br>文案');
INSERT INTO `b_property_info` VALUES (503, '', '2022-07-15 11:20:43', '', '2023-09-07 18:01:52', '招工最低年龄配置', 1, 'job_reply_age_min', '18');
INSERT INTO `b_property_info` VALUES (504, '', '2022-07-15 11:21:02', '', '2023-09-07 18:01:54', '招工最高年龄配置', 1, 'job_reply_age_max', '80');
INSERT INTO `b_property_info` VALUES (506, '', '2022-11-03 13:58:18', '', '2023-09-07 18:04:34', '招募中任务，超过预定开工时间 x 小时，自动隐藏', 1, 'task_timeout_autohide', '2');
INSERT INTO `b_property_info` VALUES (507, '', '2022-11-09 10:18:22', '', '2024-01-31 17:29:31', '城市级别-发展版城市(城市编码, 多个英文逗号隔开)', 1, 'city_level_fz', '330782,330783,330726');
INSERT INTO `b_property_info` VALUES (508, '', '2022-11-09 10:18:22', '', '2024-01-29 16:15:06', '城市级别-开城版城市(城市编码, 多个英文逗号隔开)', 1, 'city_level_kc', '0');
INSERT INTO `b_property_info` VALUES (509, '', '2022-11-09 10:18:22', '', '2024-01-29 16:15:08', '城市级别-成熟版城市(城市编码, 多个英文逗号隔开)', 1, 'city_level_cs', '0');
INSERT INTO `b_property_info` VALUES (510, '', '2022-11-16 13:34:44', '', '2023-09-07 18:04:08', '任务自动审核开关 1开 0关', 1, 'task_auto_audit', '1');
INSERT INTO `b_property_info` VALUES (511, '', '2022-11-16 13:34:44', '', '2023-09-07 18:04:12', '任务自动审核 低于多少通过', 1, 'task_auto_audit_price', '17');
INSERT INTO `b_property_info` VALUES (512, '', '2022-11-16 13:34:44', '', '2023-09-07 18:04:15', '任务自动审核 限定时间段内(注意格式,英文冒号和逗号)', 1, 'task_auto_audit_time', '08:30,22:00');
INSERT INTO `b_property_info` VALUES (513, '', '2022-11-16 13:34:44', '', '2024-01-16 17:13:00', '任务自动审核禁止关键字敏感词', 1, 'task_auto_audit_banword', '政府,国家,长期,共产党,人民,手机号,中国,按摩,小姐,陪睡,身份证,中福');
INSERT INTO `b_property_info` VALUES (514, '', '2022-11-16 13:34:44', '', '2023-09-07 18:01:29', '核酸要求开关 1开 0关', 1, 'nucleic_acid_require', '0');
INSERT INTO `b_property_info` VALUES (516, '', '2022-12-02 15:47:59', '', NULL, '用户评分-雇员完单率合格分(超过x%算100%)', 1, 'userScore_finishWork', '85');
INSERT INTO `b_property_info` VALUES (517, '', '2022-12-02 15:47:59', '', NULL, '用户评分-雇主发单准确率合格分(超过x%算100%)', 1, 'userScore_taskAccuracy', '85');
INSERT INTO `b_property_info` VALUES (518, '', '2022-12-02 15:47:59', '', NULL, '用户评分-新用户在x次数内评分规则:率x(总单数÷x)', 1, 'userScore_assume', '5');
INSERT INTO `b_property_info` VALUES (519, '', '2022-12-02 15:47:59', '', NULL, '用户评分-大于发布时间+x分钟需准时开工,小于则接单时间+x', 1, 'userScore_lateCondition', '30');
INSERT INTO `b_property_info` VALUES (520, '', '2022-12-02 15:47:59', '', NULL, '用户评分-雇员迟到开工时间补偿开始时间+x分钟内不算迟到', 1, 'userScore_lateOffset', '15');
INSERT INTO `b_property_info` VALUES (521, '', '2022-12-17 09:28:23', '', '2023-09-07 18:04:30', '任务标题关键词', 1, 'task_placeholder', '打包,搬运,卸货,手工活,快递,包装');
INSERT INTO `b_property_info` VALUES (522, '', '2023-01-03 13:35:30', '', '2023-09-07 17:58:58', '1', 1, '17034766635', '90');
INSERT INTO `b_property_info` VALUES (527, '', '2023-01-03 14:48:29', '', NULL, '测试3', 10, '17052436284', '90');
INSERT INTO `b_property_info` VALUES (528, '', '2023-01-05 16:34:44', '', NULL, '测试', 10, '17052436283', '90');
INSERT INTO `b_property_info` VALUES (529, '', '2023-01-05 16:35:18', '', NULL, '测试3', 10, '17052436263', '90');
INSERT INTO `b_property_info` VALUES (530, '', '2023-03-24 09:30:24', '', '2024-01-29 18:23:22', '是否开启自动提现 0 关闭 1 开启', 1, 'draw_apply_check_flag', '0');
INSERT INTO `b_property_info` VALUES (531, '', '2023-03-24 09:31:23', '', '2023-09-07 18:01:01', '自动审核提现数据量 -1:无限制', 1, 'draw_apply_check_count', '30');
INSERT INTO `b_property_info` VALUES (532, '', '2023-04-19 15:36:20', '', '2023-09-07 18:04:06', '系统配置通用短信验证码', 1, 'sys_check_code', '159357');
INSERT INTO `b_property_info` VALUES (533, '', '2023-04-19 15:42:58', '', '2023-07-08 15:00:51', '1.开启：雇员性别为男，则看到任务要求男/不限的任务。雇员性别为女，则看到任务要求女/不限的任务 2.关闭：雇员可以看到所有性别的任务', 1, '性别要求进行任务展示', '1');
INSERT INTO `b_property_info` VALUES (534, '', '2023-04-20 09:36:49', '', '2024-02-29 17:04:08', '人脸比对分数通过阈值', 1, 'user_verify_score', '80');
INSERT INTO `b_property_info` VALUES (535, '', '2023-04-21 11:57:53', '', '2023-09-07 18:02:47', '提现方式关闭 0支付宝 1微信 2 银行卡 两个如1,2', 1, 'pay_close_list', '10');
INSERT INTO `b_property_info` VALUES (536, '', '2023-04-21 11:59:35', '', '2023-09-07 18:02:43', '支付渠道关闭 -1不需要保证金支付 0支付宝 1微信 2余额 3乐刷微信小程序 4乐刷微信JSAPI 5乐刷支付宝H5 两个如 1,2', 1, 'pay_channel_close_list', '10');
INSERT INTO `b_property_info` VALUES (537, '', '2023-04-22 09:09:35', '', '2023-09-07 18:04:26', '发展版雇员任务性别展示功能 0 关闭 1 开启', 1, 'task_find_sex_flag', '0');
INSERT INTO `b_property_info` VALUES (538, '', '2023-05-06 11:21:14', '', '2023-05-06 11:28:02', '到达开工路途时间,分钟', 1, '到达开工路途时间', '45');
INSERT INTO `b_property_info` VALUES (539, '', '2023-05-06 15:44:13', '', '2023-09-07 18:01:08', '自动提现结束时间HM', 1, 'draw_apply_end_time', '1800');
INSERT INTO `b_property_info` VALUES (540, '', '2023-05-06 15:44:40', '', '2023-09-07 18:01:10', '自动提现开始时间HM', 1, 'draw_apply_start_time', '830');
INSERT INTO `b_property_info` VALUES (541, '', '2023-05-06 17:47:31', '', '2023-05-06 17:53:31', '准确任务比例85', 1, '准确任务比例', '0.85');
INSERT INTO `b_property_info` VALUES (542, '', '2023-05-08 15:37:07', '', NULL, '有效邀请时间 默认 6.0', 1, 'valid_invitation_time', '6.0');
INSERT INTO `b_property_info` VALUES (543, '', '2023-05-29 18:27:04', '', '2023-07-07 16:36:15', '周冲活动-上周完成数量', 1, '周冲活动-上周完成数量', '10');
INSERT INTO `b_property_info` VALUES (544, '', '2023-05-30 15:56:22', '', '2023-07-08 13:46:37', '周冲活动-金额金额', 1, '周冲活动-金额金额', '10');
INSERT INTO `b_property_info` VALUES (546, '', '2023-06-16 11:39:47', '', '2023-07-08 09:37:28', '周冲活动开关，0 关闭 1 开启', 1, '周冲活动-活动开关', '1');
INSERT INTO `b_property_info` VALUES (547, '', '2023-06-20 14:07:04', '', '2023-09-07 18:00:54', '在线客服地址', 1, 'customer_service_url', 'http://im.linglinggong.net/chat/say/redchat.html?cuid=0&amp;uname=0&amp;gid=1&amp;wname=0&amp;du=2&amp;uid={userId}');
INSERT INTO `b_property_info` VALUES (548, '', '2023-06-26 14:22:23', '', '2024-03-02 09:32:49', '支付渠道开启 -1不需要保证金支付 0支付宝 1微信 2余额 3乐刷微信小程序 4乐刷微信JSAPI 5乐刷支付宝H5 两个如 1,2', 1, 'pay_channel_open_list', '0,1,2,3,4');
INSERT INTO `b_property_info` VALUES (549, '', '2023-06-26 16:17:21', '', '2023-07-08 11:32:56', '0 关闭 1 开启', 1, '雇主新人优惠包-活动开关', '1');
INSERT INTO `b_property_info` VALUES (550, '', '2023-07-03 14:21:26', '', '2023-07-08 11:37:23', '雇员新人红包-活动开关 0 关闭 1 开启', 1, '雇员新人红包-活动开关', '1');
INSERT INTO `b_property_info` VALUES (551, '', '2023-07-04 17:19:18', '', '2023-08-22 09:54:47', '雇员新人红包-提现最低额度', 1, '雇员新人红包-提现最低额度', '10');
INSERT INTO `b_property_info` VALUES (552, '', '2023-07-04 17:42:30', '', '2023-07-05 09:45:55', '1', 1, '雇主多久未发布任务数（天）', '1');
INSERT INTO `b_property_info` VALUES (553, '', '2023-07-04 17:44:24', '', '2023-09-07 18:03:24', '雇主多久未发布任务数（天）', 1, 'release_task_day_count', '360');
INSERT INTO `b_property_info` VALUES (554, '', '2023-07-04 17:45:24', '', '2023-09-07 18:03:21', '接单显示时间控制（天）', 1, 'release_order_day_count', '2');
INSERT INTO `b_property_info` VALUES (556, '', '2023-07-14 14:02:40', '', '2023-07-21 19:33:48', '评分回暖金额-立即回暖', 1, '评分回暖金额-立即回暖', '39.98');
INSERT INTO `b_property_info` VALUES (557, '', '2023-07-14 14:08:31', '', '2023-07-21 19:33:41', '评分回暖金额-加速回暖', 1, '评分回暖金额-加速回暖', '69.99');
INSERT INTO `b_property_info` VALUES (558, '', '2023-07-14 15:21:43', '', '2023-09-27 13:59:37', '低范围-回复分值', 1, '低范围-回复分值', '2');
INSERT INTO `b_property_info` VALUES (559, '', '2023-07-14 15:23:01', '', '2023-09-27 13:59:20', '中范围-回复分值', 1, '中范围-回复分值', '4');
INSERT INTO `b_property_info` VALUES (560, '', '2023-07-14 16:35:31', '', '2023-09-04 18:24:19', '加速回暖-回复分值', 1, '加速回暖-回复分值', '10');
INSERT INTO `b_property_info` VALUES (561, '', '2023-07-17 14:29:14', '', NULL, '购买工装-原价', 1, '购买工装-原价', '59');
INSERT INTO `b_property_info` VALUES (562, '', '2023-07-17 14:29:45', '', '2023-09-13 15:53:26', '购买工装-优惠价', 1, '购买工装-优惠价', '19.9');
INSERT INTO `b_property_info` VALUES (563, '', '2023-07-17 14:32:20', '', NULL, '购买工装-打折 97折', 1, '购买工装-打折', '97');
INSERT INTO `b_property_info` VALUES (564, '', '2023-07-17 17:38:57', '', '2023-09-07 18:00:23', '小程序默认id（app跳转小程序支付使用）', 1, 'applet_id', 'gh_b28fa89d3db2');
INSERT INTO `b_property_info` VALUES (565, '', '2023-07-18 09:55:09', '', NULL, '周榜奖励值配置', 1, 'week_billboard_reward', '100');
INSERT INTO `b_property_info` VALUES (566, '', '2023-07-20 10:25:09', '', '2023-10-17 13:43:47', '商品折扣到期时间', 1, 'discount_expiration_time', '2023-10-17 17:58:35');
INSERT INTO `b_property_info` VALUES (568, '', '2023-07-29 10:50:10', '', '2024-03-04 16:00:15', '开工是否校验人脸打卡 0 不校验 1 校验', 1, 'user_cloud_auth_flag', '1');
INSERT INTO `b_property_info` VALUES (569, '', '2023-08-01 13:48:21', '', '2023-09-26 14:47:10', '邀请雇主开关 0 关闭 1 开启', 1, 'invite_employer_switch', '1');
INSERT INTO `b_property_info` VALUES (570, '', '2023-08-01 14:08:11', '', '2023-09-07 18:00:45', '是否开启防止篡改 0 否 1 是', 1, 'check_user_prevent_distortKey_flag', '0');
INSERT INTO `b_property_info` VALUES (571, '', '2023-08-02 10:54:38', '', '2023-09-07 18:04:37', '认证失败次数', 1, 'task_user_auth_count', '3');
INSERT INTO `b_property_info` VALUES (572, '', '2023-08-02 10:54:59', '', '2023-09-07 09:07:59', '豁免期天数', 1, 'user_exempt_date', '1');
INSERT INTO `b_property_info` VALUES (573, '', '2023-08-02 10:55:30', '', '2024-03-04 16:00:09', '是否开启人脸打卡 0 关闭 1 开启', 1, 'task_user_auth_flag', '1');
INSERT INTO `b_property_info` VALUES (574, '', '2023-08-02 10:56:33', '', '2023-09-07 18:00:01', '订单无差评(系统)', 1, 'activity_good_model_order', '2');
INSERT INTO `b_property_info` VALUES (575, '', '2023-08-02 10:57:21', '', '2023-09-07 18:00:04', '工时(系统)', 1, 'activity_good_model_time', '12');
INSERT INTO `b_property_info` VALUES (576, '', '2023-08-02 10:57:51', '', '2023-09-07 17:59:58', '雇主数（系统）', 1, 'activity_employer_count', '1');
INSERT INTO `b_property_info` VALUES (577, '', '2023-08-02 10:58:22', '', '2023-09-07 18:00:09', '评分(评分)', 1, 'activity_score', '70');
INSERT INTO `b_property_info` VALUES (578, '', '2023-08-02 10:58:45', '', NULL, '用户正式零工体验天数', 1, 'user_experience_day', '10');
INSERT INTO `b_property_info` VALUES (579, '', '2023-08-02 10:59:10', '', NULL, '到期体验提醒天数', 1, 'user_experience_notice_day', '3');
INSERT INTO `b_property_info` VALUES (580, '', '2023-08-11 10:52:02', '', '2024-03-04 10:12:47', '优先推送展示设置 是否购卡0没有/1月卡/2季卡/3年卡/4其他 参照userDetail CardUserType', 1, 'task_release_user_flag', '0');
INSERT INTO `b_property_info` VALUES (581, '', '2023-08-14 15:22:15', '', '2023-09-07 17:59:54', '差评', 1, 'activityBadcount', '10');
INSERT INTO `b_property_info` VALUES (582, '', '2023-08-14 17:55:30', '', '2023-09-15 16:53:08', '差评订单数', 1, 'activity_bad_count', '4');
INSERT INTO `b_property_info` VALUES (583, '', '2023-04-19 15:42:58', '', '2023-09-07 18:04:43', '1.开启：雇员性别为男，则看到任务要求男/不限的任务。雇员性别为女，则看到任务要求女/不限的任务 2.关闭：雇员可以看到所有性别的任务', 1, 'task_user_sex_config', '1');
INSERT INTO `b_property_info` VALUES (584, '', '2023-08-25 09:40:54', '', '2023-09-07 18:03:50', '单王周榜设置', 1, 'single_king_weekly_list_config', '本周前100名单王送出新版工装');
INSERT INTO `b_property_info` VALUES (585, '', '2023-08-26 08:57:45', '', '2023-09-07 18:03:53', '单王周榜开关设置 0 关 1 开', 1, 'single_king_weekly_open_config', '0');
INSERT INTO `b_property_info` VALUES (586, '', '2023-08-28 14:42:08', '', '2023-09-07 18:04:18', '结算发单奖励是否执行 0 关闭 1 开启', 1, 'task_bill_reward_flag', '1');
INSERT INTO `b_property_info` VALUES (587, '', '2023-08-28 14:42:40', '', '2023-09-27 14:32:00', '三单奖励金额', 1, 'task_bill_reward_user_coupon_three_account', '10');
INSERT INTO `b_property_info` VALUES (588, '', '2023-08-28 14:43:01', '', '2023-09-27 14:32:08', '6单奖励金额', 1, 'task_bill_reward_user_coupon_six_account', '20');
INSERT INTO `b_property_info` VALUES (589, '', '2023-08-29 14:00:45', '', '2024-01-31 17:30:43', '发单奖励开放区域', 1, 'task_bill_reward_open_region', '330782,330726');
INSERT INTO `b_property_info` VALUES (590, '', '2023-09-01 10:07:21', '', '2023-09-08 10:02:36', '雇主客服电话', 1, 'employer_telephone', '0579-85581998');
INSERT INTO `b_property_info` VALUES (591, '', '2023-09-04 15:03:22', '', '2023-09-04 15:03:34', '邀请好友开关 0关闭 1开启', 1, '邀请好友', '1');
INSERT INTO `b_property_info` VALUES (592, '', '2023-06-16 11:37:22', '', '2023-09-08 11:47:19', '1.开启：男性雇员，任务展示：男/不限任务；女性雇员，任务展示：女/不限任务 2.关闭：所有雇员可以看到所有任务', 1, 'gender_requirements_for_task_presentation', '1');
INSERT INTO `b_property_info` VALUES (593, '', '2023-06-16 11:38:02', '', '2023-07-04 15:23:50', '分钟，迟到规则使用', 1, 'travel_time', '45');
INSERT INTO `b_property_info` VALUES (594, '', '2023-06-16 11:38:26', '', '2023-09-07 18:01:47', '周冲活动-上周完成数量 小于配置值', 1, 'last_week_completed_quantity', '3');
INSERT INTO `b_property_info` VALUES (595, '', '2023-06-16 11:39:45', '', '2023-08-14 09:29:34', '0 关闭 1 开启 本周重新开启，会执行本周全部任务（周冲）', 1, 'zhouchong_activity_switch', '1');
INSERT INTO `b_property_info` VALUES (596, '', '2023-06-16 11:40:13', '', '2023-06-20 09:32:45', '周冲活动-金额金额', 1, 'zhouchong_activity_amount', '10');
INSERT INTO `b_property_info` VALUES (597, '', '2023-07-05 09:33:36', '', '2023-09-08 14:06:26', '0 关闭 1 开启', 1, 'employer_newcomer_activity_switch', '1');
INSERT INTO `b_property_info` VALUES (598, '', '2023-07-11 09:21:04', '', '2023-09-08 14:13:23', '0 关闭 1 开启', 1, 'new_user_red_envelope_flag', '1');
INSERT INTO `b_property_info` VALUES (599, '', '2023-07-11 09:22:09', '', '2024-03-04 13:48:17', '雇员新人红包-提现最低额度', 1, 'minimum_withdrawal_amount', '20');
INSERT INTO `b_property_info` VALUES (600, '', '2023-07-19 13:48:22', '', '2023-09-07 18:02:01', '评分回暖金额-立即回暖', 1, 'immediate_recovery', '39.9');
INSERT INTO `b_property_info` VALUES (601, '', '2023-07-19 13:49:14', '', '2023-09-07 17:59:46', '评分回暖金额-加速回暖', 1, 'accelerated_recovery', '69.9');
INSERT INTO `b_property_info` VALUES (602, '', '2023-07-19 13:49:55', '', '2023-09-07 18:01:44', '低范围-回复分值', 1, 'low_range', '4');
INSERT INTO `b_property_info` VALUES (603, '', '2023-07-19 13:50:12', '', '2023-09-07 18:01:46', '中范围-回复分值', 1, 'medium_range', '2');
INSERT INTO `b_property_info` VALUES (605, '', '2023-07-19 13:50:28', '', '2023-09-07 17:59:52', '加速回暖-回复分值', 1, 'accelerated_recovery_score', '10');
INSERT INTO `b_property_info` VALUES (606, '', '2023-07-19 13:51:15', '', '2023-10-12 15:45:30', '购买工装-原价', 1, 'tooling_original_price', '69');
INSERT INTO `b_property_info` VALUES (607, '', '2023-07-19 13:51:35', '', '2023-10-17 13:44:42', '购买工装-优惠价', 1, 'tooling_preferential_price', '50');
INSERT INTO `b_property_info` VALUES (608, '', '2023-07-19 13:51:53', '', '2023-09-07 18:04:58', '购买工装-打折', 1, 'tooling_discounts', '97');
INSERT INTO `b_property_info` VALUES (609, '', '2023-09-08 14:32:30', '', NULL, '抽奖工时', 1, 'lottery_hours', '6');
INSERT INTO `b_property_info` VALUES (610, '', '2023-09-20 09:30:33', '', '2023-09-25 15:59:39', '关闭用户充值渠道（用户id,\',\'隔开）', 1, 'pay_user_close_list', '0,1,2,3,4,5');
INSERT INTO `b_property_info` VALUES (611, '', '2023-09-22 15:58:24', '', NULL, '0.85 准确任务比例85', 1, 'accurate_task_proportion', '0.85');
INSERT INTO `b_property_info` VALUES (612, '', '2023-09-23 11:46:06', '', '2024-02-01 13:42:20', '报名次数义乌-试用', 12, 'order_apply_warning_0_330782', '2');
INSERT INTO `b_property_info` VALUES (614, '', '2023-09-23 11:47:22', '', '2024-02-01 13:45:15', '报名次数义乌-金牌', 12, 'order_apply_warning_2_330782', '5');
INSERT INTO `b_property_info` VALUES (615, '', '2023-09-23 11:48:05', '', '2024-02-22 19:14:10', '报名次数义乌-正式', 12, 'order_apply_warning_1_330782', '7');
INSERT INTO `b_property_info` VALUES (616, '', '2023-09-23 11:46:06', '', '2023-09-26 16:08:50', '报名次数东阳-试用', 12, 'order_apply_warning_0_330783', '5');
INSERT INTO `b_property_info` VALUES (617, '', '2023-09-23 11:47:22', '', NULL, '报名次数东阳-金牌', 12, 'order_apply_warning_2_330783', '5');
INSERT INTO `b_property_info` VALUES (618, '', '2023-09-23 11:48:05', '', NULL, '报名次数东阳-正式', 12, 'order_apply_warning_1_330783', '3');
INSERT INTO `b_property_info` VALUES (619, '', '2023-09-23 14:12:39', '', '2023-10-10 14:06:00', '义乌经纪人城市开关设置：true 开启 和false 关闭', 12, 'broker_flag_330782', 'true');
INSERT INTO `b_property_info` VALUES (621, '', '2023-09-23 14:14:16', '', '2023-12-23 15:00:09', '东阳经纪人城市开关设置：true 开启 和false 关闭', 12, 'broker_flag_330783', 'true');
INSERT INTO `b_property_info` VALUES (625, '', '2023-09-23 14:56:38', '', NULL, '用户提现黑名单设置，填写用户id,逗号隔开', 1, 'draw_apply_user_close_list', '120347');
INSERT INTO `b_property_info` VALUES (626, '', '2023-10-13 11:31:01', '', '2023-10-31 13:12:55', '东阳-试用提现手续费', 12, 'free_region_Identity_0_330783', '0.00');
INSERT INTO `b_property_info` VALUES (627, '', '2023-10-13 11:31:42', '', '2023-10-31 13:14:08', '东阳-正式提现手续费', 12, 'free_region_Identity_1_330783', '0.00');
INSERT INTO `b_property_info` VALUES (628, '', '2023-10-13 11:32:17', '', '2023-10-31 13:14:26', '东阳-金牌提现手续费', 12, 'free_region_Identity_2_330783', '0.00');
INSERT INTO `b_property_info` VALUES (629, '', '2023-10-13 11:31:01', '', '2023-10-30 18:12:56', '义乌-试用提现手续费', 12, 'free_region_Identity_0_330782', '0.05');
INSERT INTO `b_property_info` VALUES (630, '', '2023-10-13 11:31:42', '', '2023-10-30 18:13:11', '义乌-正式提现手续费', 12, 'free_region_Identity_1_330782', '0.03');
INSERT INTO `b_property_info` VALUES (631, '', '2023-10-13 11:32:17', '', '2023-10-30 18:09:53', '义乌-金牌提现手续费', 12, 'free_region_Identity_2_330782', '0.00');
INSERT INTO `b_property_info` VALUES (632, '', '2023-10-13 11:31:01', '', '2023-10-31 13:28:14', '金华市-金东区-试用提现手续费', 12, 'free_region_Identity_0_330703', '0.05');
INSERT INTO `b_property_info` VALUES (633, '', '2023-10-13 11:31:42', '', '2023-10-31 13:28:28', '金华市-金东区-正式提现手续费', 12, 'free_region_Identity_1_330703', '0.03');
INSERT INTO `b_property_info` VALUES (634, '', '2023-10-13 11:32:17', '', '2023-10-31 13:14:19', '金华市-金东区-金牌提现手续费', 12, 'free_region_Identity_2_330703', '0.00');
INSERT INTO `b_property_info` VALUES (635, '', '2023-10-13 11:31:01', '', '2023-10-31 13:28:09', '金华市-婺城区-试用提现手续费', 12, 'free_region_Identity_0_330702', '0.05');
INSERT INTO `b_property_info` VALUES (636, '', '2023-10-13 11:31:42', '', '2023-10-31 13:28:23', '金华市-婺城区-正式提现手续费', 12, 'free_region_Identity_1_330702', '0.03');
INSERT INTO `b_property_info` VALUES (637, '', '2023-10-13 11:32:17', '', '2023-10-31 13:14:14', '金华市-婺城区-金牌提现手续费', 12, 'free_region_Identity_2_330702', '0.00');
INSERT INTO `b_property_info` VALUES (638, '', '2023-10-18 18:39:26', '', '2024-03-02 11:16:04', '金牌之星城市配置330782', 1, 'gold_star_city_configuration', '330783,220726');
INSERT INTO `b_property_info` VALUES (639, '', '2023-10-24 14:09:27', '', '2023-11-14 14:54:47', '题库请求地址', 1, 'exam_base_url', 'http://47.111.185.107:8101/');
INSERT INTO `b_property_info` VALUES (640, '', '2023-10-24 14:10:35', '', '2023-11-16 10:11:31', '雇员题库id', 1, 'exam_0_id', '1724322475626168321');
INSERT INTO `b_property_info` VALUES (641, '', '2023-10-24 14:10:58', '', '2023-11-14 15:09:34', '雇主题库id', 1, 'exam_1_id', '1724322287683600385');
INSERT INTO `b_property_info` VALUES (642, '', '2023-10-26 11:41:34', '', '2024-03-04 14:17:41', '客服工作时间', 1, 'customer_service_date', '7:30-20:00');
INSERT INTO `b_property_info` VALUES (643, '', '2023-10-27 17:08:19', '', NULL, '售前主客服id', 1, 'customer_service_user_id', '1');
INSERT INTO `b_property_info` VALUES (644, '', '2023-10-28 14:45:47', '', '2024-01-27 15:10:28', '题库答题及格分数', 1, 'exam_pass_score', '100');
INSERT INTO `b_property_info` VALUES (645, '', '2023-10-31 13:24:59', '', NULL, '虚拟任务-东阳-街道配置', 12, 'task_region_330783', '1');
INSERT INTO `b_property_info` VALUES (646, '', '2023-10-31 13:25:27', '', NULL, '1', 12, 'task_region_330702', '1');
INSERT INTO `b_property_info` VALUES (647, '', '2023-10-31 13:28:35', '', NULL, '1', 12, 'task_region_330782', '1');
INSERT INTO `b_property_info` VALUES (648, '', '2023-10-31 13:28:50', '', NULL, '1', 12, 'task_region_330703', '1');
INSERT INTO `b_property_info` VALUES (650, '', '2023-11-11 14:27:43', '', '2024-01-13 10:56:37', '新用户展示-雇员多少单内无差评', 1, 'task_employee_order_count', '3');
INSERT INTO `b_property_info` VALUES (651, '', '2023-11-11 14:28:23', '', '2024-01-13 10:52:42', '新用户展示-雇员多少单内差评小于设置', 1, 'taskr_employer_evaluate_count', '1');
INSERT INTO `b_property_info` VALUES (652, '', '2023-11-11 14:28:49', '', '2024-01-13 11:20:57', '新用户展示-雇主发布任务数', 1, 'task_user_employer_count', '2');
INSERT INTO `b_property_info` VALUES (653, '', '2023-11-13 15:21:42', '', '2023-11-30 22:23:11', '消息中心雇员热门活动 图片,链接', 1, 'popular_activities_employee', 'message center/Dingtalk_20231130214857.jpg,/pages/sendCode/index');
INSERT INTO `b_property_info` VALUES (654, '', '2023-11-13 15:22:09', '', '2023-11-27 15:26:04', '消息中心雇主热门活动 图片,链接', 1, 'popular_activities_employer', 'FodqKJX38IEWMwnKNhOSuE1FV7fp,/pages/invite-friend/new-boss-invite-friend');
INSERT INTO `b_property_info` VALUES (655, '', '2023-11-14 11:26:43', '', NULL, '最后登入时间', 1, 'lastLoginTime', '5');
INSERT INTO `b_property_info` VALUES (656, '', '2023-11-14 11:28:31', '', NULL, '最后完成订单时间', 1, 'finalOrderCompletionTime', '5');
INSERT INTO `b_property_info` VALUES (657, '', '2023-11-14 11:31:24', '', '2023-11-14 11:31:41', '最后3个订单距离', 1, 'lastThreeOrders', '200');
INSERT INTO `b_property_info` VALUES (658, '', '2023-11-14 11:34:44', '', NULL, '最后订单距离', 1, 'lastOrders', '100');
INSERT INTO `b_property_info` VALUES (659, '', '2023-11-15 13:29:59', '', '2023-11-16 15:20:12', '任务发布所需时长（最长时间）', 1, 'task_release_hour', '16');
INSERT INTO `b_property_info` VALUES (660, '', '2023-11-21 09:47:29', '', '2024-02-23 09:16:19', '新用户展示延迟展示时间-任务发布多久后到任务大厅', 1, 'task_user_date', '5');
INSERT INTO `b_property_info` VALUES (661, '', '2023-11-29 09:17:28', '', '2024-03-02 09:49:54', '外网访问IP白名单', 1, 'external_network_ip', '115.212.2.210,115.211.173.204,115.211.169.78,192.168.3.21,115.211.174.31,125.112.236.145,60.182.10.176,192.168.3.21,125.112.87.121,125.112.239.132,125.112.234.253,115.212.6.106,115.211.170.87,125.112.235.199,115.212.7.184');
INSERT INTO `b_property_info` VALUES (662, '', '2023-11-29 09:18:09', '', '2024-02-21 18:40:22', '两单间隔时间（分钟）', 1, 'task_user_order_time', '10');
INSERT INTO `b_property_info` VALUES (663, '', '2023-11-29 10:19:45', '', '2023-11-29 11:45:26', '视频压缩配置命令', 1, 'video_compress_config', 'avthumb/mp4/s/800x600/vb/1.25m');
INSERT INTO `b_property_info` VALUES (664, '', '2023-12-25 09:28:17', '', '2023-12-25 09:33:26', '敏感词', 1, 'sensitive', '投资');
INSERT INTO `b_property_info` VALUES (665, '', '2023-12-25 09:34:04', '', NULL, '敏感词', 1, 'task_auto_audit_sensitive', '政府,国家,长期,投资');
INSERT INTO `b_property_info` VALUES (666, '', '2024-01-02 10:45:01', '', '2024-01-30 14:36:16', '系统资质是否展示 0 展示 1 不展示', 1, 'system_is_show', '0');
INSERT INTO `b_property_info` VALUES (667, '', '2024-01-27 14:37:26', '', NULL, '0企业 1商户', 1, 'choose_weChat_payment_method', '0');
INSERT INTO `b_property_info` VALUES (668, '', '2024-01-31 11:43:38', '', NULL, '330726', 1, 'broker_flag_330726', 'true');
INSERT INTO `b_property_info` VALUES (669, '', '2024-01-31 13:55:29', '', NULL, '26', 12, 'broker_flag_330726', 'true');
INSERT INTO `b_property_info` VALUES (670, '', '2024-02-21 17:50:45', '', '2024-02-21 17:50:57', '封禁1天', 11, '5', '1-19.9');

-- ----------------------------
-- Table structure for gen_table
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table`  (
  `table_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '表名称',
  `table_comment` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '表描述',
  `sub_table_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '关联子表的表名',
  `sub_table_fk_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '子表关联的外键名',
  `class_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '实体类名称',
  `tpl_category` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作 sub主子表操作）',
  `package_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '生成模块名',
  `business_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '生成业务名',
  `function_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '生成功能名',
  `function_author` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '生成功能作者',
  `gen_type` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '0' COMMENT '生成代码方式（0zip压缩包 1自定义路径）',
  `gen_path` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '/' COMMENT '生成路径（不填默认项目路径）',
  `options` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '其它生成选项',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`table_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '代码生成业务表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of gen_table
-- ----------------------------
INSERT INTO `gen_table` VALUES (1, 't_user', '会员用户', '', NULL, 'TUser', 'crud', 'com.ruoyi.project', 'userApp', 'userApp', '会员用户', 'ruoyi', '0', '/', '{\"parentMenuId\":\"1062\",\"treeName\":\"\",\"treeParentCode\":\"\",\"parentMenuName\":\"会员用户管理\",\"treeCode\":\"\"}', 'admin', '2024-01-23 22:27:45', '', '2024-01-23 22:34:57', '');

-- ----------------------------
-- Table structure for gen_table_column
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column`  (
  `column_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_id` bigint(20) NULL DEFAULT NULL COMMENT '归属表编号',
  `column_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '列描述',
  `column_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '列类型',
  `java_type` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'JAVA字段名',
  `is_pk` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '是否主键（1是）',
  `is_increment` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '是否自增（1是）',
  `is_required` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '是否必填（1是）',
  `is_insert` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '是否为插入字段（1是）',
  `is_edit` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '是否编辑字段（1是）',
  `is_list` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '是否列表字段（1是）',
  `is_query` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '是否查询字段（1是）',
  `query_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
  `html_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
  `dict_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '字典类型',
  `sort` int(11) NULL DEFAULT NULL COMMENT '排序',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`column_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '代码生成业务表字段' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of gen_table_column
-- ----------------------------
INSERT INTO `gen_table_column` VALUES (1, 1, 'id', '主键ID', 'bigint(20)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2024-01-23 22:27:45', NULL, '2024-01-23 22:34:57');
INSERT INTO `gen_table_column` VALUES (2, 1, 'user_name', '用户名称', 'varchar(25)', 'String', 'userName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2024-01-23 22:27:45', NULL, '2024-01-23 22:34:57');
INSERT INTO `gen_table_column` VALUES (3, 1, 'password', '密码', 'varchar(50)', 'String', 'password', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2024-01-23 22:27:45', NULL, '2024-01-23 22:34:57');
INSERT INTO `gen_table_column` VALUES (4, 1, 'mobile', '手机号', 'varchar(11)', 'String', 'mobile', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2024-01-23 22:27:45', NULL, '2024-01-23 22:34:57');
INSERT INTO `gen_table_column` VALUES (5, 1, 'photo', '头像', 'varchar(127)', 'String', 'photo', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2024-01-23 22:27:45', NULL, '2024-01-23 22:34:57');
INSERT INTO `gen_table_column` VALUES (6, 1, 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', '1', '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 6, 'admin', '2024-01-23 22:27:45', NULL, '2024-01-23 22:34:57');
INSERT INTO `gen_table_column` VALUES (7, 1, 'last_login_ip', '最后登录IP', 'varchar(255)', 'String', 'lastLoginIp', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2024-01-23 22:27:45', NULL, '2024-01-23 22:34:57');
INSERT INTO `gen_table_column` VALUES (8, 1, 'last_login_time', '最后登录时间', 'datetime', 'Date', 'lastLoginTime', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 8, 'admin', '2024-01-23 22:27:45', NULL, '2024-01-23 22:34:57');
INSERT INTO `gen_table_column` VALUES (9, 1, 'channel', '注册渠道', 'bigint(20)', 'Long', 'channel', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 9, 'admin', '2024-01-23 22:27:45', NULL, '2024-01-23 22:34:57');
INSERT INTO `gen_table_column` VALUES (10, 1, 'user_status', '用户状态 0 正常  1 删除  2 锁定', 'int(3)', 'Integer', 'userStatus', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', '', 10, 'admin', '2024-01-23 22:27:45', NULL, '2024-01-23 22:34:57');

-- ----------------------------
-- Table structure for qrtz_blob_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_blob_triggers`;
CREATE TABLE `qrtz_blob_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `blob_data` blob NULL COMMENT '存放持久化Trigger对象',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = 'Blob类型的触发器表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of qrtz_blob_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_calendars
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_calendars`;
CREATE TABLE `qrtz_calendars`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '调度名称',
  `calendar_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '日历名称',
  `calendar` blob NOT NULL COMMENT '存放持久化calendar对象',
  PRIMARY KEY (`sched_name`, `calendar_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '日历信息表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of qrtz_calendars
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_cron_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_cron_triggers`;
CREATE TABLE `qrtz_cron_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `cron_expression` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'cron表达式',
  `time_zone_id` varchar(80) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '时区',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = 'Cron类型的触发器表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of qrtz_cron_triggers
-- ----------------------------
INSERT INTO `qrtz_cron_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', '0/10 * * * * ?', 'Asia/Shanghai');
INSERT INTO `qrtz_cron_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', '0/15 * * * * ?', 'Asia/Shanghai');
INSERT INTO `qrtz_cron_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', '0/20 * * * * ?', 'Asia/Shanghai');

-- ----------------------------
-- Table structure for qrtz_fired_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_fired_triggers`;
CREATE TABLE `qrtz_fired_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '调度名称',
  `entry_id` varchar(95) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '调度器实例id',
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `instance_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '调度器实例名',
  `fired_time` bigint(13) NOT NULL COMMENT '触发的时间',
  `sched_time` bigint(13) NOT NULL COMMENT '定时器制定的时间',
  `priority` int(11) NOT NULL COMMENT '优先级',
  `state` varchar(16) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '状态',
  `job_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '任务名称',
  `job_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '任务组名',
  `is_nonconcurrent` varchar(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '是否并发',
  `requests_recovery` varchar(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '是否接受恢复执行',
  PRIMARY KEY (`sched_name`, `entry_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '已触发的触发器表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of qrtz_fired_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_job_details
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '调度名称',
  `job_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '任务名称',
  `job_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '任务组名',
  `description` varchar(250) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '相关介绍',
  `job_class_name` varchar(250) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '执行任务类名称',
  `is_durable` varchar(1) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '是否持久化',
  `is_nonconcurrent` varchar(1) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '是否并发',
  `is_update_data` varchar(1) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '是否更新数据',
  `requests_recovery` varchar(1) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '是否接受恢复执行',
  `job_data` blob NULL COMMENT '存放持久化job对象',
  PRIMARY KEY (`sched_name`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '任务详细信息表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of qrtz_job_details
-- ----------------------------
INSERT INTO `qrtz_job_details` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', NULL, 'com.ruoyi.project.monitor.job.util.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F5045525449455373720028636F6D2E72756F79692E70726F6A6563742E6D6F6E69746F722E6A6F622E646F6D61696E2E4A6F6200000000000000010200084C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E000978720029636F6D2E72756F79692E6672616D65776F726B2E7765622E646F6D61696E2E42617365456E7469747900000000000000010200074C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C0006706172616D7371007E00034C000672656D61726B71007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000018CFCC23C0878707400007070707400013174000E302F3130202A202A202A202A203F74001172795461736B2E72794E6F506172616D7374000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000001740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E697A0E58F82EFBC8974000133740001317800);
INSERT INTO `qrtz_job_details` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', NULL, 'com.ruoyi.project.monitor.job.util.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F5045525449455373720028636F6D2E72756F79692E70726F6A6563742E6D6F6E69746F722E6A6F622E646F6D61696E2E4A6F6200000000000000010200084C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E000978720029636F6D2E72756F79692E6672616D65776F726B2E7765622E646F6D61696E2E42617365456E7469747900000000000000010200074C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C0006706172616D7371007E00034C000672656D61726B71007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000018CFCC23C0878707400007070707400013174000E302F3135202A202A202A202A203F74001572795461736B2E7279506172616D7328277279272974000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000002740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E69C89E58F82EFBC8974000133740001317800);
INSERT INTO `qrtz_job_details` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', NULL, 'com.ruoyi.project.monitor.job.util.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F5045525449455373720028636F6D2E72756F79692E70726F6A6563742E6D6F6E69746F722E6A6F622E646F6D61696E2E4A6F6200000000000000010200084C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E000978720029636F6D2E72756F79692E6672616D65776F726B2E7765622E646F6D61696E2E42617365456E7469747900000000000000010200074C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C0006706172616D7371007E00034C000672656D61726B71007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000018CFCC23C0878707400007070707400013174000E302F3230202A202A202A202A203F74003872795461736B2E72794D756C7469706C65506172616D7328277279272C20747275652C20323030304C2C203331362E3530442C203130302974000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000003740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E5A49AE58F82EFBC8974000133740001317800);

-- ----------------------------
-- Table structure for qrtz_locks
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_locks`;
CREATE TABLE `qrtz_locks`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '调度名称',
  `lock_name` varchar(40) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '悲观锁名称',
  PRIMARY KEY (`sched_name`, `lock_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '存储的悲观锁信息表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of qrtz_locks
-- ----------------------------
INSERT INTO `qrtz_locks` VALUES ('RuoyiScheduler', 'STATE_ACCESS');
INSERT INTO `qrtz_locks` VALUES ('RuoyiScheduler', 'TRIGGER_ACCESS');

-- ----------------------------
-- Table structure for qrtz_paused_trigger_grps
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
CREATE TABLE `qrtz_paused_trigger_grps`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '调度名称',
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  PRIMARY KEY (`sched_name`, `trigger_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '暂停的触发器表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of qrtz_paused_trigger_grps
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_scheduler_state
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_scheduler_state`;
CREATE TABLE `qrtz_scheduler_state`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '调度名称',
  `instance_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '实例名称',
  `last_checkin_time` bigint(13) NOT NULL COMMENT '上次检查时间',
  `checkin_interval` bigint(13) NOT NULL COMMENT '检查间隔时间',
  PRIMARY KEY (`sched_name`, `instance_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '调度器状态表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of qrtz_scheduler_state
-- ----------------------------
INSERT INTO `qrtz_scheduler_state` VALUES ('RuoyiScheduler', 'xieshaohui1709546682532', 1709560418136, 15000);

-- ----------------------------
-- Table structure for qrtz_simple_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simple_triggers`;
CREATE TABLE `qrtz_simple_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `repeat_count` bigint(7) NOT NULL COMMENT '重复的次数统计',
  `repeat_interval` bigint(12) NOT NULL COMMENT '重复的间隔时间',
  `times_triggered` bigint(10) NOT NULL COMMENT '已经触发的次数',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '简单触发器的信息表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of qrtz_simple_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_simprop_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
CREATE TABLE `qrtz_simprop_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `str_prop_1` varchar(512) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'String类型的trigger的第一个参数',
  `str_prop_2` varchar(512) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'String类型的trigger的第二个参数',
  `str_prop_3` varchar(512) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'String类型的trigger的第三个参数',
  `int_prop_1` int(11) NULL DEFAULT NULL COMMENT 'int类型的trigger的第一个参数',
  `int_prop_2` int(11) NULL DEFAULT NULL COMMENT 'int类型的trigger的第二个参数',
  `long_prop_1` bigint(20) NULL DEFAULT NULL COMMENT 'long类型的trigger的第一个参数',
  `long_prop_2` bigint(20) NULL DEFAULT NULL COMMENT 'long类型的trigger的第二个参数',
  `dec_prop_1` decimal(13, 4) NULL DEFAULT NULL COMMENT 'decimal类型的trigger的第一个参数',
  `dec_prop_2` decimal(13, 4) NULL DEFAULT NULL COMMENT 'decimal类型的trigger的第二个参数',
  `bool_prop_1` varchar(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'Boolean类型的trigger的第一个参数',
  `bool_prop_2` varchar(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'Boolean类型的trigger的第二个参数',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '同步机制的行锁表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of qrtz_simprop_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_triggers`;
CREATE TABLE `qrtz_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '触发器的名字',
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '触发器所属组的名字',
  `job_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'qrtz_job_details表job_name的外键',
  `job_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'qrtz_job_details表job_group的外键',
  `description` varchar(250) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '相关介绍',
  `next_fire_time` bigint(13) NULL DEFAULT NULL COMMENT '上一次触发时间（毫秒）',
  `prev_fire_time` bigint(13) NULL DEFAULT NULL COMMENT '下一次触发时间（默认为-1表示不触发）',
  `priority` int(11) NULL DEFAULT NULL COMMENT '优先级',
  `trigger_state` varchar(16) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '触发器状态',
  `trigger_type` varchar(8) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '触发器的类型',
  `start_time` bigint(13) NOT NULL COMMENT '开始时间',
  `end_time` bigint(13) NULL DEFAULT NULL COMMENT '结束时间',
  `calendar_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '日程表名称',
  `misfire_instr` smallint(2) NULL DEFAULT NULL COMMENT '补偿执行的策略',
  `job_data` blob NULL COMMENT '存放持久化job对象',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  INDEX `sched_name`(`sched_name`, `job_name`, `job_group`) USING BTREE,
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `job_name`, `job_group`) REFERENCES `qrtz_job_details` (`sched_name`, `job_name`, `job_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '触发器详细信息表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of qrtz_triggers
-- ----------------------------
INSERT INTO `qrtz_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', 'TASK_CLASS_NAME1', 'DEFAULT', NULL, 1709546690000, -1, 5, 'PAUSED', 'CRON', 1709546683000, 0, NULL, 2, '');
INSERT INTO `qrtz_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', 'TASK_CLASS_NAME2', 'DEFAULT', NULL, 1709546685000, -1, 5, 'PAUSED', 'CRON', 1709546684000, 0, NULL, 2, '');
INSERT INTO `qrtz_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', 'TASK_CLASS_NAME3', 'DEFAULT', NULL, 1709546700000, -1, 5, 'PAUSED', 'CRON', 1709546685000, 0, NULL, 2, '');

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `config_id` int(5) NOT NULL AUTO_INCREMENT COMMENT '参数主键',
  `config_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '参数名称',
  `config_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '参数键名',
  `config_value` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '参数键值',
  `config_type` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`config_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '参数配置表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1, '主框架页-默认皮肤样式名称', 'sys.index.skinName', 'skin-blue', 'Y', 'admin', '2024-01-12 16:20:35', '', NULL, '蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow');
INSERT INTO `sys_config` VALUES (2, '用户管理-账号初始密码', 'sys.userApp.initPassword', '123456', 'Y', 'admin', '2024-01-12 16:20:35', '', NULL, '初始化密码 123456');
INSERT INTO `sys_config` VALUES (3, '主框架页-侧边栏主题', 'sys.index.sideTheme', 'theme-dark', 'Y', 'admin', '2024-01-12 16:20:35', '', NULL, '深黑主题theme-dark，浅色主题theme-light，深蓝主题theme-blue');
INSERT INTO `sys_config` VALUES (4, '账号自助-是否开启用户注册功能', 'sys.account.registerUser', 'false', 'Y', 'admin', '2024-01-12 16:20:35', '', NULL, '是否开启注册用户功能（true开启，false关闭）');
INSERT INTO `sys_config` VALUES (5, '用户管理-密码字符范围', 'sys.account.chrtype', '0', 'Y', 'admin', '2024-01-12 16:20:35', '', NULL, '默认任意字符范围，0任意（密码可以输入任意字符），1数字（密码只能为0-9数字），2英文字母（密码只能为a-z和A-Z字母），3字母和数字（密码必须包含字母，数字）,4字母数字和特殊字符（目前支持的特殊字符包括：~!@#$%^&*()-=_+）');
INSERT INTO `sys_config` VALUES (6, '用户管理-初始密码修改策略', 'sys.account.initPasswordModify', '1', 'Y', 'admin', '2024-01-12 16:20:35', '', NULL, '0：初始密码修改策略关闭，没有任何提示，1：提醒用户，如果未修改初始密码，则在登录时就会提醒修改密码对话框');
INSERT INTO `sys_config` VALUES (7, '用户管理-账号密码更新周期', 'sys.account.passwordValidateDays', '0', 'Y', 'admin', '2024-01-12 16:20:35', '', NULL, '密码更新周期（填写数字，数据初始化值为0不限制，若修改必须为大于0小于365的正整数），如果超过这个周期登录系统时，则在登录时就会提醒修改密码对话框');
INSERT INTO `sys_config` VALUES (8, '主框架页-菜单导航显示风格', 'sys.index.menuStyle', 'default', 'Y', 'admin', '2024-01-12 16:20:35', '', NULL, '菜单导航显示风格（default为左侧导航菜单，topnav为顶部导航菜单）');
INSERT INTO `sys_config` VALUES (9, '主框架页-是否开启页脚', 'sys.index.footer', 'true', 'Y', 'admin', '2024-01-12 16:20:36', '', NULL, '是否开启底部页脚显示（true显示，false隐藏）');
INSERT INTO `sys_config` VALUES (10, '主框架页-是否开启页签', 'sys.index.tagsView', 'true', 'Y', 'admin', '2024-01-12 16:20:36', '', NULL, '是否开启菜单多页签显示（true显示，false隐藏）');
INSERT INTO `sys_config` VALUES (11, '用户登录-黑名单列表', 'sys.login.blackIPList', '', 'Y', 'admin', '2024-01-12 16:20:36', '', NULL, '设置登录IP黑名单限制，多个匹配项以;分隔，支持匹配（*通配、网段）');

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `dept_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父部门id',
  `ancestors` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '祖级列表',
  `dept_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '部门名称',
  `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
  `leader` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '负责人',
  `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '邮箱',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 110 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '部门表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (100, 0, '0', '若依科技', 0, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2024-01-12 16:20:18', '', NULL);
INSERT INTO `sys_dept` VALUES (101, 100, '0,100', '深圳总公司', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2024-01-12 16:20:18', '', NULL);
INSERT INTO `sys_dept` VALUES (102, 100, '0,100', '长沙分公司', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2024-01-12 16:20:18', '', NULL);
INSERT INTO `sys_dept` VALUES (103, 101, '0,100,101', '研发部门', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2024-01-12 16:20:18', '', NULL);
INSERT INTO `sys_dept` VALUES (104, 101, '0,100,101', '市场部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2024-01-12 16:20:18', '', NULL);
INSERT INTO `sys_dept` VALUES (105, 101, '0,100,101', '测试部门', 3, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2024-01-12 16:20:18', '', NULL);
INSERT INTO `sys_dept` VALUES (106, 101, '0,100,101', '财务部门', 4, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2024-01-12 16:20:18', '', NULL);
INSERT INTO `sys_dept` VALUES (107, 101, '0,100,101', '运维部门', 5, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2024-01-12 16:20:18', '', NULL);
INSERT INTO `sys_dept` VALUES (108, 102, '0,100,102', '市场部门', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2024-01-12 16:20:18', '', NULL);
INSERT INTO `sys_dept` VALUES (109, 102, '0,100,102', '财务部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2024-01-12 16:20:18', '', NULL);

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data`  (
  `dict_code` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int(4) NULL DEFAULT 0 COMMENT '字典排序',
  `dict_label` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '表格回显样式',
  `is_default` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 30 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '字典数据表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES (1, 1, '男', '0', 'sys_user_sex', '', '', 'Y', '0', 'admin', '2024-01-12 16:20:33', '', NULL, '性别男');
INSERT INTO `sys_dict_data` VALUES (2, 2, '女', '1', 'sys_user_sex', '', '', 'N', '0', 'admin', '2024-01-12 16:20:33', '', NULL, '性别女');
INSERT INTO `sys_dict_data` VALUES (3, 3, '未知', '2', 'sys_user_sex', '', '', 'N', '0', 'admin', '2024-01-12 16:20:33', '', NULL, '性别未知');
INSERT INTO `sys_dict_data` VALUES (4, 1, '显示', '0', 'sys_show_hide', '', 'primary', 'Y', '0', 'admin', '2024-01-12 16:20:33', '', NULL, '显示菜单');
INSERT INTO `sys_dict_data` VALUES (5, 2, '隐藏', '1', 'sys_show_hide', '', 'danger', 'N', '0', 'admin', '2024-01-12 16:20:33', '', NULL, '隐藏菜单');
INSERT INTO `sys_dict_data` VALUES (6, 1, '正常', '0', 'sys_normal_disable', '', 'primary', 'Y', '0', 'admin', '2024-01-12 16:20:33', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (7, 2, '停用', '1', 'sys_normal_disable', '', 'danger', 'N', '0', 'admin', '2024-01-12 16:20:33', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (8, 1, '正常', '0', 'sys_job_status', '', 'primary', 'Y', '0', 'admin', '2024-01-12 16:20:33', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (9, 2, '暂停', '1', 'sys_job_status', '', 'danger', 'N', '0', 'admin', '2024-01-12 16:20:33', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (10, 1, '默认', 'DEFAULT', 'sys_job_group', '', '', 'Y', '0', 'admin', '2024-01-12 16:20:34', '', NULL, '默认分组');
INSERT INTO `sys_dict_data` VALUES (11, 2, '系统', 'SYSTEM', 'sys_job_group', '', '', 'N', '0', 'admin', '2024-01-12 16:20:34', '', NULL, '系统分组');
INSERT INTO `sys_dict_data` VALUES (12, 1, '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', '0', 'admin', '2024-01-12 16:20:34', '', NULL, '系统默认是');
INSERT INTO `sys_dict_data` VALUES (13, 2, '否', 'N', 'sys_yes_no', '', 'danger', 'N', '0', 'admin', '2024-01-12 16:20:34', '', NULL, '系统默认否');
INSERT INTO `sys_dict_data` VALUES (14, 1, '通知', '1', 'sys_notice_type', '', 'warning', 'Y', '0', 'admin', '2024-01-12 16:20:34', '', NULL, '通知');
INSERT INTO `sys_dict_data` VALUES (15, 2, '公告', '2', 'sys_notice_type', '', 'success', 'N', '0', 'admin', '2024-01-12 16:20:34', '', NULL, '公告');
INSERT INTO `sys_dict_data` VALUES (16, 1, '正常', '0', 'sys_notice_status', '', 'primary', 'Y', '0', 'admin', '2024-01-12 16:20:34', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (17, 2, '关闭', '1', 'sys_notice_status', '', 'danger', 'N', '0', 'admin', '2024-01-12 16:20:34', '', NULL, '关闭状态');
INSERT INTO `sys_dict_data` VALUES (18, 99, '其他', '0', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2024-01-12 16:20:34', '', NULL, '其他操作');
INSERT INTO `sys_dict_data` VALUES (19, 1, '新增', '1', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2024-01-12 16:20:34', '', NULL, '新增操作');
INSERT INTO `sys_dict_data` VALUES (20, 2, '修改', '2', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2024-01-12 16:20:34', '', NULL, '修改操作');
INSERT INTO `sys_dict_data` VALUES (21, 3, '删除', '3', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2024-01-12 16:20:34', '', NULL, '删除操作');
INSERT INTO `sys_dict_data` VALUES (22, 4, '授权', '4', 'sys_oper_type', '', 'primary', 'N', '0', 'admin', '2024-01-12 16:20:34', '', NULL, '授权操作');
INSERT INTO `sys_dict_data` VALUES (23, 5, '导出', '5', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2024-01-12 16:20:34', '', NULL, '导出操作');
INSERT INTO `sys_dict_data` VALUES (24, 6, '导入', '6', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2024-01-12 16:20:34', '', NULL, '导入操作');
INSERT INTO `sys_dict_data` VALUES (25, 7, '强退', '7', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2024-01-12 16:20:34', '', NULL, '强退操作');
INSERT INTO `sys_dict_data` VALUES (26, 8, '生成代码', '8', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2024-01-12 16:20:34', '', NULL, '生成操作');
INSERT INTO `sys_dict_data` VALUES (27, 9, '清空数据', '9', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2024-01-12 16:20:34', '', NULL, '清空操作');
INSERT INTO `sys_dict_data` VALUES (28, 1, '成功', '0', 'sys_common_status', '', 'primary', 'N', '0', 'admin', '2024-01-12 16:20:34', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (29, 2, '失败', '1', 'sys_common_status', '', 'danger', 'N', '0', 'admin', '2024-01-12 16:20:34', '', NULL, '停用状态');

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`  (
  `dict_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '字典类型',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_id`) USING BTREE,
  UNIQUE INDEX `dict_type`(`dict_type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '字典类型表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES (1, '用户性别', 'sys_user_sex', '0', 'admin', '2024-01-12 16:20:32', '', NULL, '用户性别列表');
INSERT INTO `sys_dict_type` VALUES (2, '菜单状态', 'sys_show_hide', '0', 'admin', '2024-01-12 16:20:32', '', NULL, '菜单状态列表');
INSERT INTO `sys_dict_type` VALUES (3, '系统开关', 'sys_normal_disable', '0', 'admin', '2024-01-12 16:20:32', '', NULL, '系统开关列表');
INSERT INTO `sys_dict_type` VALUES (4, '任务状态', 'sys_job_status', '0', 'admin', '2024-01-12 16:20:32', '', NULL, '任务状态列表');
INSERT INTO `sys_dict_type` VALUES (5, '任务分组', 'sys_job_group', '0', 'admin', '2024-01-12 16:20:32', '', NULL, '任务分组列表');
INSERT INTO `sys_dict_type` VALUES (6, '系统是否', 'sys_yes_no', '0', 'admin', '2024-01-12 16:20:32', '', NULL, '系统是否列表');
INSERT INTO `sys_dict_type` VALUES (7, '通知类型', 'sys_notice_type', '0', 'admin', '2024-01-12 16:20:32', '', NULL, '通知类型列表');
INSERT INTO `sys_dict_type` VALUES (8, '通知状态', 'sys_notice_status', '0', 'admin', '2024-01-12 16:20:32', '', NULL, '通知状态列表');
INSERT INTO `sys_dict_type` VALUES (9, '操作类型', 'sys_oper_type', '0', 'admin', '2024-01-12 16:20:32', '', NULL, '操作类型列表');
INSERT INTO `sys_dict_type` VALUES (10, '系统状态', 'sys_common_status', '0', 'admin', '2024-01-12 16:20:33', '', NULL, '登录状态列表');

-- ----------------------------
-- Table structure for sys_job
-- ----------------------------
DROP TABLE IF EXISTS `sys_job`;
CREATE TABLE `sys_job`  (
  `job_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `job_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT 'DEFAULT' COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '调用目标字符串',
  `cron_expression` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT 'cron执行表达式',
  `misfire_policy` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '3' COMMENT '计划执行错误策略（1立即执行 2执行一次 3放弃执行）',
  `concurrent` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '1' COMMENT '是否并发执行（0允许 1禁止）',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '0' COMMENT '状态（0正常 1暂停）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '备注信息',
  PRIMARY KEY (`job_id`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '定时任务调度表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_job
-- ----------------------------
INSERT INTO `sys_job` VALUES (1, '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '0/10 * * * * ?', '3', '1', '1', 'admin', '2024-01-12 16:20:37', '', NULL, '');
INSERT INTO `sys_job` VALUES (2, '系统默认（有参）', 'DEFAULT', 'ryTask.ryParams(\'ry\')', '0/15 * * * * ?', '3', '1', '1', 'admin', '2024-01-12 16:20:37', '', NULL, '');
INSERT INTO `sys_job` VALUES (3, '系统默认（多参）', 'DEFAULT', 'ryTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)', '0/20 * * * * ?', '3', '1', '1', 'admin', '2024-01-12 16:20:37', '', NULL, '');

-- ----------------------------
-- Table structure for sys_job_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_job_log`;
CREATE TABLE `sys_job_log`  (
  `job_log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务日志ID',
  `job_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '调用目标字符串',
  `job_message` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '日志信息',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '0' COMMENT '执行状态（0正常 1失败）',
  `exception_info` varchar(2000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '异常信息',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`job_log_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '定时任务调度日志表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_job_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_logininfor
-- ----------------------------
DROP TABLE IF EXISTS `sys_logininfor`;
CREATE TABLE `sys_logininfor`  (
  `info_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '访问ID',
  `login_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '登录账号',
  `ipaddr` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '操作系统',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
  `msg` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '提示消息',
  `login_time` datetime(0) NULL DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY (`info_id`) USING BTREE,
  INDEX `idx_sys_logininfor_s`(`status`) USING BTREE,
  INDEX `idx_sys_logininfor_lt`(`login_time`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 116 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '系统访问记录' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_logininfor
-- ----------------------------
INSERT INTO `sys_logininfor` VALUES (100, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '1', '验证码错误', '2024-01-12 16:22:12');
INSERT INTO `sys_logininfor` VALUES (101, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2024-01-12 16:22:16');
INSERT INTO `sys_logininfor` VALUES (102, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2024-01-23 22:20:10');
INSERT INTO `sys_logininfor` VALUES (103, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2024-01-23 22:38:43');
INSERT INTO `sys_logininfor` VALUES (104, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '1', '验证码错误', '2024-01-23 22:41:38');
INSERT INTO `sys_logininfor` VALUES (105, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2024-01-23 22:41:41');
INSERT INTO `sys_logininfor` VALUES (106, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '1', '验证码错误', '2024-01-23 22:49:25');
INSERT INTO `sys_logininfor` VALUES (107, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '1', '验证码错误', '2024-01-23 22:49:31');
INSERT INTO `sys_logininfor` VALUES (108, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2024-01-23 22:49:34');
INSERT INTO `sys_logininfor` VALUES (109, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '1', '验证码错误', '2024-03-04 14:59:43');
INSERT INTO `sys_logininfor` VALUES (110, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2024-03-04 14:59:47');
INSERT INTO `sys_logininfor` VALUES (111, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2024-03-04 17:29:17');
INSERT INTO `sys_logininfor` VALUES (112, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2024-03-04 17:38:22');
INSERT INTO `sys_logininfor` VALUES (113, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2024-03-04 17:40:40');
INSERT INTO `sys_logininfor` VALUES (114, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2024-03-04 17:42:38');
INSERT INTO `sys_logininfor` VALUES (115, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2024-03-04 18:05:53');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '菜单名称',
  `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父菜单ID',
  `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
  `url` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '#' COMMENT '请求地址',
  `target` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '打开方式（menuItem页签 menuBlank新窗口）',
  `menu_type` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `is_refresh` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '1' COMMENT '是否刷新（0刷新 1不刷新）',
  `perms` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '#' COMMENT '菜单图标',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1069 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '菜单权限表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, '系统管理', 0, 1, '#', '', 'M', '0', '1', '', 'fa fa-gear', 'admin', '2024-01-12 16:20:20', '', NULL, '系统管理目录');
INSERT INTO `sys_menu` VALUES (2, '系统监控', 0, 2, '#', '', 'M', '0', '1', '', 'fa fa-video-camera', 'admin', '2024-01-12 16:20:20', '', NULL, '系统监控目录');
INSERT INTO `sys_menu` VALUES (3, '系统工具', 0, 3, '#', '', 'M', '0', '1', '', 'fa fa-bars', 'admin', '2024-01-12 16:20:20', '', NULL, '系统工具目录');
INSERT INTO `sys_menu` VALUES (4, '若依官网', 0, 4, 'http://ruoyi.vip', 'menuBlank', 'C', '0', '1', '', 'fa fa-location-arrow', 'admin', '2024-01-12 16:20:20', '', NULL, '若依官网地址');
INSERT INTO `sys_menu` VALUES (100, '用户管理', 1, 1, '/system/userApp', '', 'C', '0', '1', 'system:userApp:view', 'fa fa-userApp-o', 'admin', '2024-01-12 16:20:20', '', NULL, '用户管理菜单');
INSERT INTO `sys_menu` VALUES (101, '角色管理', 1, 2, '/system/role', '', 'C', '0', '1', 'system:role:view', 'fa fa-userApp-secret', 'admin', '2024-01-12 16:20:20', '', NULL, '角色管理菜单');
INSERT INTO `sys_menu` VALUES (102, '菜单管理', 1, 3, '/system/menu', '', 'C', '0', '1', 'system:menu:view', 'fa fa-th-list', 'admin', '2024-01-12 16:20:20', '', NULL, '菜单管理菜单');
INSERT INTO `sys_menu` VALUES (103, '部门管理', 1, 4, '/system/dept', '', 'C', '0', '1', 'system:dept:view', 'fa fa-outdent', 'admin', '2024-01-12 16:20:20', '', NULL, '部门管理菜单');
INSERT INTO `sys_menu` VALUES (104, '岗位管理', 1, 5, '/system/post', '', 'C', '0', '1', 'system:post:view', 'fa fa-address-card-o', 'admin', '2024-01-12 16:20:20', '', NULL, '岗位管理菜单');
INSERT INTO `sys_menu` VALUES (105, '字典管理', 1, 6, '/system/dict', '', 'C', '0', '1', 'system:dict:view', 'fa fa-bookmark-o', 'admin', '2024-01-12 16:20:20', '', NULL, '字典管理菜单');
INSERT INTO `sys_menu` VALUES (106, '参数设置', 1, 7, '/system/config', '', 'C', '0', '1', 'system:config:view', 'fa fa-sun-o', 'admin', '2024-01-12 16:20:21', '', NULL, '参数设置菜单');
INSERT INTO `sys_menu` VALUES (107, '通知公告', 1, 8, '/system/notice', '', 'C', '0', '1', 'system:notice:view', 'fa fa-bullhorn', 'admin', '2024-01-12 16:20:21', '', NULL, '通知公告菜单');
INSERT INTO `sys_menu` VALUES (108, '日志管理', 1, 9, '#', '', 'M', '0', '1', '', 'fa fa-pencil-square-o', 'admin', '2024-01-12 16:20:21', '', NULL, '日志管理菜单');
INSERT INTO `sys_menu` VALUES (109, '在线用户', 2, 1, '/monitor/online', '', 'C', '0', '1', 'monitor:online:view', 'fa fa-userApp-circle', 'admin', '2024-01-12 16:20:21', '', NULL, '在线用户菜单');
INSERT INTO `sys_menu` VALUES (110, '定时任务', 2, 2, '/monitor/job', '', 'C', '0', '1', 'monitor:job:view', 'fa fa-tasks', 'admin', '2024-01-12 16:20:21', '', NULL, '定时任务菜单');
INSERT INTO `sys_menu` VALUES (111, '数据监控', 2, 3, '/monitor/data', '', 'C', '0', '1', 'monitor:data:view', 'fa fa-bug', 'admin', '2024-01-12 16:20:21', '', NULL, '数据监控菜单');
INSERT INTO `sys_menu` VALUES (112, '服务监控', 2, 4, '/monitor/server', '', 'C', '0', '1', 'monitor:server:view', 'fa fa-server', 'admin', '2024-01-12 16:20:21', '', NULL, '服务监控菜单');
INSERT INTO `sys_menu` VALUES (113, '缓存监控', 2, 5, '/monitor/cache', '', 'C', '0', '1', 'monitor:cache:view', 'fa fa-cube', 'admin', '2024-01-12 16:20:21', '', NULL, '缓存监控菜单');
INSERT INTO `sys_menu` VALUES (114, '表单构建', 3, 1, '/tool/build', '', 'C', '0', '1', 'tool:build:view', 'fa fa-wpforms', 'admin', '2024-01-12 16:20:21', '', NULL, '表单构建菜单');
INSERT INTO `sys_menu` VALUES (115, '代码生成', 3, 2, '/tool/gen', '', 'C', '0', '1', 'tool:gen:view', 'fa fa-code', 'admin', '2024-01-12 16:20:21', '', NULL, '代码生成菜单');
INSERT INTO `sys_menu` VALUES (116, '系统接口', 3, 3, '/tool/swagger', '', 'C', '0', '1', 'tool:swagger:view', 'fa fa-gg', 'admin', '2024-01-12 16:20:21', '', NULL, '系统接口菜单');
INSERT INTO `sys_menu` VALUES (500, '操作日志', 108, 1, '/monitor/operlog', '', 'C', '0', '1', 'monitor:operlog:view', 'fa fa-address-book', 'admin', '2024-01-12 16:20:21', '', NULL, '操作日志菜单');
INSERT INTO `sys_menu` VALUES (501, '登录日志', 108, 2, '/monitor/logininfor', '', 'C', '0', '1', 'monitor:logininfor:view', 'fa fa-file-image-o', 'admin', '2024-01-12 16:20:21', '', NULL, '登录日志菜单');
INSERT INTO `sys_menu` VALUES (1000, '用户查询', 100, 1, '#', '', 'F', '0', '1', 'system:userApp:list', '#', 'admin', '2024-01-12 16:20:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1001, '用户新增', 100, 2, '#', '', 'F', '0', '1', 'system:userApp:add', '#', 'admin', '2024-01-12 16:20:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1002, '用户修改', 100, 3, '#', '', 'F', '0', '1', 'system:userApp:edit', '#', 'admin', '2024-01-12 16:20:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1003, '用户删除', 100, 4, '#', '', 'F', '0', '1', 'system:userApp:remove', '#', 'admin', '2024-01-12 16:20:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1004, '用户导出', 100, 5, '#', '', 'F', '0', '1', 'system:userApp:export', '#', 'admin', '2024-01-12 16:20:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1005, '用户导入', 100, 6, '#', '', 'F', '0', '1', 'system:userApp:import', '#', 'admin', '2024-01-12 16:20:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1006, '重置密码', 100, 7, '#', '', 'F', '0', '1', 'system:userApp:resetPwd', '#', 'admin', '2024-01-12 16:20:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1007, '角色查询', 101, 1, '#', '', 'F', '0', '1', 'system:role:list', '#', 'admin', '2024-01-12 16:20:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1008, '角色新增', 101, 2, '#', '', 'F', '0', '1', 'system:role:add', '#', 'admin', '2024-01-12 16:20:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1009, '角色修改', 101, 3, '#', '', 'F', '0', '1', 'system:role:edit', '#', 'admin', '2024-01-12 16:20:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1010, '角色删除', 101, 4, '#', '', 'F', '0', '1', 'system:role:remove', '#', 'admin', '2024-01-12 16:20:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1011, '角色导出', 101, 5, '#', '', 'F', '0', '1', 'system:role:export', '#', 'admin', '2024-01-12 16:20:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1012, '菜单查询', 102, 1, '#', '', 'F', '0', '1', 'system:menu:list', '#', 'admin', '2024-01-12 16:20:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1013, '菜单新增', 102, 2, '#', '', 'F', '0', '1', 'system:menu:add', '#', 'admin', '2024-01-12 16:20:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1014, '菜单修改', 102, 3, '#', '', 'F', '0', '1', 'system:menu:edit', '#', 'admin', '2024-01-12 16:20:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1015, '菜单删除', 102, 4, '#', '', 'F', '0', '1', 'system:menu:remove', '#', 'admin', '2024-01-12 16:20:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1016, '部门查询', 103, 1, '#', '', 'F', '0', '1', 'system:dept:list', '#', 'admin', '2024-01-12 16:20:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1017, '部门新增', 103, 2, '#', '', 'F', '0', '1', 'system:dept:add', '#', 'admin', '2024-01-12 16:20:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1018, '部门修改', 103, 3, '#', '', 'F', '0', '1', 'system:dept:edit', '#', 'admin', '2024-01-12 16:20:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1019, '部门删除', 103, 4, '#', '', 'F', '0', '1', 'system:dept:remove', '#', 'admin', '2024-01-12 16:20:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1020, '岗位查询', 104, 1, '#', '', 'F', '0', '1', 'system:post:list', '#', 'admin', '2024-01-12 16:20:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1021, '岗位新增', 104, 2, '#', '', 'F', '0', '1', 'system:post:add', '#', 'admin', '2024-01-12 16:20:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1022, '岗位修改', 104, 3, '#', '', 'F', '0', '1', 'system:post:edit', '#', 'admin', '2024-01-12 16:20:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1023, '岗位删除', 104, 4, '#', '', 'F', '0', '1', 'system:post:remove', '#', 'admin', '2024-01-12 16:20:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1024, '岗位导出', 104, 5, '#', '', 'F', '0', '1', 'system:post:export', '#', 'admin', '2024-01-12 16:20:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1025, '字典查询', 105, 1, '#', '', 'F', '0', '1', 'system:dict:list', '#', 'admin', '2024-01-12 16:20:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1026, '字典新增', 105, 2, '#', '', 'F', '0', '1', 'system:dict:add', '#', 'admin', '2024-01-12 16:20:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1027, '字典修改', 105, 3, '#', '', 'F', '0', '1', 'system:dict:edit', '#', 'admin', '2024-01-12 16:20:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1028, '字典删除', 105, 4, '#', '', 'F', '0', '1', 'system:dict:remove', '#', 'admin', '2024-01-12 16:20:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1029, '字典导出', 105, 5, '#', '', 'F', '0', '1', 'system:dict:export', '#', 'admin', '2024-01-12 16:20:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1030, '参数查询', 106, 1, '#', '', 'F', '0', '1', 'system:config:list', '#', 'admin', '2024-01-12 16:20:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1031, '参数新增', 106, 2, '#', '', 'F', '0', '1', 'system:config:add', '#', 'admin', '2024-01-12 16:20:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1032, '参数修改', 106, 3, '#', '', 'F', '0', '1', 'system:config:edit', '#', 'admin', '2024-01-12 16:20:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1033, '参数删除', 106, 4, '#', '', 'F', '0', '1', 'system:config:remove', '#', 'admin', '2024-01-12 16:20:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1034, '参数导出', 106, 5, '#', '', 'F', '0', '1', 'system:config:export', '#', 'admin', '2024-01-12 16:20:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1035, '公告查询', 107, 1, '#', '', 'F', '0', '1', 'system:notice:list', '#', 'admin', '2024-01-12 16:20:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1036, '公告新增', 107, 2, '#', '', 'F', '0', '1', 'system:notice:add', '#', 'admin', '2024-01-12 16:20:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1037, '公告修改', 107, 3, '#', '', 'F', '0', '1', 'system:notice:edit', '#', 'admin', '2024-01-12 16:20:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1038, '公告删除', 107, 4, '#', '', 'F', '0', '1', 'system:notice:remove', '#', 'admin', '2024-01-12 16:20:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1039, '操作查询', 500, 1, '#', '', 'F', '0', '1', 'monitor:operlog:list', '#', 'admin', '2024-01-12 16:20:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1040, '操作删除', 500, 2, '#', '', 'F', '0', '1', 'monitor:operlog:remove', '#', 'admin', '2024-01-12 16:20:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1041, '详细信息', 500, 3, '#', '', 'F', '0', '1', 'monitor:operlog:detail', '#', 'admin', '2024-01-12 16:20:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1042, '日志导出', 500, 4, '#', '', 'F', '0', '1', 'monitor:operlog:export', '#', 'admin', '2024-01-12 16:20:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1043, '登录查询', 501, 1, '#', '', 'F', '0', '1', 'monitor:logininfor:list', '#', 'admin', '2024-01-12 16:20:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1044, '登录删除', 501, 2, '#', '', 'F', '0', '1', 'monitor:logininfor:remove', '#', 'admin', '2024-01-12 16:20:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1045, '日志导出', 501, 3, '#', '', 'F', '0', '1', 'monitor:logininfor:export', '#', 'admin', '2024-01-12 16:20:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1046, '账户解锁', 501, 4, '#', '', 'F', '0', '1', 'monitor:logininfor:unlock', '#', 'admin', '2024-01-12 16:20:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1047, '在线查询', 109, 1, '#', '', 'F', '0', '1', 'monitor:online:list', '#', 'admin', '2024-01-12 16:20:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1048, '批量强退', 109, 2, '#', '', 'F', '0', '1', 'monitor:online:batchForceLogout', '#', 'admin', '2024-01-12 16:20:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1049, '单条强退', 109, 3, '#', '', 'F', '0', '1', 'monitor:online:forceLogout', '#', 'admin', '2024-01-12 16:20:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1050, '任务查询', 110, 1, '#', '', 'F', '0', '1', 'monitor:job:list', '#', 'admin', '2024-01-12 16:20:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1051, '任务新增', 110, 2, '#', '', 'F', '0', '1', 'monitor:job:add', '#', 'admin', '2024-01-12 16:20:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1052, '任务修改', 110, 3, '#', '', 'F', '0', '1', 'monitor:job:edit', '#', 'admin', '2024-01-12 16:20:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1053, '任务删除', 110, 4, '#', '', 'F', '0', '1', 'monitor:job:remove', '#', 'admin', '2024-01-12 16:20:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1054, '状态修改', 110, 5, '#', '', 'F', '0', '1', 'monitor:job:changeStatus', '#', 'admin', '2024-01-12 16:20:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1055, '任务详细', 110, 6, '#', '', 'F', '0', '1', 'monitor:job:detail', '#', 'admin', '2024-01-12 16:20:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1056, '任务导出', 110, 7, '#', '', 'F', '0', '1', 'monitor:job:export', '#', 'admin', '2024-01-12 16:20:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1057, '生成查询', 115, 1, '#', '', 'F', '0', '1', 'tool:gen:list', '#', 'admin', '2024-01-12 16:20:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1058, '生成修改', 115, 2, '#', '', 'F', '0', '1', 'tool:gen:edit', '#', 'admin', '2024-01-12 16:20:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1059, '生成删除', 115, 3, '#', '', 'F', '0', '1', 'tool:gen:remove', '#', 'admin', '2024-01-12 16:20:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1060, '预览代码', 115, 4, '#', '', 'F', '0', '1', 'tool:gen:preview', '#', 'admin', '2024-01-12 16:20:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1061, '生成代码', 115, 5, '#', '', 'F', '0', '1', 'tool:gen:code', '#', 'admin', '2024-01-12 16:20:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1062, '会员用户管理', 0, 1, '#', 'menuItem', 'C', '0', '1', NULL, '#', 'admin', '2024-01-23 22:29:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1063, '会员用户', 1062, 1, '/userApp/userApp', '', 'C', '0', '1', 'userApp:userApp:view', '#', 'admin', '2024-01-23 22:36:59', '', NULL, '会员用户菜单');
INSERT INTO `sys_menu` VALUES (1064, '会员用户查询', 1063, 1, '#', '', 'F', '0', '1', 'userApp:userApp:list', '#', 'admin', '2024-01-23 22:36:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1065, '会员用户新增', 1063, 2, '#', '', 'F', '0', '1', 'userApp:userApp:add', '#', 'admin', '2024-01-23 22:36:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1066, '会员用户修改', 1063, 3, '#', '', 'F', '0', '1', 'userApp:userApp:edit', '#', 'admin', '2024-01-23 22:37:00', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1067, '会员用户删除', 1063, 4, '#', '', 'F', '0', '1', 'userApp:userApp:remove', '#', 'admin', '2024-01-23 22:37:00', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1068, '会员用户导出', 1063, 5, '#', '', 'F', '0', '1', 'userApp:userApp:export', '#', 'admin', '2024-01-23 22:37:00', '', NULL, '');

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice`  (
  `notice_id` int(4) NOT NULL AUTO_INCREMENT COMMENT '公告ID',
  `notice_title` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '公告标题',
  `notice_type` char(1) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '公告类型（1通知 2公告）',
  `notice_content` longblob NULL COMMENT '公告内容',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '0' COMMENT '公告状态（0正常 1关闭）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`notice_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '通知公告表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_notice
-- ----------------------------
INSERT INTO `sys_notice` VALUES (1, '温馨提醒：2018-07-01 若依新版本发布啦', '2', 0xE696B0E78988E69CACE58685E5AEB9, '0', 'admin', '2024-01-12 16:20:39', '', NULL, '管理员');
INSERT INTO `sys_notice` VALUES (2, '维护通知：2018-07-01 若依系统凌晨维护', '1', 0xE7BBB4E68AA4E58685E5AEB9, '0', 'admin', '2024-01-12 16:20:39', '', NULL, '管理员');
INSERT INTO `sys_notice` VALUES (3, '若依开源框架介绍', '1', 0x3C703E3C7370616E207374796C653D22636F6C6F723A20726762283233302C20302C2030293B223EE9A1B9E79BAEE4BB8BE7BB8D3C2F7370616E3E3C2F703E3C703E3C666F6E7420636F6C6F723D2223333333333333223E52756F5969E5BC80E6BA90E9A1B9E79BAEE698AFE4B8BAE4BC81E4B89AE794A8E688B7E5AE9AE588B6E79A84E5908EE58FB0E8849AE6898BE69EB6E6A186E69EB6EFBC8CE4B8BAE4BC81E4B89AE68993E980A0E79A84E4B880E7AB99E5BC8FE8A7A3E586B3E696B9E6A188EFBC8CE9998DE4BD8EE4BC81E4B89AE5BC80E58F91E68890E69CACEFBC8CE68F90E58D87E5BC80E58F91E69588E78E87E38082E4B8BBE8A681E58C85E68BACE794A8E688B7E7AEA1E79086E38081E8A792E889B2E7AEA1E79086E38081E983A8E997A8E7AEA1E79086E38081E88F9CE58D95E7AEA1E79086E38081E58F82E695B0E7AEA1E79086E38081E5AD97E585B8E7AEA1E79086E380813C2F666F6E743E3C7370616E207374796C653D22636F6C6F723A207267622835312C2035312C203531293B223EE5B297E4BD8DE7AEA1E790863C2F7370616E3E3C7370616E207374796C653D22636F6C6F723A207267622835312C2035312C203531293B223EE38081E5AE9AE697B6E4BBBBE58AA13C2F7370616E3E3C7370616E207374796C653D22636F6C6F723A207267622835312C2035312C203531293B223EE380813C2F7370616E3E3C7370616E207374796C653D22636F6C6F723A207267622835312C2035312C203531293B223EE69C8DE58AA1E79B91E68EA7E38081E799BBE5BD95E697A5E5BF97E38081E6938DE4BD9CE697A5E5BF97E38081E4BBA3E7A081E7949FE68890E7AD89E58A9FE883BDE38082E585B6E4B8ADEFBC8CE8BF98E694AFE68C81E5A49AE695B0E68DAEE6BA90E38081E695B0E68DAEE69D83E99990E38081E59BBDE99985E58C96E380815265646973E7BC93E5AD98E38081446F636B6572E983A8E7BDB2E38081E6BB91E58AA8E9AA8CE8AF81E7A081E38081E7ACACE4B889E696B9E8AEA4E8AF81E799BBE5BD95E38081E58886E5B883E5BC8FE4BA8BE58AA1E380813C2F7370616E3E3C666F6E7420636F6C6F723D2223333333333333223EE58886E5B883E5BC8FE69687E4BBB6E5AD98E582A83C2F666F6E743E3C7370616E207374796C653D22636F6C6F723A207267622835312C2035312C203531293B223EE38081E58886E5BA93E58886E8A1A8E5A484E79086E7AD89E68A80E69CAFE789B9E782B9E380823C2F7370616E3E3C2F703E3C703E3C696D67207372633D2268747470733A2F2F666F727564612E67697465652E636F6D2F696D616765732F313730353033303538333937373430313635312F35656435646236615F313135313030342E706E6722207374796C653D2277696474683A20363470783B223E3C62723E3C2F703E3C703E3C7370616E207374796C653D22636F6C6F723A20726762283233302C20302C2030293B223EE5AE98E7BD91E58F8AE6BC94E7A4BA3C2F7370616E3E3C2F703E3C703E3C7370616E207374796C653D22636F6C6F723A207267622835312C2035312C203531293B223EE88BA5E4BE9DE5AE98E7BD91E59CB0E59D80EFBC9A266E6273703B3C2F7370616E3E3C6120687265663D22687474703A2F2F72756F79692E76697022207461726765743D225F626C616E6B223E687474703A2F2F72756F79692E7669703C2F613E3C6120687265663D22687474703A2F2F72756F79692E76697022207461726765743D225F626C616E6B223E3C2F613E3C2F703E3C703E3C7370616E207374796C653D22636F6C6F723A207267622835312C2035312C203531293B223EE88BA5E4BE9DE69687E6A1A3E59CB0E59D80EFBC9A266E6273703B3C2F7370616E3E3C6120687265663D22687474703A2F2F646F632E72756F79692E76697022207461726765743D225F626C616E6B223E687474703A2F2F646F632E72756F79692E7669703C2F613E3C62723E3C2F703E3C703E3C7370616E207374796C653D22636F6C6F723A207267622835312C2035312C203531293B223EE6BC94E7A4BAE59CB0E59D80E38090E4B88DE58886E7A6BBE78988E38091EFBC9A266E6273703B3C2F7370616E3E3C6120687265663D22687474703A2F2F64656D6F2E72756F79692E76697022207461726765743D225F626C616E6B223E687474703A2F2F64656D6F2E72756F79692E7669703C2F613E3C2F703E3C703E3C7370616E207374796C653D22636F6C6F723A207267622835312C2035312C203531293B223EE6BC94E7A4BAE59CB0E59D80E38090E58886E7A6BBE78988E69CACE38091EFBC9A266E6273703B3C2F7370616E3E3C6120687265663D22687474703A2F2F7675652E72756F79692E76697022207461726765743D225F626C616E6B223E687474703A2F2F7675652E72756F79692E7669703C2F613E3C2F703E3C703E3C7370616E207374796C653D22636F6C6F723A207267622835312C2035312C203531293B223EE6BC94E7A4BAE59CB0E59D80E38090E5BEAEE69C8DE58AA1E78988E38091EFBC9A266E6273703B3C2F7370616E3E3C6120687265663D22687474703A2F2F636C6F75642E72756F79692E76697022207461726765743D225F626C616E6B223E687474703A2F2F636C6F75642E72756F79692E7669703C2F613E3C2F703E3C703E3C7370616E207374796C653D22636F6C6F723A207267622835312C2035312C203531293B223EE6BC94E7A4BAE59CB0E59D80E38090E7A7BBE58AA8E7ABAFE78988E38091EFBC9A266E6273703B3C2F7370616E3E3C6120687265663D22687474703A2F2F68352E72756F79692E76697022207461726765743D225F626C616E6B223E687474703A2F2F68352E72756F79692E7669703C2F613E3C2F703E3C703E3C6272207374796C653D22636F6C6F723A207267622834382C2034392C203531293B20666F6E742D66616D696C793A202671756F743B48656C766574696361204E6575652671756F743B2C2048656C7665746963612C20417269616C2C2073616E732D73657269663B20666F6E742D73697A653A20313270783B223E3C2F703E, '0', 'admin', '2024-01-12 16:20:39', '', NULL, '管理员');

-- ----------------------------
-- Table structure for sys_oper_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_oper_log`;
CREATE TABLE `sys_oper_log`  (
  `oper_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '模块标题',
  `business_type` int(2) NULL DEFAULT 0 COMMENT '业务类型（0其它 1新增 2修改 3删除）',
  `method` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '方法名称',
  `request_method` varchar(10) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '请求方式',
  `operator_type` int(1) NULL DEFAULT 0 COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
  `oper_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '操作人员',
  `dept_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '部门名称',
  `oper_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '主机地址',
  `oper_location` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '操作地点',
  `oper_param` varchar(2000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '请求参数',
  `json_result` varchar(2000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '返回参数',
  `status` int(1) NULL DEFAULT 0 COMMENT '操作状态（0正常 1异常）',
  `error_msg` varchar(2000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '错误消息',
  `oper_time` datetime(0) NULL DEFAULT NULL COMMENT '操作时间',
  `cost_time` bigint(20) NULL DEFAULT 0 COMMENT '消耗时间',
  PRIMARY KEY (`oper_id`) USING BTREE,
  INDEX `idx_sys_oper_log_bt`(`business_type`) USING BTREE,
  INDEX `idx_sys_oper_log_s`(`status`) USING BTREE,
  INDEX `idx_sys_oper_log_ot`(`oper_time`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '操作日志记录' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_oper_log
-- ----------------------------
INSERT INTO `sys_oper_log` VALUES (1, '代码生成', 6, 'com.ruoyi.project.tool.gen.controller.GenController.importTableSave()', 'POST', 1, 'admin', '研发部门', '/tool/gen/importTable', '127.0.0.1', '内网IP', '{\"tables\":[\"t_user\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2024-01-23 22:27:45', 304);
INSERT INTO `sys_oper_log` VALUES (2, '菜单管理', 1, 'com.ruoyi.project.system.menu.controller.MenuController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/menu/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"0\"],\"menuType\":[\"C\"],\"menuName\":[\"会员用户管理\"],\"url\":[\"\"],\"target\":[\"menuItem\"],\"perms\":[\"\"],\"orderNum\":[\"1\"],\"icon\":[\"\"],\"visible\":[\"0\"],\"isRefresh\":[\"1\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2024-01-23 22:29:59', 73);
INSERT INTO `sys_oper_log` VALUES (3, '代码生成', 2, 'com.ruoyi.project.tool.gen.controller.GenController.synchDb()', 'GET', 1, 'admin', '研发部门', '/tool/gen/synchDb/t_user', '127.0.0.1', '内网IP', '\"t_user\"', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2024-01-23 22:31:21', 122);
INSERT INTO `sys_oper_log` VALUES (4, '代码生成', 2, 'com.ruoyi.project.tool.gen.controller.GenController.editSave()', 'POST', 1, 'admin', '研发部门', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\"tableId\":[\"1\"],\"tableName\":[\"t_user\"],\"tableComment\":[\"会员用户\"],\"className\":[\"TUser\"],\"functionAuthor\":[\"ruoyi\"],\"remark\":[\"\"],\"columns[0].columnId\":[\"1\"],\"columns[0].sort\":[\"1\"],\"columns[0].columnComment\":[\"主键ID\"],\"columns[0].javaType\":[\"Long\"],\"columns[0].javaField\":[\"id\"],\"columns[0].isInsert\":[\"1\"],\"columns[0].queryType\":[\"EQ\"],\"columns[0].htmlType\":[\"input\"],\"columns[0].dictType\":[\"\"],\"columns[1].columnId\":[\"2\"],\"columns[1].sort\":[\"2\"],\"columns[1].columnComment\":[\"用户名称\"],\"columns[1].javaType\":[\"String\"],\"columns[1].javaField\":[\"userName\"],\"columns[1].isInsert\":[\"1\"],\"columns[1].isEdit\":[\"1\"],\"columns[1].isList\":[\"1\"],\"columns[1].isQuery\":[\"1\"],\"columns[1].queryType\":[\"LIKE\"],\"columns[1].htmlType\":[\"input\"],\"columns[1].dictType\":[\"\"],\"columns[2].columnId\":[\"3\"],\"columns[2].sort\":[\"3\"],\"columns[2].columnComment\":[\"密码\"],\"columns[2].javaType\":[\"String\"],\"columns[2].javaField\":[\"password\"],\"columns[2].isInsert\":[\"1\"],\"columns[2].isEdit\":[\"1\"],\"columns[2].isList\":[\"1\"],\"columns[2].isQuery\":[\"1\"],\"columns[2].queryType\":[\"EQ\"],\"columns[2].htmlType\":[\"input\"],\"columns[2].dictType\":[\"\"],\"columns[3].columnId\":[\"4\"],\"columns[3].sort\":[\"4\"],\"columns[3].columnComment\":[\"手机号\"],\"columns[3].javaType\":[\"String\"],\"columns[3].javaField\":[\"mobile\"],\"columns[3].isInsert\":[\"1\"],\"columns[3].isEdit\":[\"1\"],\"columns[3].isList\":[\"1\"],\"columns[3].isQuery\":[\"1\"],\"columns[3].queryType\":[\"EQ\"],\"columns[3].htmlType\":[\"input\"],\"columns[3].dictType\":[\"\"],\"columns[4].columnId\":[\"5\"],\"columns[4].sort\":[\"5\"],\"columns[4].columnComment\":[\"头像\"],\"columns[4].javaType\":[\"String\"],\"columns[4].javaField\":[\"photo\"],\"columns[4].isInsert\":[\"1\"],\"columns[4].isEdit\":[\"1\"],\"columns[4].isList\":[\"1\"],\"columns[4].isQuery\":[\"1\"],\"columns[4].queryType\":[\"EQ\"],\"columns[4].htmlType\":[\"input\"],\"columns[4].dictType\":[\"\"],\"columns[5].columnId\":[\"6\"],\"columns[5].sort\":[\"6\"],\"columns[5].columnComment\":[\"创建时间\"],\"columns[5].javaType\":[\"Date\"],\"columns[5].javaField\":[\"createTime\"],\"columns[5].isInsert\":[\"1\"],\"columns[5].queryT', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2024-01-23 22:32:28', 130);
INSERT INTO `sys_oper_log` VALUES (5, '代码生成', 8, 'com.ruoyi.project.tool.gen.controller.GenController.download()', 'GET', 1, 'admin', '研发部门', '/tool/gen/download/t_user', '127.0.0.1', '内网IP', '\"t_user\"', NULL, 0, NULL, '2024-01-23 22:32:31', 253);
INSERT INTO `sys_oper_log` VALUES (6, '代码生成', 2, 'com.ruoyi.project.tool.gen.controller.GenController.editSave()', 'POST', 1, 'admin', '研发部门', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\"tableId\":[\"1\"],\"tableName\":[\"t_user\"],\"tableComment\":[\"会员用户\"],\"className\":[\"TUser\"],\"functionAuthor\":[\"ruoyi\"],\"remark\":[\"\"],\"columns[0].columnId\":[\"1\"],\"columns[0].sort\":[\"1\"],\"columns[0].columnComment\":[\"主键ID\"],\"columns[0].javaType\":[\"Long\"],\"columns[0].javaField\":[\"id\"],\"columns[0].isInsert\":[\"1\"],\"columns[0].queryType\":[\"EQ\"],\"columns[0].htmlType\":[\"input\"],\"columns[0].dictType\":[\"\"],\"columns[1].columnId\":[\"2\"],\"columns[1].sort\":[\"2\"],\"columns[1].columnComment\":[\"用户名称\"],\"columns[1].javaType\":[\"String\"],\"columns[1].javaField\":[\"userName\"],\"columns[1].isInsert\":[\"1\"],\"columns[1].isEdit\":[\"1\"],\"columns[1].isList\":[\"1\"],\"columns[1].isQuery\":[\"1\"],\"columns[1].queryType\":[\"LIKE\"],\"columns[1].htmlType\":[\"input\"],\"columns[1].dictType\":[\"\"],\"columns[2].columnId\":[\"3\"],\"columns[2].sort\":[\"3\"],\"columns[2].columnComment\":[\"密码\"],\"columns[2].javaType\":[\"String\"],\"columns[2].javaField\":[\"password\"],\"columns[2].isInsert\":[\"1\"],\"columns[2].isEdit\":[\"1\"],\"columns[2].isList\":[\"1\"],\"columns[2].isQuery\":[\"1\"],\"columns[2].queryType\":[\"EQ\"],\"columns[2].htmlType\":[\"input\"],\"columns[2].dictType\":[\"\"],\"columns[3].columnId\":[\"4\"],\"columns[3].sort\":[\"4\"],\"columns[3].columnComment\":[\"手机号\"],\"columns[3].javaType\":[\"String\"],\"columns[3].javaField\":[\"mobile\"],\"columns[3].isInsert\":[\"1\"],\"columns[3].isEdit\":[\"1\"],\"columns[3].isList\":[\"1\"],\"columns[3].isQuery\":[\"1\"],\"columns[3].queryType\":[\"EQ\"],\"columns[3].htmlType\":[\"input\"],\"columns[3].dictType\":[\"\"],\"columns[4].columnId\":[\"5\"],\"columns[4].sort\":[\"5\"],\"columns[4].columnComment\":[\"头像\"],\"columns[4].javaType\":[\"String\"],\"columns[4].javaField\":[\"photo\"],\"columns[4].isInsert\":[\"1\"],\"columns[4].isEdit\":[\"1\"],\"columns[4].isList\":[\"1\"],\"columns[4].isQuery\":[\"1\"],\"columns[4].queryType\":[\"EQ\"],\"columns[4].htmlType\":[\"input\"],\"columns[4].dictType\":[\"\"],\"columns[5].columnId\":[\"6\"],\"columns[5].sort\":[\"6\"],\"columns[5].columnComment\":[\"创建时间\"],\"columns[5].javaType\":[\"Date\"],\"columns[5].javaField\":[\"createTime\"],\"columns[5].isInsert\":[\"1\"],\"columns[5].queryT', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2024-01-23 22:34:57', 94);
INSERT INTO `sys_oper_log` VALUES (7, '代码生成', 8, 'com.ruoyi.project.tool.gen.controller.GenController.download()', 'GET', 1, 'admin', '研发部门', '/tool/gen/download/t_user', '127.0.0.1', '内网IP', '\"t_user\"', NULL, 0, NULL, '2024-01-23 22:35:00', 55);
INSERT INTO `sys_oper_log` VALUES (8, '角色管理', 2, 'com.ruoyi.project.system.role.controller.RoleController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/role/edit', '127.0.0.1', '内网IP', '{\"roleId\":[\"2\"],\"roleName\":[\"普通角色\"],\"roleKey\":[\"common\"],\"roleSort\":[\"2\"],\"status\":[\"0\"],\"remark\":[\"普通角色\"],\"menuIds\":[\"1,100,1000,1001,1002,1003,1004,1005,1006,101,1007,1008,1009,1010,1011,102,1012,1013,1014,1015,103,1016,1017,1018,1019,104,1020,1021,1022,1023,1024,105,1025,1026,1027,1028,1029,106,1030,1031,1032,1033,1034,107,1035,1036,1037,1038,108,500,1039,1040,1041,1042,501,1043,1044,1045,1046,1062,1063,1064,1065,1066,1067,1068,2,109,1047,1048,1049,110,1050,1051,1052,1053,1054,1055,1056,111,112,113,3,114,115,1057,1058,1059,1060,1061,116,4\"]}', '{\"msg\":\"操作成功\",\"code\":0}', 0, NULL, '2024-01-23 22:43:48', 243);
INSERT INTO `sys_oper_log` VALUES (9, 'APP接口管理', 0, 'com.ruoyi.project.api.userApp.WebUserController.loginByMobile()', 'POST', 1, NULL, NULL, '/api/userApp/login', '127.0.0.1', '内网IP', '{\"mobile\":[\"13267180272\"],\"inviteCode\":[\"0\"],\"channel\":[\"1\"]}', '{\"msg\":\"登录失败 \\r\\n### Error updating database.  Cause: java.sql.SQLException: Field \'create_time\' doesn\'t have a default value\\r\\n### The error may exist in file [F:\\\\util-project\\\\2024\\\\zhengkai4\\\\RuoYi-fast-master\\\\target\\\\classes\\\\mybatis\\\\userApp\\\\TUserMapper.xml]\\r\\n### The error may involve com.ruoyi.project.userApp.mapper.TUserMapper.insertTUser-Inline\\r\\n### The error occurred while setting parameters\\r\\n### SQL: insert into t_user          ( user_name,                          mobile,                                       last_login_ip,             last_login_time,                          user_status )           values ( ?,                          ?,                                       ?,             ?,                          ? )\\r\\n### Cause: java.sql.SQLException: Field \'create_time\' doesn\'t have a default value\\n; Field \'create_time\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'create_time\' doesn\'t have a default value\",\"code\":500}', 0, NULL, '2024-03-04 14:36:47', 156);
INSERT INTO `sys_oper_log` VALUES (10, 'APP接口管理', 0, 'com.ruoyi.project.api.userApp.WebUserController.loginByMobile()', 'POST', 1, NULL, NULL, '/api/userApp/login', '127.0.0.1', '内网IP', '{\"mobile\":[\"13267180272\"],\"inviteCode\":[\"0\"],\"channel\":[\"1\"]}', '{\"msg\":\"操作成功\",\"code\":0,\"data\":{\"token\":\"0jxKLu1nx9QABpI2BAIm+A==\"}}', 0, NULL, '2024-03-04 14:39:56', 678);
INSERT INTO `sys_oper_log` VALUES (11, 'APP接口管理', 0, 'com.ruoyi.project.api.userApp.WebUserController.loginByMobile()', 'POST', 1, NULL, NULL, '/api/userApp/login', '127.0.0.1', '内网IP', '{\"mobile\":[\"13267180272\"],\"inviteCode\":[\"0\"],\"channel\":[\"1\"]}', '{\"msg\":\"操作成功\",\"code\":0,\"data\":{\"token\":\"BEAHn2hMCjArZa9BEAFHKQ==\"}}', 0, NULL, '2024-03-04 14:40:16', 10);
INSERT INTO `sys_oper_log` VALUES (12, '用户管理', 2, 'com.ruoyi.project.system.userApp.controller.UserController.editSave()', 'POST', 1, 'admin', NULL, '/system/userApp/edit', '127.0.0.1', '内网IP', '{\"userId\":[\"1\"],\"deptId\":[\"103\"],\"userName\":[\"若依\"],\"dept.deptName\":[\"\"],\"phonenumber\":[\"15888888888\"],\"loginName\":[\"admin\"],\"sex\":[\"1\"],\"remark\":[\"管理员\"],\"status\":[\"0\"],\"roleIds\":[\"\"],\"postIds\":[\"\"]}', '', 0, NULL, '2024-03-04 17:29:42', 0);
INSERT INTO `sys_oper_log` VALUES (13, '用户管理', 2, 'com.ruoyi.project.system.userApp.controller.UserController.editSave()', 'POST', 1, 'admin', NULL, '/system/userApp/edit', '127.0.0.1', '内网IP', '{\"userId\":[\"2\"],\"deptId\":[\"105\"],\"userName\":[\"若依\"],\"dept.deptName\":[\"\"],\"phonenumber\":[\"15666666666\"],\"loginName\":[\"ry\"],\"sex\":[\"1\"],\"remark\":[\"测试员\"],\"status\":[\"0\"],\"roleIds\":[\"\"],\"postIds\":[\"\"]}', '', 0, NULL, '2024-03-04 17:29:48', 0);
INSERT INTO `sys_oper_log` VALUES (14, '菜单管理', 3, 'com.ruoyi.project.system.menu.controller.MenuController.remove()', 'GET', 1, 'admin', NULL, '/system/menu/remove/4', '127.0.0.1', '内网IP', '{}', '', 0, NULL, '2024-03-04 17:30:44', 0);
INSERT INTO `sys_oper_log` VALUES (15, 'APP接口管理', 0, 'com.ruoyi.project.bajiaostar.userApp.WebUserController.loginByMobile()', 'POST', 1, NULL, NULL, '/nt/userApp/login', '127.0.0.1', '内网IP', '{\"mobile\":[\"13267180272\"],\"inviteCode\":[\"0\"],\"channel\":[\"1\"]}', '', 0, NULL, '2024-03-04 17:50:27', 0);
INSERT INTO `sys_oper_log` VALUES (16, 'APP接口管理', 0, 'com.ruoyi.project.bajiaostar.userApp.WebUserController.updateUser()', 'POST', 1, NULL, NULL, '/nt/userApp/updateUser', '127.0.0.1', '内网IP', '{\"photo\":[\"http://www.baidu.com\"],\"userName\":[\"00000000\"]}', '', 0, NULL, '2024-03-04 17:50:49', 0);
INSERT INTO `sys_oper_log` VALUES (17, 'APP接口管理', 0, 'com.ruoyi.project.bajiaostar.userApp.WebUserController.updateUser()', 'POST', 1, NULL, NULL, '/nt/userApp/updateUser', '127.0.0.1', '内网IP', '{\"photo\":[\"http://www.baidu.com\"],\"userName\":[\"00000000\"]}', '', 0, NULL, '2024-03-04 17:56:25', 0);
INSERT INTO `sys_oper_log` VALUES (18, 'APP接口管理', 0, 'com.ruoyi.project.bajiaostar.userApp.WebUserController.loginByMobile()', 'POST', 1, NULL, NULL, '/nt/userApp/login', '127.0.0.1', '内网IP', '{\"mobile\":[\"13267180272\"],\"inviteCode\":[\"0\"],\"channel\":[\"1\"]}', '', 0, NULL, '2024-03-04 17:57:01', 0);
INSERT INTO `sys_oper_log` VALUES (19, 'APP接口管理', 0, 'com.ruoyi.project.bajiaostar.userApp.WebUserController.updateUser()', 'POST', 1, NULL, NULL, '/nt/userApp/updateUser', '127.0.0.1', '内网IP', '{\"photo\":[\"http://www.baidu.com\"],\"userName\":[\"00000000\"]}', '', 0, NULL, '2024-03-04 17:58:06', 0);
INSERT INTO `sys_oper_log` VALUES (20, 'APP接口管理', 0, 'com.ruoyi.project.bajiaostar.userApp.WebUserController.updateUser()', 'POST', 1, NULL, NULL, '/nt/userApp/updateUser', '127.0.0.1', '内网IP', '{\"photo\":[\"http://www.baidu.com\"],\"userName\":[\"00000000\"]}', '', 0, NULL, '2024-03-04 18:00:08', 0);
INSERT INTO `sys_oper_log` VALUES (21, 'APP接口管理', 0, 'com.ruoyi.project.bajiaostar.userApp.WebUserController.updateUser()', 'POST', 1, NULL, NULL, '/nt/userApp/updateUser', '127.0.0.1', '内网IP', '{\"photo\":[\"http://www.baidu.com\"],\"userName\":[\"00000000\"]}', '', 0, NULL, '2024-03-04 18:02:19', 0);
INSERT INTO `sys_oper_log` VALUES (22, 'APP接口管理', 0, 'com.ruoyi.project.bajiaostar.userApp.WebUserController.loginByMobile()', 'POST', 1, NULL, NULL, '/nt/userApp/login', '127.0.0.1', '内网IP', '{\"mobile\":[\"13267180272\"],\"inviteCode\":[\"0\"],\"channel\":[\"1\"]}', '', 0, NULL, '2024-03-04 18:02:25', 0);
INSERT INTO `sys_oper_log` VALUES (23, 'APP接口管理', 0, 'com.ruoyi.project.bajiaostar.userApp.WebUserController.updateUser()', 'POST', 1, NULL, NULL, '/nt/userApp/updateUser', '127.0.0.1', '内网IP', '{\"photo\":[\"http://www.baidu.com\"],\"userName\":[\"00000000\"]}', '', 0, NULL, '2024-03-04 18:03:28', 0);
INSERT INTO `sys_oper_log` VALUES (24, 'APP接口管理', 0, 'com.ruoyi.project.bajiaostar.userApp.WebUserController.updateUser()', 'POST', 1, NULL, NULL, '/nt/userApp/updateUser', '127.0.0.1', '内网IP', '{\"photo\":[\"http://www.baidu.com\"],\"userName\":[\"00000000\"]}', '', 0, NULL, '2024-03-04 18:05:35', 0);

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post`  (
  `post_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '岗位ID',
  `post_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '岗位编码',
  `post_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '岗位名称',
  `post_sort` int(4) NOT NULL COMMENT '显示顺序',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`post_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '岗位信息表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_post
-- ----------------------------
INSERT INTO `sys_post` VALUES (1, 'ceo', '董事长', 1, '0', 'admin', '2024-01-12 16:20:19', '', NULL, '');
INSERT INTO `sys_post` VALUES (2, 'se', '项目经理', 2, '0', 'admin', '2024-01-12 16:20:19', '', NULL, '');
INSERT INTO `sys_post` VALUES (3, 'hr', '人力资源', 3, '0', 'admin', '2024-01-12 16:20:19', '', NULL, '');
INSERT INTO `sys_post` VALUES (4, 'userApp', '普通员工', 4, '0', 'admin', '2024-01-12 16:20:19', '', NULL, '');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '角色权限字符串',
  `role_sort` int(4) NOT NULL COMMENT '显示顺序',
  `data_scope` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '角色状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '角色信息表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '超级管理员', 'admin', 1, '1', '0', '0', 'admin', '2024-01-12 16:20:20', '', NULL, '超级管理员');
INSERT INTO `sys_role` VALUES (2, '普通角色', 'common', 2, '2', '0', '0', 'admin', '2024-01-12 16:20:20', 'admin', '2024-01-23 22:43:47', '普通角色');

-- ----------------------------
-- Table structure for sys_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept`  (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `dept_id` bigint(20) NOT NULL COMMENT '部门ID',
  PRIMARY KEY (`role_id`, `dept_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '角色和部门关联表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_role_dept
-- ----------------------------
INSERT INTO `sys_role_dept` VALUES (2, 100);
INSERT INTO `sys_role_dept` VALUES (2, 101);
INSERT INTO `sys_role_dept` VALUES (2, 105);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `menu_id` bigint(20) NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '角色和菜单关联表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (2, 1);
INSERT INTO `sys_role_menu` VALUES (2, 2);
INSERT INTO `sys_role_menu` VALUES (2, 3);
INSERT INTO `sys_role_menu` VALUES (2, 4);
INSERT INTO `sys_role_menu` VALUES (2, 100);
INSERT INTO `sys_role_menu` VALUES (2, 101);
INSERT INTO `sys_role_menu` VALUES (2, 102);
INSERT INTO `sys_role_menu` VALUES (2, 103);
INSERT INTO `sys_role_menu` VALUES (2, 104);
INSERT INTO `sys_role_menu` VALUES (2, 105);
INSERT INTO `sys_role_menu` VALUES (2, 106);
INSERT INTO `sys_role_menu` VALUES (2, 107);
INSERT INTO `sys_role_menu` VALUES (2, 108);
INSERT INTO `sys_role_menu` VALUES (2, 109);
INSERT INTO `sys_role_menu` VALUES (2, 110);
INSERT INTO `sys_role_menu` VALUES (2, 111);
INSERT INTO `sys_role_menu` VALUES (2, 112);
INSERT INTO `sys_role_menu` VALUES (2, 113);
INSERT INTO `sys_role_menu` VALUES (2, 114);
INSERT INTO `sys_role_menu` VALUES (2, 115);
INSERT INTO `sys_role_menu` VALUES (2, 116);
INSERT INTO `sys_role_menu` VALUES (2, 500);
INSERT INTO `sys_role_menu` VALUES (2, 501);
INSERT INTO `sys_role_menu` VALUES (2, 1000);
INSERT INTO `sys_role_menu` VALUES (2, 1001);
INSERT INTO `sys_role_menu` VALUES (2, 1002);
INSERT INTO `sys_role_menu` VALUES (2, 1003);
INSERT INTO `sys_role_menu` VALUES (2, 1004);
INSERT INTO `sys_role_menu` VALUES (2, 1005);
INSERT INTO `sys_role_menu` VALUES (2, 1006);
INSERT INTO `sys_role_menu` VALUES (2, 1007);
INSERT INTO `sys_role_menu` VALUES (2, 1008);
INSERT INTO `sys_role_menu` VALUES (2, 1009);
INSERT INTO `sys_role_menu` VALUES (2, 1010);
INSERT INTO `sys_role_menu` VALUES (2, 1011);
INSERT INTO `sys_role_menu` VALUES (2, 1012);
INSERT INTO `sys_role_menu` VALUES (2, 1013);
INSERT INTO `sys_role_menu` VALUES (2, 1014);
INSERT INTO `sys_role_menu` VALUES (2, 1015);
INSERT INTO `sys_role_menu` VALUES (2, 1016);
INSERT INTO `sys_role_menu` VALUES (2, 1017);
INSERT INTO `sys_role_menu` VALUES (2, 1018);
INSERT INTO `sys_role_menu` VALUES (2, 1019);
INSERT INTO `sys_role_menu` VALUES (2, 1020);
INSERT INTO `sys_role_menu` VALUES (2, 1021);
INSERT INTO `sys_role_menu` VALUES (2, 1022);
INSERT INTO `sys_role_menu` VALUES (2, 1023);
INSERT INTO `sys_role_menu` VALUES (2, 1024);
INSERT INTO `sys_role_menu` VALUES (2, 1025);
INSERT INTO `sys_role_menu` VALUES (2, 1026);
INSERT INTO `sys_role_menu` VALUES (2, 1027);
INSERT INTO `sys_role_menu` VALUES (2, 1028);
INSERT INTO `sys_role_menu` VALUES (2, 1029);
INSERT INTO `sys_role_menu` VALUES (2, 1030);
INSERT INTO `sys_role_menu` VALUES (2, 1031);
INSERT INTO `sys_role_menu` VALUES (2, 1032);
INSERT INTO `sys_role_menu` VALUES (2, 1033);
INSERT INTO `sys_role_menu` VALUES (2, 1034);
INSERT INTO `sys_role_menu` VALUES (2, 1035);
INSERT INTO `sys_role_menu` VALUES (2, 1036);
INSERT INTO `sys_role_menu` VALUES (2, 1037);
INSERT INTO `sys_role_menu` VALUES (2, 1038);
INSERT INTO `sys_role_menu` VALUES (2, 1039);
INSERT INTO `sys_role_menu` VALUES (2, 1040);
INSERT INTO `sys_role_menu` VALUES (2, 1041);
INSERT INTO `sys_role_menu` VALUES (2, 1042);
INSERT INTO `sys_role_menu` VALUES (2, 1043);
INSERT INTO `sys_role_menu` VALUES (2, 1044);
INSERT INTO `sys_role_menu` VALUES (2, 1045);
INSERT INTO `sys_role_menu` VALUES (2, 1046);
INSERT INTO `sys_role_menu` VALUES (2, 1047);
INSERT INTO `sys_role_menu` VALUES (2, 1048);
INSERT INTO `sys_role_menu` VALUES (2, 1049);
INSERT INTO `sys_role_menu` VALUES (2, 1050);
INSERT INTO `sys_role_menu` VALUES (2, 1051);
INSERT INTO `sys_role_menu` VALUES (2, 1052);
INSERT INTO `sys_role_menu` VALUES (2, 1053);
INSERT INTO `sys_role_menu` VALUES (2, 1054);
INSERT INTO `sys_role_menu` VALUES (2, 1055);
INSERT INTO `sys_role_menu` VALUES (2, 1056);
INSERT INTO `sys_role_menu` VALUES (2, 1057);
INSERT INTO `sys_role_menu` VALUES (2, 1058);
INSERT INTO `sys_role_menu` VALUES (2, 1059);
INSERT INTO `sys_role_menu` VALUES (2, 1060);
INSERT INTO `sys_role_menu` VALUES (2, 1061);
INSERT INTO `sys_role_menu` VALUES (2, 1062);
INSERT INTO `sys_role_menu` VALUES (2, 1063);
INSERT INTO `sys_role_menu` VALUES (2, 1064);
INSERT INTO `sys_role_menu` VALUES (2, 1065);
INSERT INTO `sys_role_menu` VALUES (2, 1066);
INSERT INTO `sys_role_menu` VALUES (2, 1067);
INSERT INTO `sys_role_menu` VALUES (2, 1068);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `dept_id` bigint(20) NULL DEFAULT NULL COMMENT '部门ID',
  `login_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '登录账号',
  `user_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '用户昵称',
  `user_type` tinyint(2) NULL DEFAULT 0 COMMENT '用户类型（0系统用户）',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '用户邮箱',
  `phonenumber` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '手机号码',
  `sex` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '1' COMMENT '用户性别（0男 1女 2未知）',
  `idavaterStatus` int(2) NULL DEFAULT 0 COMMENT '头像审核状态0默认1拒绝2同意3审核中',
  `avatarto` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '提交审核的头像',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '头像路径',
  `auth_avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '认证头像',
  `password` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '密码',
  `salt` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '盐加密',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `login_ip` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '最后登陆IP',
  `login_date` datetime(0) NULL DEFAULT NULL COMMENT '最后登陆时间',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  `age` int(3) NULL DEFAULT NULL COMMENT '年龄',
  `pay_pwd` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '支付密码',
  `edu` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '学历',
  `expect_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '期望类型',
  `expect_work_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '期望工种',
  `expect_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '期望工作地点',
  `expect_work_time` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '期望接单时间',
  `gx_sign` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '签名',
  `lat` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '纬度',
  `lng` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '经度',
  `region` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '区域(筛选使用)',
  `stature` int(4) NOT NULL DEFAULT 0 COMMENT '身高(cm)',
  `server_flag` tinyint(2) NOT NULL DEFAULT 0 COMMENT '服务费收取标识 (0 否 1 是)',
  `vip_flag` tinyint(2) NOT NULL DEFAULT 0 COMMENT 'vip用户(0否 1是)',
  `auth_switch` tinyint(2) NOT NULL DEFAULT 0 COMMENT '身份验证开关(0关 1开)',
  `virtual_phone` varchar(13) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '虚拟小号',
  `level` int(2) NOT NULL DEFAULT 1 COMMENT '等级',
  `ad_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '区域编码',
  `user_mark` int(3) NULL DEFAULT 0 COMMENT '用户标记 0 否 1 是 参考字典 user_mark',
  PRIMARY KEY (`user_id`) USING BTREE,
  UNIQUE INDEX `uni_login_name`(`login_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 375599 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '用户信息表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 103, 'admin', '若依', 0, 'ry@163.com', '15888888888', '1', 0, NULL, '', NULL, '29c67a30398638269fe600f73a054934', '111111', '0', '0', '127.0.0.1', '2024-03-04 18:05:53', 'admin', '2024-01-12 16:20:19', '', '2024-03-04 18:05:53', '管理员', NULL, NULL, NULL, '', '', '', '', '', '', '', '', 0, 0, 0, 0, '', 1, '', 0);
INSERT INTO `sys_user` VALUES (2, 105, 'ry', '若依', 0, 'ry@qq.com', '15666666666', '1', 0, NULL, '', NULL, '8e6d98b90472783cc73c17047ddccf36', '222222', '0', '0', '127.0.0.1', NULL, 'admin', '2024-01-12 16:20:19', 'admin', '2024-03-04 17:29:48', '测试员', NULL, NULL, NULL, '', '', '', '', '', '', '', '', 0, 0, 0, 0, '', 1, '', 0);

-- ----------------------------
-- Table structure for sys_user_online
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_online`;
CREATE TABLE `sys_user_online`  (
  `sessionId` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT '用户会话id',
  `login_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '登录账号',
  `dept_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '部门名称',
  `ipaddr` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '操作系统',
  `status` varchar(10) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '在线状态on_line在线off_line离线',
  `start_timestamp` datetime(0) NULL DEFAULT NULL COMMENT 'session创建时间',
  `last_access_time` datetime(0) NULL DEFAULT NULL COMMENT 'session最后访问时间',
  `expire_time` int(5) NULL DEFAULT 0 COMMENT '超时时间，单位为分钟',
  PRIMARY KEY (`sessionId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '在线用户记录' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_user_online
-- ----------------------------
INSERT INTO `sys_user_online` VALUES ('fd3235bd-e77e-4360-9a3d-16b9492eb54a', 'admin', NULL, '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', 'on_line', '2024-03-04 18:05:28', '2024-03-04 21:52:54', 1800000);

-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post`  (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `post_id` bigint(20) NOT NULL COMMENT '岗位ID',
  PRIMARY KEY (`user_id`, `post_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '用户与岗位关联表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------
INSERT INTO `sys_user_post` VALUES (1, 1);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '用户和角色关联表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 1);

-- ----------------------------
-- Table structure for t_banner
-- ----------------------------
DROP TABLE IF EXISTS `t_banner`;
CREATE TABLE `t_banner`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `img` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '图片地址',
  `act` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '跳转地址',
  `sort` int(5) NULL DEFAULT NULL COMMENT '顺序',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `status` int(3) NULL DEFAULT NULL COMMENT '状态 0 待发布 1 正常  2 删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'banner配置' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of t_banner
-- ----------------------------

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `user_name` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名称',
  `password` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  `mobile` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号',
  `photo` varchar(127) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `last_login_ip` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最后登录IP',
  `last_login_time` datetime(0) NULL DEFAULT NULL COMMENT '最后登录时间',
  `channel` bigint(20) NULL DEFAULT NULL COMMENT '注册渠道',
  `user_status` int(3) NULL DEFAULT 0 COMMENT '用户状态 0 正常  1 删除  2 锁定',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '会员用户表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES (1, '00000000', NULL, '13267180272', 'http://www.baidu.com', '2024-03-04 14:39:56', '127.0.0.1', '2024-03-04 14:39:56', NULL, 0);

SET FOREIGN_KEY_CHECKS = 1;
