CREATE TABLE `t_app_version` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `create_time` datetime NOT NULL COMMENT '创建日期',
  `cid` bigint(20) DEFAULT NULL COMMENT '创建人id',
  `update_time` datetime DEFAULT NULL COMMENT '更新日期',
  `uid` bigint(20) DEFAULT NULL COMMENT '更新人id',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '当前数据状态（1正常，0已删除）',
  `app_version` varchar(50) DEFAULT NULL COMMENT '版本号',
  `app_type` tinyint(2) DEFAULT NULL COMMENT 'app类型(1-android,2-ios,0-其他)',
  `remark` varchar(255) DEFAULT NULL COMMENT '版本更新内容',
  `file_path` varchar(255) DEFAULT NULL COMMENT '下载文件路径',
  `ios_url` varchar(255) NOT NULL DEFAULT '' COMMENT '苹果下载路径',
  `big_upd_flag` tinyint(2) NOT NULL DEFAULT '0' COMMENT '是否是大版本更新 (0 否 1 是)',
  `compel_flag` tinyint(2) NOT NULL DEFAULT '0' COMMENT '是否强制更新标识(0 否 1 是)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=346 DEFAULT CHARSET=utf8mb4 COMMENT='app版本号管理';

ALTER TABLE `t_user_draw`
ADD COLUMN `draw_user_type` int(3) NULL COMMENT '用户身份 0:用户  1 商户' AFTER `check_date`;

CREATE TABLE `t_system_rise_fail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `rise_fall_date` date DEFAULT NULL COMMENT '日期',
  `rise_fall_price` decimal(10,2) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='涨幅信息表';